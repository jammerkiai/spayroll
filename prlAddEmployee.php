<?php
$PageSecurity = 5;
include('includes/session.inc');
$title = _('Employee Maintenance');
include('includes/header.inc');
include('includes/footer.inc');
include('includes/SQL_CommonFunctions.inc');
include('includes/prlFunctions.php');

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/tabs.min.js"></script>
<script type="text/javascript" src="jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	
	$("ul.JTabs").tabs("div.JContent > div");
	$( ".jdate" ).datepicker();
		$( ".jtime" ).timepicker({
			timeFormat: "hh:mm tt"
		});
	$("#userfile").on("change", function() {
        $("#empForm").submit();
    });
});

function myFunction()
{
alert("employee successfully added!");
}

function updateTextBox() {
    var myTextBox = document.getElementById("periodrate");
    var myTextBox1 = document.getElementById("paypermonth");
	var myTextBox2 = document.getElementById("paypersemi");
	var myTextBox3 = document.getElementById("hourlyrate");
	
	var payperSelect = document.getElementById("payperiodid");
	var status = payperSelect.options[payperSelect.selectedIndex].text;
  
       
		
		 if(status == "Monthly") {
			myTextBox.value = myTextBox2.value * 26;
			myTextBox.readOnly = "true";
			myTextBox3.value = myTextBox2.value / 8;
		 	myTextBox1.style.display = "none";
			myTextBox2.style.display = "block";
			document.getElementById("lblpaypermonth").style.display = "none";
			document.getElementById("lblpaypersemi").style.display = "block";
			
		}else if(status == "Semi-Monthly") {
			
			myTextBox1.value = myTextBox2.value * 26;
			myTextBox.value = myTextBox1.value / 2;
			myTextBox1.style.display = "block";
			myTextBox2.style.display = "block";
			myTextBox.readOnly = "true";
			document.getElementById("lblpaypermonth").style.display = "block";
			document.getElementById("lblpaypersemi").style.display = "block";
			document.getElementById("lblpaypersemi").innerHTML = "/ Day";
			myTextBox3.value = myTextBox2.value / 8;
		 
		 }else if(status == "Daily") {
			myTextBox1.style.display = "none";
			myTextBox2.style.display = "none";
			myTextBox.readOnly = "";
			document.getElementById("lblpaypermonth").style.display = "none";
			document.getElementById("lblpaypersemi").style.display = "none";
			myTextBox3.value = myTextBox.value / 8;
		 }
		
}

</script>

</head>
<?php if(isset($_POST['submit'])) {

	$allowed_filetypes = array('.jpg','.jpeg','.png','.gif');
	$max_filesize = 10485760;
	$upload_path = 'uploads/';
	$ext = substr($filename, strpos($filename,'.'), strlen($filename)-1);
	$filename = $_FILES['userfile']['name'];
	$empid = $_POST['empid'];
	$fname = $_POST['fname'];
	$mname = $_POST['mname'];
	$lname = $_POST['lname'];
	$address1 = $_POST['address1'];
	$address2 = $_POST['address2'];
	$city = $_POST['city'];
	$zip = $_POST['zip'];
	$state = $_POST['state'];
	$country = $_POST['country'];
	$gender = $_POST['gender'];
	$marital = $_POST['marital'];
	$bdate = $_POST['birthdate'];
	$birthdate = date("Y-m-d",strtotime($bdate));
	$atm = $_POST['atm'];
	$tax = $_POST['tax'];
	$sss = $_POST['sss'];
	$philhealth = $_POST['philhealth'];
	$pagibig = $_POST['pagibig'];
	$position = $_POST['position'];
	$timeIn = $_POST['timeIn'];
	$timeOut = $_POST['timeOut'];
	$status = $_POST['status'];
	$departmentid = $_POST['departmentid'];
	$taxstatusid = $_POST['taxstatusid'];
	$employmentid = $_POST['empstatid'];
	$costcenterid  = $_POST['costcenterid'];
	$payperiodid = $_POST['payperiodid'];
	$CompanyAgencyID = $_POST['CompanyAgencyID'];
	if($payperiodid == '1'){
	 $p = $_POST['periodrate'] / 2;
	 $periodrate = $p;
	}else{
     $periodrate = $_POST['periodrate'];	
	}
	$hourlyrate = $_POST['hourlyrate'];
	$paytype = $_POST['paytype'];

	$queryEmp = "INSERT INTO prlemployeemaster(firstname,middlename,lastname,address1,address2,city,state,zip,country,gender,marital,birthdate,
	atmnumber,taxactnumber,ssnumber,hdmfnumber,phnumber,taxstatusid,employmentid,costcenterid,departmentid,payperiodid,paytype,periodrate,hourlyrate,position,TimeIn,TimeOut,Active,companyagencyid) 
	VALUES('".$fname."','".$mname."','".$lname."','".$address1."','".$address2."','".$city."','".$state."','".$zip."','".$country."','".$gender."',
	'".$marital."','".$birthdate."','".$atm."','".$tax."','".$sss."','".$pagibig."','".$philhealth."','".$taxstatusid."',
	'".$employmentid."','".$costcenterid."','".$departmentid."','".$payperiodid."','".$paytype."',
	'".$periodrate."','".$hourlyrate."','".$position."','".$timeIn."','".$timeOut."','".$status."','".$CompanyAgencyID."')"; 
	mysql_query($queryEmp);
	
		
	/*if(!in_array($ext,$allowed_filetypes))
	  die('The file you attempted to upload is not allowed.');

	if(filesize($_FILES['userfile']['tmp_name']) > $max_filesize)
	  die('The file you attempted to upload is too large.');

	if(!is_writable($upload_path))
	  die('You cannot upload to the specified directory, please CHMOD it to 777.');*/
	
	if(move_uploaded_file($_FILES['userfile']['tmp_name'],$upload_path . $filename)) {

	

		$sqlUp = "SELECT * FROM uploads WHERE empid = '".$empid."'";
		$resultUp = mysql_query($sqlUp);
		$num_rowsUp = mysql_num_rows($resultUp);

		if($num_rowsUp > 0 && $filename != ""){
			$query = "UPDATE uploads SET image = '".$filename."' WHERE empid = '".$empid."'"; 
			mysql_query($query);
	   }else{ 
			$query = "INSERT INTO uploads (empid,image) VALUES ('".$empid."','".$filename."')"; 
			mysql_query($query);
		}
	  

	//echo 'Your file upload was successful!';

	} //else {
		// echo 'There was an error during the update.  Please try again.';
	//}
}
?>
<body>
<div id="content">
<br />
<div align="left" class="subheader"><a href="<?php echo $rootpath;?>/prlEmployee.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp;Add New Employee</div><br/>
<div>
	<FORM METHOD='post' id="empForm" ACTION='' enctype="multipart/form-data">
		<div id="201Main" style="background-color:transparent">
			<div class="headholder" > 
				<div class="hpictab">
					<span class="avatar">
						<input name="empid" type="hidden" maxlength="64" class="intext" value="<?php echo $id;?>">
						<img src="uploads/default.png" name="image_medium" width="93" height="90" border="1" align="middle" />
						<input type="file" id="userfile" name="userfile" accept="image/*"/>
					</span>
				</div>
				<div class="heditsave">&nbsp;
				</div>
				<div class="hnamedetail">
					<table width="100%" border="0">
					<tr>
						<td width="80%" height="51" class='theader' name="empname">
						Name
						</td>
					</tr>
					<tr>
						<td >
							<input type="text" maxlength="32" class="intext" name="fname" placeholder="First Name">
							<input type="text" maxlength="32" class="intext" name="mname" placeholder="Middle Name">
							<input type="text" maxlength="32" class="intext" name="lname" placeholder="Last Name">
						</td>
					<tr>
						<td class='theader'>
						Employee ID:
						</td>
					<tr>
						<td height="30">
					<?php	$queryEMax = "SELECT MAX(employeeid) as id FROM prlemployeemaster"; 
							$resultE = mysql_query($queryEMax);
							$rowMaxE = mysql_fetch_assoc($resultE);
							$max_id = $rowMaxE['id'];?>
							<input type="text" readonly="readonly" maxlength="64" class="intext" name="empid" value="<?php echo $max_id + 1;?>">
						</td>
					</table>
				</div>
			</div>
		</div>
<div class="main_title" style="background-color:transparent"><p>&nbsp;</p></div>
<ul class="JTabs" style="background-color:transparent">
    	<li>
        	<a href="#" role="presentation" tabindex="-1">Personal Information</a>
        </li>
        <li>
        	<a href="#" role="presentation" tabindex="-1">Accounting Settings</a>
        </li>
        <li>
        	<a href="#" role="presentation" tabindex="-1">HR Settings</a>
        </li>
</ul>
    <div class="JContent">
    	<div>
        <table width="100%" border="0" >
              <tr>
                <td height="44" colspan="2" class="theader">Contact Information</td>
                <td colspan="2" class="theader"><p>Status</p></td>
              </tr>
              <tr>
                <td width="20%" class='tdtitle'>Address 1:</td>
                <td width="29%"><input type="text" class="intext" name="address1" size="42"></td>
                <td width="20%" class='tdtitle'>Marital Status:</td>
                <td width="31%"><select name="marital" class="intext" ><option SELECTED value=""> Select One </option>
                <option value="Single"> Single </option>
                <option value="Married"> Married </option>
                <option value="Sep/Div"> Separated/Divorced </option>
                <option value="Widowed"> Widowed </option></select></td>
              </tr>
              <tr>
				<td width="20%" class='tdtitle'>Address 2:</td>
                <td width="29%"><input type="text" class="intext" name="address2" size="42"></td>
				<td class='tdtitle '>Date Of Birth:</td>
                <td>
                 <input type="text" class="intext jdate" name="birthdate" size="32"></select>
                </td>
			<tr>
				<td class='tdtitle'>City:</td>
                <td><input type="text" class="intext" maxlength="32" name="city"></td>
			</tr>
 <!--             <tr>
                <td class='tdtitle'>State:</td>
                <td><input type="text" class="intext" maxlength="32" name="state"></td>
                <td colspan="2" rowspan="4">&nbsp;</td>
              </tr> -->
              <tr>
                <td class='tdtitle'>Zip Code:</td>
                <td><input type="text" class="intext" maxlength="32" name="zip"></td>
              </tr>
              <tr>
                <td height="27" class='tdtitle'><p>Country:</p></td>
                <td><input type="text" class="intext" maxlength="32" name="country"></td>
          	</tr>
              <tr>
                <td height="27" class='tdtitle'>Gender:</td>
                <td><input type="radio" name="gender" value="M" checked="">
                  Male
                    <input type="radio" name="gender" value="F">
                Female </td>
              </tr>
          </table>
        <p>&nbsp;</p>
      </div>
        <div>
	    <table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr>
                <td height="51" colspan="4" class="theader">Account Details</td>
              </tr>
              <tr>
                <td width="20%" class='tdtitle'>Atm number:</td>
                <td width="29%"><input type="text" class="intext" maxlength="32" name="atm"></td>
                <td width="21%" class='tdtitle'>Tax Status:</td>
                <td width="30%" class='tdtitle'><SELECT class='intext' NAME='taxstatusid'>
				<?php DB_data_seek($result, 0);
				$sql = 'SELECT taxstatusid, taxstatusdescription FROM prltaxstatus';
				$result = DB_query($sql, $db);
				while ($myrow = DB_fetch_array($result)) {
					if ($myrow['taxstatusid'] == $_POST['TaxStatusID']) {
						echo '<OPTION SELECTED VALUE=';
					} else {
						echo '<OPTION VALUE=';
					}
					echo $myrow['taxstatusid'] . '>' . $myrow['taxstatusdescription'];
				}?></td>
              </tr>
              <tr>
                <td class='tdtitle'>Tax Account #:</td>
                <td><input type="text" class="intext" maxlength="32" name="tax"></td>
                <td class='tdtitle'>Pay Period:</td>
                <td><SELECT class='intext' NAME='payperiodid' id="payperiodid" onchange="updateTextBox()">
	<?php DB_data_seek($result, 0);
	$sql = 'SELECT payperiodid, payperioddesc FROM prlpayperiod WHERE active = "1" ORDER BY payperioddesc ASC';
	$result = DB_query($sql, $db);
	while ($myrow = DB_fetch_array($result)) {
		if ($myrow['payperiodid'] == $_POST['PayPeriodID']){
			echo '<OPTION SELECTED VALUE=';
		} else {
			echo '<OPTION VALUE=';
		}
		echo $myrow['payperiodid'] . '>' . $myrow['payperioddesc'];
	}?> </td>
              </tr>
              <tr>
                <td class='tdtitle'>SSS #:</td>
                <td><input type="text" class="intext" maxlength="32" name="sss"></td>
                <td class='tdtitle'>Pay Type:</td>
                <td><SELECT class='intext' NAME='paytype'>
	<?php if ($_POST['PayType'] == 0){
		echo '<OPTION SELECTED VALUE=0>' . _('Salary');
		echo '<OPTION VALUE=1>' . _('Hourly');
	} else {
		echo '<OPTION SELECTED VALUE=1>' . _('Hourly');
		echo '<OPTION VALUE=0>' . _('Salary');
	}?></select></td>
              </tr>
              <tr>
                <td class='tdtitle'>Pag-ibig #:</td>
                <td><input type="text" class="intext" maxlength="32" name="pagibig"></td>
               <td class='tdtitle'>Pay Per Hour:</td>
                <td><input type="text" class="intext" maxlength="32" name="hourlyrate" id="hourlyrate" readonly="readonly"></td>
              </tr>
              <tr>
                <td class='tdtitle'>Philhealth #:</td>
                <td><input type="text" class="intext" maxlength="32" name="philhealth"></td>
				 <td class='tdtitle'>Period Rate:</td>
                <td><input type="text" class="intext" maxlength="32" name="periodrate" id="periodrate" onchange="updateTextBox()"> 
				
				</td>
				</tr><tr>
				<td></td><td></td>
				<td>
				<input type="text" class="intext" name="paypermonth" id="paypermonth" style="display:none; float:left; margin-right:3px;" readonly="readonly" >
				<label id="lblpaypermonth" name="lblpaypermonth" style="display:none; line-height:25px;">/ Month</label>
				
				<input type="text" class="intext" name="paypersemi" id="paypersemi"  style="display:none; float:left; margin-right:3px;" onchange="updateTextBox()"> 
				<label id="lblpaypersemi" name="lblpaypersemi" style="display:none; line-height:25px;">/ Day</label>
				</td>
                
              </tr>
          </table>
	    <p>&nbsp;</p>
        </div>
     	 <div>
			<table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr>
                <td height="50" colspan="4" class="theader">Employment Details</td>
              </tr>
			  <tr>
              <td class='tdtitle'>Company/ Agency: </td>
				<td><SELECT class='intext' NAME='CompanyAgencyID'>
				<?php DB_data_seek($result, 0);
				$sql = 'SELECT CompanyAgencyID, companyagency FROM companyagency';
				$result = DB_query($sql, $db);
				while ($myrow = DB_fetch_array($result)) {
					if ($_POST['CompanyAgencyID'] == $myrow['CompanyAgencyID']){
						echo '<OPTION SELECTED VALUE=' . $myrow['CompanyAgencyID'] . '>' . $myrow['companyagency'];
					} else {
						echo '<OPTION VALUE=' . $myrow['CompanyAgencyID'] . '>' . $myrow['companyagency'];
					}
				}?> </td> 
			</tr>
              <tr>
                <td width="20%" height="25" class='tdtitle'><p>Position:</p>
              <td width="29%" scope="col"><input type="text" class="intext" maxlength="32" name="position"></th>      
              <td width="21%" class='tdtitle'>Time In    
              <td width="30%"><input type="text" class="intext jtime"  maxlength="32" name="timeIn"></td>  
			</tr>
              <tr>
                <td class='tdtitle'>Cost Center: </td>
				<td><SELECT class='intext' NAME='costcenterid'>
				<?php DB_data_seek($result, 0);
				$sql = 'SELECT code, description FROM workcentres';
				$result = DB_query($sql, $db);
				while ($myrow = DB_fetch_array($result)) {
					if ($_POST['CostCenterID'] == $myrow['code']){
						echo '<OPTION SELECTED VALUE=' . $myrow['code'] . '>' . $myrow['description'];
					} else {
						echo '<OPTION VALUE=' . $myrow['code'] . '>' . $myrow['description'];
					}
				}?> </td>
                <td class='tdtitle'>Time Out</td>
                <td>
                <input type="text" class="intext jtime"  maxlength="32" name="timeOut">
                </td>
              </tr>
              <tr>
                <td class='tdtitle'>Department:</td>
                <td><SELECT class='intext' NAME='departmentid'>
				<?php DB_data_seek($result, 0);
				$sql = 'SELECT departmentid, departmentName FROM prldepartment ORDER BY departmentName ASC';
				$result = DB_query($sql, $db);
				while ($myrow = DB_fetch_array($result)) {
					if ($_POST['DepartmentID'] == $myrow['code']){
						echo '<OPTION SELECTED VALUE=' . $myrow['departmentid'] . '>' . $myrow['departmentName'];
					} else {
						echo '<OPTION VALUE=' . $myrow['departmentid'] . '>' . $myrow['departmentName'];
					}
				}?> </td>
                <td class='tdtitle'>Employment Status:</td>
                <td><SELECT class='intext' NAME='empstatid'>
				<?php DB_data_seek($result, 0);
				$sql = 'SELECT employmentid, employmentdesc FROM prlemploymentstatus';
				$result = DB_query($sql, $db);
				while ($myrow = DB_fetch_array($result)) {
					if ($_POST['EmpStatID'] == $myrow['employmentid']){
						echo '<OPTION SELECTED VALUE=' . $myrow['employmentid'] . '>' . $myrow['employmentdesc'];
					} else {
						echo '<OPTION VALUE=' . $myrow['employmentid'] . '>' . $myrow['employmentdesc'];
					}
				}?></td>
              </tr>
              <tr>
                <td colspan="2"></td>
				<td  class='tdtitle'>Active</td>
                <td><input type="checkbox" class="intext" name="status" checked="checked" value="1"></input></td>
              </tr>
            </table>
			<p>&nbsp;</p>
         </div></div>
		 <br />
		 <p><CENTER><INPUT class='jinnerbot' TYPE='Submit' NAME='submit' VALUE='Insert New Employee' onclick='myFunction()'>
		 </form>
		 </div>
</div>
</body>
</html>