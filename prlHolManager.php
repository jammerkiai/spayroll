<?php

/* $Revision: 1.0 $ */

include('includes/prlOverTimeClass.php');

$PageSecurity = 10;
include('includes/session.inc');
$title = _('Holiday Entry');
include('includes/header.inc');
include('includes/footer.inc');
include('includes/SQL_CommonFunctions.inc');
?>
<div id="content">
<center>
<br />
<div align="left" class="subheader">
	<a href="<?php echo $rootpath;?>/prlSelectHOL.php?">
		<img src="images/back.png" width="30" height="30" />
	</a>&nbsp;&nbsp;Add Holidays Record
</div><br/>
<table WIDTH=40% BORDER=0 class='jinnertable'>
	<form name="x" action="" method="GET">
	</tr>
		<tr>
		<td width='20%' class='tableheader'>Date:</td>

		<td width='30%'><input type='text' name='holdate' id='datepicker' class='intext' value='<?php echo $_GET['holdate']; ?>'></td>


	</tr>
	
      <tr>

	      <td width='20%' class='tableheader'>Description</td>
	      <td width='30%'><input type='text' name='holdesc' class='intext' value='<?php echo $_GET['holdesc']; ?>'></td>

      </tr>
      <tr>

		<td width='20%' class='tableheader'>Employee:</p></td>
		<td width='30%'>
			<select name="emp_name" class="intext">
					<?php 
									DB_data_seek($res_name_val, 0);
									$sql_name_val = 'SELECT CONCAT(lastname, ", ",firstname) AS name FROM  prlemployeemaster
									WHERE employeeid = "' . $_GET['emp_name'] .'"';
									$res_name_val = DB_query($sql_name_val, $db);
									$row_name_val = DB_fetch_array($res_name_val);
								?>								
								<option selected value="<?php $_GET['emp_name'] ?>"><?php echo $row_name_val['name']; ?></option>
					<?php
						DB_data_seek($res_employee, 0);
						$sql_employee = 'SELECT employeeid,
										CONCAT(lastname, ", ",firstname) AS name
										FROM  prlemployeemaster ORDER BY lastname ASC';
						$res_employee = DB_query($sql_employee, $db);
						
						while ($row_employee = DB_fetch_array($res_employee)) 
						{
							?>  	  
							<option value="<?php echo $row_employee['employeeid']; ?>"><?php echo $row_employee['name']; ?></option>
						<?php  }

					?>
			</select>
		</td>

	</tr>
	
	<tr>

		<td width='20%' class='tableheader'>Hours of Duty:</td>
		<td width='30%'><input type='text' name='hoursofduty' class='intext' value='<?php echo $_GET['hoursofduty']; ?>'></td>

	</tr>
	
	<tr>

		<td width='20%' class='tableheader'>Holiday Type:</td>
		<td width='30%'>
			<select name="holtype" class="intext" onChange="this.form.submit();">
					<?php
									DB_data_seek($res_val, 0);
									$sql_val = 'SELECT holidaydesc FROM  prlholidaytable
									WHERE id = "' . $_GET['holtype'] .'"';
									$res_val = DB_query($sql_val, $db);
									$row_val = DB_fetch_array($res_val); 
								?>
								<option selected value="<?php $_GET['holtype'] ?>"><?php echo $row_val['holidaydesc'];  ?></option>
					<?php
						DB_data_seek($res_hol_id, 0);
						$sql_hol_id = 'SELECT id,holidaydesc FROM  prlholidaytable ORDER BY id ASC';
						$res_hol_id = DB_query($sql_hol_id, $db);
						
						while ($row_hol_id = DB_fetch_array($res_hol_id)) 
						{
							?>  	  
							<option value="<?php echo $row_hol_id['id']; ?>"><?php echo $row_hol_id['holidaydesc']; ?></option>
						<?php  }

					?>
			</select>
		
		</td>

	
	</form>
<form action='' method='post'>
	

	<tr>
		
		<td width='20%' class='tableheader'>Amount:</p></td>
		<?php	//for the hourly rate
				DB_data_seek($res_hourly, 0);
				$sql_hourly = 'SELECT hourlyrate FROM  prlemployeemaster
				WHERE employeeid = "' . $_GET['emp_name'] .'"';
				$res_hourly = DB_query($sql_hourly, $db);
				$row_hourly = DB_fetch_array($res_hourly); 
				
				//for the OT rate
				DB_data_seek($res_hol_rate, 0);
				$sql_hol_rate = 'SELECT holidayrate FROM  prlholidaytable
				WHERE id = "' . $_GET['holtype'] .'"';
				$res_hol_rate = DB_query($sql_hol_rate, $db);
				$row_hol_rate = DB_fetch_array($res_hol_rate); 
				
			if(isset($_GET['hoursofduty'])){
			$hoursofduty = $_GET['hoursofduty'];
			
			$pay_per_hour = $row_hourly['hourlyrate'];
			$hol_rate = $row_hol_rate['holidayrate'];
						
			$hol_amt1 = ($hoursofduty * $pay_per_hour);
			$hol_amt2 = ($hol_rate * $hol_amt1);
			$hol_amt3 = round($hol_amt2,2);
		?>
		<td width='30%' class='tableheader'><input type='text' name='hol_amt' class='intext' readonly value='<?php echo round($hol_amt2, 2); ?>' class='intext'></td>
		<?php }else {
			echo '<td></td>';
		}
				?>

	</tr>
	<tr>

		<td width='50%' colspan='2' align="right"><input type='submit' name='submit_ot' value='Submit' class='jinnerbot' align='right'></td>
	</tr>

	
</form>
</table>
</center>
</div>

<?php
			if(isset($_POST['submit_ot'])){	
				DB_data_seek($result5, 0);
				$sql5 = "SELECT hol_date FROM prlhol_trans WHERE emp_id = '". $_GET['emp_name'] ."' AND hol_date = '". $_GET['holdate'] ."'";
				$result5 = DB_query($sql5, $db);
					
					$val = mysql_num_rows($result5);
					echo "if";
					
					if($val >= 1){
					
					$message_exist = "There is a record holiday on the same date and employee.";
					echo "<script type='text/javascript'>alert('$message_exist');</script>";
					
					}
					else{ echo "elsa";
 							$hol = $_GET['holdate'];
							$hol_date = date("Y-m-d",strtotime($hol));
							$hol_desc = $_GET['holdesc'];
							$hol_id = $_GET['holtype'];
							$hol_emp_id = $_GET['emp_name'];
							$hol_hours = $_GET['hoursofduty'];
							$hol_amount = $hol_amt3;
							
							$sql = "INSERT INTO prlhol_trans (		
											hol_date,
											hol_desc,
											hol_type_no,
											emp_id,
											duty_hours,
											amount)
										VALUES ( '" . $hol_date . "',
												'" . $hol_desc . "',
												'" . $hol_id . "',
												'" . $hol_emp_id . "',
												'" . $hol_hours . "',
												'" . $hol_amount . "'
										)";
				
							$ErrMsg = _('The date') . ' ' . $hol_date . ' ' . _('could not be added because');
							$DbgMsg = _('The SQL that was used to insert the holiday but failed was');
							$result = DB_query($sql, $db, $ErrMsg, $DbgMsg); 
							
							$message_insert = "Insert holidays successful.";
							echo "<script type='text/javascript'>alert('$message_insert');</script>";
						}
			}	
				
?>
