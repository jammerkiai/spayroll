<?php
/* $Revision: 1.0 $ */

$PageSecurity = 10;
include('includes/session.inc');
$title = _('View Other Income Data');
include('includes/footer.inc');
include('includes/header.inc');
echo '<div id="content"><br /><div align="left" class="subheader"><a href="index.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp;View Other Income Record</div>';
echo '<br /><center><a class="jinnerbot2" href="prlOthIncome.php">Add Other Income Record</a><br></center>';
	
if (isset($_GET['Counter'])){
	$Counter = $_GET['Counter'];
} elseif (isset($_POST['Counter'])){
	$Counter = $_POST['Counter'];
} else {
	unset($Counter);
}

	

if (isset($_GET['delete'])) {
//the link to delete a selected record was clicked instead of the submit button

	$CancelDelete = 0;

// PREVENT DELETES IF DEPENDENT RECORDS
	if ($CancelDelete == 0) {
		$sql="DELETE FROM prlothincfile WHERE counterindex='$Counter'";
		$result = DB_query($sql, $db);
		prnMsg(_('Other Income record for') . ' ' . $Counter . ' ' . _('has been deleted'),'success');
		unset($Counter);
		unset($_SESSION['Counter']);
	} //end if Delete paypayperiod
}
	

if (!isset($Counter)) {
	echo "<FORM METHOD='post' ACTION='" . $_SERVER['PHP_SELF'] . "?" . SID . "'>";
	echo "<INPUT TYPE='hidden' NAME='New' VALUE='Yes'>";
	echo '<CENTER><TABLE>';

	$sql = "SELECT  	a.counterindex,
						a.othdate,
						a.othincid,
						a.othincamount,
						a.employeeid,
						b.othincdesc
		FROM prlothincfile a
		LEFT JOIN prlothinctable b
		ON a.othincid = b.othincid
		ORDER BY a.counterindex";
	$ErrMsg = _('The ot could not be retrieved because');
	$result = DB_query($sql,$db,$ErrMsg);

	echo '<CENTER><br /><table border=0 width="90%" class="jinnertable">';
	echo "<tr>
		
		<td class='tableheader'>" . _('Date') . "</td>
		<td class='tableheader'>" . _('Employee Name') . "</td>
		<td class='tableheader'>" . _('Other Income') . "</td>
		<td class='tableheader'>" . _('Amount') . "</td>
		
		<td class='tableheader' colspan='2'>" . _('Action') . "</td>
	</tr>";

	$k=0; //row colour counter

		while ($myrow = DB_fetch_row($result)) {

		if ($k==1){
			echo "<TR>";
			$k=0;
		} else {
			echo "<TR>";
			$k++;
		}

		
		echo '<TD>' . $myrow[1] . '</TD>';
		DB_data_seek($result_emp_name, 0);
				$sql_emp_name = 'SELECT CONCAT(lastname, ", ",firstname) AS name FROM  prlemployeemaster 
				WHERE employeeid = "'. $myrow[4] .'"';
				$result_emp_name = DB_query($sql_emp_name, $db);
				$number_emp_name = DB_fetch_array($result_emp_name);
				$emp_name = $number_emp_name['name'];
				
		echo '<TD>' . $emp_name . '</TD>';
		echo '<TD>' . $myrow[5] . '</TD>';
		echo '<TD>' . $myrow[3] . '</TD>';
		//echo '<TD>' . $myrow[4] . '</TD>';
		//echo '<TD>' . $myrow[5] . '</TD>';
		
		echo '<TD><A HREF="' . $_SERVER['PHP_SELF'] . '?' . SID . '&Counter=' . $myrow[0] . '&delete=1">' . _('Delete') .'</A></TD>';
		echo '</TR>';

	} //END WHILE LIST LOOP

	//END WHILE LIST LOOP
} //END IF SELECTED ACCOUNT


echo '</CENTER></TABLE></div>';
//end of ifs and buts!


?>