

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Time View Sheet</title>
</head>

<body>
<?php
include('includes/session.inc');
$title = _('Time View');

include('includes/header.inc');
include('includes/footer.inc');
include('includes/SQL_CommonFunctions.inc');
include('includes/prlFunctions.php');
?>

<script type="text/javascript">     
 function PrintDiv() {    
           var divToPrint = document.getElementById('divToPrint');
           var popupWin = window.open('', '_blank', 'width=300,height=300');
           popupWin.document.open();
           popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
           popupWin.document.close();
           }
</script>

<div id="content">
<br />
<div align="left" class="subheader">
	<a href="<?php echo $rootpath;?>/prlTime.php?">
		<img src="images/back.png" width="30" height="30" />
	</a>&nbsp;&nbsp;View Time Sheet
</div><br/>
<table>
	<tr>
		<td><b>Select Department: </b></td>
		<td>
			<form name="view_sheet" action="" method="GET">
								<select class="intext" name="payroll">
								<?php
									DB_data_seek($result_payroll, 0);
									$sql_payroll = 'SELECT payrollid,payrolldesc FROM  prlpayrollperiod
									WHERE payrollid = "' . $_GET['payroll'] .'"';
									$result_payroll = DB_query($sql_payroll, $db);
									$myrow_payroll = DB_fetch_array($result_payroll);
								?>								
								<option selected value=""><?php echo $myrow_payroll['payrollid'] . ' ' . $myrow_payroll['payrolldesc']; ?></option>
								<?php
									DB_data_seek($result_payroll, 0);
									$sql_payroll = 'SELECT payrollid,payrolldesc FROM  prlpayrollperiod ORDER BY payrollid ASC';
									$result_payroll = DB_query($sql_payroll, $db);
									
									while ($myrow_payroll = DB_fetch_array($result_payroll)) 
									{
										?>  	  
										<option value="<?php echo $myrow_payroll['payrollid']; ?>"><?php echo $myrow_payroll['payrollid'] . ' - ' . $myrow_payroll['payrolldesc']; ?></option>
									<?php  }

								?>
								</select>
														
								<select name="dept_id" class="intext" onchange="this.form.submit();">
								<?php
									DB_data_seek($result_deptName, 0);
									$sql_deptName = 'SELECT departmentName FROM  prldepartment
									WHERE departmentid = "' . $_GET['dept_id'] .'"';
									$result_deptName = DB_query($sql_deptName, $db);
									$myrow_deptName = DB_fetch_array($result_deptName);
								?>
								<option selected value=""><?php echo $myrow_deptName['departmentName'];  ?></option>
								<?php
									DB_data_seek($result_dept, 0);
									$sql_dept = 'SELECT departmentid,departmentName FROM  prldepartment ORDER BY departmentName ASC';
									$result_dept = DB_query($sql_dept, $db);
									 while ($myrow_dept = DB_fetch_array($result_dept)) 
									{
										?>  	  
										<option value="<?php echo $myrow_dept['departmentid']; ?>"><?php echo $myrow_dept['departmentName']; ?></option>
									<?php  } 
								?>
								</select>
								
			</form>
		</td>
	</tr>
</table>
	<input class="jinnerbot" type="button" value="PRINT PAYSLIP" onclick="PrintDiv();" />

	<?php		
		DB_data_seek($result_date, 0);
		$sql_date = 'SELECT CONCAT(startdate, " to ",enddate) as period,
							startdate,
							enddate
				FROM 
				prlpayrollperiod 
                WHERE payrollid = "' . $_GET['payroll'] .'"';
		$result_date= DB_query($sql_date, $db);
		$myrow_date = DB_fetch_array($result_date);

		$startdate = $myrow_date['startdate'];
		$enddate = $myrow_date['enddate'];
		
		//echo 'startdate'. $startdate .'<br>';
		//echo 'enddate'. $enddate .'<br>';
		
		
		$date1 = new DateTime($startdate);
		$date2 = new DateTime($enddate);
		$interval = $date1->diff($date2);
		// echo $interval->d." days ";
		$day_range = $interval->d;
		
		
		//for displaying date loop
		$start2 = $startdate; //start date
		$end2 = $enddate; //end date

		$dates = array();
		$start2 = $current = strtotime($start2);
		$end2 = strtotime($end2);

		while ($current <= $end2) {
			$dates[] = date('Y-m-d', $current);
			$current = strtotime('+1 days', $current);

		}

$loop = 1;
foreach ($dates as $date) {		
			  $loop++;
			}	
			$date_columns_count = count($dates);
?>
<br />
<div id="divToPrint" >
<table class="jinnertable" width="100%" border="0" cellspacing="1" cellpadding="1">
<tr>
  <td class='tableheader' width="15%" rowspan="3" >NAME</th>
  <td class='tableheader' width="9%" rowspan="3" scope="col">POSITION</th>
  <td class='tableheader' width="8%" rowspan="3" scope="col">TIME</th>
  <td class='tableheader' colspan="<?php echo $date_columns_count; ?>" scope="col"><div align="center">DATE</div></th>
  <td class='tableheader' width="7%" rowspan="3" scope="col">TOTAL DAYS</th>
  <td class='tableheader' width="5%" rowspan="3" scope="col">LATE</th>
  <td class='tableheader' width="5%" rowspan="3" scope="col">REG
    OT</th>
  <td class='tableheader' width="8%" rowspan="3" scope="col">LEGAL
    HOLIDAY</th>
  <td class='tableheader' width="13%" rowspan="3" scope="col">REMARKS</th>
</tr>

<?php
//for the date
	echo "<tr>";
$loop = 1;
			foreach ($dates as $date) {		
			?>
				<td class='tableheader' width="5%" scope="col"><?php echo $date; ?></th>
			<?php
			  $loop++;
			}	
			$resulta = count($dates);
	echo "</tr>";
?>

<?php
//for the day
	echo "<tr>";
$loop = 1;
			foreach ($dates as $date) {		
			?>
				<td class='tableheader' width="5%" scope="col"><?php echo date('D', strtotime($date)) ?></th>
			<?php
			  $loop++;
			}	
			$resulta = count($dates);
	echo "</tr>";
?>

<?php
  DB_data_seek($result_name, 0);
  
/*   		SELECT DISTINCT a.payrollid,b.departmentid,a.employeeid,b.lastname
		FROM prltimeentry a
		LEFT JOIN prlemployeemaster b
		ON a.employeeid = b.employeeid
		WHERE b.departmentid = '1' AND a.payrollid = '43523452'
		AND b.active = '1'
		ORDER BY b.lastname DESC */
		
		$sql_name = 'SELECT DISTINCT
				CONCAT(a.lastname, ", ", a.firstname) as NAME,
				a.position,
				a.employeeid,
				a.TimeIn,
				a.TimeOut,
				CONCAT(a.TimeIn, " to ", a.TimeOut) as TIME
				FROM 
				prlemployeemaster a
				INNER JOIN prltimeentry b
				ON a.employeeid = b.employeeid
                WHERE a.departmentid = "' . $_GET['dept_id'] .'" AND b.payrollid = "'. $_GET['payroll'].'" 
				AND a.active = "1" ORDER BY a.lastname ASC';
		$result_name = DB_query($sql_name, $db);
			
		while($myrow_name = DB_fetch_array($result_name))
		{
		  $employeeid = $myrow_name['employeeid'];
		  

?>

<tr>
  <td height="48" scope="col"><div align="left"><?php echo $myrow_name['NAME']; ?></div></td>
  <td scope="col"><div align="left"><?php echo $myrow_name['position']; ?></div></td>
  <td scope="col"> <div align="center"><?php echo $myrow_name['TIME']; ?></div></td>
  
  <?php
		//To loop the without attendance
		DB_data_seek($result_no_sched, 0);
		$sql_no_sched = 'SELECT 
				ID
				FROM 
				prltimeentry
                WHERE employeeid = "' . $employeeid .'" AND payrollid = "'.$_GET['payroll'].'" AND day BETWEEN "' . $startdate . '" AND "' . $enddate . '"';
		$result_no_sched = DB_query($sql_no_sched, $db);
		$if_no_sched = mysql_num_rows($result_no_sched);
		if($if_no_sched > 0)
		{
		// if sched not empty
		DB_data_seek($result_time, 0);
		$sql_time = 'SELECT 
				TIMEIN,
				TIMEOUT,
				TAGS
				FROM 
				prltimeentry
                WHERE employeeid = "' . $employeeid .'" AND payrollid = "'.$_GET['payroll'].'" AND day BETWEEN "' . $startdate . '" AND "' . $enddate . '"';
		$result_time = DB_query($sql_time, $db);
		while($myrow_time = DB_fetch_array($result_time))
		{
		?>
			<td scope="col"><div align="left">
			<?php
			
			// for total worked hours
				$timein = $myrow_time['TIMEIN'];
				$timeout = $myrow_time['TIMEOUT'];
				$timein_sched = $myrow_name['TimeIn'];
				$to_time = strtotime($timeout);
				$from_time = strtotime($timein_sched);
								
				if(($timein && $timeout)== null){
					echo $myrow_time['TAGS'];				
				}
				else {
				
					$total_hours = round(abs($to_time - $from_time) / 60,2);	
					$worked_hours = ($total_hours - 60);
					
					$hours = intval($worked_hours/60);
					$minutes = $worked_hours - ($hours * 60);
					
					if($timein == '0'){
						echo 'NO TIME IN';
					}
					else if($worked_hours >= 60){
						echo $hours . '.' . $minutes . '<br>';
						echo $myrow_time['TAGS'];
					}else {
						echo '0.' . $minutes . '<br>';
						echo $myrow_time['TAGS'];
					}
				
				}
			?>
			</div></td>
	<?php
		}
		}
		else{
		// if without attendance
		$loop = 1;
			foreach ($dates as $date) {		
			?>
				<td scope="col"><div align="left">0</div></td>
			<?php
			  $loop++;
			}	
			$resulta = count($dates);	
		}
  ?>
  
	<?php 
	// for number of days
		DB_data_seek($result_payroll1, 0);
		$sql_payroll1 = 'SELECT COUNT(DAY) AS no_days FROM  prltimeentry WHERE employeeid = "' . $employeeid .'" AND payrollid = "'.$_GET['payroll'].'" AND TIMEIN  != "" AND TIMEOUT  != "" AND day BETWEEN "' . $startdate . '" AND "' . $enddate . '"';
		$result_payroll1 = DB_query($sql_payroll1, $db);
		$number = DB_fetch_array($result_payroll1);
	?>
		<td scope="col"><div align="left"><?php	echo $number['no_days']; ?></div></td>
		
		<td scope="col"><div align="left">
		<?php 
		// for total late
		DB_data_seek($result_total_late, 0);
			$sql_total_late = 'SELECT 
					SUM(LATE + BREAKLATE) as total_late
					FROM 
					prltimeentry
					WHERE employeeid = "' . $employeeid .'" AND payrollid = "'.$_GET['payroll'].'" AND day BETWEEN "' . $startdate . '" AND "' . $enddate . '"';
			$result_total_late = DB_query($sql_total_late, $db);			
			$myrow_total_late = DB_fetch_array($result_total_late);
			
			$total_tardiness = $myrow_total_late['total_late'];		
			
				$hours = intval($total_tardiness/60);
				$minutes = $total_tardiness - ($hours * 60);
			//echo $hours . ' Hour(s)' . ' and ' . $minutes . ' Minute(s)';
					if($total_tardiness >= 60){
					echo $hours . ' Hour(s)' . '<br>' . $minutes . ' Minute(s)';
					}else if($total_tardiness == 0){
						echo 'No late';
					}else {
						echo $minutes . ' Minute(s)';
					}
		?>
		</div></td>
		<td scope="col"><div align="left">
		<?php
		// for OT
				DB_data_seek($result_total_ot, 0);
					$sql_total_ot = 'SELECT 
							SUM(otamount) as total_ot
							FROM 
							prlottrans
							WHERE employeeid = "' . $employeeid . '" AND otdate BETWEEN "' . $startdate . '" AND "' . $enddate . '"';
					$result_total_ot = DB_query($sql_total_ot, $db);			
					$myrow_total_ot = DB_fetch_array($result_total_ot);
				$ot = $myrow_total_ot['total_ot'];
				echo $ot;
		?></div></td>
		
		<?php 
	// for number of LH
 		DB_data_seek($result_lh, 0);
		$sql_lh = 'SELECT SUM(othincamount) AS no_LH FROM prlothincfile WHERE employeeid = "' . $employeeid .'" AND othincid = "3" 
		AND othdate BETWEEN "' . $startdate . '" AND "' . $enddate . '"';
		$result_lh = DB_query($sql_lh, $db);
		$total_lh = DB_fetch_array($result_lh); 
		$myrow_lh = $total_lh['no_LH'];
	?>
		<td scope="col"><div align="left"><?php	echo $myrow_lh; ?></div></td>
		<td scope="col"><div align="left">
		<?php
		//for remarks
		DB_data_seek($result_remarks, 0);
					$sql_remarks = "SELECT 
							remarks
							FROM 
							prlremarks
							WHERE employeeid= '". $employeeid ."' AND payrollid = '". $_GET['payroll'] ."'";
					$result_remarks = DB_query($sql_remarks, $db);			
					$myrow_remarks = DB_fetch_array($result_remarks);
					$remarks = $myrow_remarks['remarks'];
					echo $remarks;
		?>
		</div></td>

<?php } ?>

</table>
</div>
</body>
</div>
</html>
	
