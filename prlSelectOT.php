<?php
/* $Revision: 1.0 $ */

$PageSecurity = 10;
include('includes/session.inc');
$title = _('View Overtime');

include('includes/header.inc');
include('includes/footer.inc');
 echo '<div id="content"><br /><div align="left" class="subheader"><a href="index.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp;View Overtime Record</div>';
 echo '<br><center><a class="jinnerbot2" href="prlOtManager.php">Add Overtime Record</a><br></center>';
	
if (isset($_GET['Counter'])){
	$Counter = $_GET['Counter'];
} elseif (isset($_POST['Counter'])){
	$Counter = $_POST['Counter'];
} else {
	unset($Counter);
}

	

if (isset($_GET['delete'])) {
//the link to delete a selected record was clicked instead of the submit button

	$CancelDelete = 0;

// PREVENT DELETES IF DEPENDENT RECORDS IN 'SuppTrans' , PurchOrders, SupplierContacts
	if ($CancelDelete == 0) {
		$sql="DELETE FROM prlottrans WHERE counterindex='$Counter'";
		$result = DB_query($sql, $db);
		prnMsg(_('OT record for') . ' ' . $Counter . ' ' . _('has been deleted'),'success');
		unset($Counter);
		unset($_SESSION['Counter']);
	} //end if Delete paypayperiod
}
	

if (!isset($Counter)) {
/* It could still be the second time the page has been run and a record has been selected for modification - SelectedAccount will exist because it was sent with the new call. If its the first time the page has been displayed with no parameters
then none of the above are true and the list of ChartMaster will be displayed with
links to delete or edit each. These will call the same page again and allow update/input
or deletion of the records*/
   
	echo "<FORM METHOD='post' ACTION='" . $_SERVER['PHP_SELF'] . "?" . SID . "'>";
	echo "<INPUT TYPE='hidden' NAME='New' VALUE='Yes'>";
	
	echo '<CENTER><TABLE>';

	$sql = "SELECT  	counterindex,
						payrollid,
						otref,
						otdesc,
						otdate,
						overtimeid,
						employeeid,
						othours,
						otamount
		FROM prlottrans
		ORDER BY counterindex";
	$ErrMsg = _('The ot could not be retrieved because');
	$result = DB_query($sql,$db,$ErrMsg);


	echo '<a href="prlImportCsvOt.php" class="jinnerbot2">Import CSV File</a>';
	echo '<CENTER><br/><table border=0 width="90%" class="jinnertable">';
	echo "<tr>
		
		
		<td class='tableheader'>" . _('OTDesc') . "</td>
		<td class='tableheader'>" . _('OTDate ') . "</td>
		<td class='tableheader'>" . _('OT ID') . "</td>
		<td class='tableheader'>" . _('Employee Name ') . "</td>
		<td class='tableheader'>" . _('OT Hours') . "</td>
		<td class='tableheader'>" . _('Amount ') . "</td>
		<td class='tableheader'>" . _('Action ') . "</td>
	</tr>";



		while ($myrow = DB_fetch_row($result)) {


		
		
		echo '<TD>' . $myrow[3] . '</TD>';
		echo '<TD>' . $myrow[4] . '</TD>';
		echo '<TD>' . $myrow[5] . '</TD>'; 
		
				DB_data_seek($result_emp_name, 0);
				$sql_emp_name = 'SELECT CONCAT(lastname, ", ",firstname) AS name FROM  prlemployeemaster 
				WHERE employeeid = "'. $myrow[6] .'"';
				$result_emp_name = DB_query($sql_emp_name, $db);
				$number_emp_name = DB_fetch_array($result_emp_name);
				$emp_name = $number_emp_name['name'];
				
				echo '<TD>' . $emp_name . '</TD>';
				
		//echo '<TD>' . $myrow[6] . '</TD>';
		
		echo '<TD>' . $myrow[7] . '</TD>';
		echo '<TD>' . $myrow[8] . '</TD>';
		echo '<TD><A HREF="' . $_SERVER['PHP_SELF'] . '?' . SID . '&Counter=' . $myrow[0] . '&delete=1">' . _('Delete') .'</A></TD>';		
		echo '</TR>';

	} //END WHILE LIST LOOP

	//END WHILE LIST LOOP
} //END IF SELECTED ACCOUNT


echo '</CENTER></TABLE><br/></div>';
//end of ifs and buts!


?>