<?php
$PageSecurity = 5;
include('includes/session.inc');
$title = _('View Payroll Data');
include('includes/header.inc');
include('includes/footer.inc');
include('includes/SQL_CommonFunctions.inc');
include('includes/prlFunctions.php');
?>
<script type="text/javascript">     
 function PrintDiv() {    
           var divToPrint = document.getElementById('divToPrint');
           var popupWin = window.open('', '_blank', 'width=300,height=300');
           popupWin.document.open();
           popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
           popupWin.document.close();
           }
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head></head>


<?php
// for employee id
  DB_data_seek($result_name, 0);
		$sql_name = 'SELECT 
				CONCAT(lastname, ", ", firstname) as NAME,
				position,
				employeeid,
				TimeIn,
				TimeOut,
				CONCAT(TimeIn, " to ", TimeOut) as TIME
				FROM 
				prlemployeemaster
                WHERE departmentid = "' . $_GET['dept_id'] .'"';
		$result_name = DB_query($sql_name, $db);
			
		$myrow_name = DB_fetch_array($result_name);
	
		  $employeeid = $myrow_name['employeeid'];
		  

?>

<?php	
// for start to end date	
		DB_data_seek($result_date, 0);
		$sql_date = 'SELECT CONCAT(startdate, " to ",enddate) as period,
							startdate,
							enddate
				FROM 
				prlpayrollperiod 
                WHERE payrollid = "' . $_GET['payroll'] .'"';
		$result_date= DB_query($sql_date, $db);
		$myrow_date = DB_fetch_array($result_date);

		$startdate = $myrow_date['startdate'];
		$enddate = $myrow_date['enddate'];
		
		//echo 'startdate'. $startdate .'<br>';
		//echo 'enddate'. $enddate .'<br>';
		
		
		$date1 = new DateTime($startdate);
		$date2 = new DateTime($enddate);
		$interval = $date1->diff($date2);
		// echo $interval->d." days ";
		$day_range = $interval->d;
		
		
		//for displaying date loop
		$start2 = $startdate; //start date
		$end2 = $enddate; //end date

		$dates = array();
		$start2 = $current = strtotime($start2);
		$end2 = strtotime($end2);

		while ($current <= $end2) {
			$dates[] = date('Y-m-d', $current);
			$current = strtotime('+1 days', $current);

		}

$loop = 1;
foreach ($dates as $date) {		
			  $loop++;
			}	
			$date_columns_count = count($dates);
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head></head>


<body style="margin:0px; padding:0px;">
<div id="content">
<br/><div align="left" class="subheader"><a href="<?php echo $rootpath;?>/prlPaySlip.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp;View Payroll Data</div><br/>
 <div>
       <input class="jinnerbot" type="button" value="PRINT PAYROLL DATA" onclick="PrintDiv();" />
 </div>
 <br />
 <form name="x" action="" method="GET">
					<b>Select Payroll:</b>
					<select class="intext" name="payroll">
					<?php
									DB_data_seek($result_payroll, 0);
									$sql_payroll = 'SELECT payrollid,payrolldesc FROM  prlpayrollperiod
									WHERE payclosed="0" AND payrollid = "' . $_GET['payroll'] .'"';
									$result_payroll = DB_query($sql_payroll, $db);
									$myrow_payroll = DB_fetch_array($result_payroll);
								?>								
								<option selected value=""><?php echo $myrow_payroll['payrollid'] . ' ' . $myrow_payroll['payrolldesc']; ?></option>
					<?php
						DB_data_seek($result6, 0);
						$sql6 = 'SELECT payrollid,payrolldesc FROM  prlpayrollperiod WHERE payclosed="0" ORDER BY payrollid ASC';
						$result6 = DB_query($sql6, $db);
						
						while ($myrow6 = DB_fetch_array($result6)) 
						{
							?>  	  
							<option value="<?php echo $myrow6['payrollid']; ?>"><?php echo $myrow6['payrollid'] . ' - ' . $myrow6['payrolldesc']; ?></option>
						<?php  }

					?>
					</select>
					
					<b>Select Company:</b>
					<select class="intext" name="companyagencyid">
					<?php
									DB_data_seek($result_CAName, 0);
									$sql_CAName = 'SELECT companyagency FROM  companyagency
									WHERE CompanyAgencyID = "' . $_GET['companyagencyid'] .'"';
									$result_CAName = DB_query($sql_CAName, $db);
									$myrow_CAName = DB_fetch_array($result_CAName);
								?>
								<option selected value=""><?php echo $myrow_CAName['companyagency'];  ?></option>
					<?php
						DB_data_seek($result, 0);
						$sqlCA = 'SELECT CompanyAgencyID,companyagency FROM  companyagency ORDER BY companyagency ASC';
						$resultCA = DB_query($sqlCA, $db);
						
						while ($myrowCA = DB_fetch_array($resultCA)) 
						{
							?>  	  
							<option value="<?php echo $myrowCA['CompanyAgencyID']; ?>"><?php echo $myrowCA['companyagency']; ?></option>
						<?php  }

					?>
					</select>
					
					<b>Select Department:</b>
					<select class="intext" name="dept_id" onchange="this.form.submit();">
					<?php
									DB_data_seek($result_deptName, 0);
									$sql_deptName = 'SELECT departmentName FROM  prldepartment
									WHERE departmentid = "' . $_GET['dept_id'] .'"';
									$result_deptName = DB_query($sql_deptName, $db);
									$myrow_deptName = DB_fetch_array($result_deptName);
								?>
								<option selected value=""><?php echo $myrow_deptName['departmentName'];  ?></option>
					<?php
						DB_data_seek($result, 0);
						$sql = 'SELECT departmentID,departmentName FROM  prldepartment ORDER BY departmentName ASC';
						$result = DB_query($sql, $db);
						
						while ($myrow = DB_fetch_array($result)) 
						{
							?>  	  
							<option value="<?php echo $myrow['departmentID']; ?>"><?php echo $myrow['departmentName']; ?></option>
						<?php  }

					?>
					</select>
					
					
	</form>
 <div id="divToPrint" >
  <LINK HREF="/anahawdev/css/professional/default.css" REL="stylesheet" TYPE="text/css">        
  <style type="text/css">
.jinnertable > tbody > tr > td .tableheader {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #EBEBEB;
    color: #949494; 
	font-weight: bold; !important;

}
  </style>   
 
<table class="jinnertable" width="180%" border="1">
    
<?php 
$res = $_GET['payroll'];
$res2 = $_GET['dept_id'];
$res3 = $_GET['companyagencyid'];

	$sql = "SELECT DISTINCT a.employeeid,UPPER(a.lastname)as lastname,UPPER(a.firstname)as firstname,UPPER(a.middlename)as middlename,a.payperiodid,
			a.departmentid,a.periodrate,a.active,b.payrollid,c.startdate,c.enddate,d.departmentName,
			(SELECT COUNT(b.DAY) FROM  prltimeentry b WHERE b.employeeid = a.employeeid AND b.TIMEIN  != '' AND b.TIMEOUT  != ''
			AND b.day BETWEEN '" . $startdate . "' AND '" . $enddate . "') AS no_days,
			(SELECT SUM(b.LATE) FROM prltimeentry b	WHERE b.employeeid = a.employeeid) as total_late,
			(SELECT SUM(oi.othincamount) FROM  prlothincfile oi
						WHERE oi.othincid = '2' AND oi.employeeid = a.employeeid) as SpecialHoliday,
						(SELECT SUM(oi.othincamount)FROM  prlothincfile oi
						WHERE oi.othincid = '3' AND oi.employeeid = a.employeeid) as LegalHoliday,
						(SELECT SUM(ld.amount) FROM  prlloandeduction ld
						WHERE ld.loantableid = '1' AND ld.employeeid = a.employeeid) as Salary_Loan,
						(SELECT SUM(ld.amount) FROM  prlloandeduction ld
						WHERE ld.loantableid = '2' AND ld.employeeid = a.employeeid) as Cal_Loan,
						(SELECT SUM(ld.amount) FROM  prlloandeduction ld
						WHERE ld.loantableid = '3' AND ld.employeeid = a.employeeid) as Pagibig_Loan,
						(SELECT SUM(ld.amount) FROM  prlloandeduction ld
						WHERE ld.loantableid = '4' AND ld.employeeid = a.employeeid) as PagibigC_Loan,
						c.fsmonth,c.fsyear,c.deductsss, c.deducthdmf, c.deductphilhealth, c.deducttax
			FROM prlemployeemaster a
			LEFT JOIN prltimeentry b
			ON a.employeeid = b.employeeid
			LEFT JOIN prlpayrollperiod c
			ON b.payrollid = c.payrollid
			LEFT JOIN prldepartment d
			ON a.departmentid = d.departmentid
			WHERE a.active = '1'
			AND b.payrollid = '" . $res . "' AND d.departmentid = '" . $res2 . "' AND a.companyagencyid = '" . $res3 . "'
			
			ORDER BY b.payrollid DESC, a.lastname ASC, a.firstname ASC";
	$result = mysql_query($sql);
	$rows[] = array();
	
	$num_rows = mysql_num_rows($result);
	$x = 1;


if($num_rows <> 0){

		 echo'<tr>
				<td class="tableheader" width="6%">Payroll ID</th>
				<td class="tableheader" width="7%">Employee ID</th>
				<td class="tableheader" width="7%">Employee Name</th>
				<td class="tableheader" width="5%">No. of Days</th>
				<td class="tableheader" width="6%">Tardiness</th>
				<td class="tableheader" width="6%">Undertime</th>
				<td class="tableheader" width="7%">Absent</th>
				<td class="tableheader" width="5%">Basic Pay</th>
				<td class="tableheader" width="15%">Other Income</th>
				<td class="tableheader" width="6%">Ecola</th>
				<td class="tableheader" width="15%">Holiday Pay</th>
				<td class="tableheader" width="25%">Overtime Pay</th>
				<td class="tableheader" width="5%">Gross Pay</th>
				<td class="tableheader" width="15%">Loan Deduction</th>
				<td class="tableheader" width="3%">SSS</th>
				<td class="tableheader" width="5%">HDMF</th>
				<td class="tableheader" width="6%">Philhealth</th>
				<td class="tableheader" width="3%">Tax</th>
				<td class="tableheader" width="5%">Net Pay</th>
				<td class="tableheader" width="5%">Month</th>
				<td class="tableheader" width="5%">Year</th>
		</tr>';

		while ( $row = mysql_fetch_assoc($result) ) {
				$rows[] = $row;
				$id = $row['employeeid'];
				$payrollid = $row['payrollid'];
				$name = $row['lastname'] . ', ' . $row['firstname'] . ' ' . $row['middlename'];
				$isdaily = $row['payperiodid'];		
				$dSSS = $row['deductsss'];
				$dHDMF = $row['deducthdmf'];
				$dPH = $row['deductphilhealth'];
				$dTax = $row['deducttax'];
				// for number of days
					// DB_data_seek($result_payroll1, 0);
					// $sql_payroll1 = 'SELECT COUNT(DAY) AS no_days FROM  prltimeentry WHERE employeeid = "' . $id .'" AND TIMEIN  != "" AND TIMEOUT  != "" AND day BETWEEN "' . $startdate . '" AND "' . $enddate . '"';
					// $result_payroll1 = DB_query($sql_payroll1, $db);
					// $number = DB_fetch_array($result_payroll1);
			
					// $no_of_days = $number['no_days']; 
						
				$no_of_days = $row['no_days']; 
				$perioddate = $row['startdate'] . ' to ' . $row['enddate'];
				$dept = $row['departmentName'];
				// for number of days end
				
				
				// for total late
				$sql_hol_tags = "SELECT holidayshortdesc from prlholidaytable";
				$result_hol_tags = DB_query($sql_hol_tags, $db);
				$array = array();
				while($reg_hol_tags = DB_fetch_array($result_hol_tags)){
				$hol_type_tags = $reg_hol_tags['holidayshortdesc'];
				
				$sql_hol_late = "SELECT * from prltimeentry WHERE payrollid = '".$_GET['payroll']."' AND employeeid = '".$id."' AND TAGS = '".$hol_type_tags."'";
				$result_hol_late = DB_query($sql_hol_late, $db);
				$reg_hol_late = DB_fetch_array($result_hol_late);
				$actual_late1 += $reg_hol_late['ACTUAL_LATE'];
				$array[] = "'".$reg_hol_tags['holidayshortdesc']."'";
				}		
				
				$actual_late_res = join(",", $array);
				DB_data_seek($result_total_late, 0);
				$sql_total_late = "SELECT 
						SUM(LATE + BREAKLATE) as total_late
						FROM 
						prltimeentry
						WHERE employeeid = '" . $id ."' AND payrollid = '".$_GET['payroll']."' AND TAGS not in ($actual_late_res)";
				
				$result_total_late = DB_query($sql_total_late, $db);			
				$myrow_total_late = DB_fetch_array($result_total_late);
				$total_tardiness = $myrow_total_late['total_late'];
					$total_late2 = $actual_late1 + $total_tardiness;
					$late_in_decimal = ($total_late2 / 60);
					$late_rounded = round($late_in_decimal, 2);
				
				DB_data_seek($result_late_amt, 0);
					$sql_late_amt = 'SELECT 
							hourlyrate
							FROM 
							prlemployeemaster
							WHERE employeeid = "' . $id .'"';
					$result_late_amt = DB_query($sql_late_amt, $db);			
					$myrow_late_amt = DB_fetch_array($result_late_amt);				
					$hourly_rate = $myrow_late_amt['hourlyrate'];
					
					$per_minute = $hourly_rate / 60;
					$late_no_decimal = $total_late2 * $per_minute;
					$late = number_format($late_no_decimal,2);
				// for total late end
				
				// for undertime start
				DB_data_seek($result_total_undertime, 0);
					$sql_total_undertime = 'SELECT 
							SUM(UNDERTIME) as total_undertime
							FROM 
							prltimeentry
							WHERE employeeid = "' . $id .'" AND payrollid = "'.$_GET['payroll'].'" AND day BETWEEN "' . $startdate . '" AND "' . $enddate . '"';
					$result_total_undertime = DB_query($sql_total_undertime, $db);			
					$myrow_total_undertime = DB_fetch_array($result_total_undertime);
					
					$total_undertime = $myrow_total_undertime['total_undertime'];	
						$undertime_in_decimal = ($total_undertime / 60);
						$undertime_rounded = round($undertime_in_decimal, 2);
						
						$undertime_mins = ($total_undertime % 60);
						$ecola_permin = 0.03 * $undertime_mins;
						
						$str = $undertime_rounded;
						$res = explode(".", $str);
						
						$num_of_hour = $res[0];
						$ecola_per_hour = 1.87 * $num_of_hour;
						$ecola_deduct_ut = $ecola_per_hour + $ecola_permin;
						
				DB_data_seek($result_undertime_amt, 0);
					$sql_undertime_amt = 'SELECT 
							hourlyrate
							FROM 
							prlemployeemaster
							WHERE employeeid = "' . $id .'"';
					$result_undertime_amt = DB_query($sql_undertime_amt, $db);			
					$myrow_undertime_amt = DB_fetch_array($result_undertime_amt);				
					$hourly_rate = $myrow_undertime_amt['hourlyrate'];
					
					$undertime_no_decimal = ($undertime_rounded * $hourly_rate);
					
					$undertime = round($undertime_no_decimal, 2);


				// for undertime end
				
				//absent
				// for total hours based on time schedule
				DB_data_seek($result_tot_hrs, 0);
				$sql_tot_hrs = 'SELECT TimeIn,TimeOut FROM  prlemployeemaster WHERE employeeid = "' . $id .'"';
				$result_tot_hrs = DB_query($sql_tot_hrs, $db);
				$number_tot_hrs = DB_fetch_array($result_tot_hrs);
				$timein_sched = $number_tot_hrs['TimeIn'];
				$timeout_sched = $number_tot_hrs['TimeOut'];
				
					$timein = $timein_sched;
					$timeout = $timeout_sched;
					$to_time = strtotime($timeout);
					$from_time = strtotime($timein);
					$total_hours = round(abs($to_time - $from_time) / 60,2). " minute(s)";	
					$total_hours= ($total_hours - 60);
					
					$hours = intval($total_hours/60);
					$minutes = $total_hours - ($hours * 60);
					
					$total_time_sched = $hours . '.' . $minutes;
					//echo $total_time_sched;
				
				// for number of absent
				DB_data_seek($result_absent, 0);
				$sql_absent = 'SELECT COUNT(TAGS) AS no_tags FROM  prltimeentry WHERE employeeid = "' . $id .'" AND payrollid = "'.$_GET['payroll'].'" AND TIMEIN  = "" AND TIMEOUT  = "" AND day BETWEEN "' . $startdate . '" AND "' . $enddate . '" AND TAGS = "absent"';
				$result_absent = DB_query($sql_absent, $db);
				$number_absent = DB_fetch_array($result_absent);
				$absent_total_num = $number_absent['no_tags']; 
				
				$absent_deduct = $total_time_sched * $absent_total_num;
				// for number of absent end
				
				// for getting the hourly rate to compute absent amount
				DB_data_seek($result_hourly_amt, 0);
				$sql_hourly_amt = 'SELECT hourlyrate FROM prlemployeemaster WHERE employeeid = "' . $id .'"';
				$result_hourly_amt = DB_query($sql_hourly_amt, $db);
				$number_hourly_amt = DB_fetch_array($result_hourly_amt);
				$hourly_amt = $number_hourly_amt['hourlyrate']; 
				
				$absent1 = $hourly_amt * $absent_deduct;
				$absent = number_format($absent1,2);
				// for number of absent end
				//absent end	
			
			
				//basic pay
				// DB_data_seek($result_basic_pay, 0);
					// $sql_basic_pay = 'SELECT 
							// periodrate
							// FROM 
							// prlemployeemaster
							// WHERE employeeid = "' . $id .'"';
					// $result_basic_pay = DB_query($sql_basic_pay, $db);			
					// $myrow_basic_pay = DB_fetch_array($result_basic_pay);
					// $bp = $myrow_basic_pay['periodrate'];
					$bp = $row['periodrate'];
				//basic pay end
				
				// for other income start
				DB_data_seek($result_OTHINC, 0);
				$sql_OTHINC = 'SELECT SUM(othincamount) AS no_OTHINC FROM  prlothincfile WHERE employeeid = "'. $id .'" AND othdate BETWEEN "'.$startdate.'" AND "'.$enddate.'"';
				$result_OTHINC = DB_query($sql_OTHINC, $db);
				$number_OTHINC = DB_fetch_array($result_OTHINC);
				$other_income = $number_OTHINC['no_OTHINC'];
				// for other income end
					
				
				// for HOLIDAYS
				
				   $sql_hol = "SELECT a.holidaydesc, a.holidayrate,
										b.TIMEIN, b.TIMEOUT
								FROM prlholidaytable  as a,
								prltimeentry as b
								WHERE a.holidaydate	 = b.DAY
								AND b.employeeid = '".$id."' AND b.payrollid = '".$_GET['payroll']."'";
					$result_hol = mysql_query($sql_hol) or die ("Error in query: $sql_hol " . mysql_error());
					while($row_hol = mysql_fetch_array($result_hol)){
					$hol_desc = $row_hol['holidaydesc'];
					$hol_rate = $row_hol['holidayrate'];
					$hol_timein = $row_hol['TIMEIN'];
					$hol_timeout = $row_hol['TIMEOUT'];
					
					if(($row_hol['TIMEIN'] || $row_hol['TIMEOUT']) == NULL){
						$HOL_amount = 0;
					}
					elseif(($row_hol['TIMEIN'] && $row_hol['TIMEOUT']) !== NULL){
					$to_timeout = strtotime($hol_timeout);
					$from_timeout = strtotime($hol_timein);
					
					$HOL_time = round(abs($from_timeout - $to_timeout) / 60,2);
					$HOL_time_minus_break = $HOL_time - 60;
					$hours = intval($HOL_time_minus_break/60);
					$minutes = $HOL_time_minus_break - ($hours * 60);
						//$hours = $hours * 2;
						$HOL_amount2 = $hours * $hourly_rate;
						$HOL_amount = $HOL_amount2 * $hol_rate;
					}
					$hol += $HOL_amount;
				   } 
				// for holidays end
				
				
				// for OT
				DB_data_seek($result_total_ot, 0);
					$sql_total_ot = 'SELECT 
							SUM(otamount) as total_ot
							FROM 
							prlottrans
							WHERE employeeid = "' . $id .'" AND otdate BETWEEN "' . $startdate . '" AND "' . $enddate . '"';
					$result_total_ot = DB_query($sql_total_ot, $db);			
					$myrow_total_ot = DB_fetch_array($result_total_ot);
				$ot = $myrow_total_ot['total_ot'];
				// OT end
				
				// for the Ecola				
				$sql_ecola = "SELECT * from prltimeentry WHERE payrollid = '".$_GET['payroll']."' AND employeeid = '".$id."' AND TAGS = 'Absent'";
				$result_ecola = mysql_query($sql_ecola, $db);
				$reg_days1 = mysql_num_rows($result_ecola);
				$reg_days2 = 13 - $reg_days1;
				$Ecola1 = 15 * $reg_days2;
				$Ecola = $Ecola1 - $ecola_deduct_ut;
				// for the Ecola end
				
				$SpecialHoliday = $row['SpecialHoliday'];
				$LegalHoliday = $row['LegalHoliday'];
				// $Salary_Loan = $row['Salary_Loan'];
				// $Cal_Loan = $row['Cal_Loan'];
				// $Pagibig_Loan = $row['Pagibig_Loan'];
				// $PagibigC_Loan = $row['PagibigC_Loan'];
				/* 
				 if($isdaily ==4){
					$bp1 = $bp * $no_of_days;
				}else{
					$bp1 = $bp;
				} */
				$bp1 = $bp;
				
				$sql_sss_div = 'SELECT payperiodid FROM prlemployeemaster WHERE employeeid = "' . $id .'"';
				$result_sss_div = DB_query($sql_sss_div, $db);			
				$myrow_sss_div = DB_fetch_array($result_sss_div);
				$sss_div1 = $myrow_sss_div['payperiodid'];
							
							if($sss_div1 == 4){
								$divided = 4;
							}elseif($sss_div1 == 2){
								$divided = 1;
							}elseif($sss_div1 == 1){
								$divided = 2;
							}
				$bp_divided =  $bp1 * $divided;
				
				//sss
				if($dSSS== 0){
				DB_data_seek($result_sss, 0);
					$sql_sss = 'SELECT 
							employeess
							FROM 
							prlsstable
							WHERE "' . $bp_divided . '" BETWEEN rangefrom AND rangeto';
					$result_sss = DB_query($sql_sss, $db);			
					$myrow_sss = DB_fetch_array($result_sss);
					$sss_tot = $myrow_sss['employeess'] / $divided;
					$sss = $sss_tot;
					}else{$sss = "0.00";}
				//sss end
				
				//$hdmf = $row['hdmf'];
				//pagibig
				if($dHDMF == 0){
				DB_data_seek($result_hdmf, 0);
					$sql_hdmf = 'SELECT 
							employeeshare
							FROM 
							prlhdmftable
							WHERE "' . $bp_divided . '" BETWEEN rangefrom AND rangeto';
					$result_hdmf = DB_query($sql_hdmf, $db);			
					$myrow_hdmf = DB_fetch_array($result_hdmf);
					$hdmf_tot = $myrow_hdmf['employeeshare'] / $divided;
					$hdmf = $hdmf_tot;
					}else{ $hdmf = "0.00";}
				//pagibig end
				
				//philhealth
				if($dPH == 0){
				DB_data_seek($result_ph, 0);
					$sql_ph = 'SELECT 
							employeeph
							FROM 
							prlphilhealth
							WHERE "' . $bp_divided . '" BETWEEN rangefrom AND rangeto';
					$result_ph = DB_query($sql_ph, $db);			
					$myrow_ph = DB_fetch_array($result_ph);
					$philhealth_tot = $myrow_ph['employeeph'] / $divided;
					$philhealth = $philhealth_tot;
					}else{$philhealth = "0.00";}
				//philhealth end				
				
				
				//$tax = $row['tax'];
				
				//tax
				if($dTax== 0){
				DB_data_seek($result_tax, 0);
					$sql_tax = 'SELECT 
							total
							FROM 
							prltaxtablerate
							WHERE "' . $bp_divided . '" BETWEEN rangefrom AND rangeto';
					$result_tax = DB_query($sql_tax, $db);			
					$myrow_tax = DB_fetch_array($result_tax);
					$tax_tot = $myrow_tax['total'] / $divided;
					$tax = $tax_tot;
					}else{ $tax = "0.00";}
				//tax-end
				
				//$other_income = $Ecola + $SpecialHoliday + $LegalHoliday;
				/* if($isdaily == 4){
					$basicpay =  $bp * $no_of_days;
				}else{
					$basicpay =  $bp;
				} */
				$gp = $bp1 - $late - $absent1 - $undertime;
				// echo '$gp = '.$gp.'<br>';
				// echo '$bp1 = '.$bp1.'<br>';
				// echo '$late = '.$late.'<br>';
				// echo '$absent1 = '.$absent1.'<br>';
				// echo '$undertime = '.$undertime.'<br>';
				$gp2 = $ot + $Ecola + $hol;
				// echo '$gp2 = '.$gp2.'<br>';
				// echo '$ot = '.$ot.'<br>';
				// echo '$Ecola = '.$Ecola.'<br>';
				// echo '$hol = '.$hol.'<br>';
				
				DB_data_seek($result_tot, 0);
									$sql_othinc_tot = "SELECT SUM(othincamount) as tot_othinc
													FROM prlothincfile
													WHERE othdate BETWEEN '".$startdate."' AND '".$enddate."' AND employeeid = '".$id."'";
									$result_othinc_tot = DB_query($sql_othinc_tot, $db);	
									$myrow_othinc_tot = DB_fetch_array($result_othinc_tot);
									$oth_inc = $myrow_othinc_tot['tot_othinc'];
				$grosspay = $gp + $gp2 + $oth_inc;
				// echo '$grosspay = '.$grosspay.'<br>';
				// echo '$gp = '.$gp.'<br>';
				// echo '$gp2 = '.$gp2.'<br>';
				// echo '$oth_inc = '.$oth_inc.'<br>';
				
				//$loan = $Salary_Loan + $Cal_Loan + $Pagibig_Loan + $PagibigC_Loan;
				$np = $sss + $hdmf + $philhealth + $tax;
				// echo '$np = '.$np.'<br>';
				// echo '$sss = '.$sss.'<br>';
				// echo '$hdmf = '.$hdmf.'<br>';
				// echo '$philhealth = '.$philhealth.'<br>';
				// echo '$tax = '.$tax.'<br>';
				$n1 = $grosspay - $np;
				/* echo '$n1 = '.$n1.'<br>';
				echo '$grosspay = '.$grosspay.'<br>';
				echo '$np = '.$np.'<br>'; */
				$month = $row['fsmonth'];
				$year = $row['fsyear'];
				
		echo '<tr>
			<td>'?><?php echo $payrollid; '</td>'?>
		 <?php echo '<td>'?><?php echo $id; '</td>'?>
		 <?php echo '<td>'?><?php echo $name; '</td>'?>
		 <?php echo '<td>'?><?php echo $no_of_days; '</td>'?>
		 <?php echo '<td>'?><?php echo $late; '</td>'?>
		 <?php echo '<td>'?><?php echo number_format($undertime,2); '</td>'?>
		 <?php echo '<td>'?><?php echo $absent; '</td>'?>
		 <?php echo '<td>'?><?php echo number_format($gp,2); '</td>'?>
		 <?php echo '<td>'?>
			<?php //for other income
				DB_data_seek($result_oth, 0);
				$sql_othrincome = "SELECT ottable.othincdesc, otfile.othincamount
									FROM prlothinctable as ottable, prlothincfile as otfile
									WHERE ottable.othincid = otfile.othincid AND otfile.othdate BETWEEN '".$startdate."' AND '".$enddate."' AND otfile.employeeid = '".$id."'";
					$result_oth = DB_query($sql_othrincome, $db);			
					while($myrow_oth = DB_fetch_array($result_oth)){
					echo $myrow_oth['othincdesc'] . ' : ' . number_format($myrow_oth['othincamount'],2) . '<br>'; } ?>
		 <?php echo '</td>'?>
		 <?php echo '<td>'?><?php echo number_format($Ecola,2); '</td>'?>
		 <?php echo '<td>'?>
		 <?php // for holiday	

				   $sql_hol = "SELECT a.holidaydesc, a.holidayrate,
										b.TIMEIN, b.TIMEOUT
								FROM prlholidaytable  as a,
								prltimeentry as b
								WHERE a.holidaydate	 = b.DAY
								AND b.employeeid = '".$id."' AND b.payrollid = '".$_GET['payroll']."'";
					$result_hol = mysql_query($sql_hol) or die ("Error in query: $sql_hol " . mysql_error());
					while($row_hol = mysql_fetch_array($result_hol)){
					$hol_desc = $row_hol['holidaydesc'];
					$hol_rate = $row_hol['holidayrate'];
					$hol_timein = $row_hol['TIMEIN'];
					$hol_timeout = $row_hol['TIMEOUT'];
					
					if(($row_hol['TIMEIN'] || $row_hol['TIMEOUT']) == NULL){
						$HOL_amount = 0;
					}
					elseif(($row_hol['TIMEIN'] && $row_hol['TIMEOUT']) !== NULL){
					$to_timeout = strtotime($hol_timeout);
					$from_timeout = strtotime($hol_timein);
					
					$HOL_time = round(abs($from_timeout - $to_timeout) / 60,2);
					$HOL_time_minus_break = $HOL_time - 60;
					$hours = intval($HOL_time_minus_break/60);
					$minutes = $HOL_time_minus_break - ($hours * 60);
						//$hours = $hours * 2;
						$HOL_amount2 = $hours * $hourly_rate;
						$HOL_amount = $HOL_amount2 * $hol_rate;
						echo $hol_desc . ' : ' . number_format($HOL_amount,2). '<br>';
					}
					} ?>

		 <?php echo '</td>'?>
		 <?php echo '<td>'?>
			<?php // for overtime
				DB_data_seek($result_ot1, 0);
				$sql_overtime = "SELECT a.overtimedesc, b.otamount
									FROM prlovertimetable as a, prlottrans as b
									WHERE a.overtimeid = b.overtimeid AND b.otdate BETWEEN '".$startdate."' AND '".$enddate."' AND b.employeeid = '".$id."'";
					$result_ot1 = DB_query($sql_overtime, $db);			
					while($myrow_ot1 = DB_fetch_array($result_ot1)){ 
					echo '<label>'.$myrow_ot1['overtimedesc'] . ' : ' . number_format($myrow_ot1['otamount'],2). '</label><br>'; } ?>
		 <?php echo '</td>'?>
		 <?php echo '<td>'?><?php echo number_format($grosspay,2); '</td>'?>
		 <?php echo '<td>'?><?php
				
					/* 	
						$sqlLoan = "SELECT loantableid, loantabledesc
							FROM prlloantable 
							ORDER BY loantabledesc ASC
							";
							$result_Loan = DB_query($sqlLoan,$db);
							$loanAmt = 0;
						while($row_Loan = DB_fetch_array($result_Loan)){
							$loantableid =  $row_Loan['loantableid'];
							
							$sqlLoan2 = "SELECT SUM(a.amortization) as tot_amor,
							b.amortization
							FROM prlloandeduct a,
							prlloanfile b
							WHERE a.date BETWEEN '".$startdate."' AND '".$enddate."' 
							AND a.loan_file_id = b.loanfileid AND b.loantableid = '".$loantableid."' AND a.skip_deduct = '0' AND b.employeeid = '".$id."'
							AND a.balance >= '0'
							";
							$result_Loan2 = DB_query($sqlLoan2,$db);
							$row_Loan2 = DB_fetch_array($result_Loan2);
							//echo '<label>'.$row_Loan['loantabledesc'] . '__' . number_format($row_Loan2['tot_amor'],2). '</label><br>';
							$loanAmt += $row_Loan2['amortization'];
							} */
						
							$sqlLoan2 = "SELECT a.amortization, b.loantabledesc
							FROM prlloanfile a,
							prlloantable b,
							prlloandeduct c
							WHERE a.loantableid = b.loantableid AND a.employeeid = '".$id."'
							AND c.date BETWEEN '".$startdate."' AND '".$enddate."'
							AND c.skip_deduct = '0'
							AND c.balance >= '0'
							AND c.loan_file_id = a.loanfileid
							";
							$result_Loan2 = DB_query($sqlLoan2,$db);
						while($row_Loan2 = DB_fetch_array($result_Loan2)){
							$loanAmt += $row_Loan2['amortization'];
						}
		
			echo number_format($loanAmt,2);
					 
		 ?>
		 <?php echo '</td><td>'?><?php echo number_format($sss,2); ?>
		 <?php echo '</td><td>'?><?php echo number_format($hdmf,2); ?>
		 <?php echo '</td><td>'?><?php echo number_format($philhealth,2); ?>
		 <?php echo '</td><td>'?><?php echo number_format($tax,2); ?>
		 <?php echo '</td><td>'?><?php $netpay = $n1 - $loanAmt; echo number_format($netpay,2); ?>
		 <?php echo '</td><td>'?><?php echo $month; ?>
		 <?php echo '</td><td>'?><?php echo $year; ?>
		 <?php echo '</td></tr>';
		$x++;
		}
}else{
	echo "<font color='red'><b>NO RECORDS FOUND!</b></font>";
}
?>	
  
  </table>

  </div>
</div>       

	  
</body>
</html>