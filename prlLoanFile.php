<?php

/* $Revision: 1.16 $ */

$PageSecurity = 5;

include('includes/session.inc');

$title = _('Employee Loan Maintenance');

include('includes/header.inc');
include('includes/footer.inc');
include('includes/SQL_CommonFunctions.inc');



if (isset($_GET['LoanFileId'])){
	$LoanFileId = strtoupper($_GET['LoanFileId']);
} elseif (isset($_POST['LoanFileId'])){
	$LoanFileId = strtoupper($_POST['LoanFileId']);
} else {
	unset($LoanFileId);
}




if (isset($_POST['submit'])) {

	//initialise no input errors assumed initially before we test
	$InputError = 0;
	$LoanBal=$_GET['LoanAmount'] - $_POST['YTDDeduction'];
	if ($LoanBal<0) 
	{
	  $InputError = 1;
	  prnMsg(_('Can not post. Total Deduction is greater that Loan Amount by') . ' ' . $LoanBal);
	}

	/* actions to take once the user has clicked the submit button
	ie the page has called itself with some user input */

	//first off validate inputs sensible


	if ($InputError != 1){  

		$SQL_LoanDate = FormatDateForSQL($_POST['LoanDate']);
		$SQL_StartDeduction = FormatDateForSQL($_POST['StartDeduction']);
		if (!isset($_GET['New'])) {
			$sql = "UPDATE prlloanfile SET
					loanfiledesc='" . DB_escape_string($_POST['LoanFileDesc']) . "',
					employeeid='" . DB_escape_string($_POST['EmployeeID']) . "',				
					loandate='$SQL_LoanDate',
					loantableid='" . DB_escape_string($_POST['LoanTableID']) . "',
					loanamount='" . DB_escape_string($_POST['LoanAmount']) ."',
					no_of_payments='" . DB_escape_string($_POST['NoOfPayments']) . "',
					amortization='" . DB_escape_string($_POST['Amortization']) . "',
					startdeduction='$SQL_StartDeduction',
					amortization='" . DB_escape_string($_POST['Amortization']) . "',
					loanbalance='$LoanBal',
					accountcode='" . DB_escape_string($_POST['AccountCode']) . "'
                WHERE loanfileid = '$LoanFileId'";
			$ErrMsg = _('The employee loan could not be updated because');
			$DbgMsg = _('The SQL that was used to update the employee loan but failed was');
			$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);
			
			$sql2="DELETE FROM prlloandeduct WHERE loan_file_id='$LoanFileId'";
			$result2 = DB_query($sql2, $db);
			
			$sql = "UPDATE prlloandeduct SET
					amortization='" . DB_escape_string($_POST['Amortization']) . "'
                WHERE loan_file_id = '$LoanFileId'";
			$ErrMsg = _('The employee loan could not be updated because');
			$DbgMsg = _('The SQL that was used to update the employee loan but failed was');
			$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);
			prnMsg(_('The employee loan master record for') . ' ' . $LoanFileId . ' ' . _('has been updated'),'success');
			
		} else { //its a new employee
				$sql = "INSERT INTO prlloanfile (		
					
					loanfiledesc,
					employeeid,
					loandate,
					loantableid,
					loanamount,
					no_of_payments,
					amortization,
					startdeduction,
					loanbalance,
					accountcode)
				VALUES ( 
					'" . DB_escape_string($_POST['LoanFileDesc']) ."',
					'" . DB_escape_string($_POST['EmployeeID']) ."',
					'" . $SQL_LoanDate . "',
					'" . DB_escape_string($_POST['LoanTableID']) . "',
					'" . DB_escape_string($_POST['LoanAmount']) . "',
					'" . DB_escape_string($_POST['NoOfPayments']) . "',
					'" . DB_escape_string($_POST['Amortization']) . "',			
					'" . $SQL_StartDeduction . "',
					'" . DB_escape_string($_POST['LoanAmount']) . "',
					'" . DB_escape_string($_POST['AccountCode']) . "'
					)";

			$ErrMsg = _('The employee loan') . ' ' . $_POST['LoanFileDesc'] . ' ' . _('could not be added because');
			$DbgMsg = _('The SQL that was used to insert the employee loan but failed was');
			$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);

			prnMsg(_('A new employee loan for') . ' ' . $_POST['LoanFileDesc'] . ' ' . _('has been added to the database'),'success');
			
			
			unset ($LoanFileId);
			unset($_POST['LoanFileDesc']);
			unset($_POST['EmployeeID']);
			unset($_POST['LoanDate']);
			unset($_POST['LoanTableID']);
			unset($_POST['LoanAmount']);
			unset($_POST['NoOfPayments']);
			unset($_POST['Amortization']);
			unset($_POST['StartDeduction']);
			unset($_POST['AccountCode']);
			
			//header('Location: prlViewLoan.php?&"'.$LoanFileId.'"&"'.$_POST['EmployeeID'].'"');
		}
		
	} else {

		prnMsg(_('Validation failed') . _('no updates or deletes took place'),'warn');

	}

} elseif (isset($_POST['delete']) AND $_POST['delete'] != '') {

//the link to delete a selected record was clicked instead of the submit button

	$CancelDelete = 0;

// PREVENT DELETES IF DEPENDENT RECORDS IN 'SuppTrans' , PurchOrders, SupplierContacts

	if ($CancelDelete == 0) {
		$sql="DELETE FROM prlloanfile WHERE loanfileid='$LoanFileId'";
		$result = DB_query($sql, $db);
		
		$sql2="DELETE FROM prlloandeduct WHERE loan_file_id='$LoanFileId'";
		$result2 = DB_query($sql2, $db);
		
		prnMsg(_('Employee loan record for') . ' ' . $LoanFileId . ' ' . _('has been deleted'),'success');
		unset($LoanFileId);
		unset($_SESSION['LoanFileId']);
	} //end if Delete employee
} //end of (isset($_POST['submit'])) 



if (!isset($LoanFileId)) {
/*If the page was called without $LoanFileId passed to page then assume a new employee is to be entered show a form 
with a Employee Code field other wise the form showing the fields with the existing entries against the employee will 
show for editing with only a hidden LoanFileId field*/
	echo "<div id='content'><br/><div align='left' class='subheader'><a href='prlSelectLoan.php?'><img src='images/back.png' width='30' height='30' /></a>&nbsp;&nbsp;Add New Employee Loan</div><br/>";
	echo "<FORM METHOD='get' ACTION='" . $_SERVER['PHP_SELF'] . "?" . SID . "'>";
	echo "<INPUT TYPE='hidden' NAME='New' VALUE='Yes'>";
	echo '<CENTER><br/><TABLE class="jinnertable">';
	//echo "me her";
	//echo '<TR><TD class="tableheader">' . _('Loan Ref') . ":</TD> <TD><INPUT TYPE='text' class='intext' NAME='LoanFileId' SIZE=11 MAXLENGTH=10></TD></TR>";
	echo '<TR><TD class="tableheader">' . _('Description') . ":</TD>
		<TD><input type='Text' class='intext' name='LoanFileDesc' value='".$_GET['LoanFileDesc']."' SIZE=42 MAXLENGTH=40></TD></TR>";
	echo '<TR><TD class="tableheader">' . _('Employee Name') . ":</TD><TD><SELECT class='intext' NAME='EmployeeID' value='".$_GET['EmployeeID']."'>";		
	DB_data_seek($result, 0);
	$sql = 'SELECT employeeid, lastname, firstname FROM prlemployeemaster';
	$result = DB_query($sql, $db);
	while ($myrow = DB_fetch_array($result)) {
		if ($_GET['EmployeeID'] == $myrow['employeeid']){
			echo '<OPTION SELECTED VALUE=' . $myrow['employeeid'] . '>' . $myrow['lastname'] . ',' . $myrow['firstname'];
		} else {
			echo '<OPTION VALUE=' . $myrow['employeeid'] . '>' . $myrow['lastname'] . ',' . $myrow['firstname'];
		}
	} //end while loop
	$DateString = Date($_SESSION['DefaultDateFormat']);	
	echo '<TR><TD class="tableheader">' . _('Loan Date') . ' (' . $_SESSION['DefaultDateFormat'] . "):</TD><TD><input type='Text' class='intext datepicker2' name='LoanDate' value='".$_GET['LoanDate']."' SIZE=12 MAXLENGTH=10></TD></TR>";
	echo '<TR><TD class="tableheader">' . _('Loan Type') . ":</TD><TD><SELECT class='intext' NAME='LoanTableID'>";		
	DB_data_seek($result, 0);
	$sql = 'SELECT loantableid, loantabledesc FROM prlloantable';
	$result = DB_query($sql, $db);
	while ($myrow = DB_fetch_array($result)) {
		if ($_GET['LoanTableID'] == $myrow['loantableid']){
			echo '<OPTION SELECTED VALUE=' . $myrow['loantableid'] . '>' . $myrow['loantabledesc'];
		} else {
			echo '<OPTION VALUE=' . $myrow['loantableid'] . '>' . $myrow['loantabledesc'];
		}
	} //end while loop
	
	echo '<TR><TD class="tableheader">' . _('Start of deduction') . ' (' . $_SESSION['DefaultDateFormat'] . "):</TD><TD><input type='Text' class='intext datepicker2' name='StartDeduction' value='".$_GET['StartDeduction']."' SIZE=12 MAXLENGTH=10></TD></TR>";		
	echo '<TR><TD class="tableheader">' . _('Account Code') . ":</TD>
	<TD><SELECT class='intext' NAME='AccountCode'>";		
	DB_data_seek($result, 0);
	$sql = 'SELECT accountcode, accountname FROM chartmaster';
	$result = DB_query($sql, $db);
	while ($myrow = DB_fetch_array($result)) {
		if ($_GET['AccountCode'] == $myrow['accountcode']){
			echo '<OPTION SELECTED VALUE=' . $myrow['accountcode'] . '>' . $myrow['accountname'];
		} else {
			echo '<OPTION VALUE=' . $myrow['accountcode'] . '>' . $myrow['accountname'];
		}
	} //end while loop	
	echo "</SELECT></TD></TR>";
	
	echo '<TR><TD class="tableheader">' . _('Loan Amount') . "</TD>
		<TD><input type='Text' class='intext' name='LoanAmount' SIZE=14 MAXLENGTH=12 value='". $_GET['LoanAmount'] ."'></TD></TR>";
	echo '<TR><TD class="tableheader">' . _('No. of Payments') . "</TD>"; ?>
		<TD>
		<select class="intext" name="NoOfPayments" onChange="this.form.submit();">
			<option selected value="<?php echo $_GET['NoOfPayments']; ?>"><?php echo $_GET['NoOfPayments']; ?></option>
		<?php for ($x=1; $x<=36; $x++) {
			echo "<option value='$x'>$x</option><br>";
		}?>	
		</select>
		</TD></TR>
	<?php	
	//echo "</TABLE><p><CENTER><INPUT class='jinnerbot' TYPE='Submit' NAME='submit' VALUE='" . _('Insert New Employee Loan') . "'>";
	echo '</FORM>'; 
	
	$loanamount = $_GET['LoanAmount'];
	$noofpayments = $_GET['NoOfPayments'];
	$amortization = $loanamount / $noofpayments;
	?>
	
	<tr>
		<td class='tableheader'>Amortization:</td>
		<td><input type='Text' class='intext' name='Amortization' readonly SIZE=14 MAXLENGTH=12 value='<?php echo number_format($amortization,2); ?>'></td>
	</tr>
	<form action='' method='post'>
	<tr>	
			<input type='hidden' name='LoanFileDesc' value='<?php echo $_GET['LoanFileDesc']; ?>'>
			<input type='hidden' name='EmployeeID' value='<?php echo $_GET['EmployeeID']; ?>'>
			<input type='hidden' name='LoanDate' value='<?php echo $_GET['LoanDate']; ?>'>
			<input type='hidden' name='LoanTableID' value='<?php echo $_GET['LoanTableID']; ?>'>
			<input type='hidden' name='StartDeduction' value='<?php echo $_GET['StartDeduction']; ?>'>
			<input type='hidden' name='AccountCode' value='<?php echo $_GET['AccountCode']; ?>'>
			<input type='hidden' name='LoanAmount' value='<?php echo $_GET['LoanAmount']; ?>'>
			<input type='hidden' name='NoOfPayments' value='<?php echo $_GET['NoOfPayments']; ?>'>
			<input type='hidden' name='Amortization' value='<?php echo $amortization; ?>'>
		<td></td><td><INPUT class='jinnerbot' TYPE='Submit' NAME='submit' VALUE='Insert New Employee Loan'></td>
	</tr>
	</form>
<?php
	echo "</TABLE>";
} else {
//SupplierID exists - either passed when calling the form or from the form itself
	echo '<div id="content"><br/><div align="left" class="subheader"><a href="prlSelectLoan.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp;Update Employee Loan</div><br/>';
	echo "<FORM METHOD='post' action='" . $_SERVER['PHP_SELF'] . '?' . SID ."'>";
	echo '<CENTER><TABLE class="jinnertable">';
		if (!isset($_POST['New'])) {
		$sql = "SELECT  loanfileid,
						loanfiledesc,
						employeeid,
						loandate,
						loantableid,
						loanamount,
						no_of_payments,
						amortization,
						startdeduction,
						ytddeduction,
						accountcode
			FROM prlloanfile
			WHERE loanfileid = '$LoanFileId'";
			$result = DB_query($sql, $db);
			$myrow = DB_fetch_array($result);
		$_POST['LoanFileDesc'] = $myrow['loanfiledesc'];
		$_POST['EmployeeID'] = $myrow['employeeid'];	
		$_POST['LoanDate'] = ConvertSQLDate($myrow['loandate']);	
		$_POST['LoanTableID']  = $myrow['loantableid'];
		$_POST['LoanAmount']  = $myrow['loanamount'];
		$_POST['NoOfPayments']  = $myrow['no_of_payments'];
		$_POST['Amortization']  = $myrow['amortization'];
		$_POST['StartDeduction']  = ConvertSQLDate($myrow['startdeduction']);
		$_POST['YTDDeduction']  = $myrow['ytddeduction'];
		$_POST['AccountCode']  = $myrow['accountcode'];
		echo "<INPUT TYPE=HIDDEN NAME='LoanFileId' VALUE='$LoanFileId'>";
	} else {
	// its a new supplier being added
		echo "<INPUT TYPE=HIDDEN NAME='New' VALUE='Yes'>";
		echo '<TR><TD class="tableheader">' . _('Loan Ref') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='LoanFileId' VALUE='$LoanFileId' SIZE=12 MAXLENGTH=10></TD></TR>";
	}
	echo '<TR><br /><TD class="tableheader">' . _('Description') . ":</TD>
		<TD><input type='Text' class='intext' name='LoanFileDesc' value='" . $_POST['LoanFileDesc'] . "' SIZE=42 MAXLENGTH=40></TD></TR>";
	echo '<TR><TD class="tableheader">' . _('Employee Name') . ":</TD><TD><SELECT class='intext' NAME='EmployeeID'>";		
	DB_data_seek($result, 0);
	$sql = 'SELECT employeeid, lastname, firstname FROM prlemployeemaster';
	$result = DB_query($sql, $db);
	while ($myrow = DB_fetch_array($result)) {
		if ($myrow['employeeid'] == $_POST['EmployeeID']) {
			echo '<OPTION SELECTED VALUE=';
		} else {
			echo '<OPTION VALUE=';
		}
		echo $myrow['employeeid'] . '>' . $myrow['lastname'] . ',' . $myrow['firstname'];		
	} //end while loop
echo '</SELECT></TD></TR><TR><TD class="tableheader">' . _('Loan Date:') . ' (' . $_SESSION['DefaultDateFormat'] . "):</TD>	
	<TD><input type='Text' id='datepicker' class='intext' name='LoanDate' SIZE=12 MAXLENGTH=10 value=" . $_POST['LoanDate'] . '></TD></TR>';
	echo '<TR><TD class="tableheader">' . _('Loan Type') . ":</TD><TD><SELECT class='intext' NAME='LoanTableID'>";
	DB_data_seek($result, 0);
	$sql = 'SELECT loantableid, loantabledesc FROM prlloantable';
	$result = DB_query($sql, $db);
	while ($myrow = DB_fetch_array($result)) {
		if ($myrow['loantableid'] == $_POST['LoanTableID']) {
			echo '<OPTION SELECTED VALUE=';
		} else {
			echo '<OPTION VALUE=';
		}
		echo $myrow['loantableid'] . '>' . $myrow['loantabledesc'];
	} //end while loop
	
	echo '</SELECT></TD></TR><TR><TD class="tableheader">' . _('Start Deduction') . ' (' . $_SESSION['DefaultDateFormat'] . "):</TD>	
	<TD><input type='Text' class='intext' name='StartDeduction' SIZE=12 MAXLENGTH=10 value=" . $_POST['StartDeduction'] . '></TD></TR>';
	echo '<TR><TD class="tableheader">' . _('Account Code') . ":</TD><TD><SELECT class='intext' NAME='AccountCode'>";
	DB_data_seek($result, 0);
	$sql = 'SELECT accountcode, accountname FROM chartmaster';
	$result = DB_query($sql, $db);
	while ($myrow = DB_fetch_array($result)) {
		if ($myrow['accountcode'] == $_POST['AccountCode']) {
			echo '<OPTION SELECTED VALUE=';
		} else {
			echo '<OPTION VALUE=';
		}
		echo $myrow['accountcode'] . '>' . $myrow['accountname'];
	} //end while loop
	
	echo '<TR><TD class="tableheader">' . _('Loan Amount') . ":</TD>
		<TD><input type='Text' class='intext' name='LoanAmount' SIZE=14 MAXLENGTH=12 value='" . $_POST['LoanAmount'] . "'></TD></TR>";
	echo '<TR><TD class="tableheader">' . _('No. of Payments') . "</TD>"; ?>
		<TD>
		<select class="intext" name="NoOfPayments">
			<option selected value="<?php echo $_POST['NoOfPayments']; ?>"><?php echo $_POST['NoOfPayments']; ?></option>
		<?php for ($x=1; $x<=36; $x++) {
		  echo "<option value='$x'>$x</option><br>";
		}?>	
		</select>
		</TD></TR>
	<?php
	
	$loanamount = $_POST['LoanAmount'];
	$noofpayments = $_POST['NoOfPayments'];
	$amortization = $loanamount / $noofpayments;
		
	echo '<TR><TD class="tableheader">' . _('Amortization') . ":</TD>
		<TD><input type='Text' class='intext' name='Amortization' readonly SIZE=14 MAXLENGTH=12 value='" . round($amortization,2) . "'>
		<INPUT TYPE='Submit' NAME='submit' VALUE='" . _('Compute Amortization') . "'>
		</TD></TR>";
	

	if (isset($_POST['New'])) {
		echo "</TABLE><P><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Add These New Employee Loan Details') . "'></FORM>";
	} else {
		echo "</TABLE><P><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Update Employee Loan') . "'>";
		echo '<P><FONT COLOR=red><B>' . _('WARNING') . ': ' . _('There is no second warning if you hit the delete button below') . '. ' . _('However checks will be made to ensure there are no outstanding purchase orders or existing accounts payable transactions before the deletion is processed') . '<BR></FONT></B><br />';
		echo "<INPUT TYPE='Submit' class='jinnerbot' NAME='delete' VALUE='" . _('Delete Employee Loan') . "' onclick=\"return confirm('" . _('Are you sure you wish to delete this employee loan?') . "');\"></FORM>";
		//echo "<BR><CENTER><A HREF='$rootpath/SupplierContacts.php?" . SID . "SupplierID=$SupplierID'>" . _('Review Contact Details') . '</A></CENTER></div>';
	}

} // end of main ifs
?>