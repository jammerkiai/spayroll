<?php
/* $Revision: 1.17 $ */
// Display demo user name and password within login form if $allow_demo_mode is true
include ('includes/LanguageSetup.php');
if ($allow_demo_mode == True AND !isset($demo_text)) {
  $demo_text = _('login as user') .': <i>
' . _('demo') . '</i><br>
' ._('with password') . ': <i>' . _('anahaw') . '</i>'; } elseif (!isset($demo_text)) { $demo_text = _('Please login here'); } ?> <html>
<head>
<title><?php echo $_SESSION['CompanyRecord']['coyname'];?>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo _('iso-8859-1'); ?>
" />
<link rel="stylesheet" href="css/<?php echo $theme;?>
/login.css" type="text/css" />
<style type="text/css">
body
{
  background: #303641;
}
.jlogholder
{
  display: block;
  width: 300px;
  margin: 0px auto;
  text-align: center;
}

.jlogholder > h1
{
  margin: 30% 0px 0px 0px;
  padding: 0px;
  color: #FFFFFF;
  font-size: 60px;
}
.jlogholder > h3
{
  margin:0px 0px 20px 0px;
  padding: 0px;
  color: #e9e9e9;
}
.userlog
{
  background: none repeat scroll 0 0 padding-box #373E4A;
  border: 1px solid #495363;
  border-radius: 3px;
  padding:10px 10px;
  width: 299px;
  display: block;
  transition: all 300ms ease-in-out 0s;
  color: #FFFFFF;
  margin:10px 0px;
}

.userlogBot
{
  background: none repeat scroll 0 0 padding-box #373E4A;
  border: 1px solid #495363;
  border-radius: 3px;
  padding:15px 10px;
  width: 299px;
  display: block;
  transition: all 300ms ease-in-out 0s;
  color: #FFFFFF;
  text-transform: uppercase;
  margin:10px 0px;
  cursor: pointer;
}
.userlogBot:hover
{
  background: #444d5d;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div class="jlogholder">
  <h1>PCC</h1>
  <h3>PAYROLL</h3>
</div>
<form action="<?php echo $_server['php_self'];?>" name="loginform" method="post">
  <table border="0" cellpadding="3" cellspacing="0" width="100%">
    <tr>
      <td>
        <div class="jlogholder">
          <div style="display:none;"><span class="loginText"><?php echo _('Company'); ?>:</span><br/>
          <?php
            if ($AllowCompanySelectionBox == true)
            {
                echo '<SELECT name="CompanyNameField">
                  '; $DirHandle = dir('companies/'); while (false != ($CompanyEntry = $DirHandle->read())){ if (is_dir('companies/' . $CompanyEntry) AND $CompanyEntry != '..' AND $CompanyEntry != 'CVS' AND $CompanyEntry!='.'){ echo "
                  <option value='$CompanyEntry'>$CompanyEntry"; } } echo '
                </select>'; 
            } 
            else 
            { echo '<input type="TEXT" name="CompanyNameField" value="' . $DefaultCompany . '">'; } ?> <br/>
          </div>

                <span style="display:none;" class="loginText"><?php echo _('User name'); ?>:</span>
                <input class="userlog" placeholder="Username" type="TEXT" name="UserNameEntryField"/>
                <span style="display:none;" class="loginText"><?php echo _('Password'); ?>:</span>
                <input class="userlog" placeholder="Password" type="PASSWORD" name="Password">
                <input  class="userlogBot" type="submit" value="<?php echo _('Login'); ?>" name="SubmitUser" />

        </div>
      </td>
    </tr>
  </table>      
</form>
<script language="JavaScript" type="text/javascript">
    //<![CDATA[
            <!--
            document.forms[0].CompanyField.select();
            document.forms[0].CompanyField.focus();
            //-->
    //]]>
    </script>
</body>
</html>