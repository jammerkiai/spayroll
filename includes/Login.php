<?php
/* $Revision: 1.17 $ */
// Display demo user name and password within login form if $allow_demo_mode is true
include ('includes/LanguageSetup.php');
include('includes/footer.inc');
if ($allow_demo_mode == True AND !isset($demo_text)) {
  $demo_text = _('login as user') .': <i>
' . _('demo') . '</i><br>
' ._('with password') . ': <i>' . _('anahaw') . '</i>'; } elseif (!isset($demo_text)) { $demo_text = _('Please login here'); } ?> <html>
<head>
<title><?php echo $_SESSION['CompanyRecord']['coyname'];?>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo _('iso-8859-1'); ?>
" />
<link rel="stylesheet" href="css/<?php echo $theme;?>
/login.css" type="text/css" />
<style type="text/css">
body {
	background-color: #FBFBFB;
}
.header {
	background-color: #2E5DAB;
}
.footer {
	background-color: #2E5DAB;
	color: #FFFFFF;
	text-align: center;
}
.content {
	background-color: #F7F7F7;
}
#login
{
	background-color: #F7F7F7;
	font: Helvetica;
	width:  573;
	height: 294;
	alignment-adjust:central;
}
.jlogholder
{
  display: block;
  width: 300px;
  margin: 0px auto;
  text-align: center;
}

.jlogholder > h1
{
  margin: 30% 0px 0px 0px;
  padding: 0px;
  color: #FFFFFF;
  font-size: 60px;
}
.jlogholder > h3
{
  margin:0px 0px 20px 0px;
  padding: 0px;
  color: #FFFFFF;
}
.userlog
{
  background: none repeat scroll 0 0 padding-box #F7F7F7;
  border: 1px solid #e7e7e7;
  border-radius: 3px;
  padding:10px 10px;
  width: 299px;
  display: block;
  transition: all 300ms ease-in-out 0s;
  color: #2E5DAB;
  margin:10px 0px;
}

.userlogBot
{
  background: none repeat scroll 0 0 padding-box #2e5dab;
  border: 1px solid #e7e7e7;
  border-radius: 3px;
  padding:15px 10px;
  width: 299px;
  display: block;
  transition: all 300ms ease-in-out 0s;
  color: #F7F7F7;
  text-transform: uppercase;
  margin:10px 0px;
  cursor: pointer;
}
.userlogBot:hover
{
  background: #2e5dab;
}

.module-box {
    border-radius: 4px;
    box-shadow: 0 1px 5px rgba(50, 50, 50, 0.2);
    margin-top: 100px;
    min-height: 240px;
    overflow: hidden;
	width: 325px;
}

#content 
{
	background-color: #F7F7F7;
	font: Helvetica;
	width:  100%;
	padding:10px;
	bottom: 20px;
	top: 100px;
    left: 0;
    overflow-y: auto;
    position: fixed;
    right: 0;
    z-index: 1;
}
#footer {
	position:fixed;
	left:0px;
	height:20px;
	width:100%;
	background:#2e5dab;
	bottom: 0;
    right: 0;
	overflow: auto;
    width: 100%;
    z-index: 1;
}
.footer {
	position:fixed;
	font-family: Helvetica;
	color: #FFFFFF;
	font-weight: normal;
	font-size: 12px;
	background:#2e5dab;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
 <div id="header" class="header">
	<img src="images/index.gif" width="180" height="100" alt=""></a>
</div>
<div id="content">
<form action="<?php echo $_server['php_self'];?>" name="loginform" method="post">
		<center>
        
        
            
			<div  class="module-box">
            <div class="jlogholder">
			<br/>
          <div style="display:none;"><span class="loginText"><?php echo _('Company'); ?>:</span><br/>
          <?php
            if ($AllowCompanySelectionBox == true)
            {
                echo '<SELECT name="CompanyNameField">
                  '; $DirHandle = dir('companies/'); while (false != ($CompanyEntry = $DirHandle->read())){ if (is_dir('companies/' . $CompanyEntry) AND $CompanyEntry != '..' AND $CompanyEntry != 'CVS' AND $CompanyEntry!='.'){ echo "
                  <option value='$CompanyEntry'>$CompanyEntry"; } } echo '
                </select>'; 
            } 
            else 
            { echo '<input type="TEXT" name="CompanyNameField" value="' . $DefaultCompany . '">'; } ?> <br/>
          </div>
            	<img src="images/login.jpg" width="300" height="50">
            	<span style="display:none;" class="loginText"><?php echo _('User name'); ?>:</span>
             	<input class="userlog" placeholder="Username" type="TEXT" name="UserNameEntryField"/>
                <span style="display:none;" class="loginText"><?php echo _('Password'); ?>:</span>
                <input class="userlog" placeholder="Password" type="PASSWORD" name="Password">
                <input  class="userlogBot" type="submit" value="<?php echo _('Login'); ?>" name="SubmitUser" />
           
            </div>
			</div>
		</center>
		
    
      
</form>
</div>
<script language="JavaScript" type="text/javascript">
    //<![CDATA[
            <!--
            document.forms[0].CompanyField.select();
            document.forms[0].CompanyField.focus();
            //-->
    //]]>
    </script>
</body>
</html>