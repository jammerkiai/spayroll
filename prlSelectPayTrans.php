<?php
/* $Revision: 1.0 $ */

$PageSecurity = 10;
include('includes/session.inc');
$title = _('View Payroll Data');

include('includes/header.inc');


	
if (isset($_GET['Counter'])){
	$Counter = $_GET['Counter'];
} elseif (isset($_POST['Counter'])){
	$Counter = $_POST['Counter'];
} else {
	unset($Counter);
}

	
/*
if (isset($_GET['delete'])) {
//the link to delete a selected record was clicked instead of the submit button

	$CancelDelete = 0;
	
	$sql = "SELECT payrollid
				FROM prlpayrollperiod
				WHERE prlpayrollperiod.payrollid='" . $PayrollID . "'
				AND prlpayrollperiod.payclosed='1'";
		$PayDetails = DB_query($sql,$db);
		if(DB_num_rows($PayDetails)>0)
		{
		  $CancelDelete = 1;
		  prnMsg('Payroll is closed. Can not delete this record...','success');
		}


// PREVENT DELETES IF DEPENDENT RECORDSs
	if ($CancelDelete == 0) {
		$sql="DELETE FROM prlpayrolltrans WHERE counterindex='$Counter'";
		$result = DB_query($sql, $db);
		prnMsg(_('Payroll record ') . ' ' . $Counter . ' ' . _('has been deleted'),'success');
		unset($Counter);
		unset($_SESSION['Counter']);
	} //end of Delete 
}
*/
	

if (!isset($Counter)) {
	echo "<FORM METHOD='post' ACTION='" . $_SERVER['PHP_SELF'] . "?" . SID . "'>";
	echo "<INPUT TYPE='hidden' NAME='New' VALUE='Yes'>";
	echo '<CENTER><TABLE>';

	$sql = "SELECT  	pt.payrollid,
						pt.employeeid,
						CONCAT(em.lastname,', ',em.firstname)as emp_name,
						
						pt.basicpay,
						pt.othincome,
						pt.absent,
						pt.late,
						pt.otpay,
						pt.grosspay,
						pt.loandeduction,
						pt.sss,
						pt.hdmf,
						pt.philhealth,
						pt.tax,
						pt.netpay,
						pt.fsmonth,
						pt.fsyear
		FROM prlpayrolltrans pt
		INNER JOIN prlemployeemaster em
		ON pt.employeeid = em.employeeid
		ORDER BY emp_name ASC";
	$ErrMsg = _('Payroll record could not be retrieved because');
	$result = DB_query($sql,$db,$ErrMsg);

	echo '<CENTER><br /><table border=0 width="100%" class="jinnertable">';
	echo "<tr><td><b>MONTHLY</b></td></tr>
	<tr>
		<td class='tableheader'>" . _('Payroll ID ') . "</td>
		<td class='tableheader'>" . _('Employee ID') . "</td>
		<td class='tableheader'>" . _('Employee Name') . "</td>
	
		<td class='tableheader'>" . _('Basic Pay') . "</td>
		<td class='tableheader'>" . _('Other Income') . "</td>
		<td class='tableheader'>" . _('Absent') . "</td>
		<td class='tableheader'>" . _('Late') . "</td>
		<td class='tableheader'>" . _('Overtime Pay') . "</td>
		<td class='tableheader'>" . _('Gross Pay') . "</td>
		<td class='tableheader'>" . _('Loan Deduction') . "</td>
		<td class='tableheader'>" . _('SSS') . "</td>
		<td class='tableheader'>" . _('HDMF') . "</td>
		<td class='tableheader'>" . _('PhilHealth') . "</td>
		<td class='tableheader'>" . _('Tax') . "</td>
		<td class='tableheader'>" . _('Net Pay') . "</td>
		<td class='tableheader'>" . _('Month') . "</td>
		<td class='tableheader'>" . _('Year') . "</td>		
	</tr>";

	$k=0; //row colour counter

		while ($myrow = DB_fetch_row($result)) {

		if ($k==1){
			echo "<TR>";
			$k=0;
		} else {
			echo "<TR>";
			$k++;
		}

		echo '<TD>' . $myrow[0] . '</TD>';
		echo '<TD>' . $myrow[1] . '</TD>';
		echo '<TD>' . $myrow[2] . '</TD>';
		echo '<TD>' . $myrow[3] . '</TD>';
		echo '<TD>' . $myrow[4] . '</TD>';
		echo '<TD>' . $myrow[5] . '</TD>';
		echo '<TD>' . $myrow[6] . '</TD>';
		echo '<TD>' . $myrow[7] . '</TD>';
		echo '<TD>' . $myrow[8] . '</TD>';
		echo '<TD>' . $myrow[9] . '</TD>';
		echo '<TD>' . $myrow[10] . '</TD>';
		echo '<TD>' . $myrow[11] . '</TD>';
		echo '<TD>' . $myrow[12] . '</TD>';
		echo '<TD>' . $myrow[13] . '</TD>';
		echo '<TD>' . $myrow[14] . '</TD>';
		echo '<TD>' . $myrow[15] . '</TD>';
		echo '<TD>' . $myrow[16] . '</TD>';
		
		//echo '<TD><A HREF="' . $_SERVER['PHP_SELF'] . '?' . SID . '&Counter=' . $myrow[0] . '&delete=1">' . _('Delete') .'</A></TD>';
		
		
		
		echo '</TR>';

	} //END WHILE LIST LOOP

	//END WHILE LIST LOOP
	
	$sql2 = "SELECT DISTINCT pt.payrollid,a.employeeid,CONCAT(e.lastname,', ',e.firstname) as emp_name,
			(SELECT ROUND(SUM((b.reghrs)/8),0)  
			FROM `prldailytrans` b 
			WHERE a.employeeid = b.employeeid)as no_of_days, 
			a.latehrs, a.absenthrs,
			ROUND((pt.periodrate * (SELECT ROUND(SUM((b.reghrs)/8),0)  
			FROM `prldailytrans` b 
			WHERE a.employeeid = b.employeeid) - (e.hourlyrate * a.latehrs) - (e.hourlyrate * a.absenthrs)),2) as basic_pay,
			pt.othincome, pt.otpay, 
			ROUND(((pt.periodrate * (SELECT ROUND(SUM((b.reghrs)/8),0)  
			FROM `prldailytrans` b 
			WHERE a.employeeid = b.employeeid) - (e.hourlyrate * a.latehrs) - (e.hourlyrate * a.absenthrs)) + (pt.othincome + pt.otpay)),2)as grosspay,
			pt.loandeduction,pt.sss,pt.hdmf,pt.philhealth,pt.tax,
			ROUND(((pt.periodrate * (SELECT ROUND(SUM((b.reghrs)/8),0)  
			FROM `prldailytrans` b 
			WHERE a.employeeid = b.employeeid) - (e.hourlyrate * a.latehrs) - (e.hourlyrate * a.absenthrs)) + (pt.othincome + pt.otpay)- 
			(pt.loandeduction + pt.sss + pt.hdmf + pt.philhealth + pt.tax)),2)as netpay
			,pt.fsmonth,pt.fsyear
			FROM `prldailytrans` a
			INNER JOIN prlemployeemaster e
			ON
			a.employeeid = e.employeeid
			INNER JOIN prlpayrolltrans pt
			ON a.employeeid = pt.employeeid
			WHERE e.payperiodid = '50'
			ORDER BY e.lastname ASC, a.employeeid ASC";
	$ErrMsg2 = _('Payroll record could not be retrieved because');
	$result2 = DB_query($sql2,$db,$ErrMsg2);
	
	echo "<tr></tr>
	<tr><td><b>DAILY</b></td></tr>
	<tr>
		<td class='tableheader'>" . _('Payroll ID ') . "</td>
		<td class='tableheader'>" . _('Employee ID') . "</td>
		<td class='tableheader'>" . _('Employee Name') . "</td>
		<td class='tableheader'>" . _('No. Of Days') . "</td>
		<td class='tableheader'>" . _('Tardiness') . "</td>
		<td class='tableheader'>" . _('Undertime') . "</td>
		<td class='tableheader'>" . _('Basic Pay') . "</td>
		<td class='tableheader'>" . _('Other Income') . "</td>
		<td class='tableheader'>" . _('Overtime Pay') . "</td>
		<td class='tableheader'>" . _('Gross Pay') . "</td>
		<td class='tableheader'>" . _('Loan Deduction') . "</td>
		<td class='tableheader'>" . _('SSS') . "</td>
		<td class='tableheader'>" . _('HDMF') . "</td>
		<td class='tableheader'>" . _('PhilHealth') . "</td>
		<td class='tableheader'>" . _('Tax') . "</td>
		<td class='tableheader'>" . _('Net Pay') . "</td>
		<td class='tableheader'>" . _('FS Month') . "</td>
		<td class='tableheader'>" . _('FS Year') . "</td>
	</tr>";
	
	$k2=0; //row colour counter

		while ($myrow2 = DB_fetch_row($result2)) {

		if ($k2==1){
			echo "<TR>";
			$k2=0;
		} else {
			echo "<TR>";
			$k2++;
		}

		echo '<TD>' . $myrow2[0] . '</TD>';
		echo '<TD>' . $myrow2[1] . '</TD>';
		echo '<TD>' . $myrow2[2] . '</TD>';
		echo '<TD>' . $myrow2[3] . '</TD>';
		echo '<TD>' . $myrow2[4] . '</TD>';
		echo '<TD>' . $myrow2[5] . '</TD>';
		echo '<TD>' . $myrow2[6] . '</TD>';
		echo '<TD>' . $myrow2[7] . '</TD>';
		echo '<TD>' . $myrow2[8] . '</TD>';
		echo '<TD>' . $myrow2[9] . '</TD>';
		echo '<TD>' . $myrow2[10] . '</TD>';
		echo '<TD>' . $myrow2[11] . '</TD>';
		echo '<TD>' . $myrow2[12] . '</TD>';
		echo '<TD>' . $myrow2[13] . '</TD>';
		echo '<TD>' . $myrow2[14] . '</TD>';
		echo '<TD>' . $myrow2[15] . '</TD>';
		echo '<TD>' . $myrow2[16] . '</TD>';
		echo '<TD>' . $myrow2[17] . '</TD>';
				
		echo '</TR>';

	} //END WHILE LIST LOOP
} //END IF SELECTED ACCOUNT


echo '</CENTER></TABLE>';
//end of ifs and buts!

include('includes/footer.inc');
?>