<?php
/* $Revision: 1.0 $ */

$PageSecurity = 15;

include('includes/session.inc');

$title = _('Company/ Agency Table');

include('includes/header.inc');
include('includes/footer.inc');

if (isset($_GET['CompanyAgencyID'])){
	$CompanyAgencyID = $_GET['CompanyAgencyID'];
} elseif (isset($_POST['CompanyAgencyID'])){
	
	$CompanyAgencyID = $_POST['CompanyAgencyID'];
} else {
	unset($CompanyAgencyID);
}


if (isset($_POST['submit'])) {

	//initialise no input errors assumed initially before we test

	$InputError = 0;

	/* actions to take once the user has clicked the submit button
	ie the page has called itself with some user input */

	//first off validate inputs sensible

	if (strpos($_POST['companyagency'],'&')>0 OR strpos($_POST['companyagency'],"'")>0) {
		$InputError = 1;
		prnMsg( _('The company/agency description cannot contain the character') . " '&' " . _('or the character') ." '",'error');
	}
	if (trim($_POST['companyagency']) == '') {
		$InputError = 1;
		prnMsg( _('The company/agency description may not be empty'), 'error');
	}
	
	// if (strlen($CompanyAgencyID) == 0) {
		// $InputError = 1;
		// prnMsg(_('The company agency id cannot be empty'),'error');
	// }

	if ($InputError !=1) {
	
			if (!isset($_POST['New'])) {

			$sql = "UPDATE companyagency SET companyagency='" . DB_escape_string($_POST['companyagency']) . "' 
						WHERE CompanyAgencyID = '$CompanyAgencyID'";

			$ErrMsg = _('The company/ agency could not be updated because');
			$DbgMsg = _('The SQL that was used to update the cost center table but failed was');
			$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);
			prnMsg(_('The company/agency table master record for') . ' ' . $CompanyAgencyID . ' ' . _('has been updated'),'success');

		} else { //its a new cost center record

			$sql = "INSERT INTO companyagency (companyagency)
					 VALUES (
					 	'" .DB_escape_string($_POST['companyagency']) . "')"; 

			$ErrMsg = _('The company/agency') . ' ' . $_POST['companyagency'] . ' ' . _('could not be added because');
			$DbgMsg = _('The SQL that was used to insert the company/ agency table but failed was');
			$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);

			prnMsg(_('A new company/gency table for') . ' ' . $_POST['companyagency'] . ' ' . _('has been added to the database'),'success');

			unset ($CompanyAgencyID);
			unset($_POST['companyagency']);
	
		}
		
	} else {

		prnMsg(_('Validation failed') . _('no updates or deletes took place'),'warn');

	}

} elseif (isset($_POST['delete']) AND $_POST['delete'] != '') {

//the link to delete a selected record was clicked instead of the submit button

	$CancelDelete = 0;

// PREVENT DELETES IF DEPENDENT RECORDS FOUND
	if ($CancelDelete == 0) {
		$sql="DELETE FROM workcentres WHERE CompanyAgencyID='$CompanyAgencyID'";
		$result = DB_query($sql, $db);
		prnMsg(_('company/agency table record for') . ' ' . $CompanyAgencyID . ' ' . _('has been deleted'),'success');
		unset($CompanyAgencyID);
		unset($_SESSION['CompanyAgencyID']);
	} 
}


if (!isset($CompanyAgencyID)) {

	echo '<div id="content"><br/><div align="left" class="subheader"><a href="index.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp;Add Company/ Agency</div>';
	echo "<FORM METHOD='post' ACTION='" . $_SERVER['PHP_SELF'] . "?" . SID . "'>";

	echo "<INPUT TYPE='hidden' NAME='New' VALUE='Yes'>";

	echo '<CENTER><br /><TABLE class="jinnertable">';
	//echo '<TR><TD class="tableheader">' . _('Company/ Agency ID') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='CompanyAgencyID' SIZE=5 MAXLENGTH=4></TD></TR>";
	echo '<TR><TD class="tableheader">' . _('Company/ Agency') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='companyagency' SIZE=41 MAXLENGTH=40></TD></TR>";
	echo "</SELECT></TD></TR></TABLE><p><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Insert New Company/Agency') . "'><br />";
	echo '</FORM>';
	
		$sql = "SELECT CompanyAgencyID,
			companyagency
			FROM companyagency
			ORDER BY companyagency";

	$ErrMsg = _('Could not get company/ agency because');
	$result = DB_query($sql,$db,$ErrMsg);
	
	echo '<CENTER><table border=0 width="50%" class="jinnertable">';
	echo "<tr>
		<td class='tableheader'>" . _('Company/ Agency ID') . "</td>
		<td class='tableheader'>" . _('Company/ Agency') . "</td>
		<td class='tableheader' colspan='2'>Action</td>
	</tr>";

		
	$k=0; //row colour counter
	while ($myrow = DB_fetch_row($result)) {

		if ($k==1){
			echo "<TR>";
			$k=0;
		} else {
			echo "<TR>";
			$k++;
		}
		echo '<TD>' . $myrow[0] . '</TD>';
		echo '<TD>' . $myrow[1] . '</TD>';
		echo '<TD><A HREF="' . $_SERVER['PHP_SELF'] . '?' . SID . '&CompanyAgencyID=' . $myrow[0] . '">' . _('Edit') . '</A></TD>';
		echo '<TD><A HREF="' . $_SERVER['PHP_SELF'] . '?' . SID . '&CompanyAgencyID=' . $myrow[0] . '&delete=1">' . _('Delete') .'</A></TD>';
		echo '</TR>';

	} //END WHILE LIST LOOP
	echo '</table></CENTER><p>';


} else {
	echo '<div id="content"><br/><div align="left" class="subheader"><a href="prlSelectCompany.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp;Edit Company/ Agency</div>';
	echo "<FORM METHOD='post' ACTION='" . $_SERVER['PHP_SELF'] . "?" . SID . "'>";
	echo '<CENTER><br/><TABLE class="jinnertable">';

	//if (!isset($_POST['New'])) {
	if (!isset($_POST['New'])) {
		$sql = "SELECT CompanyAgencyID, 
				companyagency
			FROM companyagency 
			WHERE CompanyAgencyID = '$CompanyAgencyID'";
				  
		$result = DB_query($sql, $db);
		$myrow = DB_fetch_array($result);
		
		$_POST['companyagency']  = $myrow['companyagency'];
		echo "<INPUT TYPE=HIDDEN NAME='CompanyAgencyID' VALUE='$CompanyAgencyID'>";

	} else {
	// its a new cost center being added
		echo "<INPUT TYPE=HIDDEN NAME='New' VALUE='Yes'>";
		// echo '<TR><TD class="tableheader">' . _('Cost Center Code') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='CostCenterID' VALUE='$CostCenterID' SIZE=5 MAXLENGTH=4></TD></TR>";
	}
	echo "<TR><TD class='tableheader'>" . _('Company/ Agency') . ':' . "</TD><TD><input type='Text' class='intext' name='companyagency' SIZE=41 MAXLENGTH=40 value='" . $_POST['companyagency'] . "'></TD></TR>";
	echo '</SELECT></TD></TR>';

	if (isset($_POST['New'])) {
		echo "</TABLE><P><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Add These New company/agency Details') . "'></FORM>";
	} else {
		echo "</TABLE><P><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Update company/agency Table') . "'>";
		echo '<P><FONT COLOR=red><B>' . _('WARNING') . ': ' . _('There is no second warning if you hit the delete button below') . '. ' . _('However checks will be made to ensure before the deletion is processed') . '<BR></FONT></B>';
		echo "<br/><INPUT TYPE='Submit' class='jinnerbot' NAME='delete' VALUE='" . _('Delete company/agency Table') . "' onclick=\"return confirm('" . _('Are you sure you wish to delete this cost center?') . "');\"></FORM></div>";
	}

} // end of main ifs


?>