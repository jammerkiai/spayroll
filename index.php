<?php

/* Modified By: ME*/


$PageSecurity = 1;

include('includes/session.inc');
include('includes/SQL_CommonFunctions.inc');
include('includes/prlFunctions.php');
$title=_('Main Menu');

/*The module link codes are hard coded in a switch statement below to determine the options to show for each tab */
$ModuleLink = array('PRL', 'system');
/*The headings showing on the tabs accross the main index used also in WWW_Users for defining what should be visible to the user */
$ModuleList = array(_('Payroll'), _('Setup'));

if (isset($_GET['Application'])){ /*This is sent by this page (to itself) when the user clicks on a tab */
	$_SESSION['Module'] = $_GET['Application'];
}

include('includes/header.inc');

if (count($_SESSION['AllowedPageSecurityTokens'])==1){

/* if there is only one security access and its 1 (it has to be 1 for this page came up at all)- it must be a customer log on need to limit the menu to show only the customer accessible stuff this is what the page looks like for customers logging in */
?>

	
		<tr>
<?php
	include('includes/footer.inc');
	exit;
} else {  /* Security settings DO allow seeing the main menu */

?>
		<table border="0" width="100%" style="display:none">
			<tr>
			<td class="main_menu">
				<table class="main_menu" cellspacing='0'>
					<tr>

	<?php


	$i=0;

	while ($i < count($ModuleLink)){

		// This determines if the user has display access to the module see config.php and header.inc
		// for the authorisation and security code
		if ($_SESSION['ModulesEnabled'][$i]==1)	{

			// If this is the first time the application is loaded then it is possible that
			// SESSION['Module'] is not set if so set it to the first module that is enabled for the user
			if (!isset($_SESSION['Module'])OR $_SESSION['Module']==''){
				$_SESSION['Module']=$ModuleLink[$i];
			}
			if ($ModuleLink[$i] == $_SESSION['Module']){
				//echo "<td class='main_menu_selected'><a href='". $_SERVER['PHP_SELF'] .'?'. SID . '&Application='. $ModuleLink[$i] ."'>". $ModuleList[$i] .'</a></td>';
			} else {
				//echo "<td class='main_menu_unselected'><a href='". $_SERVER['PHP_SELF'] .'?'. SID . '&Application='. $ModuleLink[$i] ."'>". $ModuleList[$i] .'</a></td>';
			}
		}
		$i++;
	}

	?>
					</tr>
				</table>
			</td>
			</tr>
		</table>
		
	<?php


	switch ($_SESSION['Module']) {

	Case 'orders': //Sales Orders
	Case 'AR': //Debtors Module
	Case 'AP': //Creditors Module
	Case 'PRL': //Payroll Module

	unset($ReceiptBatch);
	unset($AllocTrans);

	?>

<div id="content"  style="margin-left:150px; margin-right:150px;">
		

			<?php

								$sqlUserLog = "SELECT a.usergroupid FROM www_users a 
										WHERE a.realname = '" . $_SESSION['UsersRealName'] . "'
										";
				
								$resultUserLog = mysql_query($sqlUserLog);
								$rowsUserLog[] = array();
										
								$rowsUserLog = mysql_fetch_assoc($resultUserLog) ;
								$rowsUserLog[] = $rowUserLog;
								$UserLog = $rowsUserLog['usergroupid'];
											
				//echo  "MINA ".$UserLog.$_SESSION['UsersRealName'];
				
			?>
			
			<?php if($UserLog =="9" || $UserLog =="8" ){?>
			<div style="display: block; width:1170px;  height: 100px;">
                     <div class="span12"><h3 style="color: #8F9999;">HUMAN RESOURCES</h3></div>
				<div id="hr">
			<?php }else {?>	
			<div style="display: block; width:1170px;  height: 100px;">
                    <div class="span12"><h3 style="color: #8F9999;">HUMAN RESOURCES</h3></div>
				<div id="hr">    
			<?php }?>
				
						 
					
							<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlEmployee.php?">Employees list</a></div>
							<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlTime.php?">Time Sheet</a></div>
							<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlSelectOT.php?">Overtime Sheet</a></div>
					<!--	<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlSelectHOL.php?">Holidays Sheet</a></div>	-->
							
				</div>
			</div>	 
				
			<?php if($UserLog =="10" || $UserLog =="8" ){?>
			<div style="display: block; width:1170px;  height: 100px;">
                    <div class="span12"><h3 style="color: #8F9999;">PAYROLL</h3></div>
				<div id="payroll" class="span13">
			<?php }else {?>	
			<div style="display: block; width:1170px;  height: 100px;">
                    <div class="span12"><h3 style="color: #8F9999;">PAYROLL</h3></div>
				<div id="payroll" class="span13">
			<?php }?>	
					
						
							<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlSelectOthIncome.php?">Other Income</a></p></div>
							<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlSelectLoan.php?">Loan Deduction</a></p></div>
							<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlSelectPayroll.php?">Payroll Period</a></p></div>
							<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlPaySlip.php?">Pay Slip</a></p></div>
						
				</div>
			</div>
			<?php if($UserLog =="8"){?>
			<div style="display: block; width:1170px;  height: 280px;">
			<div class="span12"><h3 style="color: #8F9999;">MAINTENANCE</h3></div>
				<div id="maintenance" class="span13">
			<?php }else {?>	
			<div style="display: block; width:1170px;  height: 280px;">
			<div class="span12"><h3  style="color: #8F9999;">MAINTENANCE</h3></div>
				<div id="maintenance" class="span13">
				
			<?php }?>		
				  
							<div class="span13">
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlTax.php?">Add/Update Tax Table</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlSelectTaxStatus.php?">Add/Update Tax Status</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlSSS.php?">Add/Update SSS Table</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlPH.php?">Add/Update PhilHealth Table</a></p></div>
							</div>
							<div class="span13">
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlHDMF.php?">Add/Update HDMF Table</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlEmploymentStatus.php?">Add/Update Employment Status</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlPayPeriod.php?">Add/Update Pay Period Table</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlOvertime.php?">Add/Update Overtime Table</a></p></div>
							</div>
							<div class="span13">
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlLoanTable.php?">Add/Update Loan Table</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlOthIncTable.php?">Add/Update Other Income Table</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlSelectCompany.php?">Add/Update Company or Agency</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlCostCenter.php?">Add/Update Cost Center</a></p></div>
							</div>
							<div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlSelectDepartment.php?">Add/Update Department</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/CompanyPreferences.php?">Company Preferences</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/WWW_Users.php?">User Accounts</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlHoliday_table.php?">Add/Update Holidays Table</a></p></div>
							</div>
				</div>
			</div>
			<div style="display: none; width:1170px;  height: 225px;">
				<div class="span12"><h3 style="color: #8F9999;">REPORTS</h3>
					<div id="maintenance"  class="span12">
						 
							<div class="span12">
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlRepPayrollRegister.php?">Payroll Register</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlRepBankTrans.php?">Bank Transmittal</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlRepCashTrans.php?">Over the Counter Listing</a></p></div>
							</div>
							<div class="span12">
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlRepSSSPremium.php?">SSS Monthly Remittance</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlRepHDMFPremium.php?">HDMF Monthly Remittance</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlRepTax.php?">Tax Monthly Return</a></p></div>
							</div>
							<div class="span12">
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlRepPHPremium.php?">Philhealth Monthly Remittance</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlRepPayrollRegYTD.php?">YTD Payroll Register</a></p></div>
									<div class="span3"><p class="cmenu"><a href="<?php echo $rootpath;?>/prlRepTaxYTD.php?">AlphaList</a></p></div>
							</div>
					</div>
				</div>
			</div>
		</div>
</div>


	<?php
		break;

	/* ********************* 	END OF Payroll OPTIONS **************************** */
	/* ********************* 	END OF Payroll OPTIONS **************************** */
	/* ********************* 	END OF Payroll OPTIONS **************************** */
	/* ********************* 	END OF Payroll OPTIONS **************************** */


	Case 'PO': /* Purchase Ordering */
	Case 'stock': //Inventory Module
	Case 'manuf': //Manufacturing Module
	Case 'system': //System setup
	Case 'GL': //General Ledger
	} //end of module switch
} /* end of if security allows to see the full menu */

// all tables started are ended within this index script which means 2 outstanding from footer.

include('includes/footer.inc');

function OptionHeadings() {

global $rootpath, $theme;



}

?>
