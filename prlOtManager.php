<?php

/* $Revision: 1.0 $ */

include('includes/prlOverTimeClass.php');

$PageSecurity = 10;
include('includes/session.inc');
$title = _('Overtime Entry');
include('includes/header.inc');
include('includes/footer.inc');
include('includes/SQL_CommonFunctions.inc');
?>
<div id="content">
<center>
<br />
<div align="left" class="subheader">
	<a href="<?php echo $rootpath;?>/prlSelectOT.php?">
		<img src="images/back.png" width="30" height="30" />
	</a>&nbsp;&nbsp;Add Overtime Record
</div><br/>
<table WIDTH=40% BORDER=0 class='jinnertable'>
	<form name="x" action="" method="GET">
	</tr>
		<tr>
		<td width='20%' class='tableheader'>Date:</td>

		<td width='30%'><input type='text' name='otdate' class='intext' id='datepicker' value='<?php echo $_GET['otdate']; ?>'></td>


	</tr>
	
      <tr>

	      <td width='20%' class='tableheader'>Description</td>
	      <td width='30%'><input type='text' name='otdesc' class='intext' value='<?php echo $_GET['otdesc']; ?>'></td>

      </tr>
      <tr>

		<td width='20%' class='tableheader'>Employee:</p></td>
		<td width='30%'>
			<select name="emp_name" class="intext">
					<?php 
									DB_data_seek($res_name_val, 0);
									$sql_name_val = 'SELECT CONCAT(lastname, ", ",firstname) AS name FROM  prlemployeemaster
									WHERE employeeid = "' . $_GET['emp_name'] .'"';
									$res_name_val = DB_query($sql_name_val, $db);
									$row_name_val = DB_fetch_array($res_name_val);
								?>								
								<option selected value="<?php $_GET['emp_name'] ?>"><?php echo $row_name_val['name']; ?></option>
					<?php
						DB_data_seek($res_employee, 0);
						$sql_employee = 'SELECT employeeid,
										CONCAT(lastname, ", ",firstname) AS name
										FROM  prlemployeemaster ORDER BY lastname ASC';
						$res_employee = DB_query($sql_employee, $db);
						
						while ($row_employee = DB_fetch_array($res_employee)) 
						{
							?>  	  
							<option value="<?php echo $row_employee['employeeid']; ?>"><?php echo $row_employee['name']; ?></option>
						<?php  }

					?>
			</select>
		</td>

	</tr>
	
	<tr>

		<td width='20%' class='tableheader'>OT Hours:</td>
		<td width='30%'><input type='text' name='othours' class='intext' value='<?php echo $_GET['othours']; ?>'></td>

	</tr>
	
	<tr>

		<td width='20%' class='tableheader'>Overtime Type:</td>
		<td width='30%'>
			<select name="ot_type" class="intext" onChange="this.form.submit();">
					<?php
									DB_data_seek($res_val, 0);
									$sql_val = 'SELECT overtimedesc FROM  prlovertimetable
									WHERE overtimeid = "' . $_GET['ot_type'] .'"';
									$res_val = DB_query($sql_val, $db);
									$row_val = DB_fetch_array($res_val); 
								?>
								<option selected value="<?php $_GET['ot_type'] ?>"><?php echo $row_val['overtimedesc'];  ?></option>
					<?php
						DB_data_seek($res_ot_id, 0);
						$sql_ot_id = 'SELECT overtimeid,overtimedesc FROM  prlovertimetable ORDER BY overtimeid ASC';
						$res_ot_id = DB_query($sql_ot_id, $db);
						
						while ($row_ot_id = DB_fetch_array($res_ot_id)) 
						{
							?>  	  
							<option value="<?php echo $row_ot_id['overtimeid']; ?>"><?php echo $row_ot_id['overtimedesc']; ?></option>
						<?php  }

					?>
			</select>
		
		</td>

	
	</form>
<form action='' method='post'>
	

	<tr>
		
		<td width='20%' class='tableheader'>Overtime Amount:</p></td>
		<?php	//for the hourly rate
				DB_data_seek($res_hourly, 0);
				$sql_hourly = 'SELECT hourlyrate FROM  prlemployeemaster
				WHERE employeeid = "' . $_GET['emp_name'] .'"';
				$res_hourly = DB_query($sql_hourly, $db);
				$row_hourly = DB_fetch_array($res_hourly); 
				
				//for the OT rate
				DB_data_seek($res_ot_rate, 0);
				$sql_ot_rate = 'SELECT overtimerate FROM  prlovertimetable
				WHERE overtimeid = "' . $_GET['ot_type'] .'"';
				$res_ot_rate = DB_query($sql_ot_rate, $db);
				$row_ot_rate = DB_fetch_array($res_ot_rate); 
				
			if(isset($_GET['othours'])){
			$othours = $_GET['othours'];
			
			$pay_per_hour = $row_hourly['hourlyrate'];
			$ot_rate = $row_ot_rate['overtimerate'];
			
			$ot_amt1 = ($othours * $pay_per_hour);
			$ot_amt2 = ($ot_rate * $ot_amt1);
		?>
		<td width='30%' class='tableheader'><input type='text' name='otamt' class='intext' value='<?php echo $ot_amt2; ?>' class='intext'></td>
		<?php }else {
			echo '<td></td>';
		}
				?>

	</tr>
	<tr>

		<td width='50%' colspan='2' align="right"><input type='submit' name='submit_ot' value='Submit' class='jinnerbot' align='right'></td>
	</tr>

	
</form>
</table>
</center>
</div>

<?php
			
			if(isset($_POST['submit_ot'])){	
				DB_data_seek($result5, 0);
				$sql5 = "SELECT otdate FROM prlottrans WHERE employeeid = '". $_GET['emp_name'] ."' AND otdate = '". $_GET['otdate'] ."'";
				$result5 = DB_query($sql5, $db);
					
					$val = mysql_num_rows($result5);
					
					if($val >= 1){
					
					$message_exist = "There is a record overtime on the same date and employee.";
					echo "<script type='text/javascript'>alert('$message_exist');</script>";
					
					}
					else{
 							$ot = $_GET['otdate'];
							$ot_date = date("Y-m-d",strtotime($ot));
							$ot_desc = $_GET['otdesc'];
							$ot_id = $_GET['ot_type'];
							$ot_emp_id = $_GET['emp_name'];
							$ot_hours = $_GET['othours'];
							$ot_amount = $ot_amt2;
							
							$sql = "INSERT INTO prlottrans (		
											otdate,
											otdesc,
											overtimeid,
											employeeid,
											othours,
											otamount)
										VALUES ( '" . $ot_date . "',
												'" . $ot_desc . "',
												'" . $ot_id . "',
												'" . $ot_emp_id . "',
												'" . $ot_hours . "',
												'" . $ot_amount . "'
										)";
				
							$ErrMsg = _('The date') . ' ' . $ot_date . ' ' . _('could not be added because');
							$DbgMsg = _('The SQL that was used to insert the department but failed was');
							$result = DB_query($sql, $db, $ErrMsg, $DbgMsg); 
							
							$message_insert = "Insert overtime successful.";
							echo "<script type='text/javascript'>alert('$message_insert');</script>";
						}
			}	
				
?>


<?php


?>