<?php
/* $Revision: 1.13 $ */
$PageSecurity =1;

include('includes/session.inc');
include('includes/footer.inc');


?>
<html>
<head>
    <title><?php echo $_SESSION['CompanyRecord']['coyname'];?> - <?php echo _('Log Off'); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
    <link rel="stylesheet" href="css/<?php echo $theme;?>/login.css" type="text/css" />
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <div id="header" class="header">
	<img src="images/index.gif" width="180" height="100" alt=""></a>
        <div id="content">
        
		<center>
            <form action="<?php echo $_SERVER['PHP_SELF'];?>" name="loginform" method="post">
            <div class="module-box">
         		<table border="0" cellpadding="3" cellspacing="0" width="100%">
					<tr>
						<td align="center" class="loginText">
						  <br /><br /><?php echo _('Thank you for using PCC Payroll'); ?><br /><br />
				<?php echo $_SESSION['CompanyRecord']['coyname'];?>
							<br />
							<a href=" <?php echo $rootpath;?>/index.php" style="color:#000000"><b><?php echo _('Click here to Login Again'); ?></b></a>
						</td>
					</tr>
                 </table>
            	
            
            </div>
            </form>
		</center>
</body>
</html>

<?php
	// Cleanup
	session_start();
	session_unset();
	session_destroy();
?>
</body>
</html>


