<?php
/* $Revision: 1.0 $ */

$PageSecurity = 10;
include('includes/session.inc');
$title = _('Employee Loan Maintenance');

include('includes/header.inc');
include('includes/footer.inc');
?>

<div id='content'><br/><div align='left' class='subheader'>
	<a href='prlSelectLoan.php?'>
		<img src='images/back.png' width='30' height='30'/>
	</a>&nbsp;&nbsp;View Loan
</div>

<?php
	DB_data_seek($result_emp_name, 0);
				//for employee name
				$sql_emp_name = 'SELECT CONCAT(lastname, ", ",firstname) AS name,
								payperiodid FROM  prlemployeemaster 
				WHERE employeeid = "'. $_GET['empid'] .'"';
				$result_emp_name = DB_query($sql_emp_name, $db);
				$number_emp_name = DB_fetch_array($result_emp_name);
				$emp_name = $number_emp_name['name'];
				$payperiod = $number_emp_name['payperiodid'];
				
				//for selecting records
			/* 	$sql_data = "SELECT 
						a.loanfileid,
						a.loanfiledesc,
						a.employeeid,
						a.loantableid,
						a.loanamount,
						a.amortization,
						b.loantabledesc,
						a.loanbalance
					FROM prlloanfile a,
					prlloantable b
					WHERE a.prlloanfile = '".$_GET['LoanFileId']."'
					ORDER BY a.loanfileid";
				$ErrMsg = _('The employee master could not be retrieved because');
				$result_data = DB_query($sql_data,$db,$ErrMsg);
				$myrow_data = DB_fetch_array($result_data); */
				
				// for loan description and type start
			 	$sql_desc = "SELECT loanfiledesc,loantableid
							FROM prlloanfile 
							WHERE loanfileid = '".$_GET['LoanFileId']."'";
				$result_desc = DB_query($sql_desc,$db);
				$myrow_desc = DB_fetch_array($result_desc); 
				
				$sql_type = "SELECT loantabledesc, loantableid
							FROM prlloantable 
							WHERE loantableid = '".$myrow_desc['loantableid']."'";
				$result_type = DB_query($sql_type,$db);
				$myrow_type = DB_fetch_array($result_type);
				// for loan description and type end
				
				
				$sql_record = "SELECT startdeduction,
									loanamount,
									amortization
									FROM prlloanfile 
									WHERE loanfileid = '".$_GET['LoanFileId']."'";
				$result_record = DB_query($sql_record,$db);
				$myrow_record = DB_fetch_array($result_record);
				
				//for counting if theres a record
				$sql_count = "SELECT * FROM prlloandeduct WHERE loan_file_id = '".$_GET['LoanFileId']."'";
				$result_count = DB_query($sql_count,$db);
				$myrow_count = DB_num_rows($result_count);
				
				$skip_count = "SELECT SUM(skip_deduct) as skip_deduct1 FROM prlloandeduct WHERE loan_file_id = '".$_GET['LoanFileId']."'";
				$result_skip_count = DB_query($skip_count,$db);
				$myrow_skip_count = DB_fetch_array($result_skip_count);
				$skip_deduct = $myrow_skip_count['skip_deduct1'];
				
				
				$loan_amount = $myrow_record['loanamount'];
				$amortization = $myrow_record['amortization'];
?>

<p><b>Name:</b> <?php echo $emp_name; ?></p>
<p><b>Loan Description:</b> <?php echo $myrow_desc['loanfiledesc']; ?></p>
<p><b>Loan Type:</b> <?php echo $myrow_type['loantabledesc']; ?></p>
<p><b>Loan Amount:</b> <?php echo 'Php. '. number_format($loan_amount,2); ?></p>

<?php if($myrow_count <= 0){ // this will show if theres no record
			$start = $myrow_record['startdeduction']; //start date
			$loan_amount = $myrow_record['loanamount'];
			$amortization = $myrow_record['amortization'];
			//$balance = $myrow[7];
			$loan_file_id = $_GET['LoanFileId'];
			$loan_type = $myrow_type['loantableid'];
			$empid = $_GET['empid'];
			
			//for day range
			if ($payperiod == 1){
				$day_range_val = 14;
			} else if($payperiod == 2){
				$day_range_val = 14;
			} else if($payperiod == 3){
				$day_range_val = 7;
			} else if($payperiod == 4){
				$day_range_val = 7;
			}
			
			//for no. of deduct
			$no_of_deduct = ($loan_amount / $amortization) + $skip_deduct;
			$day_range = $day_range_val;
			
			
			$dates = array();
			$start = $current = strtotime($start);

			
			$x=1;
			while ($x <= $no_of_deduct) {
				$dates[] = date('Y-m-d', $current);
				$current = strtotime('+'.$day_range.' days', $current);
				$x++;
			}
			?>
			
			<form action='' method='post'>	
				
				<table>
					<?php 
					$loop = 1;
					foreach ($dates as $date) {
					$amortization1 = $amortization * $loop;
					$balance1 = $loan_amount - $amortization1;
					echo 
					"<tr style='display:none'>
						<td><input type='text' name='loan_file_id". $loop ."' value='$loan_file_id'></td>
						<td><input type='text' name='date". $loop ."' value='$date'></td>
						<td><input type='text' name='loan_amount". $loop ."' value='$loan_amount'></td>
						<td><input type='text' name='amortization". $loop ."' value='$amortization'></td>
						<td><input type='text' name='balance". $loop ."' value='$balance1'></td>
						<td><input type='text' name='loan_type". $loop ."' value='$loan_type'></td>						
					</tr>";
					$loop++;
					} ?>
					<tr>
						<td><input type='submit' name='submit_loan' value='Click to process loan'></td>
					</tr>
				</table>
				
			</form>
<?php	}
		else{ // this will show if theres a record ?>
		<center>
		<form action='prlViewLoanUpdate.php' method='post'>
		<table border=0 class="jinnertable" width="70%">
			<tr>
				<td class='tableheader'>Date</td>
				<td class='tableheader'>Amortization</td>
				<td class='tableheader'>Balance</td>
				<td class='tableheader' style='width: 150px;'>Skip Deduction</td>
			</tr>
			<?php 
			$today = date('c');
			$counter = 0;
			$counter_payment = 1;
			while($myrow_fetch = DB_fetch_array($result_count)){
			$day = date('c', strtotime($myrow_fetch['date'])); ?>
			<tr	<?php if($myrow_fetch['balance']<0){ echo "style='display:none'"; } ?>>
				<td <?php if($myrow_fetch['skip_deduct'] == 1){ echo "id='yes_deduct'"; } ?>><?php echo $myrow_fetch['date']; ?></td>
				<td <?php if($myrow_fetch['skip_deduct'] == 1){ echo "id='yes_deduct'"; } ?>><?php echo number_format($myrow_fetch['amortization'],2); ?></td>
				<td <?php if($myrow_fetch['skip_deduct'] == 1){ echo "id='yes_deduct'"; } ?>><?php echo number_format($myrow_fetch['balance'],2); ?></td>
				<td>
				<input type='radio' name='deduct<? {echo $counter;} ?>' value='1'
				<?php if($day < $today){ echo "disabled"; } ?>
				<?php if($myrow_fetch['skip_deduct']==1){ echo "checked"; } ?>/> yes |
				<input type='hidden' name='loan_id[]' value='<?php echo $myrow_fetch['id']; ?>'>
				<input type='radio' name='deduct<? {echo $counter;}?>' value='0'
				<?php if($day < $today){ echo "disabled"; } ?>				
				<?php if($myrow_fetch['skip_deduct']==0){ echo "checked"; } ?>/> no
				</td>
				
			</tr>
			<?php 	$counter++;
					$counter_payment++;
			} ?>
				<input type='hidden' name='loanfileid' value='<?php echo $_GET['LoanFileId']; ?>'>
				<input type='hidden' name='empid' value='<?php echo $_GET['empid']; ?>'>
			<tr>
				<td colspan='3'><input style='float: right' type='submit' name='update_loan' value='Update Skip Deduction'>
				</td>
		</form>
				<?php
				$sql_bal = "SELECT * FROM prlloandeduct WHERE loan_file_id = '".$_GET['LoanFileId']."' AND skip_deduct = 0";
							$result_bal = DB_query($sql_bal, $db);
				?>
				<form action='prlViewLoanUpdateBal.php' method='post'>
				<?php	
				$loop2 = 1;
				while($row_bal = DB_fetch_array($result_bal)){
				$amortization2 = $amortization * $loop2;
				$balance2 = $loan_amount - $amortization2;
					echo "<input type='hidden' name='loanfileid' value='".$_GET['LoanFileId']."'>";
					echo "<input type='hidden' name='empid' value='".$_GET['empid']."'>";
					echo "<input type='hidden' name='id[]' value='".$row_bal['id']."'>";
					echo "<input type='hidden' name='balance[]' value='".$balance2."'>";
					$loop2++;
				}?>		
					<td>
					<?php
						$sql_adjust = "SELECT balance, COUNT(balance) as count
							FROM prlloandeduct 
							WHERE loan_file_id = '".$_GET['LoanFileId']."'
							GROUP BY balance
							HAVING COUNT(balance) > 1";
						$result_adjust = DB_query($sql_adjust,$db);
						$row_adjust = DB_fetch_array($result_adjust);
						$same_balance = $row_adjust['count'];
					?>
						<input type='submit' name='submit' value='Update Balance' <?php //if($same_balance < 1){ echo 'style="display:none"';} ?> >
					</td>
				</form>
				
			</tr>
		</table>
		
		</center>
<?php 	} ?>

	


<?php 
	if(isset($_POST['submit_loan'])){
		for ($i=1; $i <= $no_of_deduct ; $i++)
					{
					$sql = "INSERT INTO prlloandeduct (		
												loan_file_id,
												loan_type,
												date,
												loan_amount,
												amortization,
												balance)
											VALUES ( '" . $_POST['loan_file_id' . $i] . "',
													'" . $_POST['loan_type' . $i] . "',
													'" . $_POST['date' . $i] . "',
													'" . $_POST['loan_amount' . $i] . "',
													'" . $_POST['amortization' . $i] . "',
													'" . $_POST['balance' . $i] . "'
											)";
					
							$ErrMsg = _('The date') . ' ' . $_POST['loan_file_id' . $i] . ' ' . _('could not be added because');
							$DbgMsg = _('The SQL that was used to insert the loan but failed was');
							$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);
								
					} ?>
					
					<script type='text/javascript'>	
					if(confirm('Loan Processed.')){
						window.location=window.location;  
						}
					</script>	
<?php } ?>
    
			<!--

			<script type='text/javascript'>
			function delete_row(id, loanfileid, empid){
			var answer = confirm('Are you sure want to delete this record? ');
			if(answer) {//if user clicked ok
			//redirect to url with action as delete and id to the record to be deleted	
			window.location = 'prlViewLoanDelete.php?action=delete&id=' + id + '&LoanFileId=' + loanfileid + '&empid=' + empid;
				}
				}
			
			</script> 

			-->


