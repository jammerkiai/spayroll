<?php 
ob_start(); 

include('includes/session.inc');
include('includes/header.inc');

	DB_data_seek($result_emp_name, 0);
				//for employee name
				$sql_emp_name = 'SELECT CONCAT(lastname, ", ",firstname) AS name,
								payperiodid FROM  prlemployeemaster 
				WHERE employeeid = "'. $_GET['empid'] .'"';
				$result_emp_name = DB_query($sql_emp_name, $db);
				$number_emp_name = DB_fetch_array($result_emp_name);
				$emp_name = $number_emp_name['name'];
				$payperiod = $number_emp_name['payperiodid'];
				
				//for selecting records
				$sql = "SELECT 
						a.loanfileid,
						a.loanfiledesc,
						a.employeeid,
						a.loantableid,
						a.loanamount,
						a.amortization,
						b.loantabledesc,
						a.loanbalance,
						a.startdeduction
					FROM prlloanfile a
					INNER JOIN prlloantable b
					ON a.loantableid = b.loantableid
					ORDER BY a.loanfileid";
				$ErrMsg = _('The employee master could not be retrieved because');
				$result = DB_query($sql,$db,$ErrMsg);
				$myrow = DB_fetch_row($result);
				
				$sql_record = "SELECT startdeduction,
									loanamount,
									amortization
									FROM prlloanfile 
									WHERE loanfileid = '".$_GET['LoanFileId']."'";
				$result_record = DB_query($sql_record,$db);
				$myrow_record = DB_fetch_array($result_record);
				
				//for counting if theres a record
				$sql_count = "SELECT * FROM prlloandeduct WHERE loan_file_id = '".$_GET['LoanFileId']."'";
				$result_count = DB_query($sql_count,$db);
				$myrow_count = DB_num_rows($result_count);
				
				$skip_count = "SELECT SUM(skip_deduct) as skip_deduct1 FROM prlloandeduct WHERE loan_file_id = '".$_GET['LoanFileId']."'";
				$result_skip_count = DB_query($skip_count,$db);
				$myrow_skip_count = DB_fetch_array($result_skip_count);
				$skip_deduct = $myrow_skip_count['skip_deduct1'];

			
			/* $start = $myrow[8]; //start date
			$loan_amount = $myrow[4];
			$amortization = $myrow[5]; */
			$start = $myrow_record['startdeduction']; //start date
			$loan_amount = $myrow_record['loanamount'];
			$amortization = $myrow_record['amortization'];
			
			//$balance = $myrow[7];
			$loan_file_id = $_GET['LoanFileId'];
			$empid = $_GET['empid'];
			
			//for day range
			if ($payperiod == 1){
				$day_range_val = 14;
			} else if($payperiod == 2){
				$day_range_val = 14;
			} else if($payperiod == 3){
				$day_range_val = 7;
			} else if($payperiod == 4){
				$day_range_val = 7;
			}
			
			//for no. of deduct
			$no_of_deduct = ($loan_amount / $amortization) + $skip_deduct;
			$day_range = $day_range_val;
			
			
			$dates = array();
			$start = $current = strtotime($start);

			
			$x=1;
			while ($x <= $no_of_deduct) {
				$dates[] = date('Y-m-d', $current);
				$current = strtotime('+'.$day_range.' days', $current);
				$x++;
			}
			?>
			
			<div id='content'><br/><div align='left' class='subheader'>
				<a href='prlViewLoan.php?&LoanFileId=<?php echo $loan_file_id; ?>&empid=<?php echo $empid; ?>'>
					<img src='images/back.png' width='30' height='30'/>
				</a>&nbsp;&nbsp;View Loan
			</div>
			
			<form action='' method='post'>	
				
				<table>
					<?php 
					$loop = 1;
					foreach ($dates as $date) {
					$amortization1 = $amortization * $loop;
					$balance1 = $loan_amount - $amortization1;					
					
					echo 
					"<tr style='display:none'>
						<td><input type='text' name='loan_file_id". $loop ."' value='$loan_file_id'></td>
						<td><input type='text' name='date". $loop ."' value='$date'></td>
						<td><input type='text' name='loan_amount". $loop ."' value='$loan_amount'></td>
						<td><input type='text' name='amortization". $loop ."' value='$amortization'></td>
						<td><input type='text' name='balance". $loop ."' value='$balance1'></td>
					</tr>";
					$loop++;
					} ?>
					<tr>
						<td><input style='margin: 30px 0 0 50px;' type='submit' name='submit_loan' value='Click to adjust payment'></td>
					</tr>
				</table>
				
			</form>
			
	<?php 
	if(isset($_POST['submit_loan']))
		{
		for ($i=1; $i <= $no_of_deduct ; $i++)
			{
					$sql_entry = 'SELECT * FROM  prlloandeduct
							WHERE loan_file_id = "'. $_GET['LoanFileId'] .'" AND date = "'. $_POST['date' . $i] .'"';
							$result_entry = DB_query($sql_entry, $db);
							$number_entry = DB_num_rows($result_entry);
					if($number_entry > 0){
						echo "Theres record";
					}
					else{
					$sql = "INSERT INTO prlloandeduct (		
												loan_file_id,
												date,
												loan_amount,
												amortization,
												balance)
											VALUES ( '" . $_POST['loan_file_id' . $i] . "',
													'" . $_POST['date' . $i] . "',
													'" . $_POST['loan_amount' . $i] . "',
													'" . $_POST['amortization' . $i] . "',
													'" . $_POST['balance' . $i] . "'
											)";
					
							$ErrMsg = _('The date') . ' ' . $_POST['loan_file_id' . $i] . ' ' . _('could not be added because');
							$DbgMsg = _('The SQL that was used to insert the loan but failed was');
							$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);
							
					} 		
			} header("Location: prlViewLoan.php?&LoanFileId=".$loan_file_id."&empid=".$empid."");
		} 
	?>
		