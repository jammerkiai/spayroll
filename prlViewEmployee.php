<?php

$PageSecurity = 5;
include('includes/session.inc');
$title = _('Employee Maintenance');
include('includes/header.inc');
include('includes/footer.inc');
include('includes/SQL_CommonFunctions.inc');
include('includes/prlFunctions.php');

$eid = $_GET['EmployeeID'];
$sql = "SELECT a.employeeid,UPPER(a.lastname)as lastname,UPPER(a.firstname)as firstname,UPPER(a.middlename)as middlename,
		a.address1,a.address2,a.city,a.state,a.zip,a.country,a.marital,a.birthdate,a.TimeIn,a.TimeOut,a.Active,a.gender,
		a.atmnumber,a.taxactnumber,a.ssnumber,a.hdmfnumber,a.phnumber,a.taxstatusid,a.payperiodid,a.paytype,a.periodrate,
		a.hourlyrate,a.position,b.image,a.departmentid,a.costcenterid,a.employmentid,a.companyagencyid
			FROM 
			prlemployeemaster a
			LEFT JOIN uploads b
			ON a.employeeid = b.empid
			LEFT JOIN prldepartment c
			ON a.departmentid = c.departmentid
			LEFT JOIN prlemploymentstatus d
			ON a.employmentid = d.employmentid
			LEFT JOIN companyagency ca
			ON a.companyagencyid = ca.CompanyAgencyID
			WHERE a.employeeid = '".$eid."'
			";
	$result = mysql_query($sql);
	$row = mysql_fetch_assoc($result);
		$id = $row['employeeid'];
		$fname = $row['firstname'];
		$mname = $row['middlename'];
		$lname = $row['lastname'];
		$gender = $row['gender'];
		if($row['image'] == NULL){
		$image = "default.png";
		}else{
		$image = $row['image'];}
		$address1 = $row['address1'];
		$address2 = $row['address2'];
		$city = $row['city'];
		$state = $row['state'];
		$zip = $row['zip'];
		$country = $row['country'];
		$marital = $row['marital'];
		$bdate = $row['birthdate'];
		$birthdate = date("m/d/Y",strtotime($bdate));
		$timeIn = $row['TimeIn'];
		$timeOut = $row['TimeOut'];
		$atm = $row['atmnumber'];
		$tax = $row['taxactnumber'];
		$sss = $row['ssnumber'];
		$pagibig = $row['hdmfnumber'];
		$philhealth = $row['phnumber'];
		$position = $row['position'];
		$periodrate = $row['periodrate'];
		$hourlyrate = $row['hourlyrate'];
		$taxstatus = $row['taxstatusid'];
		$departmentid = $row['departmentid'];
		$costcenterid = $row['costcenterid'];
		$CompanyAgencyID = $row['companyagencyid'];
		$employmentid = $row['employmentid'];
		$payperiod = $row['payperiodid'];
		$paytype = $row['paytype'];
		$status = $row['Active'];
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="/resources/demos/style.css">
  

  
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/tabs.min.js"></script>

<script type="text/javascript">
$(document).ready(function () {
	$("ul.JTabs").tabs("div.JContent > div");
});

  $(function() {
    $( "#datepicker" ).datepicker();
  });


function payPeriodChange() {
    if (document.getElementById("ddlPayPeriod").value != "1"){
       // alert(document.getElementById("ddlPayPeriod").value );
	  document.getElementById("lblPayperMonth").style.visibility='hidden';
	  document.getElementById("lblpesoSign").style.visibility='hidden';
	  document.getElementById("lblperMonth").style.visibility='hidden';
	  
	 }     
     if (document.getElementById("ddlPayPeriod").value == "1"){
      document.getElementById("lblperMonth").style.visibility='visible';
	  document.getElementById("lblPayperMonth").style.visibility='visible';
	  document.getElementById("lblpesoSign").style.visibility='visible';
	 }     
}


function myFunction()
{
alert("employee successfully added!");
}

function updateTextBox() {
    var myTextBox = document.getElementById("txtPeriodRate");
    var myTextBox1 = document.getElementById("paypermonth");
	var myTextBox2 = document.getElementById("paypersemi");
	var myTextBox3 = document.getElementById("hourlyrate");
	
	var payperSelect = document.getElementById("ddlPayPeriod");
	var status = payperSelect.options[payperSelect.selectedIndex].text;
  
       
		
		 if(status == "Monthly") {
			myTextBox.value = myTextBox2.value * 26;
			myTextBox.readOnly = "true";
			myTextBox3.value = myTextBox2.value / 8;
		 	myTextBox1.style.display = "none";
			myTextBox2.style.display = "block";
			document.getElementById("lblpaypermonth").style.display = "none";
			document.getElementById("lblpaypersemi").style.display = "block";
			
		}else if(status == "Semi-Monthly") {
			
			myTextBox1.value = myTextBox2.value * 26;
			myTextBox.value = myTextBox1.value / 2;
			myTextBox1.style.display = "block";
			myTextBox2.style.display = "block";
			myTextBox.readOnly = "true";
			document.getElementById("lblpaypermonth").style.display = "block";
			document.getElementById("lblpaypersemi").style.display = "block";
			document.getElementById("lblpaypersemi").innerHTML = "/ Day";
			myTextBox3.value = myTextBox2.value / 8;
		 
		 }else if(status == "Daily") {
			
			myTextBox1.style.display = "none";
			myTextBox2.style.display = "none";
			myTextBox.readOnly = "";
			document.getElementById("lblpaypermonth").style.display = "none";
			document.getElementById("lblpaypersemi").style.display = "none";
			myTextBox3.value = myTextBox.value / 8;
		 }
		
}
</script>

</head>
<?php if(isset($_POST['submit'])) {

	$allowed_filetypes = array('.jpg','.jpeg','.png','.gif');
	$max_filesize = 10485760;
	$upload_path = 'uploads/';
	$ext = substr($filename, strpos($filename,'.'), strlen($filename)-1);
	$filename = $_FILES['userfile']['name'];
	$empid = $_POST['empid'];
	$address1 = $_POST['address1'];
	$address2 = $_POST['address2'];
	$city = $_POST['city'];
	$zip = $_POST['zip'];
	$state = $_POST['state'];
	$country = $_POST['country'];
	$gender = $_POST['gender'];
	$marital = $_POST['marital'];
	$bdate = $_POST['birthdate'];
	$birthdate = date("Y-m-d",strtotime($bdate));
	$atm = $_POST['atm'];
	$tax = $_POST['tax'];
	$sss = $_POST['sss'];
	$philhealth = $_POST['philhealth'];
	$pagibig = $_POST['pagibig'];
	//$periodrate = $_POST['periodrate'];
	$hourlyrate = $_POST['hourlyrate'];
	$position = $_POST['position'];
	$timeIn = $_POST['timeIn'];
	$timeOut = $_POST['timeOut'];
	$status = $_POST['status'];
	$taxstatusid = $_POST['taxstatusid'];
	$departmentid = $_POST['departmentid'];
	$employmentid = $_POST['EmpStatID'];
	$CompanyAgencyID = $_POST['CompanyAgencyID'];
	$costcenterid = $_POST['costcenterid'];
	$payperiodid = $_POST['payperiodid'];
	//if($payperiodid == '1'){
	// $p = $_POST['periodrate'] / 2;
	 //$periodrate = $p;
	//}else{
     $periodrate = $_POST['periodrate'];	
	//}
	$paytype = $_POST['paytype'];

	$queryUpEmp = "UPDATE prlemployeemaster SET address1 = '".$address1."', address2 = '".$address2."',
	city = '".$city."',state = '".$state."',zip = '".$zip."',country = '".$country."',gender = '".$gender."',
	marital = '".$marital."',birthdate = '".$birthdate."',atmnumber = '".$atm."',taxactnumber = '".$tax."',
	ssnumber = '".$sss."',hdmfnumber = '".$pagibig."',phnumber = '".$philhealth."',TimeIn = '".$timeIn."',
	TimeOut = '".$timeOut."',periodrate = '".$periodrate."',hourlyrate = '".$hourlyrate."',position = '".$position."',
	departmentid = '".$departmentid."',taxstatusid = '".$taxstatusid."',costcenterid = '".$costcenterid."',
	employmentid = '".$employmentid."',payperiodid = '".$payperiodid."',paytype = '".$paytype."',Active = '".$status."',
	companyagencyid = '".$CompanyAgencyID."'
	WHERE employeeid = '".$empid."'"; 
	mysql_query($queryUpEmp);

	
	/*if(!in_array($ext,$allowed_filetypes))
	  die('The file you attempted to upload is not allowed.');

	if(filesize($_FILES['userfile']['tmp_name']) > $max_filesize)
	  die('The file you attempted to upload is too large.');

	if(!is_writable($upload_path))
	  die('You cannot upload to the specified directory, please CHMOD it to 777.');*/
	
	if(move_uploaded_file($_FILES['userfile']['tmp_name'],$upload_path . $filename)) {

	

		$sqlUp = "SELECT * FROM uploads WHERE empid = '".$empid."'";
		$resultUp = mysql_query($sqlUp);
		$num_rowsUp = mysql_num_rows($resultUp);

		if($num_rowsUp > 0 && $filename != ""){
			$query = "UPDATE uploads SET image = '".$filename."' WHERE empid = '".$empid."'"; 
			mysql_query($query);
	   }else{ 
			$query = "INSERT INTO uploads (empid,image) VALUES ('".$empid."','".$filename."')"; 
			mysql_query($query);
		}
	  

	//echo 'Your file upload was successful!';

	} //else {
		// echo 'There was an error during the update.  Please try again.';
	//}
	header("Location: prlViewEmployee.php?&EmployeeID=".$empid);
}elseif(isset($_POST['delete'])){
$empid = $_POST['empid'];

			$querydel = "UPDATE prlemployeemaster SET deleted = '1', active = '0' WHERE employeeid = '".$empid."'"; 
			mysql_query($querydel);
			
		header("Location: prlEmployee.php");
}
?>

<body>
<div id="content">
<br />
<div align="left" class="subheader">
	<a href="<?php echo $rootpath;?>/prlViewEmpDetails.php?&EmployeeID=<?php echo $eid;?>">
		<img src="images/back.png" width="30" height="30" />
	</a>&nbsp;&nbsp;Edit Employee Details
</div><br/>
<div>
	<FORM METHOD='post' ACTION='' enctype="multipart/form-data">
	<div id="201Main" style="background-color:transparent">
			<div class="headholder" > 
				<div class="hpictab">
					<span class="avatar">
						<input name="empid" type="hidden" maxlength="64" class="intext" value="<?php echo $id;?>">
						<img src="uploads/<?php echo $image;?>" name="image_medium" width="93" height="90" border="1" align="middle" />
						<input type="file" name="userfile" accept="image/*"/>
					</span>
				</div>
				<div class="heditsave"> &nbsp;
				</div>
				<div class="hnamedetail">
					<table width="100%" border="0">
					<tr>
						<td width="80%" height="51" class='theader' name="empname">
						Name
						</td>
					</tr>
					<tr>
						<td >
							<input type="text" maxlength="32" class="intext" value="<?php echo $fname;?>" placeholder="First Name">
							<input type="text" maxlength="32" class="intext" value="<?php echo $mname;?>" placeholder="Middle Name">
							<input type="text" maxlength="32" class="intext" value="<?php echo $lname;?>" placeholder="Last Name">
						</td>
					<tr>
						<td class='theader'>
						Employee ID:
						</td>
					<tr>
						<td height="30">
							<input disabled="disabled" type="text" maxlength="64" class="intext" value="<?php echo $id;?>">
						</td>
					</table>
				</div>
			</div>
		</div>
<div class="main_title" style="background-color:transparent"><p>&nbsp;</p></div>
<ul class="JTabs" style="background-color:transparent">
    	<li>
        	<a href="#" role="presentation" tabindex="-1">Personal Information</a>
        </li>
        <li>
        	<a href="#" role="presentation" tabindex="-1">Accounting Settings</a>
        </li>
        <li>
        	<a href="#" role="presentation" tabindex="-1">HR Settings</a>
        </li>
</ul>
    <div class="JContent">
    	<div>
        <table width="100%" border="0" >
              <tr>
                <td height="44" colspan="2" class="theader">Contact Information</td>
                <td colspan="2" class="theader"><p>Status</p></td>
              </tr>
              <tr>
                <td width="20%" class='tdtitle'>Address 1:</td>
                <td width="29%"><input type="text" class="intext" name="address1" value="<?php echo $address1;?>" size="42"></td>
				
                <td width="20%" class='tdtitle'>Marital Status:</td>
                <td width="31%"><select name="marital" class="intext" ><?php if ($marital=='Single'){
		echo '<OPTION SELECTED VALUE="Single">' . _('Single');
		echo '<OPTION VALUE="Married">' . _('Married');
		echo '<OPTION VALUE="Sep/Div">' . _('Separated/Divorced');
		echo '<OPTION VALUE="Widowed">' . _('Widowed');
	} elseif ($marital=='Married'){
		echo '<OPTION SELECTED VALUE="Married">' . _('Married');
		echo '<OPTION VALUE="Single">' . _('Single');
		echo '<OPTION VALUE="Sep/Div">' . _('Separated/Divorced');
		echo '<OPTION VALUE="Widowed">' . _('Widowed');	
	} elseif ($marital=='Sep/Div'){
	    echo '<OPTION SELECTED VALUE="Sep/Div">' . _('Separated/Divorced');
	    echo '<OPTION VALUE="Single">' . _('Single');
		echo '<OPTION VALUE="Married">' . _('Married');
	    echo '<OPTION VALUE="Widowed">' . _('Widowed');	
    } elseif ($marital=='Widowed'){
	    echo '<OPTION SELECTED VALUE="Widowed">' . _('Widowed');
	    echo '<OPTION VALUE="Single">' . _('Single');
		echo '<OPTION VALUE="Married">' . _('Married');
		echo '<OPTION VALUE="Sep/Div">' . _('Separated/Divorced');
	} else {
		echo '<OPTION SELECTED VALUE="">' . _('Select One');
		echo '<OPTION VALUE="Single">' . _('Single');
		echo '<OPTION VALUE="Married">' . _('Married');
		echo '<OPTION VALUE="Sep/Div">' . _('Separated/Divorced');
		echo '<OPTION VALUE="Widowed">' . _('Widowed');
	}?></select></td>
              </tr>
              <tr>
			   <td width="20%" class='tdtitle'>Address 2:</td>
                <td width="29%"><input type="text" name="address2" class="intext" value="<?php echo $address2;?>" size="42"></td>
                
                <td class='tdtitle'>Date Of Birth:</td>
                <td>
                <input type="text" class="intext" name="birthdate" value="<?php echo $birthdate;?>" placeholder="mm/dd/yyyy" id="datepicker" size="32"></select>
                </td>
              </tr>
              <tr>
			  <td class='tdtitle'>City:</td>
                <td><input type="text" class="intext" maxlength="32" name="city" value="<?php echo $city;?>"></td>
				</tr>
<!--				<tr>
                <td class='tdtitle'>State:</td>
                <td><input type="text" class="intext" maxlength="32" name="state" value="<?php echo $state;?>"></td>
                <td colspan="2" rowspan="4">&nbsp;</td>
              </tr> -->
              <tr>
                <td class='tdtitle'>Zip Code:</td>
                <td><input type="text" class="intext" maxlength="32" name="zip" value="<?php echo $zip;?>"></td>
              </tr>
              <tr>
                <td height="27" class='tdtitle'><p>Country:</p></td>
                <td><input type="text" class="intext" maxlength="32" name="country" value="<?php echo $country;?>"></td>
          	</tr>
              <tr>
                <td height="27" class='tdtitle'>Gender:</td>
                <td><?php if ($gender == 'F'){
				echo '<input type="radio" name="gender" value="M">' . _('Male');
				echo '<input type="radio" name="gender" value="F" checked="checked">' . _('Female');
				} else {
					echo '<input type="radio" name="gender" value="M" checked="checked">' . _('Male');
					echo '<input type="radio" name="gender" value="F">' . _('Female');
				}?></td>
              </tr>
          </table>
        <p>&nbsp;</p>
      </div>
        <div>
	    <table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr>
                <td height="51" colspan="4" class="theader">Account Details</td>
              </tr>
              <tr>
                <td width="20%" class='tdtitle'>Atm number:</td>
                <td width="29%"><input type="text" class="intext" maxlength="32" name="atm" value="<?php echo $atm;?>"></td>
                <td width="21%" class='tdtitle'>Tax Status:</td>
                <td width="30%" class='tdtitle'><SELECT class='intext' NAME='taxstatusid'>
				<?php DB_data_seek($result, 0);
				$sql = "SELECT taxstatusid, taxstatusdescription FROM prltaxstatus";
				$result = DB_query($sql, $db);
				while ($myrow = DB_fetch_array($result)) {
					if ($myrow['taxstatusid'] == $taxstatus) {
						echo '<OPTION SELECTED VALUE=';
					} else {
						echo '<OPTION VALUE=';
					}
					echo $myrow['taxstatusid'] . '>' . $myrow['taxstatusdescription'];
				}?></td>
              </tr>
              <tr>
                <td class='tdtitle'>Tax Account #:</td>
                <td><input type="text" class="intext" maxlength="32" name="tax" value="<?php echo $tax;?>"></td>
                <td class='tdtitle'>Pay Period:</td>
                <td><SELECT class='intext' NAME='payperiodid' id="ddlPayPeriod" onchange="updateTextBox()">
	<?php DB_data_seek($result, 0);
	$sql = 'SELECT payperiodid, payperioddesc FROM prlpayperiod WHERE active = "1" ORDER BY payperioddesc ASC';
	$result = DB_query($sql, $db);
	while ($myrow = DB_fetch_array($result)) {
		if ($myrow['payperiodid'] == $payperiod){
			echo '<OPTION SELECTED VALUE=';
		} else {
			echo '<OPTION VALUE=';
		}
		echo $myrow['payperiodid'] . '>' . $myrow['payperioddesc'];
	}?> </td>
              </tr>
              <tr>
                <td class='tdtitle'>SSS #:</td>
                <td><input type="text" class="intext" maxlength="32" name="sss" value="<?php echo $sss;?>"></td>
                <td class='tdtitle'>Pay Type:</td>
                <td><SELECT class='intext' NAME='paytype'>
	<?php if ($paytype == 0){
		echo '<OPTION SELECTED VALUE=0>' . _('Salary');
		echo '<OPTION VALUE=1>' . _('Hourly');
	} else {
		echo '<OPTION SELECTED VALUE=1>' . _('Hourly');
		echo '<OPTION VALUE=0>' . _('Salary');
	}?></select></td>
              </tr>
              <tr>
                <td class='tdtitle'>Pag-ibig #:</td>
                <td><input type="text" class="intext" maxlength="32" name="pagibig" value="<?php echo $pagibig;?>"></td>
                <td class='tdtitle'>Pay Per Hour:</td>
                <td><input type="text" class="intext" maxlength="32" id="hourlyrate" name="hourlyrate" value="<?php echo $hourlyrate;?>" readonly="readonly"></td>
				
				
				</td>
              </tr>
              <tr>
                <td class='tdtitle'>Philhealth #:</td>
                <td><input type="text" class="intext" maxlength="32" name="philhealth" value="<?php echo $philhealth;?>"></td>
                <td class='tdtitle'>Pay Per Period:</td>
				<td>
				
				<input id="txtPeriodRate" type="text" class="intext" maxlength="32" name="periodrate" value="<?php $periodrate2 = $periodrate; 
				echo $periodrate2;?>" onchange="updateTextBox()">
			</tr><tr>
				<td></td><td></td>
				<td>
				<input type="text" class="intext" name="paypermonth" id="paypermonth" style="<?php if($payperiod == '1'){echo "display:block;line-height:25px;float:left;";}else{echo "display:none; float:left; margin-right:3px;";} ?>" readonly="readonly" value="<?php $periodrate2 = $periodrate; 
				if($payperiod == '1'){echo $periodrate2 * 2;}elseif($payperiod == '2'){echo $periodrate2;}?>">
				<label id="lblpaypermonth" name="lblpaypermonth" style="<?php if($payperiod == '1'){echo "display:block;line-height:25px;float:left;";}else{echo "display:none; line-height:25px;";} ?>">/ Month</label>
				
				<input type="text" class="intext" name="paypersemi" id="paypersemi" style="<?php if($payperiod == '1'){echo "display:block;line-height:25px;float:left;";}else{echo "display:none;float:left;";} ?>" onchange="updateTextBox()" value="<?php $periodrate2 = $periodrate; 
				if($payperiod == '1'){echo $periodrate2 * 2 / 26;}elseif($payperiod == '2'){echo $periodrate2 / 26;}?>"> 
				<label id="lblpaypersemi" name="lblpaypersemi" style="<?php if($payperiod == '1'){echo "display:block;line-height:25px;float:left;";}else{echo "display:none;float:left;";} ?>">/ Day</label>
				</td>
              </tr>
          </table>
	    <p>&nbsp;</p>
        </div>
     	 <div>
			<table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr>
                <td height="50" colspan="4" class="theader">Employment Details</td>
              </tr>
              <tr>
               <td width="20%" height="25" class='tdtitle'><p>Position:</p>
              <td width="29%" scope="col"><input type="text" class="intext" maxlength="32" name="position" value="<?php echo $position;?>"></th>      
              <td width="21%" class='tdtitle'>Time In    
              <td width="30%"><input type="text" class="intext jtime" maxlength="32" name="timeIn" value="<?php echo $timeIn;?>"></td>  
			</tr>
			<tr>
			<td class='tdtitle'>Company/ Agency: </td>
				<td><SELECT class='intext' NAME='CompanyAgencyID'>
				<?php DB_data_seek($result, 0);
				$sql = 'SELECT CompanyAgencyID, companyagency FROM companyagency';
				$result = DB_query($sql, $db);
				while ($myrow = DB_fetch_array($result)) {
					if ($myrow['CompanyAgencyID'] == $CompanyAgencyID){
						echo '<OPTION SELECTED VALUE=' . $myrow['CompanyAgencyID'] . '>' . $myrow['companyagency'];
					} else {
						echo '<OPTION VALUE=' . $myrow['CompanyAgencyID'] . '>' . $myrow['companyagency'];
					}
				}?> </td>
			</tr>
              <tr>
			    
                <td class='tdtitle'>Cost Center: </td>
				<td><SELECT class='intext' NAME='costcenterid'>
				<?php DB_data_seek($result, 0);
				$sql = 'SELECT code, description FROM workcentres';
				$result = DB_query($sql, $db);
				while ($myrow = DB_fetch_array($result)) {
					if ($myrow['code'] == $costcenterid){
						echo '<OPTION SELECTED VALUE=' . $myrow['code'] . '>' . $myrow['description'];
					} else {
						echo '<OPTION VALUE=' . $myrow['code'] . '>' . $myrow['description'];
					}
				}?> </td>
                <td class='tdtitle'>Time Out</td>
                <td><input type="text" class="intext jtime"  maxlength="32" name="timeOut" value="<?php echo $timeOut;?>"></td>
              </tr>
              <tr>
                <td class='tdtitle'>Department:</td>
                <td><SELECT class='intext' NAME='departmentid'>
				<?php DB_data_seek($result, 0);
				$sql = 'SELECT departmentid, departmentName FROM prldepartment ORDER BY departmentName ASC';
				$result = DB_query($sql, $db);
				while ($myrow = DB_fetch_array($result)) {
					if ($myrow['departmentid'] == $departmentid ){
						echo '<OPTION SELECTED VALUE=' . $myrow['departmentid'] . '>' . $myrow['departmentName'];
					} else {
						echo '<OPTION VALUE=' . $myrow['departmentid'] . '>' . $myrow['departmentName'];
					}
				}?> </td>
                <td class='tdtitle'>Employment Status:</td>
                <td><SELECT class='intext' NAME='EmpStatID'>
				<?php DB_data_seek($result, 0);
				$sql = 'SELECT employmentid, employmentdesc FROM prlemploymentstatus';
				$result = DB_query($sql, $db);
				while ($myrow = DB_fetch_array($result)) {
					if ($myrow['employmentid'] == $id){
						echo '<OPTION SELECTED VALUE=' . $myrow['employmentid'] . '>' . $myrow['employmentdesc'];
					} else {
						echo '<OPTION VALUE=' . $myrow['employmentid'] . '>' . $myrow['employmentdesc'];
					}
				}?></td>
              </tr>
              <tr>
                <td colspan="2"></td>
				<?php if($status=='1'){$checked = "checked";}else{$checked = "";}?>
				<td  class='tdtitle'>Active</td>
                <td><input type="checkbox" class="intext" name="status" <?php echo $checked;?> value="1"></input></td>
				
              </tr>
            </table>
			<p>&nbsp;</p>
         </div></div>
			<center><p><INPUT class='jinnerbot' TYPE='Submit' NAME='submit' VALUE='Update Employee'></p></center>
			<center><P><FONT COLOR=red><B>WARNING:There is no second warning if you hit the delete button below.<BR></FONT></B></p></center>
			<center><P><FONT COLOR=red><B>However checks will be made to ensure there are no outstanding purchase orders or existing accounts payable transactions before the deletion is processed<BR></FONT></B><br /></p></center>
			<center><p><INPUT class='jinnerbot' TYPE='Submit' NAME='delete' VALUE='Delete Employee'></p></center>
		 </FORM>		
</div>		 
</body>
</html>