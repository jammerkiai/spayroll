<?php

$PageSecurity = 5;
include('includes/session.inc');
$title = _('Employee Maintenance');
include('includes/header.inc');
include('includes/footer.inc');
include('includes/SQL_CommonFunctions.inc');
include('includes/prlFunctions.php');

$eid = $_GET['EmployeeID'];
$sql = "SELECT a.employeeid,UPPER(a.lastname)as lastname,UPPER(a.firstname)as firstname,UPPER(a.middlename)as middlename,
		a.address1,a.address2,a.city,a.state,a.zip,a.country,a.marital,a.birthdate,a.TimeIn,a.TimeOut,a.Active,a.gender,
		a.atmnumber,a.taxactnumber,a.ssnumber,a.hdmfnumber,a.phnumber,a.taxstatusid,a.payperiodid,a.paytype,a.periodrate,
		a.hourlyrate,a.position,b.image,d.departmentName,w.description as cost_center,t.taxstatusdescription,p.payperioddesc,
		e.employmentdesc, a.companyagencyid, ca.companyagency
			FROM 
			prlemployeemaster a
			LEFT JOIN uploads b
			ON a.employeeid = b.empid
			LEFT JOIN prldepartment d
			ON a.departmentid = d.departmentid
			LEFT JOIN workcentres w
			ON a.costcenterid = w.code
			LEFT JOIN prltaxstatus t
			ON a.taxstatusid = t.taxstatusid
			LEFT JOIN  prlpayperiod p
			ON a.payperiodid = p.payperiodid
			LEFT JOIN prlemploymentstatus e
			ON a.employmentid = e.employmentid
			LEFT JOIN companyagency ca
			ON a.companyagencyid = ca.CompanyAgencyID
			WHERE a.employeeid = '".$eid."'
			";
	$result = mysql_query($sql);
	$row = mysql_fetch_assoc($result);
		$id = $row['employeeid'];
		$fname = $row['firstname'];
		$mname = $row['middlename'];
		$lname = $row['lastname'];
		if($row['gender'] == "M"){
		$gender = "Male";
		}else{
		$gender = "Female";}
		if($row['image'] == NULL){
		$image = "default.png";
		}else{
		$image = $row['image'];}
		$address1 = $row['address1'];
		$address2 = $row['address2'];
		$city = $row['city'];
		$state = $row['state'];
		$zip = $row['zip'];
		$country = $row['country'];
		$marital = $row['marital'];
		$bdate = $row['birthdate'];
		$birthdate = date("m/d/Y",strtotime($bdate));
		$department = $row['departmentName'];
		$companyagency = $row['companyagency'];
		$costcenter = $row['cost_center'];
		$taxstatus = $row['taxstatusdescription'];
		$timeIn = $row['TimeIn'];
		$timeOut = $row['TimeOut'];
		$atm = $row['atmnumber'];
		$tax = $row['taxactnumber'];
		$sss = $row['ssnumber'];
		$pagibig = $row['hdmfnumber'];
		$philhealth = $row['phnumber'];
		$position = $row['position'];
		$periodrate = $row['periodrate'];
		$hourlyrate = $row['hourlyrate'];
		$payperiod = $row['payperioddesc'];
		if($row['paytype'] == '0'){
		$paytype = "Salary";
		}else{
		$paytype = "Hourly";}
		$empstat = $row['employmentdesc'];
		$status = $row['Active'];
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
 
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/tabs.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	$("ul.JTabs").tabs("div.JContent > div");
});
</script>

</head>

<div id="content">
<body>
<br />
<div align="left" class="subheader">
	<a href="<?php echo $rootpath;?>/prlEmployee.php?">
		<img src="images/back.png" width="30" height="30" />
	</a>&nbsp;&nbsp;View Employee Details
</div><br/>
<div>
	
	<div id="201Main" style="background-color:transparent">
			<div class="headholder" > 
				<div class="hpictab">
					<span class="avatar">
						<input name="empid" type="hidden" maxlength="64" value="<?php echo $id;?>">
						<img src="uploads/<?php echo $image;?>" name="image_medium" width="93" height="90" border="1" align="middle" />
					</span>
				</div>
				<div class="heditsave">
					<a href="/prlViewEmployee.php?&EmployeeID=<?php echo $eid;?>" class="jinnerbot2">Edit Employee</a>
				</div>
				<div class="hnamedetail">
					<table width="100%" border="0">
					<tr>
						<td width="80%" height="51" class='theader' name="empname">
						Name:
						</td>
					</tr>
					<tr>
						<td >
							<input type="text" maxlength="32" size="42"class="intext" value="<?php echo $lname.','.$fname.' '.$mname;?>" disabled="disabled">
						</td>
					<tr>
						<td class='theader'>
						Employee ID:
						</td>
					<tr>
						<td height="30">
							<input type="text" maxlength="64" class="intext" value="<?php echo $id;?>" disabled="disabled">
						</td>
					</table>
				</div>
			</div>
		</div>
<div class="main_title" style="background-color:transparent"><p>&nbsp;</p></div>
<ul class="JTabs" style="background-color:transparent">
    	<li>
        	<a href="#" role="presentation" tabindex="-1">Personal Information</a>
        </li>
        <li>
        	<a href="#" role="presentation" tabindex="-1">Accounting Settings</a>
        </li>
        <li>
        	<a href="#" role="presentation" tabindex="-1">HR Settings</a>
        </li>
</ul>
    <div class="JContent">
    	<div>
        <table width="100%" border="0" >
              <tr>
                <td height="44" colspan="2" class="theader">Contact Information</td>
                <td colspan="2" class="theader"><p>Status</p></td>
              </tr>
              <tr>
                <td width="20%" class='tdtitle'>Address 1:</td>
                <td width="29%"><label><?php echo $address1;?></label></td>
				
                <td width="20%" class='tdtitle'>Marital Status:</td>
                <td width="31%"><label><?php echo $marital;?></label></td>
              </tr>
              <tr>
			   <td width="20%" class='tdtitle'>Address 2:</td>
                <td width="29%"><label><?php echo $address2;?></label></td>
                
                <td class='tdtitle'>Date Of Birth:</td>
                <td>
                <label><?php echo $birthdate;?></label>
                </td>
              </tr>
              <tr>
			  <td class='tdtitle'>City:</td>
                <td><label><?php echo $city;?></label></td>
				</tr>
<!--				<tr>
                <td class='tdtitle'>State:</td>
                <td><label><?php echo $state;?></label></td>
                <td colspan="2" rowspan="4">&nbsp;</td>
              </tr> -->
              <tr>
                <td class='tdtitle'>Zip Code:</td>
                <td><label><?php echo $zip;?></label></td>
              </tr>
              <tr>
                <td height="27" class='tdtitle'><p>Country:</p></td>
                <td><label><?php echo $country;?></label></td>
          	</tr>
              <tr>
                <td height="27" class='tdtitle'>Gender:</td>
                <td><label><?php echo $gender;?></label></td>
              </tr>
          </table>
        <p>&nbsp;</p>
      </div>
        <div>
	    <table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr>
                <td height="51" colspan="4" class="theader">Account Details</td>
              </tr>
              <tr>
                <td width="20%" class='tdtitle'>Atm number:</td>
                <td width="29%"><label><?php echo $atm;?></label></td>
                <td width="21%" class='tdtitle'>Tax Status:</td>
                <td width="30%"><label><?php echo $taxstatus;?></label></td>
              </tr>
              <tr>
                <td class='tdtitle'>Tax Account #:</td>
                <td><label><?php echo $tax;?></label></td>
                <td class='tdtitle'>Pay Period:</td>
                <td><label><?php echo $payperiod;?></label></td>
              </tr>
              <tr>
                <td class='tdtitle'>SSS #:</td>
                <td><label><?php echo $sss;?></label></td>
                <td class='tdtitle'>Pay Type:</td>
                <td><label><?php echo $paytype;?></label></td>
              </tr>
              <tr>
                <td class='tdtitle'>Pag-ibig #:</td>
                <td><label><?php echo $pagibig;?></label></td>
                
				<td class='tdtitle'>Pay Per Period:</td>
				<td><label><?php echo $periodrate;?></label></td>
				
              </tr>
              <tr>
                <td class='tdtitle'>Philhealth #:</td>
                <td><label><?php echo $philhealth;?></label></td>
                <td class='tdtitle'>Pay Per Hour:</td>
                <td><label><?php echo $hourlyrate;?></label></td>
              </tr>
          </table>
	    <p>&nbsp;</p>
        </div>
     	 <div>
			<table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr>
                <td height="50" colspan="4" class="theader">Employment Details</td>
              </tr>
              <tr>
               <td width="20%" height="25" class='tdtitle'><p>Position:</p>
              <td width="29%" scope="col"><label><?php echo $position;?></label></td>      
              <td width="21%" class='tdtitle'>Time In    
              <td width="30%"><label><?php echo $timeIn;?></label></td>  
			</tr>
              <tr>
			  <td class='tdtitle'>Company/ Agency: </td>
				<td><label><?php echo $companyagency;?></label></td>
                <td class='tdtitle'>Cost Center: </td>
				<td><label><?php echo $costcenter;?></label></td>
                <td class='tdtitle'>Time Out</td>
                <td><label><?php echo $timeOut;?></label></td>
              </tr>
              <tr>
                <td class='tdtitle'>Department:</td>
                <td><label><?php echo $department;?></label></td>
                <td class='tdtitle'>Employment Status:</td>
                <td><label><?php echo $empstat;?></label></td>
              </tr>
              <tr>
                <td colspan="2"></td>
				<td  class='tdtitle'>Active</td>
                <td><?php if ($status == '1'){
				
				echo '<input type="checkbox" name="status" checked="checked" disabled="disabled"></input>';
				
				} else {
					echo '<input type="checkbox" name="status" disabled="disabled"></input>';
				}?>
				
				</td>
              </tr>
            </table>
			<p>&nbsp;</p>
         </div></div>
</div>	
		 
</body>
</html>