-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 25, 2016 at 02:04 PM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spayroll`
--

-- --------------------------------------------------------

--
-- Table structure for table `chartmaster`
--

CREATE TABLE IF NOT EXISTS `chartmaster` (
  `accountcode` int(11) NOT NULL DEFAULT '0',
  `accountname` char(50) NOT NULL DEFAULT '',
  `group_` char(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`accountcode`),
  KEY `AccountCode` (`accountcode`),
  KEY `AccountName` (`accountname`),
  KEY `Group_` (`group_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chartmaster`
--

INSERT INTO `chartmaster` (`accountcode`, `accountname`, `group_`) VALUES
(1, 'Default Sales/Discounts', 'Sales'),
(1010, 'Petty Cash', 'Current Assets'),
(1020, 'Cash on Hand', 'Current Assets'),
(1030, 'Cheque Accounts', 'Current Assets'),
(1040, 'Savings Accounts', 'Current Assets'),
(1050, 'Payroll Accounts', 'Current Assets'),
(1060, 'Special Accounts', 'Current Assets'),
(1070, 'Money Market Investments', 'Current Assets'),
(1080, 'Short-Term Investments (< 90 days)', 'Current Assets'),
(1090, 'Interest Receivable', 'Current Assets'),
(1100, 'Accounts Receivable', 'Current Assets'),
(1150, 'Allowance for Doubtful Accounts', 'Current Assets'),
(1200, 'Notes Receivable', 'Current Assets'),
(1250, 'Income Tax Receivable', 'Current Assets'),
(1300, 'Prepaid Expenses', 'Current Assets'),
(1350, 'Advances', 'Current Assets'),
(1400, 'Supplies Inventory', 'Current Assets'),
(1420, 'Raw Material Inventory', 'Current Assets'),
(1440, 'Work in Progress Inventory', 'Current Assets'),
(1460, 'Finished Goods Inventory', 'Current Assets'),
(1500, 'Land', 'Fixed Assets'),
(1550, 'Bonds', 'Fixed Assets'),
(1600, 'Buildings', 'Fixed Assets'),
(1620, 'Accumulated Depreciation of Buildings', 'Fixed Assets'),
(1650, 'Equipment', 'Fixed Assets'),
(1670, 'Accumulated Depreciation of Equipment', 'Fixed Assets'),
(1700, 'Furniture & Fixtures', 'Fixed Assets'),
(1710, 'Accumulated Depreciation of Furniture & Fixtures', 'Fixed Assets'),
(1720, 'Office Equipment', 'Fixed Assets'),
(1730, 'Accumulated Depreciation of Office Equipment', 'Fixed Assets'),
(1740, 'Software', 'Fixed Assets'),
(1750, 'Accumulated Depreciation of Software', 'Fixed Assets'),
(1760, 'Vehicles', 'Fixed Assets'),
(1770, 'Accumulated Depreciation Vehicles', 'Fixed Assets'),
(1780, 'Other Depreciable Property', 'Fixed Assets'),
(1790, 'Accumulated Depreciation of Other Depreciable Prop', 'Fixed Assets'),
(1800, 'Patents', 'Fixed Assets'),
(1850, 'Goodwill', 'Fixed Assets'),
(1900, 'Future Income Tax Receivable', 'Current Assets'),
(2010, 'Bank Indedebtedness (overdraft)', 'Liabilities'),
(2020, 'Retainers or Advances on Work', 'Liabilities'),
(2050, 'Interest Payable', 'Liabilities'),
(2100, 'Accounts Payable', 'Liabilities'),
(2150, 'Goods Received Suspense', 'Liabilities'),
(2200, 'Short-Term Loan Payable', 'Liabilities'),
(2230, 'Current Portion of Long-Term Debt Payable', 'Liabilities'),
(2250, 'Income Tax Payable', 'Liabilities'),
(2300, 'GST Payable', 'Liabilities'),
(2310, 'GST Recoverable', 'Liabilities'),
(2320, 'PST Payable', 'Liabilities'),
(2330, 'PST Recoverable (commission)', 'Liabilities'),
(2340, 'Payroll Tax Payable', 'Liabilities'),
(2350, 'Withholding Income Tax Payable', 'Liabilities'),
(2360, 'Other Taxes Payable', 'Liabilities'),
(2400, 'Employee Salaries Payable', 'Liabilities'),
(2410, 'Management Salaries Payable', 'Liabilities'),
(2420, 'Director / Partner Fees Payable', 'Liabilities'),
(2450, 'Health Benefits Payable', 'Liabilities'),
(2460, 'Pension Benefits Payable', 'Liabilities'),
(2470, 'Canada Pension Plan Payable', 'Liabilities'),
(2480, 'Employment Insurance Premiums Payable', 'Liabilities'),
(2500, 'Land Payable', 'Liabilities'),
(2550, 'Long-Term Bank Loan', 'Liabilities'),
(2560, 'Notes Payable', 'Liabilities'),
(2600, 'Building & Equipment Payable', 'Liabilities'),
(2700, 'Furnishing & Fixture Payable', 'Liabilities'),
(2720, 'Office Equipment Payable', 'Liabilities'),
(2740, 'Vehicle Payable', 'Liabilities'),
(2760, 'Other Property Payable', 'Liabilities'),
(2800, 'Shareholder Loans', 'Liabilities'),
(2900, 'Suspense', 'Liabilities'),
(3100, 'Capital Stock', 'Equity'),
(3200, 'Capital Surplus / Dividends', 'Equity'),
(3300, 'Dividend Taxes Payable', 'Equity'),
(3400, 'Dividend Taxes Refundable', 'Equity'),
(3500, 'Retained Earnings', 'Equity'),
(4100, 'Product / Service Sales', 'Revenue'),
(4200, 'Sales Exchange Gains/Losses', 'Revenue'),
(4500, 'Consulting Services', 'Revenue'),
(4600, 'Rentals', 'Revenue'),
(4700, 'Finance Charge Income', 'Revenue'),
(4800, 'Sales Returns & Allowances', 'Revenue'),
(4900, 'Sales Discounts', 'Revenue'),
(5000, 'Cost of Sales', 'Cost of Goods Sold'),
(5100, 'Production Expenses', 'Cost of Goods Sold'),
(5200, 'Purchases Exchange Gains/Losses', 'Cost of Goods Sold'),
(5500, 'Direct Labour Costs', 'Cost of Goods Sold'),
(5600, 'Freight Charges', 'Cost of Goods Sold'),
(5700, 'Inventory Adjustment', 'Cost of Goods Sold'),
(5800, 'Purchase Returns & Allowances', 'Cost of Goods Sold'),
(5900, 'Purchase Discounts', 'Cost of Goods Sold'),
(6100, 'Advertising', 'Marketing Expenses'),
(6150, 'Promotion', 'Marketing Expenses'),
(6200, 'Communications', 'Marketing Expenses'),
(6250, 'Meeting Expenses', 'Marketing Expenses'),
(6300, 'Travelling Expenses', 'Marketing Expenses'),
(6400, 'Delivery Expenses', 'Marketing Expenses'),
(6500, 'Sales Salaries & Commission', 'Marketing Expenses'),
(6550, 'Sales Salaries & Commission Deductions', 'Marketing Expenses'),
(6590, 'Benefits', 'Marketing Expenses'),
(6600, 'Other Selling Expenses', 'Marketing Expenses'),
(6700, 'Permits, Licenses & License Fees', 'Marketing Expenses'),
(6800, 'Research & Development', 'Marketing Expenses'),
(6900, 'Professional Services', 'Marketing Expenses'),
(7020, 'Support Salaries & Wages', 'Operating Expenses'),
(7030, 'Support Salary & Wage Deductions', 'Operating Expenses'),
(7040, 'Management Salaries', 'Operating Expenses'),
(7050, 'Management Salary deductions', 'Operating Expenses'),
(7060, 'Director / Partner Fees', 'Operating Expenses'),
(7070, 'Director / Partner Deductions', 'Operating Expenses'),
(7080, 'Payroll Tax', 'Operating Expenses'),
(7090, 'Benefits', 'Operating Expenses'),
(7100, 'Training & Education Expenses', 'Operating Expenses'),
(7150, 'Dues & Subscriptions', 'Operating Expenses'),
(7200, 'Accounting Fees', 'Operating Expenses'),
(7210, 'Audit Fees', 'Operating Expenses'),
(7220, 'Banking Fees', 'Operating Expenses'),
(7230, 'Credit Card Fees', 'Operating Expenses'),
(7240, 'Consulting Fees', 'Operating Expenses'),
(7260, 'Legal Fees', 'Operating Expenses'),
(7280, 'Other Professional Fees', 'Operating Expenses'),
(7300, 'Business Tax', 'Operating Expenses'),
(7350, 'Property Tax', 'Operating Expenses'),
(7390, 'Corporation Capital Tax', 'Operating Expenses'),
(7400, 'Office Rent', 'Operating Expenses'),
(7450, 'Equipment Rental', 'Operating Expenses'),
(7500, 'Office Supplies', 'Operating Expenses'),
(7550, 'Office Repair & Maintenance', 'Operating Expenses'),
(7600, 'Automotive Expenses', 'Operating Expenses'),
(7610, 'Communication Expenses', 'Operating Expenses'),
(7620, 'Insurance Expenses', 'Operating Expenses'),
(7630, 'Postage & Courier Expenses', 'Operating Expenses'),
(7640, 'Miscellaneous Expenses', 'Operating Expenses'),
(7650, 'Travel Expenses', 'Operating Expenses'),
(7660, 'Utilities', 'Operating Expenses'),
(7700, 'Ammortization Expenses', 'Operating Expenses'),
(7750, 'Depreciation Expenses', 'Operating Expenses'),
(7800, 'Interest Expense', 'Operating Expenses'),
(7900, 'Bad Debt Expense', 'Operating Expenses'),
(8100, 'Gain on Sale of Assets', 'Other Revenue and Expenses'),
(8200, 'Interest Income', 'Other Revenue and Expenses'),
(8300, 'Recovery on Bad Debt', 'Other Revenue and Expenses'),
(8400, 'Other Revenue', 'Other Revenue and Expenses'),
(8500, 'Loss on Sale of Assets', 'Other Revenue and Expenses'),
(8600, 'Charitable Contributions', 'Other Revenue and Expenses'),
(8900, 'Other Expenses', 'Other Revenue and Expenses'),
(9100, 'Income Tax Provision', 'Income Tax');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `coycode` int(11) NOT NULL DEFAULT '1',
  `coyname` varchar(50) NOT NULL DEFAULT '',
  `gstno` varchar(20) NOT NULL DEFAULT '',
  `companynumber` varchar(20) NOT NULL DEFAULT '0',
  `regoffice1` varchar(40) NOT NULL DEFAULT '',
  `regoffice2` varchar(40) NOT NULL DEFAULT '',
  `regoffice3` varchar(40) NOT NULL DEFAULT '',
  `regoffice4` varchar(40) NOT NULL DEFAULT '',
  `regoffice5` varchar(20) NOT NULL DEFAULT '',
  `regoffice6` varchar(15) NOT NULL DEFAULT '',
  `telephone` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(25) NOT NULL DEFAULT '',
  `email` varchar(55) NOT NULL DEFAULT '',
  `currencydefault` varchar(4) NOT NULL DEFAULT '',
  `debtorsact` int(11) NOT NULL DEFAULT '70000',
  `pytdiscountact` int(11) NOT NULL DEFAULT '55000',
  `creditorsact` int(11) NOT NULL DEFAULT '80000',
  `payrollact` int(11) NOT NULL DEFAULT '84000',
  `grnact` int(11) NOT NULL DEFAULT '72000',
  `exchangediffact` int(11) NOT NULL DEFAULT '65000',
  `purchasesexchangediffact` int(11) NOT NULL DEFAULT '0',
  `retainedearnings` int(11) NOT NULL DEFAULT '90000',
  `gllink_debtors` tinyint(1) DEFAULT '1',
  `gllink_creditors` tinyint(1) DEFAULT '1',
  `gllink_stock` tinyint(1) DEFAULT '1',
  `freightact` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coycode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`coycode`, `coyname`, `gstno`, `companynumber`, `regoffice1`, `regoffice2`, `regoffice3`, `regoffice4`, `regoffice5`, `regoffice6`, `telephone`, `fax`, `email`, `currencydefault`, `debtorsact`, `pytdiscountact`, `creditorsact`, `payrollact`, `grnact`, `exchangediffact`, `purchasesexchangediffact`, `retainedearnings`, `gllink_debtors`, `gllink_creditors`, `gllink_stock`, `freightact`) VALUES
(1, 'Shogun Payroll System', '', '', '', '', '', '', '', '', '', '', '', 'Php', 1100, 4900, 2100, 2400, 2150, 4200, 5200, 3500, 1, 1, 1, 5600);

-- --------------------------------------------------------

--
-- Table structure for table `companyagency`
--

CREATE TABLE IF NOT EXISTS `companyagency` (
  `CompanyAgencyID` int(11) NOT NULL AUTO_INCREMENT,
  `companyagency` varchar(255) NOT NULL,
  PRIMARY KEY (`CompanyAgencyID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `companyagency`
--

INSERT INTO `companyagency` (`CompanyAgencyID`, `companyagency`) VALUES
(1, 'Hotel'),
(2, 'Motel');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `confname` varchar(35) NOT NULL DEFAULT '',
  `confvalue` text NOT NULL,
  PRIMARY KEY (`confname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`confname`, `confvalue`) VALUES
('AllowSalesOfZeroCostItems', '0'),
('AutoDebtorNo', '0'),
('CheckCreditLimits', '0'),
('Check_Price_Charged_vs_Order_Price', '1'),
('Check_Qty_Charged_vs_Del_Qty', '1'),
('CountryOfOperation', 'USD'),
('CreditingControlledItems_MustExist', '0'),
('DB_Maintenance', '1'),
('DB_Maintenance_LastRun', '2016-01-25'),
('DefaultBlindPackNote', '1'),
('DefaultCreditLimit', '1000'),
('DefaultDateFormat', 'm/d/Y'),
('DefaultDisplayRecordsMax', '50'),
('DefaultPriceList', 'WS'),
('DefaultTaxCategory', '1'),
('DefaultTheme', 'fresh'),
('Default_Shipper', '1'),
('DispatchCutOffTime', '14'),
('DoFreightCalc', '0'),
('EDIHeaderMsgId', 'D:01B:UN:EAN010'),
('EDIReference', 'WEBERP'),
('EDI_Incoming_Orders', 'companies/weberp/EDI_Incoming_Orders'),
('EDI_MsgPending', 'companies/weberp/EDI_MsgPending'),
('EDI_MsgSent', 'companies/weberp/EDI_Sent'),
('FreightChargeAppliesIfLessThan', '1000'),
('FreightTaxCategory', '1'),
('HTTPS_Only', '0'),
('MaxImageSize', '300'),
('NumberOfPeriodsOfStockUsage', '12'),
('OverChargeProportion', '30'),
('OverReceiveProportion', '20'),
('PackNoteFormat', '1'),
('PageLength', '48'),
('part_pics_dir', 'companies/weberp/part_pics'),
('PastDueDays1', '30'),
('PastDueDays2', '60'),
('PO_AllowSameItemMultipleTimes', '1'),
('QuickEntries', '10'),
('RadioBeaconFileCounter', '/home/RadioBeacon/FileCounter'),
('RadioBeaconFTP_user_name', 'RadioBeacon ftp server user name'),
('RadioBeaconHomeDir', '/home/RadioBeacon'),
('RadioBeaconStockLocation', 'BL'),
('RadioBraconFTP_server', '192.168.2.2'),
('RadioBreaconFilePrefix', 'ORDXX'),
('RadionBeaconFTP_user_pass', 'Radio Beacon remote ftp server password'),
('reports_dir', 'companies/weberp/reports'),
('RomalpaClause', 'Ownership will not pass to the buyer until the goods have been paid for in full.'),
('Show_Settled_LastMonth', '1'),
('SO_AllowSameItemMultipleTimes', '1'),
('TaxAuthorityReferenceName', 'Tax Ref'),
('YearEnd', '3');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE IF NOT EXISTS `currencies` (
  `currency` char(20) NOT NULL DEFAULT '',
  `currabrev` char(3) NOT NULL DEFAULT '',
  `country` char(50) NOT NULL DEFAULT '',
  `hundredsname` char(15) NOT NULL DEFAULT 'Cents',
  `rate` double NOT NULL DEFAULT '1',
  PRIMARY KEY (`currabrev`),
  KEY `Country` (`country`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`currency`, `currabrev`, `country`, `hundredsname`, `rate`) VALUES
('Australian Dollars', 'AUD', 'Australia', 'cents', 1),
('Canandian Dollars', 'CND', 'Canada', 'cents', 1),
('Pounds', 'GBP', 'England', 'Pence', 1),
('Philippine Peso', 'Php', 'Philippines', 'cents', 1),
('US Dollars', 'USD', 'United States', 'Cents', 1);

-- --------------------------------------------------------

--
-- Table structure for table `prldailytrans`
--

CREATE TABLE IF NOT EXISTS `prldailytrans` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `rtref` varchar(11) NOT NULL DEFAULT '',
  `rtdesc` varchar(40) NOT NULL DEFAULT '',
  `rtdate` date NOT NULL DEFAULT '0000-00-00',
  `payrollid` varchar(10) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `reghrs` decimal(12,2) NOT NULL DEFAULT '0.00',
  `absenthrs` decimal(12,2) NOT NULL DEFAULT '0.00',
  `latehrs` decimal(12,2) NOT NULL DEFAULT '0.00',
  `regamt` decimal(12,2) NOT NULL DEFAULT '0.00',
  `absentamt` decimal(12,2) NOT NULL DEFAULT '0.00',
  `lateamt` decimal(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`counterindex`),
  KEY `RTDate` (`rtdate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `prldepartment`
--

CREATE TABLE IF NOT EXISTS `prldepartment` (
  `departmentid` int(11) NOT NULL AUTO_INCREMENT,
  `departmentName` varchar(255) NOT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`departmentid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `prldepartment`
--

INSERT INTO `prldepartment` (`departmentid`, `departmentName`, `active`) VALUES
(1, 'Finance', b'1'),
(2, 'Administrator', b'1'),
(4, 'Restaurant', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `prlemphdmffile`
--

CREATE TABLE IF NOT EXISTS `prlemphdmffile` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `grosspay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerhdmf` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employeehdmf` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fsmonth` tinyint(4) NOT NULL DEFAULT '0',
  `fsyear` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `prlemphdmffile`
--

INSERT INTO `prlemphdmffile` (`counterindex`, `payrollid`, `employeeid`, `grosspay`, `employerhdmf`, `employeehdmf`, `total`, `fsmonth`, `fsyear`) VALUES
(1, '6', '21', 0.00, 0.00, 0.00, 0.00, 3, 2014),
(2, '6', '22', 0.00, 0.00, 0.00, 0.00, 3, 2014),
(5, '4', '21', 0.00, 0.00, 0.00, 0.00, 2, 2014),
(6, '4', '22', 0.00, 0.00, 0.00, 0.00, 2, 2014),
(9, '7', '21', 0.00, 0.00, 0.00, 0.00, 4, 2014),
(10, '7', '22', 0.00, 0.00, 0.00, 0.00, 4, 2014),
(11, '9', '21', 0.00, 0.00, 0.00, 0.00, 4, 2014),
(12, '9', '22', 0.00, 0.00, 0.00, 0.00, 4, 2014);

-- --------------------------------------------------------

--
-- Table structure for table `prlemployeemaster`
--

CREATE TABLE IF NOT EXISTS `prlemployeemaster` (
  `employeeid` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(40) NOT NULL DEFAULT '',
  `firstname` varchar(40) NOT NULL DEFAULT '',
  `middlename` varchar(40) NOT NULL DEFAULT '',
  `address1` varchar(100) NOT NULL DEFAULT '',
  `address2` varchar(100) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `state` varchar(20) NOT NULL DEFAULT '',
  `zip` varchar(15) NOT NULL DEFAULT '',
  `country` varchar(40) NOT NULL DEFAULT '',
  `gender` varchar(15) NOT NULL DEFAULT '',
  `phone1` varchar(20) NOT NULL DEFAULT '',
  `phone1comment` varchar(20) NOT NULL DEFAULT '',
  `phone2` varchar(20) NOT NULL DEFAULT '',
  `phone2comment` varchar(20) NOT NULL DEFAULT '',
  `email1` varchar(50) NOT NULL DEFAULT '',
  `email1comment` varchar(20) NOT NULL DEFAULT '',
  `email2` varchar(50) NOT NULL DEFAULT '',
  `email2comment` varchar(20) NOT NULL DEFAULT '',
  `atmnumber` varchar(20) NOT NULL DEFAULT '',
  `ssnumber` varchar(20) NOT NULL DEFAULT '',
  `hdmfnumber` varchar(20) NOT NULL DEFAULT '',
  `phnumber` varchar(15) NOT NULL DEFAULT '',
  `taxactnumber` varchar(15) NOT NULL DEFAULT '',
  `birthdate` date NOT NULL DEFAULT '0000-00-00',
  `hiredate` date NOT NULL DEFAULT '0000-00-00',
  `terminatedate` date NOT NULL DEFAULT '0000-00-00',
  `retireddate` date NOT NULL DEFAULT '0000-00-00',
  `paytype` tinyint(4) NOT NULL DEFAULT '0',
  `payperiodid` tinyint(4) NOT NULL DEFAULT '0',
  `periodrate` decimal(12,2) NOT NULL DEFAULT '0.00',
  `hourlyrate` decimal(12,2) NOT NULL DEFAULT '0.00',
  `glactcode` int(11) NOT NULL DEFAULT '0',
  `marital` varchar(20) NOT NULL DEFAULT '',
  `taxstatusid` varchar(10) DEFAULT '',
  `employmentid` tinyint(4) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `costcenterid` varchar(10) NOT NULL DEFAULT '',
  `companyagencyid` int(11) NOT NULL DEFAULT '0',
  `position` varchar(40) NOT NULL DEFAULT '',
  `departmentid` int(11) NOT NULL,
  `TimeIn` varchar(255) NOT NULL,
  `TimeOut` varchar(255) NOT NULL,
  PRIMARY KEY (`employeeid`),
  KEY `EmployeeName` (`lastname`,`firstname`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `prlemployeemaster`
--

INSERT INTO `prlemployeemaster` (`employeeid`, `lastname`, `firstname`, `middlename`, `address1`, `address2`, `city`, `state`, `zip`, `country`, `gender`, `phone1`, `phone1comment`, `phone2`, `phone2comment`, `email1`, `email1comment`, `email2`, `email2comment`, `atmnumber`, `ssnumber`, `hdmfnumber`, `phnumber`, `taxactnumber`, `birthdate`, `hiredate`, `terminatedate`, `retireddate`, `paytype`, `payperiodid`, `periodrate`, `hourlyrate`, `glactcode`, `marital`, `taxstatusid`, `employmentid`, `active`, `deleted`, `costcenterid`, `companyagencyid`, `position`, `departmentid`, `TimeIn`, `TimeOut`) VALUES
(1, 'Sabino', 'Fidel', 'C', '77 Pag asa St. Caniogan', '', 'Pasig', '', '1600', 'Philippines', 'M', '', '', '', '', '', '', '', '', '1083675553', '6366336', '66346436', '14585686', '13557436565', '1972-09-09', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 16900.00, 162.50, 0, 'Married', 'HF', 1, 1, 0, 'KCN', 1, 'Chef', 4, '07:00 AM', '03:00 PM');

-- --------------------------------------------------------

--
-- Table structure for table `prlemploymentstatus`
--

CREATE TABLE IF NOT EXISTS `prlemploymentstatus` (
  `employmentid` int(11) NOT NULL AUTO_INCREMENT,
  `employmentdesc` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`employmentid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `prlemploymentstatus`
--

INSERT INTO `prlemploymentstatus` (`employmentid`, `employmentdesc`) VALUES
(1, 'Regular'),
(2, 'Probationary'),
(3, 'Contractual');

-- --------------------------------------------------------

--
-- Table structure for table `prlempphfile`
--

CREATE TABLE IF NOT EXISTS `prlempphfile` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `grosspay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangefrom` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangeto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `salarycredit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerph` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerec` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employeeph` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fsmonth` tinyint(4) NOT NULL DEFAULT '0',
  `fsyear` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `prlempphfile`
--

INSERT INTO `prlempphfile` (`counterindex`, `payrollid`, `employeeid`, `grosspay`, `rangefrom`, `rangeto`, `salarycredit`, `employerph`, `employerec`, `employeeph`, `total`, `fsmonth`, `fsyear`) VALUES
(1, '6', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3, 2014),
(2, '6', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3, 2014),
(5, '4', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2, 2014),
(6, '4', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2, 2014),
(9, '7', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(10, '7', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(11, '9', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(12, '9', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014);

-- --------------------------------------------------------

--
-- Table structure for table `prlempsssfile`
--

CREATE TABLE IF NOT EXISTS `prlempsssfile` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `grosspay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangefrom` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangeto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `salarycredit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerss` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerec` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employeess` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fsmonth` tinyint(4) NOT NULL DEFAULT '0',
  `fsyear` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `prlempsssfile`
--

INSERT INTO `prlempsssfile` (`counterindex`, `payrollid`, `employeeid`, `grosspay`, `rangefrom`, `rangeto`, `salarycredit`, `employerss`, `employerec`, `employeess`, `total`, `fsmonth`, `fsyear`) VALUES
(1, '6', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3, 2014),
(2, '6', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3, 2014),
(5, '4', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2, 2014),
(6, '4', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2, 2014),
(9, '7', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(10, '7', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(11, '9', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(12, '9', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014);

-- --------------------------------------------------------

--
-- Table structure for table `prlemptaxfile`
--

CREATE TABLE IF NOT EXISTS `prlemptaxfile` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `taxableincome` decimal(12,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fsmonth` tinyint(4) NOT NULL DEFAULT '0',
  `fsyear` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `prlhdmftable`
--

CREATE TABLE IF NOT EXISTS `prlhdmftable` (
  `bracket` int(11) NOT NULL AUTO_INCREMENT,
  `rangefrom` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangeto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `dedtypeer` varchar(10) NOT NULL DEFAULT '',
  `employershare` decimal(12,2) NOT NULL DEFAULT '0.00',
  `dedtypeee` varchar(10) NOT NULL DEFAULT '',
  `employeeshare` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total` decimal(12,2) NOT NULL,
  PRIMARY KEY (`bracket`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `prlhdmftable`
--

INSERT INTO `prlhdmftable` (`bracket`, `rangefrom`, `rangeto`, `dedtypeer`, `employershare`, `dedtypeee`, `employeeshare`, `total`) VALUES
(1, 1.00, 1500.00, 'Percentage', 100.00, 'Percentage', 100.00, 200.00),
(2, 1500.01, 999999.00, 'Percentage', 100.00, 'Percentage', 100.00, 200.00),
(3, 0.00, 0.00, 'Fixed', 0.00, 'Fixed', 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `prlholidaytable`
--

CREATE TABLE IF NOT EXISTS `prlholidaytable` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `holidaydate` varchar(255) NOT NULL,
  `holidaydesc` varchar(255) NOT NULL,
  `holidayrate` decimal(6,2) NOT NULL,
  `holidayshortdesc` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `prlholidaytable`
--

INSERT INTO `prlholidaytable` (`id`, `holidaydate`, `holidaydesc`, `holidayrate`, `holidayshortdesc`) VALUES
(4, '2014-06-12', 'Independence Day', 1.00, 'LH'),
(6, 'July 29,2014', 'EIDUL FITR', 1.00, 'LH');

-- --------------------------------------------------------

--
-- Table structure for table `prlhol_trans`
--

CREATE TABLE IF NOT EXISTS `prlhol_trans` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `hol_desc` varchar(255) NOT NULL,
  `hol_date` varchar(255) NOT NULL,
  `emp_id` varchar(255) NOT NULL,
  `duty_hours` decimal(12,2) NOT NULL,
  `hol_type_no` varchar(255) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `prlloandeduct`
--

CREATE TABLE IF NOT EXISTS `prlloandeduct` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `loan_file_id` int(255) NOT NULL,
  `loan_type` int(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `loan_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `amortization` decimal(12,2) NOT NULL DEFAULT '0.00',
  `balance` decimal(12,2) NOT NULL DEFAULT '0.00',
  `skip_deduct` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `prlloandeduct`
--

INSERT INTO `prlloandeduct` (`id`, `loan_file_id`, `loan_type`, `date`, `loan_amount`, `amortization`, `balance`, `skip_deduct`) VALUES
(7, 45, 3, '2015-09-10', 1600.00, 400.00, 1200.00, 0),
(8, 45, 3, '2015-09-24', 1600.00, 400.00, 800.00, 0),
(9, 45, 3, '2015-10-08', 1600.00, 400.00, 400.00, 0),
(10, 45, 3, '2015-10-22', 1600.00, 400.00, 0.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `prlloandeduction`
--

CREATE TABLE IF NOT EXISTS `prlloandeduction` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `loantableid` tinyint(4) NOT NULL DEFAULT '0',
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `prlloanfile`
--

CREATE TABLE IF NOT EXISTS `prlloanfile` (
  `loanfileid` int(11) NOT NULL AUTO_INCREMENT,
  `loanfiledesc` varchar(40) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `loandate` date NOT NULL DEFAULT '0000-00-00',
  `loantableid` tinyint(4) NOT NULL DEFAULT '0',
  `loanamount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `amortization` decimal(12,2) NOT NULL DEFAULT '0.00',
  `no_of_payments` int(11) NOT NULL,
  `startdeduction` date NOT NULL DEFAULT '0000-00-00',
  `ytddeduction` decimal(12,2) NOT NULL DEFAULT '0.00',
  `loanbalance` decimal(12,2) NOT NULL DEFAULT '0.00',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`loanfileid`),
  KEY `LoanDate` (`loandate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `prlloanfile`
--

INSERT INTO `prlloanfile` (`loanfileid`, `loanfiledesc`, `employeeid`, `loandate`, `loantableid`, `loanamount`, `amortization`, `no_of_payments`, `startdeduction`, `ytddeduction`, `loanbalance`, `accountcode`) VALUES
(45, 'bong loan', '1', '2015-09-01', 3, 1600.00, 400.00, 4, '2015-09-10', 0.00, 1600.00, 1350);

-- --------------------------------------------------------

--
-- Table structure for table `prlloantable`
--

CREATE TABLE IF NOT EXISTS `prlloantable` (
  `loantableid` int(11) NOT NULL AUTO_INCREMENT,
  `loantabledesc` varchar(25) NOT NULL DEFAULT '',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`loantableid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `prlloantable`
--

INSERT INTO `prlloantable` (`loantableid`, `loantabledesc`, `accountcode`) VALUES
(1, 'Salary Loan', 0),
(2, 'Cal. Loan', 0),
(3, 'Pag Ibig Loan', 0),
(4, 'Pag Ibig Cal Loan', 0),
(8, 'E-loan1', 0),
(9, 'VALE', 0),
(10, 'Pabahay loan', 0),
(11, 'T-SHIRT', 0),
(12, 'Eloan2', 0),
(13, 'Eloan3', 0),
(14, 'short cashier', 0),
(15, 'Charge', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prlothincfile`
--

CREATE TABLE IF NOT EXISTS `prlothincfile` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `othfileref` varchar(10) NOT NULL DEFAULT '',
  `othfiledesc` varchar(40) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `othdate` date NOT NULL DEFAULT '0000-00-00',
  `othincid` tinyint(4) NOT NULL DEFAULT '0',
  `othincamount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`),
  KEY `OthDate` (`othdate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `prlothincfile`
--

INSERT INTO `prlothincfile` (`counterindex`, `othfileref`, `othfiledesc`, `employeeid`, `othdate`, `othincid`, `othincamount`, `accountcode`) VALUES
(2, '', '', '1', '2015-09-01', 8, 500.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `prlothinctable`
--

CREATE TABLE IF NOT EXISTS `prlothinctable` (
  `othincid` int(11) NOT NULL AUTO_INCREMENT,
  `othincdesc` varchar(25) NOT NULL DEFAULT '',
  `taxable` varchar(10) NOT NULL DEFAULT '',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`othincid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `prlothinctable`
--

INSERT INTO `prlothinctable` (`othincid`, `othincdesc`, `taxable`, `accountcode`) VALUES
(5, 'Promo Driver', 'Non-Tax', 0),
(7, 'TRANSPORTATION', 'Non-Tax', 0),
(8, 'REFUND', 'Non-Tax', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prlottrans`
--

CREATE TABLE IF NOT EXISTS `prlottrans` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) DEFAULT '',
  `otref` varchar(11) NOT NULL DEFAULT '',
  `otdesc` varchar(40) NOT NULL DEFAULT '',
  `otdate` date NOT NULL DEFAULT '0000-00-00',
  `overtimeid` tinyint(4) NOT NULL DEFAULT '0',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `othours` double NOT NULL DEFAULT '0',
  `joborder` varchar(10) NOT NULL DEFAULT '',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  `otamount` int(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`),
  KEY `Account` (`accountcode`),
  KEY `OTDate` (`otdate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `prlottrans`
--

INSERT INTO `prlottrans` (`counterindex`, `payrollid`, `otref`, `otdesc`, `otdate`, `overtimeid`, `employeeid`, `othours`, `joborder`, `accountcode`, `otamount`) VALUES
(1, '', '', 'OT', '2015-09-01', 10, '1', 4, '', 0, 625),
(3, '', '', 'sunday', '2015-09-06', 30, '1', 8, '', 0, 2600);

-- --------------------------------------------------------

--
-- Table structure for table `prlovertimetable`
--

CREATE TABLE IF NOT EXISTS `prlovertimetable` (
  `overtimeid` tinyint(4) NOT NULL DEFAULT '0',
  `overtimedesc` varchar(40) NOT NULL DEFAULT '',
  `overtimerate` decimal(6,2) NOT NULL DEFAULT '0.00',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`overtimeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prlovertimetable`
--

INSERT INTO `prlovertimetable` (`overtimeid`, `overtimedesc`, `overtimerate`, `accountcode`) VALUES
(10, 'Regular Day OT Work', 1.25, 1),
(15, 'Night Shift Pay ', 0.10, 1),
(20, 'Restday or Special Day OT Work', 1.30, 1),
(25, 'Restday or Special Day OT Work >8 hrs', 1.69, 1),
(30, 'Regular Holiday OT Work', 2.00, 1),
(35, 'Regular Holiday OT Work >8 hrs', 2.60, 1),
(40, 'Restday and Regular Holiday OT Work', 2.60, 1),
(45, 'Restday and Regular Holiday OT Work >8hr', 3.38, 1),
(50, 'Holiday sample', 1.00, 0),
(55, 'Legal holiday', 1.00, 0),
(60, 'Special Holiday', 0.30, 0);

-- --------------------------------------------------------

--
-- Table structure for table `prlpayperiod`
--

CREATE TABLE IF NOT EXISTS `prlpayperiod` (
  `payperiodid` tinyint(4) NOT NULL DEFAULT '0',
  `payperioddesc` varchar(15) NOT NULL DEFAULT '',
  `numberofpayday` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`payperiodid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prlpayperiod`
--

INSERT INTO `prlpayperiod` (`payperiodid`, `payperioddesc`, `numberofpayday`, `active`) VALUES
(1, 'Semi-Monthly', 24, 1),
(2, 'Monthly', 12, 1),
(3, 'Weekly', 52, 1),
(4, 'Daily', 312, 1);

-- --------------------------------------------------------

--
-- Table structure for table `prlpayrollperiod`
--

CREATE TABLE IF NOT EXISTS `prlpayrollperiod` (
  `payrollid` int(11) NOT NULL AUTO_INCREMENT,
  `payrolldesc` varchar(40) NOT NULL DEFAULT '',
  `payperiodid` tinyint(4) NOT NULL DEFAULT '0',
  `startdate` date NOT NULL DEFAULT '0000-00-00',
  `enddate` date NOT NULL DEFAULT '0000-00-00',
  `fsmonth` tinyint(4) NOT NULL DEFAULT '0',
  `fsyear` double NOT NULL DEFAULT '0',
  `deductsss` tinyint(4) NOT NULL DEFAULT '0',
  `deducthdmf` tinyint(4) NOT NULL DEFAULT '0',
  `deductphilhealth` tinyint(4) NOT NULL DEFAULT '0',
  `deducttax` tinyint(4) NOT NULL DEFAULT '0',
  `payclosed` tinyint(4) NOT NULL DEFAULT '0',
  `createdtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`payrollid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `prlpayrollperiod`
--

INSERT INTO `prlpayrollperiod` (`payrollid`, `payrolldesc`, `payperiodid`, `startdate`, `enddate`, `fsmonth`, `fsyear`, `deductsss`, `deducthdmf`, `deductphilhealth`, `deducttax`, `payclosed`, `createdtime`) VALUES
(1, 'Sept 1 - 15', 1, '2015-08-26', '2015-09-10', 9, 2014, 0, 0, 0, 0, 0, '2015-09-23 08:19:31');

-- --------------------------------------------------------

--
-- Table structure for table `prlpayrolltrans`
--

CREATE TABLE IF NOT EXISTS `prlpayrolltrans` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `reghrs` decimal(12,2) NOT NULL DEFAULT '0.00',
  `absenthrs` decimal(12,2) NOT NULL DEFAULT '0.00',
  `latehrs` decimal(12,2) NOT NULL DEFAULT '0.00',
  `periodrate` decimal(12,2) NOT NULL DEFAULT '0.00',
  `hourlyrate` decimal(12,2) NOT NULL DEFAULT '0.00',
  `basicpay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `othincome` decimal(12,2) NOT NULL DEFAULT '0.00',
  `absent` decimal(12,2) NOT NULL DEFAULT '0.00',
  `late` decimal(12,2) NOT NULL DEFAULT '0.00',
  `otpay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `grosspay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `loandeduction` decimal(12,2) NOT NULL DEFAULT '0.00',
  `sss` decimal(12,2) NOT NULL DEFAULT '0.00',
  `hdmf` decimal(12,2) NOT NULL DEFAULT '0.00',
  `philhealth` decimal(12,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(12,2) NOT NULL DEFAULT '0.00',
  `otherdeduction` decimal(12,2) NOT NULL DEFAULT '0.00',
  `totaldeduction` decimal(12,2) NOT NULL DEFAULT '0.00',
  `netpay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fsmonth` tinyint(4) NOT NULL DEFAULT '0',
  `fsyear` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `prlphilhealth`
--

CREATE TABLE IF NOT EXISTS `prlphilhealth` (
  `bracket` tinyint(4) NOT NULL DEFAULT '0',
  `rangefrom` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangeto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `salarycredit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerph` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerec` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employeeph` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total` decimal(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`bracket`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prlphilhealth`
--

INSERT INTO `prlphilhealth` (`bracket`, `rangefrom`, `rangeto`, `salarycredit`, `employerph`, `employerec`, `employeeph`, `total`) VALUES
(1, 1.00, 8999.99, 8000.00, 100.00, 0.00, 100.00, 200.00),
(2, 9000.00, 9999.99, 9000.00, 112.50, 0.00, 112.50, 225.00),
(3, 10000.00, 10999.99, 10000.00, 125.00, 0.00, 125.00, 250.00),
(4, 11000.00, 11999.99, 11000.00, 137.50, 0.00, 137.50, 275.00),
(5, 12000.00, 12999.99, 12000.00, 150.00, 0.00, 150.00, 300.00),
(6, 13000.00, 13999.99, 13000.00, 162.50, 0.00, 162.50, 325.00),
(7, 14000.00, 14999.99, 14000.00, 175.00, 0.00, 175.00, 350.00),
(8, 15000.00, 15999.99, 15000.00, 187.50, 0.00, 187.50, 375.00),
(9, 16000.00, 16999.99, 16000.00, 200.00, 0.00, 200.00, 400.00),
(10, 17000.00, 17999.99, 17000.00, 212.50, 0.00, 212.50, 425.00),
(11, 18000.00, 18999.99, 18000.00, 225.00, 0.00, 225.00, 450.00),
(12, 19000.00, 19999.99, 19000.00, 237.50, 0.00, 237.50, 475.00),
(13, 20000.00, 20999.99, 20000.00, 250.00, 0.00, 250.00, 500.00),
(14, 21000.00, 21999.99, 21000.00, 262.50, 0.00, 262.50, 525.00),
(15, 22000.00, 22999.00, 22000.00, 275.00, 0.00, 275.00, 550.00),
(16, 23000.00, 23999.99, 23000.00, 287.50, 0.00, 287.50, 575.00),
(17, 24000.00, 24999.99, 24000.00, 300.00, 0.00, 300.00, 600.00),
(18, 25000.00, 25999.99, 25000.00, 312.50, 0.00, 312.50, 625.00),
(19, 26000.00, 26999.99, 26000.00, 325.00, 0.00, 325.00, 650.00),
(20, 27000.00, 27999.99, 27000.00, 337.50, 0.00, 337.50, 675.00),
(21, 28000.00, 28999.99, 28000.00, 350.00, 0.00, 350.00, 700.00),
(22, 29000.00, 29999.99, 29000.00, 362.50, 0.00, 362.50, 725.00),
(23, 30000.00, 30999.99, 30000.00, 375.00, 0.00, 375.00, 750.00),
(24, 31000.00, 31999.99, 31000.00, 387.50, 0.00, 387.50, 775.00),
(25, 32000.00, 32999.99, 32000.00, 400.00, 0.00, 400.00, 800.00),
(26, 33000.00, 33999.99, 33000.00, 412.50, 0.00, 412.50, 825.00),
(27, 34000.00, 34999.99, 34000.00, 425.00, 0.00, 425.00, 850.00),
(28, 35000.00, 9999999.99, 35000.00, 437.50, 0.00, 437.50, 875.00);

-- --------------------------------------------------------

--
-- Table structure for table `prlremarks`
--

CREATE TABLE IF NOT EXISTS `prlremarks` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `payrollid` int(255) NOT NULL,
  `employeeid` int(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `prlremarks`
--

INSERT INTO `prlremarks` (`id`, `payrollid`, `employeeid`, `firstname`, `lastname`, `remarks`) VALUES
(1, 1, 1, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `prlsstable`
--

CREATE TABLE IF NOT EXISTS `prlsstable` (
  `bracket` int(11) NOT NULL AUTO_INCREMENT,
  `rangefrom` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangeto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `salarycredit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerss` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerec` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employeess` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total` decimal(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`bracket`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `prlsstable`
--

INSERT INTO `prlsstable` (`bracket`, `rangefrom`, `rangeto`, `salarycredit`, `employerss`, `employerec`, `employeess`, `total`) VALUES
(1, 1000.00, 1249.99, 1000.00, 73.70, 10.00, 36.30, 110.00),
(2, 1250.00, 1749.99, 1500.00, 110.50, 10.00, 54.50, 165.00),
(3, 1750.00, 2249.99, 2000.00, 147.30, 10.00, 72.70, 220.00),
(4, 2250.00, 2749.99, 2500.00, 184.20, 10.00, 90.80, 275.00),
(5, 2750.00, 3249.99, 3000.00, 221.00, 10.00, 109.00, 330.00),
(6, 3250.00, 3749.99, 3500.00, 257.80, 10.00, 127.20, 385.00),
(7, 3750.00, 4249.99, 4000.00, 294.70, 10.00, 145.30, 440.00),
(8, 4250.00, 4749.99, 4500.00, 331.50, 10.00, 163.50, 495.00),
(9, 4750.00, 5249.99, 5000.00, 388.30, 10.00, 181.70, 550.00),
(10, 5250.00, 5749.99, 5500.00, 405.20, 10.00, 199.80, 605.00),
(11, 5750.00, 6249.99, 6000.00, 442.00, 10.00, 218.00, 660.00),
(12, 6250.00, 6749.99, 6500.00, 478.80, 10.00, 236.20, 715.00),
(13, 6750.00, 7249.99, 7000.00, 515.70, 10.00, 254.30, 770.00),
(14, 7250.00, 7749.99, 7500.00, 552.50, 10.00, 272.50, 825.00),
(15, 7750.00, 8249.99, 8000.00, 589.30, 10.00, 290.70, 880.00),
(16, 8250.00, 8749.99, 8500.00, 626.20, 10.00, 308.80, 935.00),
(17, 8750.00, 9249.99, 9000.00, 663.00, 10.00, 327.00, 990.00),
(18, 9250.00, 9749.99, 9500.00, 699.80, 10.00, 345.20, 1045.00),
(19, 9750.00, 10249.99, 10000.00, 736.70, 10.00, 363.30, 1100.00),
(20, 10250.00, 10749.99, 10500.00, 773.50, 10.00, 381.50, 1155.00),
(21, 10750.00, 11249.99, 11000.00, 810.30, 10.00, 399.70, 1210.00),
(22, 11250.00, 11749.99, 11500.00, 847.20, 10.00, 417.80, 1265.00),
(23, 11750.00, 12249.99, 12000.00, 884.00, 10.00, 436.00, 1320.00),
(24, 12250.00, 12749.99, 12500.00, 920.80, 10.00, 454.20, 1375.00),
(25, 12750.00, 13249.99, 13000.00, 957.70, 10.00, 472.30, 1430.00),
(26, 13250.00, 13749.99, 13500.00, 994.50, 10.00, 490.50, 1485.00),
(27, 13750.00, 14249.99, 14000.00, 1031.30, 10.00, 508.70, 1540.00),
(28, 14250.00, 14749.99, 14500.00, 1068.20, 10.00, 526.80, 1595.00),
(29, 14750.00, 15249.99, 15000.00, 1105.00, 30.00, 545.00, 1650.00),
(30, 15250.00, 15749.99, 15500.00, 1141.80, 30.00, 563.20, 1705.00),
(31, 15750.00, 9999999.99, 16000.00, 1178.70, 30.00, 581.30, 1760.00);

-- --------------------------------------------------------

--
-- Table structure for table `prltaxstatus`
--

CREATE TABLE IF NOT EXISTS `prltaxstatus` (
  `taxstatusid` varchar(10) NOT NULL DEFAULT '',
  `taxstatusdescription` varchar(40) NOT NULL DEFAULT '',
  `personalexemption` decimal(12,2) NOT NULL DEFAULT '0.00',
  `additionalexemption` decimal(12,2) NOT NULL DEFAULT '0.00',
  `totalexemption` decimal(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`taxstatusid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prltaxstatus`
--

INSERT INTO `prltaxstatus` (`taxstatusid`, `taxstatusdescription`, `personalexemption`, `additionalexemption`, `totalexemption`) VALUES
('HF', 'Head of the Family', 25000.00, 0.00, 25000.00),
('HF1', 'Head of the Family with 1 dependent', 25000.00, 8000.00, 33000.00),
('HF2', 'Head of the Family with 2 dependent', 25000.00, 16000.00, 41000.00),
('HF3', 'Head of the Family with 3 dependent', 25000.00, 24000.00, 49000.00),
('HF4', 'Head of the Family with 4 dependent', 25000.00, 32000.00, 57000.00),
('ME', 'Married', 32000.00, 0.00, 32000.00),
('ME1', 'Married with 1 dependent', 32000.00, 8000.00, 40000.00),
('ME2', 'Married with 2 dependent', 32000.00, 16000.00, 48000.00),
('ME3', 'Married with 3 dependent', 32000.00, 24000.00, 56000.00),
('ME4', 'Married with 4 dependent', 32000.00, 32000.00, 64000.00),
('S', 'Single', 20000.00, 0.00, 20000.00),
('Z', 'Zero Exemption', 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `prltaxtablerate`
--

CREATE TABLE IF NOT EXISTS `prltaxtablerate` (
  `bracket` int(11) NOT NULL AUTO_INCREMENT,
  `rangefrom` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangeto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fixtaxableamount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fixtax` decimal(12,2) NOT NULL DEFAULT '0.00',
  `percentofexcessamount` double NOT NULL DEFAULT '1',
  `total` decimal(12,2) NOT NULL,
  PRIMARY KEY (`bracket`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `prltaxtablerate`
--

INSERT INTO `prltaxtablerate` (`bracket`, `rangefrom`, `rangeto`, `fixtaxableamount`, `fixtax`, `percentofexcessamount`, `total`) VALUES
(1, 0.00, 9999.99, 0.00, 0.00, 5, 10.00),
(2, 10000.00, 29999.99, 10000.00, 500.00, 10, 2000.00),
(3, 30000.00, 69999.99, 10000.00, 1000.00, 10, 3000.00),
(4, 70000.00, 139999.99, 70000.00, 8500.00, 20, 0.00),
(5, 140000.00, 249999.99, 140000.00, 22500.00, 25, 0.00),
(6, 250000.00, 499999.99, 250000.00, 50000.00, 30, 0.00),
(7, 500000.00, 99999999.99, 500000.00, 125000.00, 32, 0.00),
(8, 100000000.00, 999999999.99, 750000.00, 250000.00, 34, 3000.00);

-- --------------------------------------------------------

--
-- Table structure for table `prltimeentry`
--

CREATE TABLE IF NOT EXISTS `prltimeentry` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) NOT NULL,
  `DAY` varchar(255) NOT NULL,
  `TIMEIN` varchar(255) NOT NULL,
  `TIMEOUT` varchar(255) NOT NULL,
  `LUNCHIN` varchar(255) NOT NULL,
  `LUNCHOUT` varchar(255) NOT NULL,
  `LATE` int(255) NOT NULL,
  `BREAKLATE` int(255) NOT NULL,
  `OT` int(255) NOT NULL,
  `UNDERTIME` varchar(255) NOT NULL,
  `employeeid` varchar(11) NOT NULL,
  `TAGS` varchar(255) NOT NULL,
  `ACTUAL_LATE` int(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `prltimeentry`
--

INSERT INTO `prltimeentry` (`ID`, `payrollid`, `DAY`, `TIMEIN`, `TIMEOUT`, `LUNCHIN`, `LUNCHOUT`, `LATE`, `BREAKLATE`, `OT`, `UNDERTIME`, `employeeid`, `TAGS`, `ACTUAL_LATE`) VALUES
(2, '1', '2015-08-26', '07:00 am', '03:00 pm', '01:00 pm', '12:00 pm', 0, 0, 0, '0', '1', 'RD', 0),
(3, '1', '2015-08-27', '07:00 am', '05:00 pm', '01:00 pm', '11:00 am', 0, 60, 0, '0', '1', 'RD', 0),
(4, '1', '2015-08-28', '07:00 am', '02:00 pm', '01:00 pm', '12:00 pm', 0, 0, 0, '60', '1', 'RD', 0),
(5, '1', '2015-08-29', '07:32 am', '02:00 pm', '01:21 pm', '12:00 pm', 120, 21, 0, '60', '1', 'RD', 32),
(6, '1', '2015-08-30', '', '', '', '', 0, 0, 0, '0', '1', 'Absent', 0),
(7, '1', '2015-08-31', '07:00 am', '03:00 pm', '01:00 pm', '12:00 pm', 0, 0, 0, '0', '1', 'LH', 0),
(8, '1', '2015-09-01', '', '', '', '', 0, 0, 0, '0', '1', '', 0),
(9, '1', '2015-09-02', '', '', '', '', 0, 0, 0, '0', '1', '', 0),
(10, '1', '2015-09-03', '', '', '', '', 0, 0, 0, '0', '1', '', 0),
(11, '1', '2015-09-04', '', '', '', '', 0, 0, 0, '0', '1', '', 0),
(12, '1', '2015-09-05', '', '', '', '', 0, 0, 0, '0', '1', '', 0),
(13, '1', '2015-09-06', '', '', '', '', 0, 0, 0, '0', '1', '', 0),
(14, '1', '2015-09-07', '', '', '', '', 0, 0, 0, '0', '1', '', 0),
(15, '1', '2015-09-08', '', '', '', '', 0, 0, 0, '0', '1', '', 0),
(16, '1', '2015-09-09', '', '', '', '', 0, 0, 0, '0', '1', '', 0),
(17, '1', '2015-09-10', '', '', '', '', 0, 0, 0, '0', '1', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sched_table`
--

CREATE TABLE IF NOT EXISTS `sched_table` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `DATE` varchar(255) NOT NULL,
  `EMP_ID` varchar(255) NOT NULL,
  `TIMEIN` varchar(255) NOT NULL,
  `LUNCHOUT` varchar(255) NOT NULL,
  `LUNCHIN` varchar(255) NOT NULL,
  `TIMEOUT` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `securitygroups`
--

CREATE TABLE IF NOT EXISTS `securitygroups` (
  `secroleid` int(11) NOT NULL DEFAULT '0',
  `tokenid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`secroleid`,`tokenid`),
  KEY `secroleid` (`secroleid`),
  KEY `tokenid` (`tokenid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `securitygroups`
--

INSERT INTO `securitygroups` (`secroleid`, `tokenid`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(2, 11),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 11),
(4, 1),
(4, 2),
(4, 5),
(5, 1),
(5, 2),
(5, 3),
(5, 11),
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(6, 6),
(6, 7),
(6, 8),
(6, 9),
(6, 10),
(6, 11),
(7, 1),
(8, 1),
(8, 2),
(8, 3),
(8, 4),
(8, 5),
(8, 6),
(8, 7),
(8, 8),
(8, 9),
(8, 10),
(8, 11),
(8, 12),
(8, 13),
(8, 14),
(8, 15);

-- --------------------------------------------------------

--
-- Table structure for table `securityroles`
--

CREATE TABLE IF NOT EXISTS `securityroles` (
  `secroleid` int(11) NOT NULL AUTO_INCREMENT,
  `secrolename` text NOT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`secroleid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `securityroles`
--

INSERT INTO `securityroles` (`secroleid`, `secrolename`, `active`) VALUES
(1, 'Inquiries/Order Entry', b'0'),
(2, 'Manufac/Stock Admin', b'0'),
(3, 'Purchasing Officer', b'0'),
(4, 'AP Clerk', b'0'),
(5, 'AR Clerk', b'0'),
(6, 'Accountant', b'0'),
(7, 'Customer Log On Only', b'0'),
(8, 'System Administrator', b'1'),
(9, 'Human Resources', b'1'),
(10, 'Payroll', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `securitytokens`
--

CREATE TABLE IF NOT EXISTS `securitytokens` (
  `tokenid` int(11) NOT NULL DEFAULT '0',
  `tokenname` text NOT NULL,
  PRIMARY KEY (`tokenid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `securitytokens`
--

INSERT INTO `securitytokens` (`tokenid`, `tokenname`) VALUES
(1, 'Order Entry/Inquiries customer access only'),
(2, 'Basic Reports and Inquiries with selection options'),
(3, 'Credit notes and AR management'),
(4, 'Purchasing data/PO Entry/Reorder Levels'),
(5, 'Accounts Payable'),
(6, 'Not Used'),
(7, 'Bank Reconciliations'),
(8, 'General ledger reports/inquiries'),
(9, 'Not Used'),
(10, 'General Ledger Maintenance, stock valuation & Configuration'),
(11, 'Inventory Management and Pricing'),
(12, 'Unknown'),
(13, 'Unknown'),
(14, 'Unknown'),
(15, 'User Management and System Administration');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE IF NOT EXISTS `uploads` (
  `image` varchar(255) NOT NULL DEFAULT 'default.png',
  `empid` varchar(10) NOT NULL DEFAULT '0',
  UNIQUE KEY `empid` (`empid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`image`, `empid`) VALUES
('bong.png', '1'),
('SpiderMan.jpg', '15'),
('17709.jpg', '16'),
('17894.bmp', '20'),
('Winter.jpg', '22');

-- --------------------------------------------------------

--
-- Table structure for table `workcentres`
--

CREATE TABLE IF NOT EXISTS `workcentres` (
  `code` char(5) NOT NULL DEFAULT '',
  `location` char(5) NOT NULL DEFAULT '',
  `description` char(20) NOT NULL DEFAULT '',
  `capacity` double NOT NULL DEFAULT '1',
  `overheadperhour` decimal(10,0) NOT NULL DEFAULT '0',
  `overheadrecoveryact` int(11) NOT NULL DEFAULT '0',
  `setuphrs` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workcentres`
--

INSERT INTO `workcentres` (`code`, `location`, `description`, `capacity`, `overheadperhour`, `overheadrecoveryact`, `setuphrs`) VALUES
('ACT', '', 'Accounting', 1, 50, 560000, 0),
('ASS', 'TOR', 'Assembly', 1, 50, 560000, 0),
('EDP', '', 'EDP', 1, 50, 560000, 0),
('FIN', '', 'Finishing', 1, 50, 560000, 0),
('KCN', '', 'Kitchen', 1, 0, 0, 0),
('MAR', '', 'Marketing', 1, 50, 560000, 0),
('QA', '', 'Quality Control', 1, 50, 560000, 0),
('SAL', '', 'Sales', 1, 50, 560000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `www_users`
--

CREATE TABLE IF NOT EXISTS `www_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` text NOT NULL,
  `realname` varchar(35) NOT NULL DEFAULT '',
  `userid` varchar(255) NOT NULL,
  `customerid` varchar(10) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(55) DEFAULT NULL,
  `defaultlocation` varchar(5) NOT NULL DEFAULT '',
  `fullaccess` int(11) NOT NULL DEFAULT '1',
  `lastvisitdate` datetime DEFAULT NULL,
  `branchcode` varchar(10) NOT NULL DEFAULT '',
  `pagesize` varchar(20) NOT NULL DEFAULT 'A4',
  `modulesallowed` varchar(20) NOT NULL DEFAULT '',
  `blocked` tinyint(4) NOT NULL DEFAULT '0',
  `displayrecordsmax` int(11) NOT NULL DEFAULT '0',
  `theme` varchar(30) NOT NULL DEFAULT 'fresh',
  `language` varchar(5) NOT NULL DEFAULT 'en_GB',
  `usergroupid` int(11) NOT NULL DEFAULT '8',
  `active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `CustomerID` (`customerid`),
  KEY `DefaultLocation` (`defaultlocation`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `www_users`
--

INSERT INTO `www_users` (`id`, `password`, `realname`, `userid`, `customerid`, `phone`, `email`, `defaultlocation`, `fullaccess`, `lastvisitdate`, `branchcode`, `pagesize`, `modulesallowed`, `blocked`, `displayrecordsmax`, `theme`, `language`, `usergroupid`, `active`) VALUES
(2, '57b2ad99044d337197c0c39fd3823568ff81e48a', 'Demo User', 'demo', '', '', '', '', 8, '2016-01-25 14:04:08', '', 'A4', '1,1,1,1,1,1,1,1,1,1,', 0, 50, 'professional', 'en_GB', 8, b'1'),
(3, '57b2ad99044d337197c0c39fd3823568ff81e48a', 'Mina Escurel', 'mina', '', '', '', '', 8, '2014-03-03 06:22:11', '', 'A4', '1,1,1,1,1,1,1,1,1,1,', 0, 50, 'professional', 'en_GB', 10, b'1'),
(6, '57b2ad99044d337197c0c39fd3823568ff81e48a', 'Administrator', 'admin', '', '', '', '', 8, '2015-09-26 06:10:31', '', 'A4', '1,1,1,1,1,1,1,1,1,1,', 0, 50, 'professional', 'en_GB', 8, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `www_users_images`
--

CREATE TABLE IF NOT EXISTS `www_users_images` (
  `image` varchar(255) NOT NULL DEFAULT 'default.png',
  `userid` varchar(10) NOT NULL DEFAULT '0',
  UNIQUE KEY `empid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `securitygroups`
--
ALTER TABLE `securitygroups`
  ADD CONSTRAINT `securitygroups_secroleid_fk` FOREIGN KEY (`secroleid`) REFERENCES `securityroles` (`secroleid`),
  ADD CONSTRAINT `securitygroups_tokenid_fk` FOREIGN KEY (`tokenid`) REFERENCES `securitytokens` (`tokenid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
