<?php

/* $Revision: 1.0 $ */

$PageSecurity = 5;

include('includes/session.inc');

$title = _('Payroll Records Maintenance');

include('includes/header.inc');
include('includes/footer.inc');
include('includes/prlFunctions.php');

if (isset($_GET['PayrollID'])){
	$PayrollID = $_GET['PayrollID'];
} elseif (isset($_POST['PayrollID'])){
	$PayrollID = $_POST['PayrollID'];
} else {
	unset($PayrollID);
}


if ($_POST['Generate']==_('Generate Payroll Data'))
{
		include('includes/prlGenerateData.php');
		include('includes/prlComputeBasic.php');
		include('includes/prlComputeOthIncome.php');
		include('includes/prlComputeTD.php');
		include('includes/prlComputeOT.php');
		include('includes/prlComputeGross.php');
		include('includes/prlComputeLoan.php');
		include('includes/prlComputeSSS.php');
		include('includes/prlComputeHDMF.php');
		include('includes/prlComputePH.php');
		include('includes/prlComputeTAX.php');
		include('includes/prlComputeNet.php');	
}


if ($_POST['Close']==_('Close Payroll Period'))
{
$Status = GetOpenCloseStr(GetPayrollRow($PayrollID, &$db,11));
if ($Status=='Closed') {
   exit("Payroll is already closed. Re-open first...");
} else {  
			$sql = "SELECT loantableid,amount
				FROM prlloandeduction
				WHERE payrollid='$PayrollID'";
				$LoanDetails = DB_query($sql,$db);
				
				if(DB_num_rows($LoanDetails)>0)
				{
					while ($loanrow = DB_fetch_array($LoanDetails))
					{
						$LoanPayment=$loanrow['amount'];
						if ($LoanPayment>0 or $LoanPayment<>null) {	   
							$sql = 'UPDATE prlloanfile SET ytddeduction=ytddeduction+'.$LoanPayment.', loanbalance=loanbalance-'.$LoanPayment.'
							WHERE loantableid = ' . $loanrow['loantableid'];
							$PostLoanPay = DB_query($sql,$db);					
						}
					}
				}	
 
		$sql = "UPDATE prlpayrollperiod SET
					payclosed=1
					 WHERE payrollid = '$PayrollID'";
					$ErrMsg = _('The payroll record could not be updated because');
					$DbgMsg = _('The SQL that was used to update the payroll failed was');
					$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);
					prnMsg(_('The payroll master record for') . ' ' . $PayrollID . ' ' . _('has been closed'),'success');
	exit("Payroll is succesfully closed...");
}
}

if ($_POST['Purge']==_('Purge Payroll Period'))
{
  exit("Not implemented at this moment...");
}

if ($_POST['Reopen']==_('Re-open Payroll Period'))
{
$Status = GetOpenCloseStr(GetPayrollRow($PayrollID, &$db,11));
if ($Status=='Open') {
   exit("Payroll is already open...");
} else {  
			$sql = "SELECT loantableid,amount
				FROM prlloandeduction
				WHERE payrollid='$PayrollID'";
				$LoanDetails = DB_query($sql,$db);			
				if(DB_num_rows($LoanDetails)>0)
				{
					while ($loanrow = DB_fetch_array($LoanDetails))
					{
						$LoanPayment=$loanrow['amount'];
						if ($LoanPayment>0 or $LoanPayment<>null) {	   
							$sql = 'UPDATE prlloanfile SET ytddeduction=ytddeduction-'.$LoanPayment.', loanbalance=loanbalance+'.$LoanPayment.'
							WHERE loantableid = ' . $loanrow['loantableid'];
							$PostLoanPay = DB_query($sql,$db);					
						}
					}
				}	
 
		$sql = "UPDATE prlpayrollperiod SET
					payclosed=0
					 WHERE payrollid = '$PayrollID'";
					$ErrMsg = _('The payroll record could not be updated because');
					$DbgMsg = _('The SQL that was used to update the payroll failed was');
					$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);
					prnMsg(_('The payroll master record for') . ' ' . $PayrollID . ' ' . _('has been opened'),'success');
	exit("Payroll is succesfully re-opened...");
}
}

if (!isset($PayrollID)) {
} else {
//PayrollID exists - either passed when calling the form or from the form itself
	echo '<div id="content"><br/><div align="left" class="subheader"><a href="prlSelectPayroll.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp;View Payroll Period Details</div><br/>';
	echo "<FORM METHOD='post' action='" . $_SERVER['PHP_SELF'] . '?' . SID ."'>";
	echo '<CENTER><TABLE>';
		if (!isset($_POST['New'])) {
				$sql = "SELECT payrollid,
					payrolldesc,
					payperiodid,				
					startdate,
					enddate,
					fsmonth,
					fsyear,
					deductsss,
					deducthdmf,
					deductphilhealth,
					deducttax,
					payclosed
			FROM prlpayrollperiod
			WHERE payrollid = '$PayrollID'";
			$result = DB_query($sql, $db);
			$myrow = DB_fetch_array($result);
		$Description = $myrow['payrolldesc'];
		$PayPeriodID = GetPayPeriodDesc($myrow['payperiodid'],$db);	
		$StartDate = ConvertSQLDate($myrow['startdate']);
		$EndDate  = ConvertSQLDate($myrow['enddate']);
		$FSMonth = GetMonthStr($myrow['fsmonth']);
		$FSYear  = $myrow['fsyear'];	
		$SSS  = GetYesNoStr($myrow['deductsss']);
		$HDMF = GetYesNoStr($myrow['deducthdmf']);
		$PhilHealth  = GetYesNoStr($myrow['deductphilhealth']);
		$Tax  = GetYesNoStr($myrow['deducttax']);
		$Status = GetOpenCloseStr($myrow['payclosed']);
		echo "<INPUT TYPE=HIDDEN NAME='PayrollID' VALUE='$PayrollID'>";
		} else {
		// its a new employee  being added
		echo "<INPUT TYPE=HIDDEN NAME='New' VALUE='Yes'>";
		echo '<TR><TD class="tableheader">' . _('Payroll ID') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='PayrollID' VALUE='$PayrollID' SIZE=12 MAXLENGTH=10></TD></TR>";
		}
 	echo "<CENTER><TABLE WIDTH=30% BORDER=0><TR></TR>";		
	echo '<TR><TD WIDTH=100%>';
	echo '<CENTER><a class="jinnerbot" href="' . $rootpath . '/prlEditPayroll.php?&PayrollID='.$PayrollID.'">' . _('Edit Payroll Period') . '</a>';
	echo '</TD><TD WIDTH=100%>';
    echo '</TD></TR></TABLE><BR></CENTER>';
	//echo '<CENTER><FONT SIZE=1>' . _('') . "</FONT><INPUT class='jinnerbot2' TYPE=SUBMIT NAME='Close' VALUE='" . _('Close Payroll Period') . "'>&nbsp;&nbsp;<INPUT class='jinnerbot2' TYPE=SUBMIT NAME='Purge' VALUE='" . _('Purge Payroll Period') . "'>&nbsp;&nbsp;";
	//echo '<FONT SIZE=1>' . _('') . "</FONT><INPUT class='jinnerbot2' TYPE=SUBMIT NAME='Generate' VALUE='" . _('Generate Payroll Data') . "'>&nbsp;&nbsp;<INPUT class='jinnerbot2' TYPE=SUBMIT NAME='Reopen' VALUE='" . _('Re-open Payroll Period') . "'>";
		

?>	

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td height="180" valign="top"> 
      <table width="60%" class="jinnertable" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td class="tableheader" height="30" width="26%"> 
            <div align="right">Payroll ID:</div>
          </td>
          <td height="30" width="74%"><? echo $PayrollID; ?></td>
        </tr>
        <tr> 
          <td class="tableheader" height="30" width="26%"> 
            <div align="right">Description:</div>
          </td>
          <td height="30" width="74%"><? echo $Description; ?></td>
        </tr>
        <tr> 
          <td class="tableheader" height="30" width="26%"> 
            <div align="right">Pay Period:</div>
          </td>
          <td height="30" width="74%"><? echo $PayPeriodID; ?></td>
        </tr>
        <tr> 
          <td class="tableheader" height="30" width="26%"> 
            <div align="right">Start Date:</div>
          </td>
          <td height="30" width="74%"><? echo $StartDate; ?></td>
        </tr>
        <tr> 
          <td class="tableheader" height="30" width="26%"> 
            <div align="right">End Date:</div>
          </td>
          <td height="30" width="74%"><? echo $EndDate; ?></td>
        </tr>
        <tr> 
          <td class="tableheader" height="30" width="26%"> 
            <div align="right">FS Month:</div>
          </td>
          <td  height="30" width="74%"><? echo "$FSMonth $FSYear"; ?></td>
        </tr>
        <tr> 
          <td class="tableheader" height="30" width="26%"> 
            <div align="right">Deduct SSS:</font></div>
          </td>
          <td  height="30" width="74%"><? echo $SSS; ?></td>
        </tr>
        <tr> 
          <td class="tableheader" height="30" width="26%"> 
            <div align="right">Deduct HDMF:</div>
          </td>
          <td height="30" width="74%"><? echo $HDMF; ?></td>
        </tr>
        <tr> 
          <td class="tableheader" height="30" width="26%"> 
            <div align="right">Deduct PhilHealth:</div>
          </td>
          <td  height="30" width="74%"><? echo $PhilHealth; ?></td>
        </tr>
		 <tr> 
          <td class="tableheader" height="30" width="26%"> 
            <div align="right">Deduct Tax:</div>
          </td>
          <td  height="30" width="74%"><? echo $Tax; ?></td>
        </tr>
        <tr> 
          <td class="tableheader" height="30" width="26%"> 
            <div align="right">Payroll Status:</div>
          </td>
          <td height="30" width="74%"><? echo $Status; ?></td>
        </tr>
      </table>
	  
    </td>
  </tr>

</table>
</div>
<?
} // end of main ifs


?>