-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 11, 2014 at 05:03 AM
-- Server version: 5.5.35
-- PHP Version: 5.3.10-1ubuntu3.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pccdev`
--

-- --------------------------------------------------------

--
-- Table structure for table `chartmaster`
--

CREATE TABLE IF NOT EXISTS `chartmaster` (
  `accountcode` int(11) NOT NULL DEFAULT '0',
  `accountname` char(50) NOT NULL DEFAULT '',
  `group_` char(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`accountcode`),
  KEY `AccountCode` (`accountcode`),
  KEY `AccountName` (`accountname`),
  KEY `Group_` (`group_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chartmaster`
--

INSERT INTO `chartmaster` (`accountcode`, `accountname`, `group_`) VALUES
(1, 'Default Sales/Discounts', 'Sales'),
(1010, 'Petty Cash', 'Current Assets'),
(1020, 'Cash on Hand', 'Current Assets'),
(1030, 'Cheque Accounts', 'Current Assets'),
(1040, 'Savings Accounts', 'Current Assets'),
(1050, 'Payroll Accounts', 'Current Assets'),
(1060, 'Special Accounts', 'Current Assets'),
(1070, 'Money Market Investments', 'Current Assets'),
(1080, 'Short-Term Investments (< 90 days)', 'Current Assets'),
(1090, 'Interest Receivable', 'Current Assets'),
(1100, 'Accounts Receivable', 'Current Assets'),
(1150, 'Allowance for Doubtful Accounts', 'Current Assets'),
(1200, 'Notes Receivable', 'Current Assets'),
(1250, 'Income Tax Receivable', 'Current Assets'),
(1300, 'Prepaid Expenses', 'Current Assets'),
(1350, 'Advances', 'Current Assets'),
(1400, 'Supplies Inventory', 'Current Assets'),
(1420, 'Raw Material Inventory', 'Current Assets'),
(1440, 'Work in Progress Inventory', 'Current Assets'),
(1460, 'Finished Goods Inventory', 'Current Assets'),
(1500, 'Land', 'Fixed Assets'),
(1550, 'Bonds', 'Fixed Assets'),
(1600, 'Buildings', 'Fixed Assets'),
(1620, 'Accumulated Depreciation of Buildings', 'Fixed Assets'),
(1650, 'Equipment', 'Fixed Assets'),
(1670, 'Accumulated Depreciation of Equipment', 'Fixed Assets'),
(1700, 'Furniture & Fixtures', 'Fixed Assets'),
(1710, 'Accumulated Depreciation of Furniture & Fixtures', 'Fixed Assets'),
(1720, 'Office Equipment', 'Fixed Assets'),
(1730, 'Accumulated Depreciation of Office Equipment', 'Fixed Assets'),
(1740, 'Software', 'Fixed Assets'),
(1750, 'Accumulated Depreciation of Software', 'Fixed Assets'),
(1760, 'Vehicles', 'Fixed Assets'),
(1770, 'Accumulated Depreciation Vehicles', 'Fixed Assets'),
(1780, 'Other Depreciable Property', 'Fixed Assets'),
(1790, 'Accumulated Depreciation of Other Depreciable Prop', 'Fixed Assets'),
(1800, 'Patents', 'Fixed Assets'),
(1850, 'Goodwill', 'Fixed Assets'),
(1900, 'Future Income Tax Receivable', 'Current Assets'),
(2010, 'Bank Indedebtedness (overdraft)', 'Liabilities'),
(2020, 'Retainers or Advances on Work', 'Liabilities'),
(2050, 'Interest Payable', 'Liabilities'),
(2100, 'Accounts Payable', 'Liabilities'),
(2150, 'Goods Received Suspense', 'Liabilities'),
(2200, 'Short-Term Loan Payable', 'Liabilities'),
(2230, 'Current Portion of Long-Term Debt Payable', 'Liabilities'),
(2250, 'Income Tax Payable', 'Liabilities'),
(2300, 'GST Payable', 'Liabilities'),
(2310, 'GST Recoverable', 'Liabilities'),
(2320, 'PST Payable', 'Liabilities'),
(2330, 'PST Recoverable (commission)', 'Liabilities'),
(2340, 'Payroll Tax Payable', 'Liabilities'),
(2350, 'Withholding Income Tax Payable', 'Liabilities'),
(2360, 'Other Taxes Payable', 'Liabilities'),
(2400, 'Employee Salaries Payable', 'Liabilities'),
(2410, 'Management Salaries Payable', 'Liabilities'),
(2420, 'Director / Partner Fees Payable', 'Liabilities'),
(2450, 'Health Benefits Payable', 'Liabilities'),
(2460, 'Pension Benefits Payable', 'Liabilities'),
(2470, 'Canada Pension Plan Payable', 'Liabilities'),
(2480, 'Employment Insurance Premiums Payable', 'Liabilities'),
(2500, 'Land Payable', 'Liabilities'),
(2550, 'Long-Term Bank Loan', 'Liabilities'),
(2560, 'Notes Payable', 'Liabilities'),
(2600, 'Building & Equipment Payable', 'Liabilities'),
(2700, 'Furnishing & Fixture Payable', 'Liabilities'),
(2720, 'Office Equipment Payable', 'Liabilities'),
(2740, 'Vehicle Payable', 'Liabilities'),
(2760, 'Other Property Payable', 'Liabilities'),
(2800, 'Shareholder Loans', 'Liabilities'),
(2900, 'Suspense', 'Liabilities'),
(3100, 'Capital Stock', 'Equity'),
(3200, 'Capital Surplus / Dividends', 'Equity'),
(3300, 'Dividend Taxes Payable', 'Equity'),
(3400, 'Dividend Taxes Refundable', 'Equity'),
(3500, 'Retained Earnings', 'Equity'),
(4100, 'Product / Service Sales', 'Revenue'),
(4200, 'Sales Exchange Gains/Losses', 'Revenue'),
(4500, 'Consulting Services', 'Revenue'),
(4600, 'Rentals', 'Revenue'),
(4700, 'Finance Charge Income', 'Revenue'),
(4800, 'Sales Returns & Allowances', 'Revenue'),
(4900, 'Sales Discounts', 'Revenue'),
(5000, 'Cost of Sales', 'Cost of Goods Sold'),
(5100, 'Production Expenses', 'Cost of Goods Sold'),
(5200, 'Purchases Exchange Gains/Losses', 'Cost of Goods Sold'),
(5500, 'Direct Labour Costs', 'Cost of Goods Sold'),
(5600, 'Freight Charges', 'Cost of Goods Sold'),
(5700, 'Inventory Adjustment', 'Cost of Goods Sold'),
(5800, 'Purchase Returns & Allowances', 'Cost of Goods Sold'),
(5900, 'Purchase Discounts', 'Cost of Goods Sold'),
(6100, 'Advertising', 'Marketing Expenses'),
(6150, 'Promotion', 'Marketing Expenses'),
(6200, 'Communications', 'Marketing Expenses'),
(6250, 'Meeting Expenses', 'Marketing Expenses'),
(6300, 'Travelling Expenses', 'Marketing Expenses'),
(6400, 'Delivery Expenses', 'Marketing Expenses'),
(6500, 'Sales Salaries & Commission', 'Marketing Expenses'),
(6550, 'Sales Salaries & Commission Deductions', 'Marketing Expenses'),
(6590, 'Benefits', 'Marketing Expenses'),
(6600, 'Other Selling Expenses', 'Marketing Expenses'),
(6700, 'Permits, Licenses & License Fees', 'Marketing Expenses'),
(6800, 'Research & Development', 'Marketing Expenses'),
(6900, 'Professional Services', 'Marketing Expenses'),
(7020, 'Support Salaries & Wages', 'Operating Expenses'),
(7030, 'Support Salary & Wage Deductions', 'Operating Expenses'),
(7040, 'Management Salaries', 'Operating Expenses'),
(7050, 'Management Salary deductions', 'Operating Expenses'),
(7060, 'Director / Partner Fees', 'Operating Expenses'),
(7070, 'Director / Partner Deductions', 'Operating Expenses'),
(7080, 'Payroll Tax', 'Operating Expenses'),
(7090, 'Benefits', 'Operating Expenses'),
(7100, 'Training & Education Expenses', 'Operating Expenses'),
(7150, 'Dues & Subscriptions', 'Operating Expenses'),
(7200, 'Accounting Fees', 'Operating Expenses'),
(7210, 'Audit Fees', 'Operating Expenses'),
(7220, 'Banking Fees', 'Operating Expenses'),
(7230, 'Credit Card Fees', 'Operating Expenses'),
(7240, 'Consulting Fees', 'Operating Expenses'),
(7260, 'Legal Fees', 'Operating Expenses'),
(7280, 'Other Professional Fees', 'Operating Expenses'),
(7300, 'Business Tax', 'Operating Expenses'),
(7350, 'Property Tax', 'Operating Expenses'),
(7390, 'Corporation Capital Tax', 'Operating Expenses'),
(7400, 'Office Rent', 'Operating Expenses'),
(7450, 'Equipment Rental', 'Operating Expenses'),
(7500, 'Office Supplies', 'Operating Expenses'),
(7550, 'Office Repair & Maintenance', 'Operating Expenses'),
(7600, 'Automotive Expenses', 'Operating Expenses'),
(7610, 'Communication Expenses', 'Operating Expenses'),
(7620, 'Insurance Expenses', 'Operating Expenses'),
(7630, 'Postage & Courier Expenses', 'Operating Expenses'),
(7640, 'Miscellaneous Expenses', 'Operating Expenses'),
(7650, 'Travel Expenses', 'Operating Expenses'),
(7660, 'Utilities', 'Operating Expenses'),
(7700, 'Ammortization Expenses', 'Operating Expenses'),
(7750, 'Depreciation Expenses', 'Operating Expenses'),
(7800, 'Interest Expense', 'Operating Expenses'),
(7900, 'Bad Debt Expense', 'Operating Expenses'),
(8100, 'Gain on Sale of Assets', 'Other Revenue and Expenses'),
(8200, 'Interest Income', 'Other Revenue and Expenses'),
(8300, 'Recovery on Bad Debt', 'Other Revenue and Expenses'),
(8400, 'Other Revenue', 'Other Revenue and Expenses'),
(8500, 'Loss on Sale of Assets', 'Other Revenue and Expenses'),
(8600, 'Charitable Contributions', 'Other Revenue and Expenses'),
(8900, 'Other Expenses', 'Other Revenue and Expenses'),
(9100, 'Income Tax Provision', 'Income Tax');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `coycode` int(11) NOT NULL DEFAULT '1',
  `coyname` varchar(50) NOT NULL DEFAULT '',
  `gstno` varchar(20) NOT NULL DEFAULT '',
  `companynumber` varchar(20) NOT NULL DEFAULT '0',
  `regoffice1` varchar(40) NOT NULL DEFAULT '',
  `regoffice2` varchar(40) NOT NULL DEFAULT '',
  `regoffice3` varchar(40) NOT NULL DEFAULT '',
  `regoffice4` varchar(40) NOT NULL DEFAULT '',
  `regoffice5` varchar(20) NOT NULL DEFAULT '',
  `regoffice6` varchar(15) NOT NULL DEFAULT '',
  `telephone` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(25) NOT NULL DEFAULT '',
  `email` varchar(55) NOT NULL DEFAULT '',
  `currencydefault` varchar(4) NOT NULL DEFAULT '',
  `debtorsact` int(11) NOT NULL DEFAULT '70000',
  `pytdiscountact` int(11) NOT NULL DEFAULT '55000',
  `creditorsact` int(11) NOT NULL DEFAULT '80000',
  `payrollact` int(11) NOT NULL DEFAULT '84000',
  `grnact` int(11) NOT NULL DEFAULT '72000',
  `exchangediffact` int(11) NOT NULL DEFAULT '65000',
  `purchasesexchangediffact` int(11) NOT NULL DEFAULT '0',
  `retainedearnings` int(11) NOT NULL DEFAULT '90000',
  `gllink_debtors` tinyint(1) DEFAULT '1',
  `gllink_creditors` tinyint(1) DEFAULT '1',
  `gllink_stock` tinyint(1) DEFAULT '1',
  `freightact` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coycode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`coycode`, `coyname`, `gstno`, `companynumber`, `regoffice1`, `regoffice2`, `regoffice3`, `regoffice4`, `regoffice5`, `regoffice6`, `telephone`, `fax`, `email`, `currencydefault`, `debtorsact`, `pytdiscountact`, `creditorsact`, `payrollact`, `grnact`, `exchangediffact`, `purchasesexchangediffact`, `retainedearnings`, `gllink_debtors`, `gllink_creditors`, `gllink_stock`, `freightact`) VALUES
(1, 'PCC Payroll System', '', '', '', '', '', '', '', '', '', '', '', 'Php', 1100, 4900, 2100, 2400, 2150, 4200, 5200, 3500, 1, 1, 1, 5600);

-- --------------------------------------------------------

--
-- Table structure for table `companyagency`
--

CREATE TABLE IF NOT EXISTS `companyagency` (
  `CompanyAgencyID` int(11) NOT NULL AUTO_INCREMENT,
  `companyagency` varchar(255) NOT NULL,
  PRIMARY KEY (`CompanyAgencyID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `companyagency`
--

INSERT INTO `companyagency` (`CompanyAgencyID`, `companyagency`) VALUES
(1, 'test2'),
(2, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `confname` varchar(35) NOT NULL DEFAULT '',
  `confvalue` text NOT NULL,
  PRIMARY KEY (`confname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`confname`, `confvalue`) VALUES
('AllowSalesOfZeroCostItems', '0'),
('AutoDebtorNo', '0'),
('CheckCreditLimits', '0'),
('Check_Price_Charged_vs_Order_Price', '1'),
('Check_Qty_Charged_vs_Del_Qty', '1'),
('CountryOfOperation', 'USD'),
('CreditingControlledItems_MustExist', '0'),
('DB_Maintenance', '1'),
('DB_Maintenance_LastRun', '2014-10-11'),
('DefaultBlindPackNote', '1'),
('DefaultCreditLimit', '1000'),
('DefaultDateFormat', 'm/d/Y'),
('DefaultDisplayRecordsMax', '50'),
('DefaultPriceList', 'WS'),
('DefaultTaxCategory', '1'),
('DefaultTheme', 'fresh'),
('Default_Shipper', '1'),
('DispatchCutOffTime', '14'),
('DoFreightCalc', '0'),
('EDIHeaderMsgId', 'D:01B:UN:EAN010'),
('EDIReference', 'WEBERP'),
('EDI_Incoming_Orders', 'companies/weberp/EDI_Incoming_Orders'),
('EDI_MsgPending', 'companies/weberp/EDI_MsgPending'),
('EDI_MsgSent', 'companies/weberp/EDI_Sent'),
('FreightChargeAppliesIfLessThan', '1000'),
('FreightTaxCategory', '1'),
('HTTPS_Only', '0'),
('MaxImageSize', '300'),
('NumberOfPeriodsOfStockUsage', '12'),
('OverChargeProportion', '30'),
('OverReceiveProportion', '20'),
('PackNoteFormat', '1'),
('PageLength', '48'),
('part_pics_dir', 'companies/weberp/part_pics'),
('PastDueDays1', '30'),
('PastDueDays2', '60'),
('PO_AllowSameItemMultipleTimes', '1'),
('QuickEntries', '10'),
('RadioBeaconFileCounter', '/home/RadioBeacon/FileCounter'),
('RadioBeaconFTP_user_name', 'RadioBeacon ftp server user name'),
('RadioBeaconHomeDir', '/home/RadioBeacon'),
('RadioBeaconStockLocation', 'BL'),
('RadioBraconFTP_server', '192.168.2.2'),
('RadioBreaconFilePrefix', 'ORDXX'),
('RadionBeaconFTP_user_pass', 'Radio Beacon remote ftp server password'),
('reports_dir', 'companies/weberp/reports'),
('RomalpaClause', 'Ownership will not pass to the buyer until the goods have been paid for in full.'),
('Show_Settled_LastMonth', '1'),
('SO_AllowSameItemMultipleTimes', '1'),
('TaxAuthorityReferenceName', 'Tax Ref'),
('YearEnd', '3');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE IF NOT EXISTS `currencies` (
  `currency` char(20) NOT NULL DEFAULT '',
  `currabrev` char(3) NOT NULL DEFAULT '',
  `country` char(50) NOT NULL DEFAULT '',
  `hundredsname` char(15) NOT NULL DEFAULT 'Cents',
  `rate` double NOT NULL DEFAULT '1',
  PRIMARY KEY (`currabrev`),
  KEY `Country` (`country`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`currency`, `currabrev`, `country`, `hundredsname`, `rate`) VALUES
('Australian Dollars', 'AUD', 'Australia', 'cents', 1),
('Canandian Dollars', 'CND', 'Canada', 'cents', 1),
('Pounds', 'GBP', 'England', 'Pence', 1),
('Philippine Peso', 'Php', 'Philippines', 'cents', 1),
('US Dollars', 'USD', 'United States', 'Cents', 1);

-- --------------------------------------------------------

--
-- Table structure for table `prldailytrans`
--

CREATE TABLE IF NOT EXISTS `prldailytrans` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `rtref` varchar(11) NOT NULL DEFAULT '',
  `rtdesc` varchar(40) NOT NULL DEFAULT '',
  `rtdate` date NOT NULL DEFAULT '0000-00-00',
  `payrollid` varchar(10) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `reghrs` decimal(12,2) NOT NULL DEFAULT '0.00',
  `absenthrs` decimal(12,2) NOT NULL DEFAULT '0.00',
  `latehrs` decimal(12,2) NOT NULL DEFAULT '0.00',
  `regamt` decimal(12,2) NOT NULL DEFAULT '0.00',
  `absentamt` decimal(12,2) NOT NULL DEFAULT '0.00',
  `lateamt` decimal(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`counterindex`),
  KEY `RTDate` (`rtdate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `prldepartment`
--

CREATE TABLE IF NOT EXISTS `prldepartment` (
  `departmentid` int(11) NOT NULL AUTO_INCREMENT,
  `departmentName` varchar(255) NOT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`departmentid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `prldepartment`
--

INSERT INTO `prldepartment` (`departmentid`, `departmentName`, `active`) VALUES
(1, 'Finance', b'1'),
(2, 'Administrator', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `prlemphdmffile`
--

CREATE TABLE IF NOT EXISTS `prlemphdmffile` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `grosspay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerhdmf` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employeehdmf` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fsmonth` tinyint(4) NOT NULL DEFAULT '0',
  `fsyear` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `prlemphdmffile`
--

INSERT INTO `prlemphdmffile` (`counterindex`, `payrollid`, `employeeid`, `grosspay`, `employerhdmf`, `employeehdmf`, `total`, `fsmonth`, `fsyear`) VALUES
(1, '6', '21', 0.00, 0.00, 0.00, 0.00, 3, 2014),
(2, '6', '22', 0.00, 0.00, 0.00, 0.00, 3, 2014),
(5, '4', '21', 0.00, 0.00, 0.00, 0.00, 2, 2014),
(6, '4', '22', 0.00, 0.00, 0.00, 0.00, 2, 2014),
(9, '7', '21', 0.00, 0.00, 0.00, 0.00, 4, 2014),
(10, '7', '22', 0.00, 0.00, 0.00, 0.00, 4, 2014),
(11, '9', '21', 0.00, 0.00, 0.00, 0.00, 4, 2014),
(12, '9', '22', 0.00, 0.00, 0.00, 0.00, 4, 2014);

-- --------------------------------------------------------

--
-- Table structure for table `prlemployeemaster`
--

CREATE TABLE IF NOT EXISTS `prlemployeemaster` (
  `employeeid` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(40) NOT NULL DEFAULT '',
  `firstname` varchar(40) NOT NULL DEFAULT '',
  `middlename` varchar(40) NOT NULL DEFAULT '',
  `address1` varchar(100) NOT NULL DEFAULT '',
  `address2` varchar(100) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `state` varchar(20) NOT NULL DEFAULT '',
  `zip` varchar(15) NOT NULL DEFAULT '',
  `country` varchar(40) NOT NULL DEFAULT '',
  `gender` varchar(15) NOT NULL DEFAULT '',
  `phone1` varchar(20) NOT NULL DEFAULT '',
  `phone1comment` varchar(20) NOT NULL DEFAULT '',
  `phone2` varchar(20) NOT NULL DEFAULT '',
  `phone2comment` varchar(20) NOT NULL DEFAULT '',
  `email1` varchar(50) NOT NULL DEFAULT '',
  `email1comment` varchar(20) NOT NULL DEFAULT '',
  `email2` varchar(50) NOT NULL DEFAULT '',
  `email2comment` varchar(20) NOT NULL DEFAULT '',
  `atmnumber` varchar(20) NOT NULL DEFAULT '',
  `ssnumber` varchar(20) NOT NULL DEFAULT '',
  `hdmfnumber` varchar(20) NOT NULL DEFAULT '',
  `phnumber` varchar(15) NOT NULL DEFAULT '',
  `taxactnumber` varchar(15) NOT NULL DEFAULT '',
  `birthdate` date NOT NULL DEFAULT '0000-00-00',
  `hiredate` date NOT NULL DEFAULT '0000-00-00',
  `terminatedate` date NOT NULL DEFAULT '0000-00-00',
  `retireddate` date NOT NULL DEFAULT '0000-00-00',
  `paytype` tinyint(4) NOT NULL DEFAULT '0',
  `payperiodid` tinyint(4) NOT NULL DEFAULT '0',
  `periodrate` decimal(12,2) NOT NULL DEFAULT '0.00',
  `hourlyrate` decimal(12,2) NOT NULL DEFAULT '0.00',
  `glactcode` int(11) NOT NULL DEFAULT '0',
  `marital` varchar(20) NOT NULL DEFAULT '',
  `taxstatusid` varchar(10) DEFAULT '',
  `employmentid` tinyint(4) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `costcenterid` varchar(10) NOT NULL DEFAULT '',
  `companyagencyid` int(11) NOT NULL DEFAULT '0',
  `position` varchar(40) NOT NULL DEFAULT '',
  `departmentid` int(11) NOT NULL,
  `TimeIn` varchar(255) NOT NULL,
  `TimeOut` varchar(255) NOT NULL,
  PRIMARY KEY (`employeeid`),
  KEY `EmployeeName` (`lastname`,`firstname`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `prlemployeemaster`
--

INSERT INTO `prlemployeemaster` (`employeeid`, `lastname`, `firstname`, `middlename`, `address1`, `address2`, `city`, `state`, `zip`, `country`, `gender`, `phone1`, `phone1comment`, `phone2`, `phone2comment`, `email1`, `email1comment`, `email2`, `email2comment`, `atmnumber`, `ssnumber`, `hdmfnumber`, `phnumber`, `taxactnumber`, `birthdate`, `hiredate`, `terminatedate`, `retireddate`, `paytype`, `payperiodid`, `periodrate`, `hourlyrate`, `glactcode`, `marital`, `taxstatusid`, `employmentid`, `active`, `deleted`, `costcenterid`, `companyagencyid`, `position`, `departmentid`, `TimeIn`, `TimeOut`) VALUES
(1, 'daily', 'cristell', 'reyes', 'pag-asa st', 'caniogan', 'pasig', 'ncr', '1606', 'philippines', 'F', '', '', '', '', '', '', '', '', 'a', '2', '3', 'wq', 'a12', '1991-12-13', '0000-00-00', '0000-00-00', '0000-00-00', 0, 4, 436.00, 50.00, 0, 'Single', 'HF', 1, 1, 0, 'ACT', 2, 'front-end', 1, '9:00 AM', '6:00 PM'),
(2, 'bauson', 'Jake Semi Monthly', 'collado ', '', '', '', '', '', '', 'M', '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-02-11', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 5000.00, 100.00, 0, 'Single', 'HF', 2, 1, 0, 'ACT', 2, 'Web Developer', 2, '09:00 am', '06:00 pm'),
(3, 'Bauson', 'Jake Monthly', 'collado', '', '', '', '', '', '', 'M', '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-02-03', '0000-00-00', '0000-00-00', '0000-00-00', 0, 2, 10000.00, 100.00, 0, 'Single', 'HF', 1, 1, 0, 'ACT', 1, 'jake test', 1, '09:00 am', '06:00 pm'),
(4, 'Reyes', 'Nerissa', '', '', '', '', '', '', '', 'F', '', '', '', '', '', '', '', '', '', '33-8908203-4', '', '010502205728', '301-292-120-000', '1970-01-01', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 5668.00, 54.50, 0, 'Single', 'HF', 1, 1, 0, 'ACT', 2, 'Purchasing Officer', 1, '9:00 AM', '6:00 PM'),
(5, 'Viscaneo', 'Melanie', '', '', '', '', '', '', '', 'F', '', '', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 5668.00, 0.00, 0, '', 'HF', 1, 1, 0, 'ACT', 2, 'Sales Officer', 1, '9:00 AM', '6:00 PM'),
(6, 'Jimenez', 'Antonio', '', '', '', '', '', '', '', 'M', '', '', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 5668.00, 0.00, 0, '', 'HF', 1, 1, 0, 'ACT', 2, 'Liaison', 1, '9:00 AM', '6:00 PM'),
(7, 'Riveta', 'Cristy', '', '', '', '', '', '', '', 'F', '', '', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 5668.00, 0.00, 0, '', 'HF', 1, 1, 0, 'ACT', 2, 'Logistic staff', 1, '9:00 AM', '6:00 PM'),
(8, 'Tranquilo', 'Luen Joy', '', '', '', '', '', '', '', 'F', '', '', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 5668.00, 0.00, 0, '', 'HF', 1, 1, 0, 'ACT', 2, 'Admin Staff', 1, '9:00 AM', '6:00 PM'),
(9, 'Vinas', 'Corazon', '', '', '', '', '', '', '', 'F', '', '', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 5668.00, 0.00, 0, '', 'HF', 1, 1, 0, 'ACT', 2, 'Accounting Staff', 1, '9:00 AM', '6:00 PM'),
(10, 'Tabago', 'Jyrah', '', '', '', '', '', '', '', 'F', '', '', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 5668.00, 0.00, 0, '', 'HF', 1, 1, 0, 'ACT', 2, 'Accounting Staff', 1, '9:00 AM', '6:00 PM'),
(11, 'Vallestero', 'Joan', '', '', '', '', '', '', '', 'F', '', '', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 5668.00, 0.00, 0, '', 'HF', 1, 1, 0, 'ACT', 2, 'Purchasing Officer', 1, '9:00 AM', '6:00 PM'),
(12, 'Mangurali', 'May-Ann', '', '', '', '', '', '', '', 'F', '', '', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 5668.00, 0.00, 0, '', 'HF', 1, 1, 0, 'ACT', 2, 'Sales Staff', 1, '9:00 AM', '6:00 PM'),
(13, 'Carnay', 'Marianne Collen ', '', '', '', '', '', '', '', 'F', '', '', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 5668.00, 0.00, 0, '', 'HF', 1, 1, 0, 'ACT', 2, 'Sales Staff', 1, '9:00 AM', '6:00 PM'),
(14, 'Dumpit', 'Fe', '', 'M. HIZON ST. 10 AVE. CALOOCAN CITY ', '', 'CALOOCAN CITY', '', '', 'Philippines', 'F', '', '', '', '', '', '', '', '', '0072-352097-527', '05-0259926-8', '1060-0000-9734', '190521011029', '156-370-684-188', '1958-11-23', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 6695.00, 64.38, 0, 'Married', 'ME4', 1, 1, 0, 'ACT', 1, 'ACCOUNTING HEAD ', 1, '8:00 AM', '5:00 PM'),
(15, 'Inocencio', 'Imelda', '', '483 GEN. LUIS ST. CAYBIGA CALOOCAN CITY ', '', 'CALOOCAN CITY', '', '1420', 'Philippines', 'F', '', '', '', '', '', '', '', '', '0072-352097-528', '33-0235649-1', '1060-0200-9745', '190521784990', '161-036-441', '1969-03-25', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 6370.00, 61.25, 0, 'Single', 'HF1', 1, 1, 0, 'ACT', 1, 'PAYROLL OFFICER ', 1, '8:00 AM', '5:00 PM'),
(16, 'Centeno', 'Cristina', '', 'BLK 125 LOT 22 PHASE 2 SOUTHVILLE 8-B SAN ISIDRO RODRIGUEZ RIZAL ', '#142 PUROK 1 ISLA PUTING BATO TONDO MANILA ', 'MANILA CITY ', '', '', 'Philippines', 'F', '', '', '', '', '', '', '', '', '', '34-1363113-1', '121070355849', '', '', '1985-02-05', '0000-00-00', '0000-00-00', '0000-00-00', 0, 4, 451.00, 0.00, 0, 'Single', 'HF', 3, 1, 0, 'ACT', 1, 'ACCOUNTING STAFF', 1, '8:00 AM', '5:00 PM'),
(17, 'De Chavez', 'Marife', '', 'SOUTH BEACH PANLAITAN BUSUANG PALAWAN ', 'BLK. 10G LT 10 D CAMADA COMP. D. DAGATDAGATAN  CALOOCAN CITY ', 'CALOOCAN CITY ', '', '', 'Philippines', 'F', '', '', '', '', '', '', '', '', '', '341098216-8', '', '', '441-952-081-000', '1984-12-03', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 225.50, 0.00, 0, 'Single', 'HF1', 3, 1, 0, 'ACT', 2, 'ACCOUNTING STAFF', 1, '8:00 AM', '05:30 pm'),
(18, 'De Guzman', 'Rosemarie', '', 'JAENA , NUEVA ECIJA ', 'BLK 202 LOT3 GEMINI ST. PEMBO MAKATI CITY', 'MAKATI CITY', '', '', 'Philippines', 'F', '', '', '', '', '', '', '', '', '', '', '', '', '249-839-022000', '1983-09-22', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 225.50, 0.00, 0, 'Single', 'HF', 1, 1, 0, 'ACT', 2, '', 1, '8:00 AM', '5:30 PM'),
(19, 'Lleva', 'Aileen', '', 'BRGY. GIBAGA,SARYAYA QUEZON ', 'BLK 202 LOT3 GEMINI ST. PEMBO MAKATI CITY ', 'MAKATI CITY ', '', '', 'Philippines', 'F', '', '', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 225.50, 0.00, 0, 'Single', 'HF', 3, 1, 0, 'ACT', 2, 'ACCOUNTING STAFF', 1, '8:00 AM', '5:30 PM'),
(20, 'Cesar', 'Robelyn', '', 'Pola Or. Mindoro', 'B 0944 Paoville  Fort Bonifacio', ' Taguig City', '', '', 'Philippines ', 'F', '', '', '', '', '', '', '', '', '', '', '', '', '', '1989-11-16', '0000-00-00', '0000-00-00', '0000-00-00', 0, 4, 451.00, 0.00, 0, 'Single', 'HF', 1, 1, 0, 'ACT', 1, 'ADMIN STAFF', 1, '8:00 AM', '05:30 pm'),
(23, 'RAMOS ', 'REAFE ', '', 'PUROK 3 ANAK NAGTIPUNAN QUIRINO ', '#165 ZONE III SITIO PAGKAKAISA, SUCAT MUNTINLUPA CITY', 'MUNTINLUPA CITY ', '', '', 'Philippines', 'M', '', '', '', '', '', '', '', '', '', '', '', '', '', '1988-10-06', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 0.00, 0.00, 0, 'Single', 'HF', 3, 1, 0, 'ACT', 2, '', 1, '08:00 am', '05:30 pm'),
(24, 'Gomez', 'Richard', 'Sample', 'sadfas', 'afsdf', 'sadf', 'asdf', 'asdf', 'asdf', 'M', '', '', '', '', '', '', '', '', '4553', '345', '45', '45', '4534', '2014-05-28', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 3347.50, 64.38, 0, '', 'HF', 1, 1, 0, 'ACT', 1, '', 1, '08:00 am', '05:00 pm'),
(25, 'Bauson', 'Jeff', 'Collado', 'makati', 'makati', '', '', '', '', 'M', '', '', '', '', '', '', '', '', '', '', '', '', '', '2013-01-28', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 6370.00, 61.25, 0, 'Single', 'HF', 1, 1, 0, 'ACT', 1, '', 1, '08:00 am', '05:00 pm'),
(26, 'daily / weekly', 'employee ', 'sample', '', '', '', '', '', 'Philippines', 'M', '', '', '', '', '', '', '', '', '', '', '', '', '', '1985-01-02', '0000-00-00', '0000-00-00', '0000-00-00', 0, 4, 6000.00, 125.00, 0, 'Single', 'HF', 1, 1, 0, 'ACT', 1, 'Admin', 1, '09:00 am', '06:00 pm'),
(29, 'semi-monthly', 'employee ', 'sample', '', '', '', '', '', '', 'M', '', '', '', '', '', '', '', '', '', '', '', '', '', '1985-01-02', '0000-00-00', '0000-00-00', '0000-00-00', 0, 1, 6370.00, 61.25, 0, 'Single', 'HF', 1, 1, 0, 'ACT', 1, '', 1, '08:00 am', '05:00 pm'),
(30, 'monthly', 'employee ', 'sample', '', '', '', '', '', '', 'M', '', '', '', '', '', '', '', '', '', '', '', '', '', '1986-06-05', '0000-00-00', '0000-00-00', '0000-00-00', 0, 2, 25000.00, 150.00, 0, 'Single', 'HF', 1, 1, 0, 'ACT', 1, '', 1, '08:00 am', '05:00 pm');

-- --------------------------------------------------------

--
-- Table structure for table `prlemploymentstatus`
--

CREATE TABLE IF NOT EXISTS `prlemploymentstatus` (
  `employmentid` int(11) NOT NULL AUTO_INCREMENT,
  `employmentdesc` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`employmentid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `prlemploymentstatus`
--

INSERT INTO `prlemploymentstatus` (`employmentid`, `employmentdesc`) VALUES
(1, 'Regular'),
(2, 'Probationary'),
(3, 'Contractual');

-- --------------------------------------------------------

--
-- Table structure for table `prlempphfile`
--

CREATE TABLE IF NOT EXISTS `prlempphfile` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `grosspay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangefrom` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangeto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `salarycredit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerph` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerec` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employeeph` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fsmonth` tinyint(4) NOT NULL DEFAULT '0',
  `fsyear` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `prlempphfile`
--

INSERT INTO `prlempphfile` (`counterindex`, `payrollid`, `employeeid`, `grosspay`, `rangefrom`, `rangeto`, `salarycredit`, `employerph`, `employerec`, `employeeph`, `total`, `fsmonth`, `fsyear`) VALUES
(1, '6', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3, 2014),
(2, '6', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3, 2014),
(5, '4', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2, 2014),
(6, '4', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2, 2014),
(9, '7', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(10, '7', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(11, '9', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(12, '9', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014);

-- --------------------------------------------------------

--
-- Table structure for table `prlempsssfile`
--

CREATE TABLE IF NOT EXISTS `prlempsssfile` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `grosspay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangefrom` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangeto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `salarycredit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerss` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerec` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employeess` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fsmonth` tinyint(4) NOT NULL DEFAULT '0',
  `fsyear` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `prlempsssfile`
--

INSERT INTO `prlempsssfile` (`counterindex`, `payrollid`, `employeeid`, `grosspay`, `rangefrom`, `rangeto`, `salarycredit`, `employerss`, `employerec`, `employeess`, `total`, `fsmonth`, `fsyear`) VALUES
(1, '6', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3, 2014),
(2, '6', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3, 2014),
(5, '4', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2, 2014),
(6, '4', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2, 2014),
(9, '7', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(10, '7', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(11, '9', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(12, '9', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014);

-- --------------------------------------------------------

--
-- Table structure for table `prlemptaxfile`
--

CREATE TABLE IF NOT EXISTS `prlemptaxfile` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `taxableincome` decimal(12,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fsmonth` tinyint(4) NOT NULL DEFAULT '0',
  `fsyear` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `prlhdmftable`
--

CREATE TABLE IF NOT EXISTS `prlhdmftable` (
  `bracket` int(11) NOT NULL AUTO_INCREMENT,
  `rangefrom` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangeto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `dedtypeer` varchar(10) NOT NULL DEFAULT '',
  `employershare` decimal(12,2) NOT NULL DEFAULT '0.00',
  `dedtypeee` varchar(10) NOT NULL DEFAULT '',
  `employeeshare` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total` decimal(12,2) NOT NULL,
  PRIMARY KEY (`bracket`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `prlhdmftable`
--

INSERT INTO `prlhdmftable` (`bracket`, `rangefrom`, `rangeto`, `dedtypeer`, `employershare`, `dedtypeee`, `employeeshare`, `total`) VALUES
(1, 1.00, 1500.00, 'Percentage', 100.00, 'Percentage', 100.00, 200.00),
(2, 1500.01, 999999.00, 'Percentage', 100.00, 'Percentage', 100.00, 200.00),
(3, 0.00, 0.00, 'Fixed', 0.00, 'Fixed', 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `prlholidaytable`
--

CREATE TABLE IF NOT EXISTS `prlholidaytable` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `holidaydate` varchar(255) NOT NULL,
  `holidaydesc` varchar(255) NOT NULL,
  `holidayrate` decimal(6,2) NOT NULL,
  `holidayshortdesc` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `prlholidaytable`
--

INSERT INTO `prlholidaytable` (`id`, `holidaydate`, `holidaydesc`, `holidayrate`, `holidayshortdesc`) VALUES
(4, '2014-06-12', 'Independence Day', 1.00, 'LH'),
(6, 'July 29,2014', 'EIDUL FITR', 1.00, 'LH');

-- --------------------------------------------------------

--
-- Table structure for table `prlhol_trans`
--

CREATE TABLE IF NOT EXISTS `prlhol_trans` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `hol_desc` varchar(255) NOT NULL,
  `hol_date` varchar(255) NOT NULL,
  `emp_id` varchar(255) NOT NULL,
  `duty_hours` decimal(12,2) NOT NULL,
  `hol_type_no` varchar(255) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `prlhol_trans`
--

INSERT INTO `prlhol_trans` (`id`, `hol_desc`, `hol_date`, `emp_id`, `duty_hours`, `hol_type_no`, `amount`) VALUES
(2, 'sample holiday desc', '2014-07-15', '15', 2.00, '2', 500.00),
(6, 'sample2', '2014-07-18', '15', 2.50, '3', 53.59),
(10, 'Independence Day', '2014-06-12', '26', 7.00, '2', 1050.00),
(11, 'Independence Day', '2014-06-12', '29', 8.00, '2', 1200.00),
(12, 'Independence Day', '2014-06-12', '30', 8.00, '2', 1200.00);

-- --------------------------------------------------------

--
-- Table structure for table `prlloandeduct`
--

CREATE TABLE IF NOT EXISTS `prlloandeduct` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `loan_file_id` int(255) NOT NULL,
  `loan_type` int(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `loan_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `amortization` decimal(12,2) NOT NULL DEFAULT '0.00',
  `balance` decimal(12,2) NOT NULL DEFAULT '0.00',
  `skip_deduct` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1084 ;

--
-- Dumping data for table `prlloandeduct`
--

INSERT INTO `prlloandeduct` (`id`, `loan_file_id`, `loan_type`, `date`, `loan_amount`, `amortization`, `balance`, `skip_deduct`) VALUES
(722, 10, 7, '2014-05-23', 20000.00, 2000.00, 18000.00, 0),
(723, 10, 7, '2014-06-06', 20000.00, 2000.00, 16000.00, 0),
(724, 10, 7, '2014-06-20', 20000.00, 2000.00, 14000.00, 0),
(725, 10, 7, '2014-07-04', 20000.00, 2000.00, 12000.00, 0),
(726, 10, 7, '2014-07-18', 20000.00, 2000.00, 10000.00, 0),
(727, 10, 7, '2014-08-01', 20000.00, 2000.00, 8000.00, 0),
(728, 10, 7, '2014-08-15', 20000.00, 2000.00, 6000.00, 0),
(729, 10, 7, '2014-08-29', 20000.00, 2000.00, 4000.00, 0),
(730, 10, 7, '2014-09-12', 20000.00, 2000.00, 2000.00, 0),
(731, 10, 7, '2014-09-26', 20000.00, 2000.00, 0.00, 0),
(1033, 38, 9, '2014-08-15', 9000.00, 500.00, 8500.00, 0),
(1034, 38, 9, '2014-08-29', 9000.00, 500.00, 8000.00, 0),
(1035, 38, 9, '2014-09-12', 9000.00, 500.00, 7500.00, 0),
(1036, 38, 9, '2014-09-26', 9000.00, 500.00, 7000.00, 0),
(1037, 38, 9, '2014-10-10', 9000.00, 500.00, 6500.00, 0),
(1038, 38, 9, '2014-10-24', 9000.00, 500.00, 6000.00, 0),
(1039, 38, 9, '2014-11-07', 9000.00, 500.00, 5500.00, 0),
(1040, 38, 9, '2014-11-21', 9000.00, 500.00, 5000.00, 0),
(1041, 38, 9, '2014-12-05', 9000.00, 500.00, 4500.00, 0),
(1042, 38, 9, '2014-12-19', 9000.00, 500.00, 4000.00, 0),
(1043, 38, 9, '2015-01-02', 9000.00, 500.00, 3500.00, 0),
(1044, 38, 9, '2015-01-16', 9000.00, 500.00, 3000.00, 0),
(1045, 38, 9, '2015-01-30', 9000.00, 500.00, 2500.00, 0),
(1046, 38, 9, '2015-02-13', 9000.00, 500.00, 2000.00, 0),
(1047, 38, 9, '2015-02-27', 9000.00, 500.00, 1500.00, 0),
(1048, 38, 9, '2015-03-13', 9000.00, 500.00, 1000.00, 0),
(1049, 38, 9, '2015-03-27', 9000.00, 500.00, 500.00, 0),
(1050, 38, 9, '2015-04-10', 9000.00, 500.00, 0.00, 0),
(1051, 37, 9, '2014-08-15', 10000.00, 500.00, 9500.00, 0),
(1052, 37, 9, '2014-08-29', 10000.00, 500.00, 9000.00, 0),
(1053, 37, 9, '2014-09-12', 10000.00, 500.00, 8500.00, 0),
(1054, 37, 9, '2014-09-26', 10000.00, 500.00, 8000.00, 0),
(1055, 37, 9, '2014-10-10', 10000.00, 500.00, 7500.00, 0),
(1056, 37, 9, '2014-10-24', 10000.00, 500.00, 7000.00, 0),
(1057, 37, 9, '2014-11-07', 10000.00, 500.00, 6500.00, 0),
(1058, 37, 9, '2014-11-21', 10000.00, 500.00, 6000.00, 0),
(1059, 37, 9, '2014-12-05', 10000.00, 500.00, 5500.00, 0),
(1060, 37, 9, '2014-12-19', 10000.00, 500.00, 5000.00, 0),
(1061, 37, 9, '2015-01-02', 10000.00, 500.00, 4500.00, 0),
(1062, 37, 9, '2015-01-16', 10000.00, 500.00, 4000.00, 0),
(1063, 37, 9, '2015-01-30', 10000.00, 500.00, 3500.00, 0),
(1064, 37, 9, '2015-02-13', 10000.00, 500.00, 3000.00, 0),
(1065, 37, 9, '2015-02-27', 10000.00, 500.00, 2500.00, 0),
(1066, 37, 9, '2015-03-13', 10000.00, 500.00, 2000.00, 0),
(1067, 37, 9, '2015-03-27', 10000.00, 500.00, 1500.00, 0),
(1068, 37, 9, '2015-04-10', 10000.00, 500.00, 1000.00, 0),
(1069, 37, 9, '2015-04-24', 10000.00, 500.00, 500.00, 0),
(1070, 37, 9, '2015-05-08', 10000.00, 500.00, 0.00, 0),
(1080, 40, 12, '2014-08-09', 1650.00, 412.50, 1237.50, 0),
(1081, 40, 12, '2014-08-23', 1650.00, 412.50, 825.00, 0),
(1082, 40, 12, '2014-09-06', 1650.00, 412.50, 412.50, 0),
(1083, 40, 12, '2014-09-20', 1650.00, 412.50, 0.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `prlloandeduction`
--

CREATE TABLE IF NOT EXISTS `prlloandeduction` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `loantableid` tinyint(4) NOT NULL DEFAULT '0',
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `prlloanfile`
--

CREATE TABLE IF NOT EXISTS `prlloanfile` (
  `loanfileid` int(11) NOT NULL AUTO_INCREMENT,
  `loanfiledesc` varchar(40) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `loandate` date NOT NULL DEFAULT '0000-00-00',
  `loantableid` tinyint(4) NOT NULL DEFAULT '0',
  `loanamount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `amortization` decimal(12,2) NOT NULL DEFAULT '0.00',
  `no_of_payments` int(11) NOT NULL,
  `startdeduction` date NOT NULL DEFAULT '0000-00-00',
  `ytddeduction` decimal(12,2) NOT NULL DEFAULT '0.00',
  `loanbalance` decimal(12,2) NOT NULL DEFAULT '0.00',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`loanfileid`),
  KEY `LoanDate` (`loandate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `prlloanfile`
--

INSERT INTO `prlloanfile` (`loanfileid`, `loanfiledesc`, `employeeid`, `loandate`, `loantableid`, `loanamount`, `amortization`, `no_of_payments`, `startdeduction`, `ytddeduction`, `loanbalance`, `accountcode`) VALUES
(37, 'MEL  VALE', '15', '2014-07-30', 9, 10000.00, 500.00, 20, '2014-08-15', 0.00, 10000.00, 7200),
(38, 'Fe VALE', '14', '2014-07-30', 9, 9000.00, 500.00, 18, '2014-08-15', 0.00, 9000.00, 7200),
(40, 'MEL  Eloan', '15', '2014-08-01', 12, 1650.00, 412.50, 4, '2014-08-09', 0.00, 0.00, 7200),
(41, 'asdf', '3', '2014-08-01', 1, 23500.00, 2350.00, 10, '2014-08-05', 0.00, 23500.00, 7200);

-- --------------------------------------------------------

--
-- Table structure for table `prlloantable`
--

CREATE TABLE IF NOT EXISTS `prlloantable` (
  `loantableid` int(11) NOT NULL AUTO_INCREMENT,
  `loantabledesc` varchar(25) NOT NULL DEFAULT '',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`loantableid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `prlloantable`
--

INSERT INTO `prlloantable` (`loantableid`, `loantabledesc`, `accountcode`) VALUES
(1, 'Salary Loan', 0),
(2, 'Cal. Loan', 0),
(3, 'Pag Ibig Loan', 0),
(4, 'Pag Ibig Cal Loan', 0),
(8, 'E-loan1', 0),
(9, 'VALE', 0),
(10, 'Pabahay loan', 0),
(11, 'T-SHIRT', 0),
(12, 'Eloan2', 0),
(13, 'Eloan3', 0),
(14, 'short cashier', 0),
(15, 'Charge', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prlothincfile`
--

CREATE TABLE IF NOT EXISTS `prlothincfile` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `othfileref` varchar(10) NOT NULL DEFAULT '',
  `othfiledesc` varchar(40) NOT NULL DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `othdate` date NOT NULL DEFAULT '0000-00-00',
  `othincid` tinyint(4) NOT NULL DEFAULT '0',
  `othincamount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`),
  KEY `OthDate` (`othdate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `prlothincfile`
--

INSERT INTO `prlothincfile` (`counterindex`, `othfileref`, `othfiledesc`, `employeeid`, `othdate`, `othincid`, `othincamount`, `accountcode`) VALUES
(7, '', '', '2', '2014-02-10', 2, 1000.00, 0),
(29, '', '', '15', '2014-08-30', 7, 750.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `prlothinctable`
--

CREATE TABLE IF NOT EXISTS `prlothinctable` (
  `othincid` int(11) NOT NULL AUTO_INCREMENT,
  `othincdesc` varchar(25) NOT NULL DEFAULT '',
  `taxable` varchar(10) NOT NULL DEFAULT '',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`othincid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `prlothinctable`
--

INSERT INTO `prlothinctable` (`othincid`, `othincdesc`, `taxable`, `accountcode`) VALUES
(5, 'Promo Driver', 'Non-Tax', 0),
(7, 'TRANSPORTATION', 'Non-Tax', 0),
(8, 'REFUND', 'Non-Tax', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prlottrans`
--

CREATE TABLE IF NOT EXISTS `prlottrans` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) DEFAULT '',
  `otref` varchar(11) NOT NULL DEFAULT '',
  `otdesc` varchar(40) NOT NULL DEFAULT '',
  `otdate` date NOT NULL DEFAULT '0000-00-00',
  `overtimeid` tinyint(4) NOT NULL DEFAULT '0',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `othours` double NOT NULL DEFAULT '0',
  `joborder` varchar(10) NOT NULL DEFAULT '',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  `otamount` int(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`),
  KEY `Account` (`accountcode`),
  KEY `OTDate` (`otdate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `prlottrans`
--

INSERT INTO `prlottrans` (`counterindex`, `payrollid`, `otref`, `otdesc`, `otdate`, `overtimeid`, `employeeid`, `othours`, `joborder`, `accountcode`, `otamount`) VALUES
(21, '', '', 'daily sample OT', '2014-06-13', 10, '26', 1, '', 0, 188),
(22, '', '', 'Sample OT semi', '2014-06-10', 10, '29', 1, '', 0, 188),
(23, '', '', 'Sample OT monthly', '2014-06-06', 10, '30', 1, '', 0, 188);

-- --------------------------------------------------------

--
-- Table structure for table `prlovertimetable`
--

CREATE TABLE IF NOT EXISTS `prlovertimetable` (
  `overtimeid` tinyint(4) NOT NULL DEFAULT '0',
  `overtimedesc` varchar(40) NOT NULL DEFAULT '',
  `overtimerate` decimal(6,2) NOT NULL DEFAULT '0.00',
  `accountcode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`overtimeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prlovertimetable`
--

INSERT INTO `prlovertimetable` (`overtimeid`, `overtimedesc`, `overtimerate`, `accountcode`) VALUES
(10, 'Regular Day OT Work', 1.25, 1),
(15, 'Night Shift Pay ', 0.10, 1),
(20, 'Restday or Special Day OT Work', 1.30, 1),
(25, 'Restday or Special Day OT Work >8 hrs', 1.69, 1),
(30, 'Regular Holiday OT Work', 2.00, 1),
(35, 'Regular Holiday OT Work >8 hrs', 2.60, 1),
(40, 'Restday and Regular Holiday OT Work', 2.60, 1),
(45, 'Restday and Regular Holiday OT Work >8hr', 3.38, 1),
(50, 'Holiday sample', 1.00, 0),
(55, 'Legal holiday', 1.00, 0),
(60, 'Special Holiday', 0.30, 0);

-- --------------------------------------------------------

--
-- Table structure for table `prlpayperiod`
--

CREATE TABLE IF NOT EXISTS `prlpayperiod` (
  `payperiodid` tinyint(4) NOT NULL DEFAULT '0',
  `payperioddesc` varchar(15) NOT NULL DEFAULT '',
  `numberofpayday` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`payperiodid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prlpayperiod`
--

INSERT INTO `prlpayperiod` (`payperiodid`, `payperioddesc`, `numberofpayday`, `active`) VALUES
(1, 'Semi-Monthly', 24, 1),
(2, 'Monthly', 12, 1),
(3, 'Weekly', 52, 0),
(4, 'Daily', 312, 1);

-- --------------------------------------------------------

--
-- Table structure for table `prlpayrollperiod`
--

CREATE TABLE IF NOT EXISTS `prlpayrollperiod` (
  `payrollid` int(11) NOT NULL AUTO_INCREMENT,
  `payrolldesc` varchar(40) NOT NULL DEFAULT '',
  `payperiodid` tinyint(4) NOT NULL DEFAULT '0',
  `startdate` date NOT NULL DEFAULT '0000-00-00',
  `enddate` date NOT NULL DEFAULT '0000-00-00',
  `fsmonth` tinyint(4) NOT NULL DEFAULT '0',
  `fsyear` double NOT NULL DEFAULT '0',
  `deductsss` tinyint(4) NOT NULL DEFAULT '0',
  `deducthdmf` tinyint(4) NOT NULL DEFAULT '0',
  `deductphilhealth` tinyint(4) NOT NULL DEFAULT '0',
  `deducttax` tinyint(4) NOT NULL DEFAULT '0',
  `payclosed` tinyint(4) NOT NULL DEFAULT '0',
  `createdtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`payrollid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `prlpayrollperiod`
--

INSERT INTO `prlpayrollperiod` (`payrollid`, `payrolldesc`, `payperiodid`, `startdate`, `enddate`, `fsmonth`, `fsyear`, `deductsss`, `deducthdmf`, `deductphilhealth`, `deducttax`, `payclosed`, `createdtime`) VALUES
(1, 'BHADZ MAY26-JUNE10-CUTTOFF', 1, '2014-05-26', '2014-06-10', 0, 0, 0, 0, 0, 1, 0, '2014-02-25 14:06:16'),
(2, 'jake semi monthly', 1, '2014-03-11', '2014-03-26', 0, 0, 0, 1, 0, 0, 0, '2014-02-25 14:26:46'),
(3, 'APRIL 26- MAY10 2014', 1, '2014-04-26', '2014-05-10', 0, 0, 1, 1, 1, 0, 0, '2014-02-25 15:00:52'),
(4, 'feb 26 semi-monthly', 1, '2014-01-26', '2014-02-10', 2, 2014, 0, 0, 0, 0, 0, '2014-02-26 02:32:01'),
(5, 'JAke test weekly payroll', 3, '2014-02-15', '2014-02-21', 0, 0, 0, 0, 0, 1, 1, '2014-02-26 02:56:24'),
(6, 'MARCH 11-25, 2014', 1, '2014-05-11', '2014-05-26', 0, 0, 0, 0, 0, 0, 0, '2014-03-20 03:43:55'),
(7, 'March 26 - April 10, 2014', 1, '2014-03-26', '2014-04-10', 0, 0, 0, 0, 0, 1, 0, '2014-04-21 04:03:58'),
(8, 'April 11 - April 25, 2014', 1, '2014-04-11', '2014-04-25', 4, 2014, 0, 0, 0, 1, 0, '2014-04-21 04:15:01'),
(9, 'March 26 - April 10, 2014', 1, '2014-03-26', '2014-04-10', 0, 0, 0, 0, 0, 1, 0, '2014-04-21 04:16:50'),
(11, 'test', 1, '2014-04-21', '2014-04-21', 0, 0, 0, 0, 0, 0, 1, '2014-04-21 07:54:48'),
(12, 'Jake UAT', 1, '2014-03-26', '2014-04-10', 0, 0, 0, 0, 0, 0, 0, '2014-04-28 02:09:55'),
(13, 'APRIL 11-25 CUTTOFF', 1, '2014-04-11', '2014-04-25', 0, 0, 0, 0, 0, 0, 0, '2014-05-05 04:00:29'),
(14, 'test loan semi-monthly', 1, '2014-05-09', '2014-05-22', 0, 0, 0, 0, 0, 0, 0, '2014-05-15 16:36:43'),
(15, 'test loan semi-monthly 2', 1, '2014-05-23', '2014-06-05', 0, 0, 0, 0, 0, 0, 0, '2014-05-16 06:21:51'),
(16, 'test loan semi-monthly', 1, '2014-07-15', '2014-07-30', 0, 0, 0, 0, 0, 0, 0, '2014-05-26 05:33:30'),
(17, 'Jun 09', 1, '2014-06-01', '2014-06-15', 6, 2014, 0, 0, 0, 0, 0, '2014-06-09 05:26:09'),
(18, 'Sample daily / weekly payroll', 3, '2014-06-10', '2014-06-16', 1, 2013, 0, 0, 0, 0, 0, '2014-06-10 16:31:29'),
(19, 'Sample Semi-Monthly', 1, '2014-06-01', '2014-06-15', 0, 0, 0, 0, 0, 0, 0, '2014-06-10 20:17:56'),
(20, 'Sample Monthly', 2, '2014-06-01', '2014-06-30', 1, 2013, 0, 0, 0, 0, 0, '2014-06-10 22:33:04'),
(21, 'June 17 sample payroll', 1, '2014-05-26', '2014-06-10', 6, 2014, 0, 0, 0, 0, 0, '2014-06-17 02:16:08'),
(22, 'Jake to Test PAyroll', 1, '2014-05-26', '2014-06-10', 0, 0, 0, 0, 0, 1, 0, '2014-06-20 06:05:45'),
(23, 'MEL JUNE 11-25, 2014', 1, '2014-06-11', '2014-06-25', 0, 0, 0, 0, 0, 1, 0, '2014-07-05 04:13:11'),
(24, 'carla cut off june 26 to july 10,2014', 1, '2014-06-26', '2014-07-10', 0, 0, 0, 0, 0, 1, 0, '2014-07-14 01:20:07'),
(25, 'MEL JULY 11-25, 2014', 1, '2014-07-11', '2014-07-25', 7, 2014, 0, 0, 0, 1, 0, '2014-08-07 08:24:49'),
(26, 'TEST JUL 26-AUG.10 SAMPLE', 1, '2014-07-26', '2014-08-10', 0, 0, 0, 0, 0, 1, 0, '2014-08-14 01:18:30');

-- --------------------------------------------------------

--
-- Table structure for table `prlpayrolltrans`
--

CREATE TABLE IF NOT EXISTS `prlpayrolltrans` (
  `counterindex` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) DEFAULT '',
  `employeeid` varchar(10) NOT NULL DEFAULT '',
  `reghrs` decimal(12,2) NOT NULL DEFAULT '0.00',
  `absenthrs` decimal(12,2) NOT NULL DEFAULT '0.00',
  `latehrs` decimal(12,2) NOT NULL DEFAULT '0.00',
  `periodrate` decimal(12,2) NOT NULL DEFAULT '0.00',
  `hourlyrate` decimal(12,2) NOT NULL DEFAULT '0.00',
  `basicpay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `othincome` decimal(12,2) NOT NULL DEFAULT '0.00',
  `absent` decimal(12,2) NOT NULL DEFAULT '0.00',
  `late` decimal(12,2) NOT NULL DEFAULT '0.00',
  `otpay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `grosspay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `loandeduction` decimal(12,2) NOT NULL DEFAULT '0.00',
  `sss` decimal(12,2) NOT NULL DEFAULT '0.00',
  `hdmf` decimal(12,2) NOT NULL DEFAULT '0.00',
  `philhealth` decimal(12,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(12,2) NOT NULL DEFAULT '0.00',
  `otherdeduction` decimal(12,2) NOT NULL DEFAULT '0.00',
  `totaldeduction` decimal(12,2) NOT NULL DEFAULT '0.00',
  `netpay` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fsmonth` tinyint(4) NOT NULL DEFAULT '0',
  `fsyear` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`counterindex`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `prlpayrolltrans`
--

INSERT INTO `prlpayrolltrans` (`counterindex`, `payrollid`, `employeeid`, `reghrs`, `absenthrs`, `latehrs`, `periodrate`, `hourlyrate`, `basicpay`, `othincome`, `absent`, `late`, `otpay`, `grosspay`, `loandeduction`, `sss`, `hdmf`, `philhealth`, `tax`, `otherdeduction`, `totaldeduction`, `netpay`, `fsmonth`, `fsyear`) VALUES
(1, '6', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3, 2014),
(2, '6', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3, 2014),
(7, '4', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2, 2014),
(8, '4', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2, 2014),
(12, '7', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(13, '7', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(15, '9', '21', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014),
(16, '9', '22', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 4, 2014);

-- --------------------------------------------------------

--
-- Table structure for table `prlphilhealth`
--

CREATE TABLE IF NOT EXISTS `prlphilhealth` (
  `bracket` tinyint(4) NOT NULL DEFAULT '0',
  `rangefrom` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangeto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `salarycredit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerph` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerec` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employeeph` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total` decimal(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`bracket`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prlphilhealth`
--

INSERT INTO `prlphilhealth` (`bracket`, `rangefrom`, `rangeto`, `salarycredit`, `employerph`, `employerec`, `employeeph`, `total`) VALUES
(1, 1.00, 8999.99, 8000.00, 100.00, 0.00, 100.00, 200.00),
(2, 9000.00, 9999.99, 9000.00, 112.50, 0.00, 112.50, 225.00),
(3, 10000.00, 10999.99, 10000.00, 125.00, 0.00, 125.00, 250.00),
(4, 11000.00, 11999.99, 11000.00, 137.50, 0.00, 137.50, 275.00),
(5, 12000.00, 12999.99, 12000.00, 150.00, 0.00, 150.00, 300.00),
(6, 13000.00, 13999.99, 13000.00, 162.50, 0.00, 162.50, 325.00),
(7, 14000.00, 14999.99, 14000.00, 175.00, 0.00, 175.00, 350.00),
(8, 15000.00, 15999.99, 15000.00, 187.50, 0.00, 187.50, 375.00),
(9, 16000.00, 16999.99, 16000.00, 200.00, 0.00, 200.00, 400.00),
(10, 17000.00, 17999.99, 17000.00, 212.50, 0.00, 212.50, 425.00),
(11, 18000.00, 18999.99, 18000.00, 225.00, 0.00, 225.00, 450.00),
(12, 19000.00, 19999.99, 19000.00, 237.50, 0.00, 237.50, 475.00),
(13, 20000.00, 20999.99, 20000.00, 250.00, 0.00, 250.00, 500.00),
(14, 21000.00, 21999.99, 21000.00, 262.50, 0.00, 262.50, 525.00),
(15, 22000.00, 22999.00, 22000.00, 275.00, 0.00, 275.00, 550.00),
(16, 23000.00, 23999.99, 23000.00, 287.50, 0.00, 287.50, 575.00),
(17, 24000.00, 24999.99, 24000.00, 300.00, 0.00, 300.00, 600.00),
(18, 25000.00, 25999.99, 25000.00, 312.50, 0.00, 312.50, 625.00),
(19, 26000.00, 26999.99, 26000.00, 325.00, 0.00, 325.00, 650.00),
(20, 27000.00, 27999.99, 27000.00, 337.50, 0.00, 337.50, 675.00),
(21, 28000.00, 28999.99, 28000.00, 350.00, 0.00, 350.00, 700.00),
(22, 29000.00, 29999.99, 29000.00, 362.50, 0.00, 362.50, 725.00),
(23, 30000.00, 30999.99, 30000.00, 375.00, 0.00, 375.00, 750.00),
(24, 31000.00, 31999.99, 31000.00, 387.50, 0.00, 387.50, 775.00),
(25, 32000.00, 32999.99, 32000.00, 400.00, 0.00, 400.00, 800.00),
(26, 33000.00, 33999.99, 33000.00, 412.50, 0.00, 412.50, 825.00),
(27, 34000.00, 34999.99, 34000.00, 425.00, 0.00, 425.00, 850.00),
(28, 35000.00, 9999999.99, 35000.00, 437.50, 0.00, 437.50, 875.00);

-- --------------------------------------------------------

--
-- Table structure for table `prlremarks`
--

CREATE TABLE IF NOT EXISTS `prlremarks` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `payrollid` int(255) NOT NULL,
  `employeeid` int(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `prlremarks`
--

INSERT INTO `prlremarks` (`id`, `payrollid`, `employeeid`, `firstname`, `lastname`, `remarks`) VALUES
(1, 1, 19, '', '', 'Remarks Sample'),
(2, 4, 14, '', '', 'mike Remarks'),
(3, 5, 14, '', '', ''),
(4, 1, 2, '', '', ''),
(5, 2, 15, '', '', ''),
(6, 6, 20, '', '', ''),
(7, 6, 16, '', '', 'March 19, 2014- Went to valenzuela City'),
(8, 12, 14, '', '', ''),
(9, 13, 14, '', '', '04-16-14 Went to Manila s'),
(10, 13, 15, '', '', ''),
(11, 14, 2, '', '', ''),
(12, 15, 2, '', '', ''),
(13, 3, 14, '', '', ''),
(14, 3, 15, '', '', ''),
(15, 16, 15, '', '', ''),
(16, 1, 14, '', '', ''),
(17, 1, 15, '', '', ''),
(18, 17, 25, '', '', ''),
(19, 18, 26, '', '', ''),
(20, 19, 29, '', '', ''),
(21, 20, 30, '', '', ''),
(22, 21, 15, '', '', ''),
(23, 1, 14, '', '', ''),
(24, 1, 15, '', '', ''),
(25, 0, 14, '', '', ''),
(26, 22, 15, '', '', ''),
(27, 23, 14, '', '', 'June 23,14 Went to Valenzuela'),
(28, 0, 15, '', '', ''),
(29, 23, 15, '', '', 'June 25,14 Half Day'),
(30, 24, 14, '', '', 'july 7  with late permission (UNDERTIME) '),
(31, 24, 15, '', '', ''),
(32, 25, 14, '', '', 'july 16 work suspended due to typhoon GLENDA! signal #3'),
(33, 25, 15, '', '', 'july 16 work suspended due to typhoon GLENDA! signal #3'),
(34, 26, 15, '', '', ''),
(35, 26, 14, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `prlsstable`
--

CREATE TABLE IF NOT EXISTS `prlsstable` (
  `bracket` int(11) NOT NULL AUTO_INCREMENT,
  `rangefrom` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangeto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `salarycredit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerss` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employerec` decimal(12,2) NOT NULL DEFAULT '0.00',
  `employeess` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total` decimal(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`bracket`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `prlsstable`
--

INSERT INTO `prlsstable` (`bracket`, `rangefrom`, `rangeto`, `salarycredit`, `employerss`, `employerec`, `employeess`, `total`) VALUES
(1, 1000.00, 1249.99, 1000.00, 73.70, 10.00, 36.30, 110.00),
(2, 1250.00, 1749.99, 1500.00, 110.50, 10.00, 54.50, 165.00),
(3, 1750.00, 2249.99, 2000.00, 147.30, 10.00, 72.70, 220.00),
(4, 2250.00, 2749.99, 2500.00, 184.20, 10.00, 90.80, 275.00),
(5, 2750.00, 3249.99, 3000.00, 221.00, 10.00, 109.00, 330.00),
(6, 3250.00, 3749.99, 3500.00, 257.80, 10.00, 127.20, 385.00),
(7, 3750.00, 4249.99, 4000.00, 294.70, 10.00, 145.30, 440.00),
(8, 4250.00, 4749.99, 4500.00, 331.50, 10.00, 163.50, 495.00),
(9, 4750.00, 5249.99, 5000.00, 388.30, 10.00, 181.70, 550.00),
(10, 5250.00, 5749.99, 5500.00, 405.20, 10.00, 199.80, 605.00),
(11, 5750.00, 6249.99, 6000.00, 442.00, 10.00, 218.00, 660.00),
(12, 6250.00, 6749.99, 6500.00, 478.80, 10.00, 236.20, 715.00),
(13, 6750.00, 7249.99, 7000.00, 515.70, 10.00, 254.30, 770.00),
(14, 7250.00, 7749.99, 7500.00, 552.50, 10.00, 272.50, 825.00),
(15, 7750.00, 8249.99, 8000.00, 589.30, 10.00, 290.70, 880.00),
(16, 8250.00, 8749.99, 8500.00, 626.20, 10.00, 308.80, 935.00),
(17, 8750.00, 9249.99, 9000.00, 663.00, 10.00, 327.00, 990.00),
(18, 9250.00, 9749.99, 9500.00, 699.80, 10.00, 345.20, 1045.00),
(19, 9750.00, 10249.99, 10000.00, 736.70, 10.00, 363.30, 1100.00),
(20, 10250.00, 10749.99, 10500.00, 773.50, 10.00, 381.50, 1155.00),
(21, 10750.00, 11249.99, 11000.00, 810.30, 10.00, 399.70, 1210.00),
(22, 11250.00, 11749.99, 11500.00, 847.20, 10.00, 417.80, 1265.00),
(23, 11750.00, 12249.99, 12000.00, 884.00, 10.00, 436.00, 1320.00),
(24, 12250.00, 12749.99, 12500.00, 920.80, 10.00, 454.20, 1375.00),
(25, 12750.00, 13249.99, 13000.00, 957.70, 10.00, 472.30, 1430.00),
(26, 13250.00, 13749.99, 13500.00, 994.50, 10.00, 490.50, 1485.00),
(27, 13750.00, 14249.99, 14000.00, 1031.30, 10.00, 508.70, 1540.00),
(28, 14250.00, 14749.99, 14500.00, 1068.20, 10.00, 526.80, 1595.00),
(29, 14750.00, 15249.99, 15000.00, 1105.00, 30.00, 545.00, 1650.00),
(30, 15250.00, 15749.99, 15500.00, 1141.80, 30.00, 563.20, 1705.00),
(31, 15750.00, 9999999.99, 16000.00, 1178.70, 30.00, 581.30, 1760.00);

-- --------------------------------------------------------

--
-- Table structure for table `prltaxstatus`
--

CREATE TABLE IF NOT EXISTS `prltaxstatus` (
  `taxstatusid` varchar(10) NOT NULL DEFAULT '',
  `taxstatusdescription` varchar(40) NOT NULL DEFAULT '',
  `personalexemption` decimal(12,2) NOT NULL DEFAULT '0.00',
  `additionalexemption` decimal(12,2) NOT NULL DEFAULT '0.00',
  `totalexemption` decimal(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`taxstatusid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prltaxstatus`
--

INSERT INTO `prltaxstatus` (`taxstatusid`, `taxstatusdescription`, `personalexemption`, `additionalexemption`, `totalexemption`) VALUES
('HF', 'Head of the Family', 25000.00, 0.00, 25000.00),
('HF1', 'Head of the Family with 1 dependent', 25000.00, 8000.00, 33000.00),
('HF2', 'Head of the Family with 2 dependent', 25000.00, 16000.00, 41000.00),
('HF3', 'Head of the Family with 3 dependent', 25000.00, 24000.00, 49000.00),
('HF4', 'Head of the Family with 4 dependent', 25000.00, 32000.00, 57000.00),
('ME', 'Married', 32000.00, 0.00, 32000.00),
('ME1', 'Married with 1 dependent', 32000.00, 8000.00, 40000.00),
('ME2', 'Married with 2 dependent', 32000.00, 16000.00, 48000.00),
('ME3', 'Married with 3 dependent', 32000.00, 24000.00, 56000.00),
('ME4', 'Married with 4 dependent', 32000.00, 32000.00, 64000.00),
('S', 'Single', 20000.00, 0.00, 20000.00),
('Z', 'Zero Exemption', 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `prltaxtablerate`
--

CREATE TABLE IF NOT EXISTS `prltaxtablerate` (
  `bracket` int(11) NOT NULL AUTO_INCREMENT,
  `rangefrom` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rangeto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fixtaxableamount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `fixtax` decimal(12,2) NOT NULL DEFAULT '0.00',
  `percentofexcessamount` double NOT NULL DEFAULT '1',
  `total` decimal(12,2) NOT NULL,
  PRIMARY KEY (`bracket`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `prltaxtablerate`
--

INSERT INTO `prltaxtablerate` (`bracket`, `rangefrom`, `rangeto`, `fixtaxableamount`, `fixtax`, `percentofexcessamount`, `total`) VALUES
(1, 0.00, 9999.99, 0.00, 0.00, 5, 10.00),
(2, 10000.00, 29999.99, 10000.00, 500.00, 10, 2000.00),
(3, 30000.00, 69999.99, 30000.00, 2500.00, 15, 0.00),
(4, 70000.00, 139999.99, 70000.00, 8500.00, 20, 0.00),
(5, 140000.00, 249999.99, 140000.00, 22500.00, 25, 0.00),
(6, 250000.00, 499999.99, 250000.00, 50000.00, 30, 0.00),
(7, 500000.00, 99999999.99, 500000.00, 125000.00, 32, 0.00),
(8, 100000000.00, 999999999.99, 750000.00, 250000.00, 34, 3000.00);

-- --------------------------------------------------------

--
-- Table structure for table `prltimeentry`
--

CREATE TABLE IF NOT EXISTS `prltimeentry` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `payrollid` varchar(10) NOT NULL,
  `DAY` varchar(255) NOT NULL,
  `TIMEIN` varchar(255) NOT NULL,
  `TIMEOUT` varchar(255) NOT NULL,
  `LUNCHIN` varchar(255) NOT NULL,
  `LUNCHOUT` varchar(255) NOT NULL,
  `LATE` int(255) NOT NULL,
  `BREAKLATE` int(255) NOT NULL,
  `OT` int(255) NOT NULL,
  `UNDERTIME` varchar(255) NOT NULL,
  `employeeid` varchar(11) NOT NULL,
  `TAGS` varchar(255) NOT NULL,
  `ACTUAL_LATE` int(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=580 ;

--
-- Dumping data for table `prltimeentry`
--

INSERT INTO `prltimeentry` (`ID`, `payrollid`, `DAY`, `TIMEIN`, `TIMEOUT`, `LUNCHIN`, `LUNCHOUT`, `LATE`, `BREAKLATE`, `OT`, `UNDERTIME`, `employeeid`, `TAGS`, `ACTUAL_LATE`) VALUES
(1, '1', '2014-02-16', '', '', '', '', 0, 0, 0, '0', '1', 'RD', 0),
(2, '1', '2014-02-17', '02:09 pm', '06:00 pm', '', '', 309, 0, 0, '0', '1', '', 0),
(3, '1', '2014-02-18', '09:10 am', '05:00 pm', '', '', 30, 0, 0, '60', '1', 'LH', 0),
(4, '1', '2014-02-19', '09:20 am', '05:49 pm', '', '', 60, 0, 0, '11', '1', '', 0),
(5, '1', '2014-02-20', '09:07 am', '06:00 pm', '', '', 30, 0, 0, '0', '1', '', 0),
(6, '1', '2014-02-21', '09:32 am', '06:00 pm', '', '', 120, 0, 0, '0', '1', '', 0),
(7, '1', '2014-02-22', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '1', '', 0),
(8, '2', '2014-02-01', '09:19 am', '05:00 pm', '01:13 pm', '12:00 pm', 60, 13, 0, '60', '2', '', 0),
(9, '2', '2014-02-02', '', '', '', '', 0, 0, 0, '0', '2', 'RD', 0),
(10, '2', '2014-02-03', '09:00 am', '05:00 pm', '', '', 0, 0, 0, '60', '2', '', 0),
(11, '2', '2014-02-04', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '2', '', 0),
(12, '2', '2014-02-05', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '2', '', 0),
(13, '2', '2014-02-06', '10:00 am', '03:00 pm', '', '', 120, 0, 0, '180', '2', '', 0),
(14, '2', '2014-02-07', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '2', '', 0),
(15, '2', '2014-02-08', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '2', '', 0),
(16, '2', '2014-02-09', '', '', '', '', 0, 0, 0, '0', '2', 'RD', 0),
(17, '2', '2014-02-10', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '2', '', 0),
(18, '2', '2014-02-11', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '2', '', 0),
(19, '2', '2014-02-12', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '2', '', 0),
(20, '2', '2014-02-13', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '2', '', 0),
(21, '2', '2014-02-14', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '2', '', 0),
(23, '3', '2014-02-01', '10:00 am', '06:00 pm', '', '', 120, 0, 0, '0', '3', '', 0),
(24, '3', '2014-02-02', '', '', '', '', 0, 0, 0, '0', '3', 'RD', 0),
(25, '3', '2014-02-03', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(26, '3', '2014-02-04', '09:20 am', '06:00 pm', '', '', 60, 0, 0, '0', '3', '', 0),
(27, '3', '2014-02-05', '09:00 am', '04:00 pm', '', '', 0, 0, 0, '120', '3', '', 0),
(28, '3', '2014-02-06', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(29, '3', '2014-02-07', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(30, '3', '2014-02-08', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(31, '3', '2014-02-09', '', '', '', '', 0, 0, 0, '0', '3', 'RD', 0),
(32, '3', '2014-02-10', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(33, '3', '2014-02-11', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(34, '3', '2014-02-12', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(35, '3', '2014-02-13', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(36, '3', '2014-02-14', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(37, '3', '2014-02-15', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(38, '3', '2014-02-16', '', '', '', '', 0, 0, 0, '0', '3', 'RD', 0),
(39, '3', '2014-02-17', '09:00 am', '06:00 pm', '01:24 pm', '12:00 pm', 0, 24, 0, '0', '3', '', 0),
(40, '3', '2014-02-18', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(41, '3', '2014-02-19', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(42, '3', '2014-02-20', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(43, '3', '2014-02-21', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(44, '3', '2014-02-22', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(45, '3', '2014-02-23', '', '', '', '', 0, 0, 0, '0', '3', 'RD', 0),
(46, '3', '2014-02-24', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(47, '3', '2014-02-25', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(48, '3', '2014-02-26', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(49, '3', '2014-02-27', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(50, '3', '2014-02-28', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '3', '', 0),
(51, '1', '2014-02-16', '10:00 am', '', '', '', 120, 0, 0, '0', '7', '', 0),
(52, '1', '2014-02-17', '08:00 am', '', '', '', 0, 0, 0, '0', '7', '', 0),
(53, '1', '2014-02-18', '09:00 am', '', '', '', 0, 0, 0, '0', '7', '', 0),
(54, '1', '2014-02-19', '', '', '', '', 0, 0, 0, '0', '7', '', 0),
(55, '1', '2014-02-20', '', '', '', '', 0, 0, 0, '0', '7', '', 0),
(56, '1', '2014-02-21', '', '', '', '', 0, 0, 0, '0', '7', '', 0),
(57, '1', '2014-02-22', '08:00 am', '05:00 pm', '', '06:00 pm', 0, 0, 0, '60', '7', '', 0),
(58, '1', '2014-02-16', '09:06 am', '04:00 pm', '', '', 30, 0, 0, '120', '19', '', 0),
(59, '1', '2014-02-17', '11:10 am', '', '', '', 240, 0, 0, '0', '19', '', 0),
(60, '1', '2014-02-18', '09:02 am', '', '', '', 0, 0, 0, '0', '19', '', 0),
(61, '1', '2014-02-19', '', '', '', '', 0, 0, 0, '0', '19', 'RD', 0),
(62, '1', '2014-02-20', '', '', '', '', 0, 0, 0, '0', '19', 'SL', 0),
(63, '1', '2014-02-21', '', '', '', '', 0, 0, 0, '0', '19', 'VL', 0),
(64, '1', '2014-02-22', '09:00 am', '03:00 pm', '', '', 0, 0, 0, '180', '19', '', 0),
(65, '4', '2014-01-26', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(66, '4', '2014-01-27', '09:06 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(67, '4', '2014-01-28', '09:10 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(68, '4', '2014-01-29', '09:20 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(69, '4', '2014-01-30', '09:40 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(70, '4', '2014-01-31', '09:00 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(71, '4', '2014-02-01', '09:00 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(72, '4', '2014-02-02', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(73, '4', '2014-02-03', '09:00 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(74, '4', '2014-02-04', '09:00 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(75, '4', '2014-02-05', '09:00 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(76, '4', '2014-02-06', '09:00 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(77, '4', '2014-02-07', '09:00 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(78, '4', '2014-02-08', '09:00 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(79, '4', '2014-02-09', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(80, '4', '2014-02-10', '09:00 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(81, '5', '2014-02-15', '09:02 am', '06:00 pm', '', '', 0, 0, 0, '0', '14', '', 0),
(82, '5', '2014-02-16', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(83, '5', '2014-02-17', '09:10 am', '06:00 pm', '', '', 30, 0, 0, '0', '14', '', 0),
(84, '5', '2014-02-18', '10:01 am', '06:00 pm', '', '', 120, 0, 0, '0', '14', '', 0),
(85, '5', '2014-02-19', '11:02 am', '05:00 pm', '', '', 240, 0, 0, '60', '14', '', 0),
(86, '5', '2014-02-20', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '14', '', 0),
(87, '5', '2014-02-21', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '14', '', 0),
(88, '1', '2014-02-16', '11:00 am', '06:00 pm', '', '', 120, 0, 0, '0', '2', '', 0),
(89, '1', '2014-02-17', '12:00 pm', '08:00 pm', '', '', 240, 0, 0, '0', '2', '', 0),
(90, '1', '2014-02-18', '', '', '', '', 0, 0, 0, '0', '2', '', 0),
(91, '1', '2014-02-19', '', '', '', '', 0, 0, 0, '0', '2', '', 0),
(92, '1', '2014-02-20', '', '', '', '', 0, 0, 0, '0', '2', '', 0),
(93, '1', '2014-02-21', '', '', '', '', 0, 0, 0, '0', '2', '', 0),
(94, '1', '2014-02-22', '', '', '', '', 0, 0, 0, '0', '2', '', 0),
(95, '2', '2014-02-15', '09:19 AM', '5:00 PM', '1:13 PM', '12:00 PM', 60, 13, 0, '60', '2', 'LH', 0),
(96, '2', '2014-03-11', '08:03 am', '05:07 pm', '01:29 pm', '12:33 pm', 0, 0, 0, '0', '15', '', 0),
(97, '2', '2014-03-12', '08:01 am', '05:20 pm', '01:28 pm', '12:34 pm', 0, 0, 0, '0', '15', '', 0),
(98, '2', '2014-03-13', '08:07 am', '05:05 pm', '01:29pm ', '12:32 pm', 30, 0, 0, '0', '15', '', 0),
(99, '2', '2014-03-14', '08:13 am', '05:12 pm', '01:28 pm', '12:32 pm', 30, 0, 0, '0', '15', '', 0),
(100, '2', '2014-03-15', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(101, '2', '2014-03-16', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(102, '2', '2014-03-17', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(103, '2', '2014-03-18', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(104, '2', '2014-03-19', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(105, '2', '2014-03-20', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(106, '2', '2014-03-21', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(107, '2', '2014-03-22', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(108, '2', '2014-03-23', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(109, '2', '2014-03-24', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(110, '2', '2014-03-25', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(111, '2', '2014-03-26', '07:58 am', '05:07 pm', '01:25 pm', '12:33 pm', 0, 0, 0, '0', '15', 'RD', 0),
(112, '6', '2014-03-11', '7:53AM ', '5:35PM  ', '1:29PM', '12:32PM ', 0, -3, 0, '0', '20', '', 0),
(113, '6', '2014-03-12', '7:43PM ', '5:40PM ', '1:28PM ', '12:32PM ', 703, -4, 0, '0', '20', '', 0),
(114, '6', '2014-03-13', '7:41PM ', '5:42PM ', '1:29PM ', '12:31PM ', 701, -2, 0, '0', '20', '', 0),
(115, '6', '2014-03-14', '7:40PM ', '5:42PM ', '1:28PM ', '12:34PM ', 700, -6, 0, '0', '20', '', 0),
(116, '6', '2014-03-15', '7:42PM ', '5:08PM ', '1:27PM ', '12:31PM ', 702, -4, 0, '0', '20', '', 0),
(117, '6', '2014-03-16', '', '', '', '', 0, 0, 0, '0', '20', '', 0),
(118, '6', '2014-03-17', '7:59PM ', '5:42PM ', '1:28PM ', '12:31PM ', 719, -3, 0, '0', '20', '', 0),
(119, '6', '2014-03-18', '7:47AM ', '5:38PM ', '1:28PM ', '12:31PM ', 0, -3, 0, '0', '20', '', 0),
(120, '6', '2014-03-19', '7:44AM ', '5:38PM ', '1:28PM ', '12:32PM ', 0, -4, 0, '0', '20', '', 0),
(121, '6', '2014-03-20', '7:38AM ', '5:41PM ', '1:27PM ', '12:31PM ', 0, -4, 0, '0', '20', '', 0),
(122, '6', '2014-03-21', '7:55AM ', '5:41PM ', '1:28PM ', '12:36PM ', 0, -8, 0, '0', '20', '', 0),
(123, '6', '2014-03-22', '7:37AM ', '5:13PM', '1:28PM ', '12:32PM ', 0, -4, 0, '0', '20', '', 0),
(124, '6', '2014-03-23', '', '', '', '', 0, 0, 0, '0', '20', '', 0),
(125, '6', '2014-03-24', '7:49AM ', '5:37PM ', '1:26PM ', '12:32PM ', 0, -6, 0, '0', '20', '', 0),
(126, '6', '2014-03-25', '7:40AM ', '12:39PM ', '', '', 0, 0, 0, '261', '20', '', 0),
(127, '6', '2014-03-11', '7:37AM ', '4:33PM ', '01:00 pm', '11:00 am', 0, 60, 0, '0', '16', '', 0),
(128, '6', '2014-03-12', '7:40AM ', '06:20 pm', '01:23 pm', '12:00 pm', 0, 23, 0, '0', '16', '', 0),
(129, '6', '2014-03-13', '7:39AM ', '1:29PM ', '01:22 pm', '12:00 pm', 0, 22, 0, '211', '16', '', 0),
(130, '6', '2014-03-14', '7:46AM ', '5:39PM ', '01:05 pm', '12:00 pm', 0, 5, 0, '0', '16', '', 0),
(131, '6', '2014-03-15', '7:46AM ', '5:05PM ', '01:41 pm', '12:38 pm', 0, 3, 0, '0', '16', '', 0),
(132, '6', '2014-03-16', '', '', '', '', 0, 0, 0, '0', '16', '', 0),
(133, '6', '2014-03-17', '9:01AM ', '5:41PM ', '01:18 pm', '12:00 pm', 120, 18, 0, '0', '16', '', 0),
(134, '6', '2014-03-18', '', '', '', '', 0, 0, 0, '0', '16', 'Absent', 0),
(135, '6', '2014-03-19', '7:42AM ', '', '', '', 0, 0, 0, '0', '16', '', 0),
(136, '6', '2014-03-20', '7:38AM ', '5:41PM ', '01:37 pm', '12:00 pm', 0, 37, 0, '0', '16', '', 0),
(137, '6', '2014-03-21', '7:55AM ', '5:39PM ', '01:01 pm', '12:00 pm', 0, 1, 0, '0', '16', '', 0),
(138, '6', '2014-03-22', '7:39AM ', '', '', '', 0, 0, 0, '0', '16', '', 0),
(139, '6', '2014-03-23', '', '', '', '', 0, 0, 0, '0', '16', '', 0),
(140, '6', '2014-03-24', '', '', '', '', 0, 0, 0, '0', '16', '', 0),
(141, '6', '2014-03-25', '', '', '', '', 0, 0, 0, '0', '16', '', 0),
(142, '12', '2014-03-26', '07:41 am', '05:27 pm', '', '', 0, 0, 0, '0', '14', '', 0),
(143, '12', '2014-03-27', '07:54 am', '05:00 pm', '', '', 0, 0, 0, '0', '14', '', 0),
(144, '12', '2014-03-28', '08:04 am', '05:38 pm', '', '', 0, 0, 0, '0', '14', '', 0),
(145, '12', '2014-03-29', '07:47 am', '05:19 pm', '', '', 0, 0, 0, '0', '14', '', 0),
(146, '12', '2014-03-30', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(147, '12', '2014-03-31', '08:00 am', '05:11 pm', '', '', 0, 0, 0, '0', '14', '', 0),
(148, '12', '2014-04-01', '08:13 am', '05:30 pm', '', '', 30, 0, 0, '0', '14', '', 0),
(149, '12', '2014-04-02', '07:54 am', '05:36 pm', '', '', 0, 0, 0, '0', '14', '', 0),
(150, '12', '2014-04-03', '07:46 am', '05:41 pm', '', '', 0, 0, 0, '0', '14', '', 0),
(151, '12', '2014-04-04', '07:55 am', '05:22 pm', '', '', 0, 0, 0, '0', '14', '', 0),
(152, '12', '2014-04-05', '07:51 am', '05:14 pm', '', '', 0, 0, 0, '0', '14', '', 0),
(153, '12', '2014-04-06', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(154, '12', '2014-04-07', '08:08 am', '03:40 pm', '', '', 30, 0, 0, '80', '14', '', 0),
(155, '12', '2014-04-08', '', '', '', '', 0, 0, 0, '0', '14', 'Absent', 0),
(156, '12', '2014-04-09', '', '', '', '', 0, 0, 0, '0', '14', 'Absent', 0),
(157, '12', '2014-04-10', '07:56 am', '05:09 pm', '', '', 0, 0, 0, '0', '14', '', 0),
(158, '13', '2014-04-11', '8:08AM ', '5:28PM ', '1:30 pm', '12:30PM ', 30, 0, 0, '0', '14', '', 0),
(159, '13', '2014-04-12', '7:52AM', '5:11PM ', '1:30PM ', '12:30PM ', 0, 0, 0, '0', '14', '', 0),
(160, '13', '2014-04-13', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(161, '13', '2014-04-14', '', '', '', '', 0, 0, 0, '0', '14', 'Absent', 0),
(162, '13', '2014-04-15', '7:45AM ', '5:00PM', '1:30PM ', '12:30PM ', 0, 0, 0, '0', '14', '', 0),
(163, '13', '2014-04-16', '7:56AM ', '5:24PM ', '1:30PM ', '12:30', 0, 0, 0, '0', '14', '', 0),
(164, '13', '2014-04-17', '8:00AM', '5:00PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', 'LH', 0),
(165, '13', '2014-04-18', '8:00AM', '05:00 pm', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', 'LH', 0),
(166, '13', '2014-04-19', '8:00AM', '5:00PM', '1:30PM', '12:33 pm', 0, 0, 0, '0', '14', 'SH', 0),
(167, '13', '2014-04-20', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(168, '13', '2014-04-21', '8:03AM ', '5:31PM ', '1:30PM ', '12:30PM ', 0, 0, 0, '0', '14', '', 0),
(169, '13', '2014-04-22', '7:38AM ', '5:12PM ', '1:30PM ', '12:30PM ', 0, 0, 0, '0', '14', '', 0),
(170, '13', '2014-04-23', '7:50am ', '4:15PM ', '1:30PM ', '12:30PM', 0, 0, 0, '45', '14', '', 0),
(171, '13', '2014-04-24', '8:01AM ', '5:17PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', '', 0),
(172, '13', '2014-04-25', '8:03AM ', '5:36PM ', '1:30PM ', '12:30PM ', 0, 0, 0, '0', '14', '', 0),
(173, '13', '2014-04-11', '7|:46AM', '5:04PM', '01:30 pm', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(174, '13', '2014-04-12', '7:54AM ', '5:09PM ', '1:30PM ', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(175, '13', '2014-04-13', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(176, '13', '2014-04-14', '8:13AM ', '5:16PM', '1:30PM', '12:30PM', 30, 0, 0, '0', '15', '', 0),
(177, '13', '2014-04-15', '8:00AM ', '5:06PM ', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(178, '13', '2014-04-16', '7:39AM ', '1:00pm', '01:30 pm', '12:30 pm', 0, 0, 0, '240', '15', '', 0),
(179, '13', '2014-04-17', '8:00AM', '5:00PM', '01:30 pm', '12:30PM', 0, 0, 0, '0', '15', 'LH', 0),
(180, '13', '2014-04-18', '8:00AM', '5:00PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', 'LH', 0),
(181, '13', '2014-04-19', '8:00AM', '5:00PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', 'SH', 0),
(182, '13', '2014-04-20', '', '', '', '', 0, 0, 0, '0', '15', 'RD', 0),
(183, '13', '2014-04-21', '8:00AM ', '5:12PM ', '1:30PM', '12:30PM ', 0, 0, 0, '0', '15', '', 0),
(184, '13', '2014-04-22', '7:49AM ', '5:13PM', '1:30PM ', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(185, '13', '2014-04-23', '7:36AM ', '5:08PM ', '1:30PM', '12:30PM ', 0, 0, 0, '0', '15', '', 0),
(186, '13', '2014-04-24', '7:36AM ', '5:05PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(187, '13', '2014-04-25', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(188, '14', '2014-05-09', '08:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(189, '14', '2014-05-10', '08:01 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(190, '14', '2014-05-11', '08:01 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(191, '14', '2014-05-12', '08:01 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(192, '14', '2014-05-13', '08:01 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(193, '14', '2014-05-14', '08:01 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(194, '14', '2014-05-15', '08:01 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(195, '14', '2014-05-16', '', '', '', '', 0, 0, 0, '0', '2', '', 0),
(196, '14', '2014-05-17', '08:01 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(197, '14', '2014-05-18', '', '', '', '', 0, 0, 0, '0', '2', '', 0),
(198, '14', '2014-05-19', '', '', '', '', 0, 0, 0, '0', '2', '', 0),
(199, '14', '2014-05-20', '', '', '', '', 0, 0, 0, '0', '2', '', 0),
(200, '14', '2014-05-21', '', '', '', '', 0, 0, 0, '0', '2', '', 0),
(201, '14', '2014-05-22', '', '', '', '', 0, 0, 0, '0', '2', '', 0),
(202, '14', '2014-05-23', '', '', '', '', 0, 0, 0, '0', '2', '', 0),
(203, '15', '2014-05-23', '09:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(204, '15', '2014-05-24', '09:10 am', '06:00 pm', '12:00 pm', '11:00 am', 30, 0, 0, '0', '2', '', 0),
(205, '15', '2014-05-25', '09:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(206, '15', '2014-05-26', '09:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(207, '15', '2014-05-27', '', '', '', '', 0, 0, 0, '0', '2', 'RD', 0),
(208, '15', '2014-05-28', '09:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(209, '15', '2014-05-29', '09:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(210, '15', '2014-05-30', '09:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(211, '15', '2014-05-31', '09:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(212, '15', '2014-06-01', '09:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(213, '15', '2014-06-02', '', '', '', '', 0, 0, 0, '0', '2', 'RD', 0),
(214, '15', '2014-06-03', '09:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(215, '15', '2014-06-04', '09:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(216, '15', '2014-06-05', '09:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '2', '', 0),
(217, '3', '2014-04-26', '7:46AM', '5:14PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', '', 0),
(218, '3', '2014-04-27', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(219, '3', '2014-04-28', '7:47AM', '5:14PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', '', 0),
(220, '3', '2014-04-29', '8:07AM', '5:10PM', '1:30PM', '12:30 pm', 30, 0, 0, '0', '14', '', 0),
(221, '3', '2014-04-30', '8:08AM', '5:44PM', '1:30PM', '12:30PM', 30, 0, 0, '0', '14', '', 0),
(222, '3', '2014-05-01', '8:00AM', '5:00PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', 'LH', 0),
(223, '3', '2014-05-02', '7:58AM', '5:32PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', '', 0),
(224, '3', '2014-05-03', '7:47AM', '5:10PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', '', 0),
(225, '3', '2014-05-04', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(226, '3', '2014-05-05', '', '', '', '', 0, 0, 0, '0', '14', 'Absent', 0),
(227, '3', '2014-05-06', '7:56AM', '5:37PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', '', 0),
(228, '3', '2014-05-07', '7:50AM', '5:13PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', '', 0),
(229, '3', '2014-05-08', '7:57AM', '5:11PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', '', 0),
(230, '3', '2014-05-09', '7:51AM', '5:10PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', '', 0),
(231, '3', '2014-05-10', '7:31AM', '5:14PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', '', 0),
(232, '3', '2014-04-26', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(233, '3', '2014-04-27', '', '', '', '', 0, 0, 0, '0', '15', 'RD', 0),
(234, '3', '2014-04-28', '7:31AM', '5:08PM', '1:30PM', '12:30 PM', 0, 0, 0, '0', '15', '', 0),
(235, '3', '2014-04-29', '8:08AM', '5:10PM', '1:30PM', '12:30PM', 30, 0, 0, '0', '15', '', 0),
(236, '3', '2014-04-30', '7:45AM', '5:10PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(237, '3', '2014-05-01', '8:00AM', '5:00PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', 'LH', 0),
(238, '3', '2014-05-02', '8:01AM', '5:12PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(239, '3', '2014-05-03', '7:59AM', '5:09PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(240, '3', '2014-05-04', '', '', '', '', 0, 0, 0, '0', '15', 'RD', 0),
(241, '3', '2014-05-05', '10:50AM', '5:17PM', '1:30PM', '12:30PM', 240, 0, 0, '0', '15', '', 0),
(242, '3', '2014-05-06', '8:02AM', '5:18PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(243, '3', '2014-05-07', '7:45AM', '5:22PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(244, '3', '2014-05-08', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(245, '3', '2014-05-09', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(246, '3', '2014-05-10', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(247, '16', '2014-07-15', '09:00 am', '04:11 pm', '12:16 pm', '11:00 am', 120, 16, 0, '49', '15', '', 0),
(248, '16', '2014-07-16', '08:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '15', 'LH', 0),
(249, '16', '2014-07-17', '08:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '15', '', 0),
(250, '16', '2014-07-18', '08:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '15', 'SH', 0),
(251, '16', '2014-07-19', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(252, '16', '2014-07-20', '', '', '', '', 0, 0, 0, '0', '15', 'SH', 0),
(253, '16', '2014-07-21', '', '', '', '', 0, 0, 0, '0', '15', 'RD', 0),
(254, '16', '2014-07-22', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(255, '16', '2014-07-23', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(256, '16', '2014-07-24', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(257, '16', '2014-07-25', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(258, '16', '2014-07-26', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(259, '16', '2014-07-27', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(260, '16', '2014-07-28', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(261, '16', '2014-07-29', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(262, '16', '2014-07-30', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(263, '16', '2014-07-31', '', '', '', '', 0, 0, 0, '', '15', '', 0),
(264, '1', '2014-05-11', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(265, '1', '2014-05-12', '7:52AM', '5:33 PM', '1:30PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(266, '1', '2014-05-13', '8:04 AM', '5:32 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(267, '1', '2014-05-14', '8:01 AM', '5:35 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(268, '1', '2014-05-15', '', '', '', '', 0, 0, 0, '0', '14', 'Absent', 0),
(269, '1', '2014-05-16', '7:37 AM', '5:35 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(270, '1', '2014-05-17', '7:41 AM', '5:06 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(271, '1', '2014-05-18', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(272, '1', '2014-05-19', '8:12 AM', '5:09 PM', '1:30 PM', '12:30 PM', 30, 0, 0, '0', '14', '', 0),
(273, '1', '2014-05-20', '7:38 AM', '5:24 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(274, '1', '2014-05-21', '7:56 AM', '5:21 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(275, '1', '2014-05-22', '7:30 AM', '5:08 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(276, '1', '2014-05-23', '7:50 AM', '5:24 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(277, '1', '2014-05-24', '7:40 AM', '5:03 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(278, '1', '2014-05-25', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(279, '1', '2014-05-11', '', '', '', '', 0, 0, 0, '0', '15', 'RD', 0),
(280, '1', '2014-05-12', '08:09am', '05:10 pm', '01:29 pm', '12:55 pm', 30, 0, 0, '0', '15', '', 0),
(281, '1', '2014-05-13', '08:21am', '05:10 pm', '1:27PM', '12:34 pm', 60, 0, 0, '0', '15', '', 0),
(282, '1', '2014-05-14', '07:55 am', '05:14 pm', '1:30PM', '12:42 pm', 0, 0, 0, '0', '15', '', 0),
(283, '1', '2014-05-15', '11:39 AM', '5:13 PM', '1:30 PM', '12:30 PM', 240, 0, 0, '0', '15', '', 0),
(284, '1', '2014-05-16', '7:47 AM', '5:13 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '15', '', 0),
(285, '1', '2014-05-17', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(286, '1', '2014-05-18', '', '', '', '', 0, 0, 0, '0', '15', 'RD', 0),
(287, '1', '2014-05-19', '7:42 AM', '5:10 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '15', '', 0),
(288, '1', '2014-05-20', '7:37 AM', '5:16 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '15', '', 0),
(289, '1', '2014-05-21', '7:41 AM', '5:20 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '15', '', 0),
(290, '1', '2014-05-22', '7:54 AM', '5:04 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '15', '', 0),
(291, '1', '2014-05-23', '7:31 AM', '5:06 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '15', '', 0),
(292, '1', '2014-05-24', '7:31 AM', '5:03 PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '15', '', 0),
(293, '1', '2014-05-25', '', '', '', '', 0, 0, 0, '0', '15', 'RD', 0),
(294, '17', '2014-06-01', '', '', '', '', 0, 0, 0, '0', '25', 'RD', 0),
(295, '17', '2014-06-02', '08:00 am', '05:00 pm', '01:15 pm', '12:00 pm', 0, 15, 0, '0', '25', '', 0),
(296, '17', '2014-06-03', '08:00 am', '05:25 pm', '', '', 0, 0, 0, '0', '25', '', 0),
(297, '17', '2014-06-04', '08:00 am', '05:00 pm', '', '', 0, 0, 0, '0', '25', '', 0),
(298, '17', '2014-06-05', '08:00 am', '05:00 pm', '', '', 0, 0, 0, '0', '25', '', 0),
(299, '17', '2014-06-06', '08:00 am', '05:00 pm', '', '', 0, 0, 0, '0', '25', '', 0),
(300, '17', '2014-06-07', '08:00 am', '05:00 pm', '', '', 0, 0, 0, '0', '25', '', 0),
(301, '17', '2014-06-08', '', '', '', '', 0, 0, 0, '0', '25', 'RD', 0),
(302, '17', '2014-06-09', '08:00 am', '05:00 pm', '', '', 0, 0, 0, '0', '25', '', 0),
(303, '17', '2014-06-10', '08:00 am', '05:00 pm', '', '', 0, 0, 0, '0', '25', '', 0),
(304, '17', '2014-06-11', '08:00 am', '05:00 pm', '', '', 0, 0, 0, '0', '25', '', 0),
(305, '17', '2014-06-12', '08:00 am', '05:00 pm', '', '', 0, 0, 0, '0', '25', '', 0),
(306, '17', '2014-06-13', '08:00 am', '05:00 pm', '', '', 0, 0, 0, '0', '25', '', 0),
(307, '17', '2014-06-14', '08:00 am', '08:00 pm', '', '', 0, 0, 0, '0', '25', '', 0),
(308, '17', '2014-06-15', '', '', '', '', 0, 0, 0, '0', '25', 'RD', 0),
(309, '18', '2014-06-10', '09:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '26', '', 0),
(310, '18', '2014-06-11', '09:00 am', '06:30 pm', '', '', 0, 0, 0, '0', '26', '', 0),
(311, '18', '2014-06-12', '09:00 am', '05:00 pm', '', '', 0, 0, 0, '60', '26', 'LH', 0),
(312, '18', '2014-06-13', '11:00 am', '07:00 pm', '', '', 240, 0, 0, '0', '26', '', 121),
(313, '18', '2014-06-14', '', '', '', '', 0, 0, 0, '0', '26', 'SH', 0),
(314, '18', '2014-06-15', '', '', '', '', 0, 0, 0, '0', '26', 'RD', 0),
(315, '18', '2014-06-16', '', '', '', '', 0, 0, 0, '0', '26', '', 0),
(316, '19', '2014-06-01', '', '', '', '', 0, 0, 0, '0', '29', 'RD', 0),
(317, '19', '2014-06-02', '09:00 am', '05:00 pm', '', '', 120, 0, 0, '0', '29', '', 60),
(318, '19', '2014-06-03', '08:00 am', '02:00 pm', '', '', 0, 0, 0, '180', '29', '', 0),
(319, '19', '2014-06-04', '', '', '', '', 0, 0, 0, '0', '29', 'Absent', 0),
(320, '19', '2014-06-05', '', '', '', '', 0, 0, 0, '0', '29', 'Absent', 0),
(321, '19', '2014-06-06', '08:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '29', '', 0),
(322, '19', '2014-06-07', '08:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '29', '', 0),
(323, '19', '2014-06-08', '', '', '', '', 0, 0, 0, '0', '29', 'RD', 0),
(324, '19', '2014-06-09', '08:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '29', '', 0),
(325, '19', '2014-06-10', '08:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '29', '', 0),
(326, '19', '2014-06-11', '08:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '29', '', 0),
(327, '19', '2014-06-12', '', '', '', '', 0, 0, 0, '0', '29', 'LH', 0),
(328, '19', '2014-06-13', '08:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '29', '', 0),
(329, '19', '2014-06-14', '08:10 am', '03:25 pm', '', '', 30, 0, 0, '95', '29', 'JS', 10),
(330, '19', '2014-06-15', '', '', '', '', 0, 0, 0, '0', '29', 'RD', 0),
(331, '20', '2014-06-01', '', '', '', '', 0, 0, 0, '0', '30', 'RD', 0),
(332, '20', '2014-06-02', '08:00 am', '04:00 pm', '12:30 pm', '11:00 am', 0, 30, 0, '60', '30', '', 0),
(333, '20', '2014-06-03', '09:00 am', '05:00 pm', '12:00 pm', '11:00 am', 120, 0, 0, '0', '30', '', 0),
(334, '20', '2014-06-04', '08:00 am', '05:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '30', '', 0),
(335, '20', '2014-06-05', '08:00 am', '05:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '30', '', 0),
(336, '20', '2014-06-06', '08:00 am', '06:00 pm', '12:00 pm', '11:00 am', 0, 0, 0, '0', '30', '', 0),
(337, '20', '2014-06-07', '', '', '', '', 0, 0, 0, '0', '30', 'Absent', 0),
(338, '20', '2014-06-08', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(339, '20', '2014-06-09', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(340, '20', '2014-06-10', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(341, '20', '2014-06-11', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(342, '20', '2014-06-12', '', '', '', '', 0, 0, 0, '0', '30', 'LH', 0),
(343, '20', '2014-06-13', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(344, '20', '2014-06-14', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(345, '20', '2014-06-15', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(346, '20', '2014-06-16', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(347, '20', '2014-06-17', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(348, '20', '2014-06-18', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(349, '20', '2014-06-19', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(350, '20', '2014-06-20', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(351, '20', '2014-06-21', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(352, '20', '2014-06-22', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(353, '20', '2014-06-23', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(354, '20', '2014-06-24', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(355, '20', '2014-06-25', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(356, '20', '2014-06-26', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(357, '20', '2014-06-27', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(358, '20', '2014-06-28', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(359, '20', '2014-06-29', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(360, '20', '2014-06-30', '', '', '', '', 0, 0, 0, '0', '30', '', 0),
(361, '18', '2014-02-17', '09:00 AM', '05:00 PM', '01:03 PM', '12:00 PM', 0, 0, 0, '', '26', 'sdf', 0),
(362, '21', '2014-05-26', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(363, '21', '2014-05-27', '08:15 am', '05:00 pm', '', '', 30, 0, 0, '0', '15', '', 0),
(364, '21', '2014-05-28', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(365, '21', '2014-05-29', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(366, '21', '2014-05-30', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(367, '21', '2014-05-31', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(368, '21', '2014-06-01', '', '', '', '', 0, 0, 0, '0', '15', 'RD', 0),
(369, '21', '2014-06-02', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(370, '21', '2014-06-03', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(371, '21', '2014-06-04', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(372, '21', '2014-06-05', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(373, '21', '2014-06-06', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(374, '21', '2014-06-07', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(375, '21', '2014-06-08', '', '', '', '', 0, 0, 0, '0', '15', 'RD', 0),
(376, '21', '2014-06-09', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(377, '21', '2014-06-10', '08:03 am', '05:36 pm', '', '', 0, 0, 0, '0', '15', 'LH', 0),
(378, '1', '2014-05-26', '8:00AM', '5:00PM', '1:10pm', '12:32 pm', 0, 0, 0, '0', '14', '', 0),
(379, '1', '2014-05-27', '7:49AM ', '5:13PM ', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(380, '1', '2014-05-28', '7:48AM ', '5:19PM', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(381, '1', '2014-05-29', '7:55 AM', '5:28PM ', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(382, '1', '2014-05-30', '7:52AM', '5:42PM', '1:30PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(383, '1', '2014-05-31', '7:55AM', '05:39pm', '01:27 pm', '12:32 pm', 0, 0, 0, '0', '14', '', 0),
(384, '1', '2014-06-01', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(385, '1', '2014-06-02', '7:55AM ', '5:22PM', '1:30PM', '12:30pm', 0, 0, 0, '0', '14', '', 0),
(386, '1', '2014-06-03', '8:16AM ', '5:11PM', '1:30 PM', '12:30 PM', 60, 0, 0, '0', '14', '', 0),
(387, '1', '2014-06-04', '', '5:38PM', '12:26PM', '', 0, 0, 0, '0', '14', '', 0),
(388, '1', '2014-06-05', '7:38AM ', '5:06PM ', '1:30PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(389, '1', '2014-06-06', '8:10AM ', '5:40PM ', '1:30PM', '12:30 PM', 30, 0, 0, '0', '14', '', 0),
(390, '1', '2014-06-07', '7:43AM', '5:13PM ', '1:30 PM', '12:30 PM', 0, 0, 0, '0', '14', '', 0),
(391, '1', '2014-06-08', '', '', '', '', 0, 0, 0, '0', '14', 'RD', 0),
(392, '1', '2014-06-09', '8:29AM ', '5:20PM ', '1:30PM', '12:30PM', 60, 0, 0, '0', '14', '', 0),
(393, '1', '2014-06-10', '7:55AM', '5:39PM', '1:30PM ', '12:30PM ', 0, 0, 0, '0', '14', '', 0),
(394, '1', '2014-05-26', '08:00 am', '06:00 pm', '', '', 0, 0, 0, '0', '15', '', 0),
(395, '1', '2014-05-27', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(396, '1', '2014-05-28', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(397, '1', '2014-05-29', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(398, '1', '2014-05-30', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(399, '1', '2014-05-31', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(400, '1', '2014-06-01', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(401, '1', '2014-06-02', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(402, '1', '2014-06-03', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(403, '1', '2014-06-04', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(404, '1', '2014-06-05', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(405, '1', '2014-06-06', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(406, '1', '2014-06-07', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(407, '1', '2014-06-08', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(408, '1', '2014-06-09', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(409, '1', '2014-06-10', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(410, '', '1970-01-01', '', '', '', '', 0, 0, 0, '', '14', '', 0),
(411, '22', '2014-05-26', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(412, '22', '2014-05-27', '8:09 AM', '5:14PM ', '1:30PM', '12:30 PM', 30, 0, 0, '0', '15', '', 0),
(413, '22', '2014-05-28', '7:58AM ', '5:11 pm', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(414, '22', '2014-05-29', '7:56AM', '5:00PM ', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(415, '22', '2014-05-30', '8:02AM ', '5:11PM ', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(416, '22', '2014-05-31', '8:02AM ', '5:11PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(417, '22', '2014-06-01', '', '', '', '', 0, 0, 0, '0', '15', 'RD', 0),
(418, '22', '2014-06-02', '8:10AM ', '5:08PM ', '1:30PM', '12:30PM', 30, 0, 0, '0', '15', '', 0),
(419, '22', '2014-06-03', '8:15AM ', '5:12PM ', '1:30PM', '12:30PM', 30, 0, 0, '0', '15', '', 0),
(420, '22', '2014-06-04', '7:57AM ', '5:31PM ', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(421, '22', '2014-06-05', '7:46AM', '5:06PM', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(422, '22', '2014-06-06', '7:51AM ', '5:10PM ', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(423, '22', '2014-06-07', '7:28AM ', '5:04PM ', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(424, '22', '2014-06-08', '', '', '', '', 0, 0, 0, '0', '15', 'RD', 0),
(425, '22', '2014-06-09', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(426, '22', '2014-06-10', '8:12AM ', '5:36PM ', '1:30PM', '12:30PM', 30, 0, 0, '0', '15', '', 0),
(427, '23', '2014-06-11', '7:50 am', '5:35 pm', '01:24 pm', '12:33 pm', 0, 0, 0, '0', '14', '', 0),
(428, '23', '2014-06-12', '', '', '', '', 0, 0, 0, '0', '14', 'LH', 0),
(429, '23', '2014-06-13', '7:48 am', '5:14 pm', '1:28 pm', '12:39 pm', 0, 0, 0, '0', '14', '', 0),
(430, '23', '2014-06-14', '7:45 am', '5:06 pm', '1:23 pm', '12:31 pm', 0, 0, 0, '0', '14', '', 0),
(431, '23', '2014-06-15', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(432, '23', '2014-06-16', '7:52 am', '5:08 pm', '1:28 pm', '12:31 pm', 0, 0, 0, '0', '14', '', 0),
(433, '23', '2014-06-17', '7:54 am', '5:18 pm', '1:26 pm', '12:33 pm', 0, 0, 0, '0', '14', '', 0),
(434, '23', '2014-06-18', '7:55 am', '5:21 pm', '1:28 pm', '12:31 pm', 0, 0, 0, '0', '14', '', 0),
(435, '23', '2014-06-19', '8:17 am', '5:36 pm', '1:21 pm', '12:33 pm', 60, 0, 0, '0', '14', '', 17),
(436, '23', '2014-06-20', '8:07 am', '5:40 pm', '1:22 pm', '12:31 pm', 30, 0, 0, '0', '14', '', 7),
(437, '23', '2014-06-21', '7:53 am', '5:16 pm', '1:28 pm', '12:32 pm', 0, 0, 0, '0', '14', '', 0),
(438, '23', '2014-06-22', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(439, '23', '2014-06-23', '8:00AM ', '5:37 pm', '1:30 pm', '12:30PM ', 0, 0, 0, '0', '14', '', 0),
(440, '23', '2014-06-24', '8:01 am', '5:23 pm', '1:28 pm', '12:32 pm', 0, 0, 0, '0', '14', '', 1),
(441, '23', '2014-06-25', '8:00 am', '5:17 pm', '1:26 pm', '12:33 pm', 0, 0, 0, '0', '14', '', 0),
(442, '', '1970-01-01', '', '', '', '', 0, 0, 0, '', '15', '', 0),
(443, '23', '2014-06-11', '8:14 am', '5:09 pm', '1:29 pm', '12:32 pm', 30, 0, 0, '0', '15', '', 14),
(444, '23', '2014-06-12', '', '', '', '', 0, 0, 0, '0', '15', 'LH', 0),
(445, '23', '2014-06-13', '8:29 am', '5:12 pm', '1:28 pm', '12:41 pm', 60, 0, 0, '0', '15', '', 29),
(446, '23', '2014-06-14', '7:24 am', '5:04 pm', '1:28 pm', '12:34 pm', 0, 0, 0, '0', '15', '', 0),
(447, '23', '2014-06-15', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(448, '23', '2014-06-16', '8:12 am', '5:07 pm', '1:29 pm', '12:33 pm', 30, 0, 0, '0', '15', '', 12),
(449, '23', '2014-06-17', '7:47 am', '5:07 pm', '1:29 pm', '12:31 pm', 0, 0, 0, '0', '15', '', 0),
(450, '23', '2014-06-18', '8:05 am', '5:16 pm', '1:28 pm', '12:30 pm', 0, 0, 0, '0', '15', '', 5),
(451, '23', '2014-06-19', '8:13 am', '5:17 pm', '1:28 pm', '12:32 pm', 30, 0, 0, '0', '15', '', 13),
(452, '23', '2014-06-20', '8:15 am', '5:11 pm', '1:30 pm', '12:30 pm', 30, 0, 0, '0', '15', '', 15),
(453, '23', '2014-06-21', '7:24 am', '5:08 pm', '1:28 pm', '12:32 pm', 0, 0, 0, '0', '15', '', 0),
(454, '23', '2014-06-22', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(455, '23', '2014-06-23', '8:10 am', '5:11 pm', '1:28 pm', '12:33 pm', 30, 0, 0, '0', '15', '', 10),
(456, '23', '2014-06-24', '8:02 am', '5:10 pm', '1:30 pm', '12:35 pm', 0, 0, 0, '0', '15', '', 2),
(457, '23', '2014-06-25', '', '5:39 pm', '12:41pm', '', 0, 0, 0, '0', '15', '', 0),
(458, '24', '2014-06-26', '8:16am', '5:42pm', '1:27pm', '12:34 pm', 60, 0, 0, '0', '14', '', 16),
(459, '24', '2014-06-27', '7:48am', '5:48pm', '1:18pm', '12:36 pm', 0, 0, 0, '0', '14', '', 0),
(460, '24', '2014-06-28', '7:44am', '5:09pm', '1:25pm', '12:31 pm', 0, 0, 0, '0', '14', '', 0),
(461, '24', '2014-06-29', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(462, '24', '2014-06-30', '7:45am', '5:17pm', '1:27pm', '12:38 pm', 0, 0, 0, '0', '14', '', 0),
(463, '24', '2014-07-01', '8:09am', '5:18pm', '1:27pm', '12:31 pm', 30, 0, 0, '0', '14', '', 9),
(464, '24', '2014-07-02', '8:06am', '05:33 pm', '1:23pm', '12:33 pm', 30, 0, 0, '0', '14', '', 6),
(465, '24', '2014-07-03', '7:57am', '05:53 pm', '1:27PM', '12:32 pm', 0, 0, 0, '0', '14', '', 0),
(466, '24', '2014-07-04', '12:16 pm', '05:00 pm', '', '', 0, 0, 0, '240', '14', '', 0),
(467, '24', '2014-07-05', '', '', '', '', 0, 0, 0, '0', '14', 'Absent', 0),
(468, '24', '2014-07-06', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(469, '24', '2014-07-07', '10:00 am', '08:00 pm', '01:30 pm', '12:00 pm', 120, 30, 0, '0', '14', '', 120),
(470, '24', '2014-07-08', '6:59am', '5:17pm', '1:27pm', '12:31 pm', 0, 0, 0, '0', '14', '', 0),
(471, '24', '2014-07-09', '7:32am', '5:15pm', '1:25pm', '12:31 pm', 0, 0, 0, '0', '14', '', 0),
(472, '24', '2014-07-10', '7:56am', '5:20pm', '1:26pm', '12:32 pm', 0, 0, 0, '0', '14', '', 0),
(473, '24', '2014-07-11', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(474, '24', '2014-07-12', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(475, '24', '2014-07-13', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(476, '24', '2014-07-14', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(477, '24', '2014-07-15', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(478, '24', '2014-07-16', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(479, '24', '2014-07-17', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(480, '24', '2014-07-18', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(481, '24', '2014-07-19', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(482, '24', '2014-07-20', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(483, '24', '2014-07-21', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(484, '24', '2014-07-22', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(485, '24', '2014-07-23', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(486, '24', '2014-07-24', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(487, '24', '2014-07-25', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(488, '24', '2014-06-26', '7:55am', '5:15pm', '1:28pm', '12:34 pm', 0, 0, 0, '0', '15', '', 0),
(489, '24', '2014-06-27', '7:55am', '5:31pm', '1:29pm', '12:33 pm', 0, 0, 0, '0', '15', '', 0),
(490, '24', '2014-06-28', '7:38am', '5:08pm', '1:28pm', '12:33 pm', 0, 0, 0, '0', '15', '', 0),
(491, '24', '2014-06-29', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(492, '24', '2014-06-30', '12:30 pm', '05:00 pm', '', '', 0, 0, 0, '240', '15', '', 0),
(493, '24', '2014-07-01', '7:59am', '5:16pm', '1:26pm', '12:31 pm', 0, 0, 0, '0', '15', '', 0),
(494, '24', '2014-07-02', '8:06am', '5:17pm', '1:28pm', '12:35 pm', 30, 0, 0, '0', '15', '', 6),
(495, '24', '2014-07-03', '8:08am', '5:09pm', '1:27pm', '12:32 pm', 30, 0, 0, '0', '15', '', 8),
(496, '24', '2014-07-04', '7:35am', '5:00pm', '1:29pm', '12:35 pm', 0, 0, 0, '0', '15', '', 0),
(497, '24', '2014-07-05', '8:04am', '5:07pm', '1:29pm', '12:33 pm', 0, 0, 0, '0', '15', '', 4),
(498, '24', '2014-07-06', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(499, '24', '2014-07-07', '8:14am', '5:12pm', '1:29pm', '12:36 pm', 30, 0, 0, '0', '15', '', 14),
(500, '24', '2014-07-08', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(501, '24', '2014-07-09', '7:30am', '5:14pm', '1:26pm', '12:35 pm', 0, 0, 0, '0', '15', '', 0),
(502, '24', '2014-07-10', '7:53am', '5:40pm', '1:28pm', '12:30 pm', 0, 0, 0, '0', '15', '', 0),
(503, '24', '2014-07-11', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(504, '24', '2014-07-12', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(505, '24', '2014-07-13', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(506, '24', '2014-07-14', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(507, '24', '2014-07-15', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(508, '24', '2014-07-16', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(509, '24', '2014-07-17', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(510, '24', '2014-07-18', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(511, '24', '2014-07-19', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(512, '24', '2014-07-20', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(513, '24', '2014-07-21', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(514, '24', '2014-07-22', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(515, '24', '2014-07-23', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(516, '24', '2014-07-24', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(517, '24', '2014-07-25', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(518, '25', '2014-07-11', '', '', '', '', 0, 0, 0, '0', '14', 'Absent', 0),
(519, '25', '2014-07-12', '08:02 am', '05:10 pm', '01:25 pm', '12:34 pm', 0, 0, 0, '0', '14', '', 2),
(520, '25', '2014-07-13', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(521, '25', '2014-07-14', '08:04 am', '05:12 pm', '01:29 pm', '12:32pm', 0, 0, 0, '0', '14', '', 4),
(522, '25', '2014-07-15', '7:57AM', '05:18 pm', '1:27 PM', '12:32 pm', 0, 0, 0, '0', '14', '', 0),
(523, '25', '2014-07-16', '8:00am', '5:00PM', '1:30pm', '12:30pm', 0, 0, 0, '0', '14', '', 0),
(524, '25', '2014-07-17', '8:01am', '7:21pm', '1:30PM', '12:30PM', 0, 0, 0, '0', '14', '', 1),
(525, '25', '2014-07-18', '7:57AM', '05:42 pm', '1:25 PM', '12:32 pm', 0, 0, 0, '0', '14', '', 0),
(526, '25', '2014-07-19', '7:53AM', '05:15 pm', '1:27 PM', '12:33 pm', 0, 0, 0, '0', '14', '', 0),
(527, '25', '2014-07-20', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(528, '25', '2014-07-21', '7:49AM', '05:17 pm', '1:27 PM', '12:39PM', 0, 0, 0, '0', '14', '', 0),
(529, '25', '2014-07-22', '08:08am', '05:15 pm', '1:28 PM', '12:37pm', 30, 0, 0, '0', '14', '', 8),
(530, '25', '2014-07-23', '7:47AM', '05:18 pm', '1:25 PM', '12:33PM', 0, 0, 0, '0', '14', '', 0),
(531, '25', '2014-07-24', '7:51AM', '05:35 pm', '1:29 PM', '12:34 pm', 0, 0, 0, '0', '14', '', 0),
(532, '25', '2014-07-25', '8:13AM', '05:11 pm', '1:25 PM', '12:41pm', 30, 0, 0, '0', '14', '', 13),
(533, '25', '2014-07-11', '7:47AM', '5:00pm', '01:28 pm', '12:33PM', 0, 0, 0, '0', '15', '', 0),
(534, '25', '2014-07-12', '07:45 am', '05:11 pm', '01:25 pm', '12:35 pm', 0, 0, 0, '0', '15', '', 0),
(535, '25', '2014-07-13', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(536, '25', '2014-07-14', '7:46AM', '05:10 pm', '01:29 pm', '12:31pm', 0, 0, 0, '0', '15', '', 0),
(537, '25', '2014-07-15', '07:50 am', '05:17 pm', '1:28 PM', '12:34PM', 0, 0, 0, '0', '15', '', 0),
(538, '25', '2014-07-16', '8:00AM', '5:00pm', '1:30PM', '12:30pm', 0, 0, 0, '0', '15', '', 0),
(539, '25', '2014-07-17', '7:44AM', '05:08 pm', '1:30PM', '12:30PM', 0, 0, 0, '0', '15', '', 0),
(540, '25', '2014-07-18', '7:52AM', '05:12 pm', '1:26 PM', '12:33 pm', 0, 0, 0, '0', '15', '', 0),
(541, '25', '2014-07-19', '7:56AM', '5:06PM', '1:28 PM', '12:31pm', 0, 0, 0, '0', '15', '', 0),
(542, '25', '2014-07-20', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(543, '25', '2014-07-21', '8:14AM', '05:15 pm', '1:27 PM', '12:33PM', 30, 0, 0, '0', '15', '', 14),
(544, '25', '2014-07-22', '8:18AM', '05:12 pm', '1:29 PM', '12:33 pm', 60, 0, 0, '0', '15', '', 18),
(545, '25', '2014-07-23', '8:44AM', '05:15 pm', '1:26 PM', '12:32PM', 120, 0, 0, '0', '15', '', 44),
(546, '25', '2014-07-24', '7:57AM', '05:07 pm', '1:29 PM', '12:35pm', 0, 0, 0, '0', '15', '', 0),
(547, '25', '2014-07-25', '7:47AM', '05:10 pm', '1:29 pm', '12:40pm', 0, 0, 0, '0', '15', '', 0),
(548, '26', '2014-07-26', '7:35AM ', '5:07pm', '1:25PM ', '12:33PM ', 0, 0, 0, '0', '15', '', 0),
(549, '26', '2014-07-27', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(550, '26', '2014-07-28', '8:45am', '5:12pm', '1:28pm', '12:40pm', 120, 0, 0, '0', '15', '', 45),
(551, '26', '2014-07-29', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(552, '26', '2014-07-30', '', '', '', '', 0, 0, 0, '0', '15', 'Absent', 0),
(553, '26', '2014-07-31', '8:10am', '5:00pm', '1:29pm', '12:34pm', 30, 0, 0, '0', '15', '', 10),
(554, '26', '2014-08-01', '8:19am', '5:10pm', '1:28pm', '12:42pm', 60, 0, 0, '0', '15', '', 19),
(555, '26', '2014-08-02', '7:52AM', '5:08pm', '1:28pm', '12:32pm', 0, 0, 0, '0', '15', '', 0),
(556, '26', '2014-08-03', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(557, '26', '2014-08-04', '7:58am', '5:11pm', '1:24pm', '12:31pm', 0, 0, 0, '0', '15', '', 0),
(558, '26', '2014-08-05', '7:55am', '5:12pm', '1:29pm', '12:30pm', 0, 0, 0, '0', '15', '', 0),
(559, '26', '2014-08-06', '7:54am', '5:09pm', '1:28pm', '12:35pm', 0, 0, 0, '0', '15', '', 0),
(560, '26', '2014-08-07', '7:51am', '5:07pm', '1:28pm', '12:33pm', 0, 0, 0, '0', '15', '', 0),
(561, '26', '2014-08-08', '7:40am', '5:05pm', '1:28pm', '12:30pm', 0, 0, 0, '0', '15', '', 0),
(562, '26', '2014-08-09', '7:50am', '5:05pm', '1:29pm', '12:33pm', 0, 0, 0, '0', '15', '', 0),
(563, '26', '2014-08-10', '', '', '', '', 0, 0, 0, '0', '15', '', 0),
(564, '26', '2014-07-26', '7:52am', '05:07 pm', '01:25 pm', '12:34 pm', 0, 0, 0, '0', '14', '', 0),
(565, '26', '2014-07-27', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(566, '26', '2014-07-28', '7:51AM', '5:20PM', '1:23 PM', '12:32PM', 0, 0, 0, '0', '14', '', 0),
(567, '26', '2014-07-29', '', '', '', '', 0, 0, 0, '0', '14', 'LH', 0),
(568, '26', '2014-07-30', '7:54AM', '5:14pm', '1:22 PM', '12:33 pm', 0, 0, 0, '0', '14', '', 0),
(569, '26', '2014-07-31', '7:49am', '5:22pm', '1:28pm', '12:32pm', 0, 0, 0, '0', '14', '', 0),
(570, '26', '2014-08-01', '7:57am', '5:09pm', '1:28pm', '12:32pm', 0, 0, 0, '0', '14', '', 0),
(571, '26', '2014-08-02', '7:53am', '5:12pm', '1:28pm', '12:34pm', 0, 0, 0, '0', '14', '', 0),
(572, '26', '2014-08-03', '', '', '', '', 0, 0, 0, '0', '14', '', 0),
(573, '26', '2014-08-04', '7:44am', '5:26pm', '1:26pm', '12:30pm', 0, 0, 0, '0', '14', '', 0),
(574, '26', '2014-08-05', '8:17am', '5:00pm', '1:29pm', '12:31pm', 60, 0, 0, '0', '14', '', 17),
(575, '26', '2014-08-06', '8:15am', '5:10pm', '1:28pm', '12:33pm', 30, 0, 0, '0', '14', '', 15),
(576, '26', '2014-08-07', '8:02am', '5:11pm', '1:26pm', '12:32pm', 0, 0, 0, '0', '14', '', 2),
(577, '26', '2014-08-08', '8:18am', '5:07pm', '1:28pm', '12:31pm', 60, 0, 0, '0', '14', '', 18),
(578, '26', '2014-08-09', '7:57am', '5:06pm', '1:25pm', '12:32pm', 0, 0, 0, '0', '14', '', 0),
(579, '26', '2014-08-10', '', '', '', '', 0, 0, 0, '0', '14', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sched_table`
--

CREATE TABLE IF NOT EXISTS `sched_table` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `DATE` varchar(255) NOT NULL,
  `EMP_ID` varchar(255) NOT NULL,
  `TIMEIN` varchar(255) NOT NULL,
  `LUNCHOUT` varchar(255) NOT NULL,
  `LUNCHIN` varchar(255) NOT NULL,
  `TIMEOUT` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `sched_table`
--

INSERT INTO `sched_table` (`id`, `DATE`, `EMP_ID`, `TIMEIN`, `LUNCHOUT`, `LUNCHIN`, `TIMEOUT`) VALUES
(1, '', '', '9:00 AM', '11:00 AM', '12:00 PM', '6:00 PM'),
(2, '', '', '2/14/2014', 'EMP_1', '9:00 AM', '11:00 AM'),
(3, '', '', '2/14/2014', 'EMP_1', '9:00 AM', '11:00 AM'),
(4, '2/14/2014', 'EMP_1', '9:00 AM', '11:00 AM', '12:00 PM', '6:00 PM'),
(5, '2/14/2014', 'EMP_2', '9:00 AM', '11:00 AM', '12:00 PM', '6:00 PM'),
(6, '2/14/2014', 'EMP_1', '9:30 AM', '11:00 AM', '12:00 PM', '6:00 PM'),
(7, '2/14/2014', 'EMP_3', '9:30 AM', '11:00 AM', '12:00 PM', '6:00 PM'),
(8, '2/14/2014', 'EMP_5', '9:30 AM', '11:00 AM', '12:00 PM', '6:00 PM');

-- --------------------------------------------------------

--
-- Table structure for table `securitygroups`
--

CREATE TABLE IF NOT EXISTS `securitygroups` (
  `secroleid` int(11) NOT NULL DEFAULT '0',
  `tokenid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`secroleid`,`tokenid`),
  KEY `secroleid` (`secroleid`),
  KEY `tokenid` (`tokenid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `securitygroups`
--

INSERT INTO `securitygroups` (`secroleid`, `tokenid`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(2, 11),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 11),
(4, 1),
(4, 2),
(4, 5),
(5, 1),
(5, 2),
(5, 3),
(5, 11),
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(6, 6),
(6, 7),
(6, 8),
(6, 9),
(6, 10),
(6, 11),
(7, 1),
(8, 1),
(8, 2),
(8, 3),
(8, 4),
(8, 5),
(8, 6),
(8, 7),
(8, 8),
(8, 9),
(8, 10),
(8, 11),
(8, 12),
(8, 13),
(8, 14),
(8, 15);

-- --------------------------------------------------------

--
-- Table structure for table `securityroles`
--

CREATE TABLE IF NOT EXISTS `securityroles` (
  `secroleid` int(11) NOT NULL AUTO_INCREMENT,
  `secrolename` text NOT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`secroleid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `securityroles`
--

INSERT INTO `securityroles` (`secroleid`, `secrolename`, `active`) VALUES
(1, 'Inquiries/Order Entry', b'0'),
(2, 'Manufac/Stock Admin', b'0'),
(3, 'Purchasing Officer', b'0'),
(4, 'AP Clerk', b'0'),
(5, 'AR Clerk', b'0'),
(6, 'Accountant', b'0'),
(7, 'Customer Log On Only', b'0'),
(8, 'System Administrator', b'1'),
(9, 'Human Resources', b'1'),
(10, 'Payroll', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `securitytokens`
--

CREATE TABLE IF NOT EXISTS `securitytokens` (
  `tokenid` int(11) NOT NULL DEFAULT '0',
  `tokenname` text NOT NULL,
  PRIMARY KEY (`tokenid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `securitytokens`
--

INSERT INTO `securitytokens` (`tokenid`, `tokenname`) VALUES
(1, 'Order Entry/Inquiries customer access only'),
(2, 'Basic Reports and Inquiries with selection options'),
(3, 'Credit notes and AR management'),
(4, 'Purchasing data/PO Entry/Reorder Levels'),
(5, 'Accounts Payable'),
(6, 'Not Used'),
(7, 'Bank Reconciliations'),
(8, 'General ledger reports/inquiries'),
(9, 'Not Used'),
(10, 'General Ledger Maintenance, stock valuation & Configuration'),
(11, 'Inventory Management and Pricing'),
(12, 'Unknown'),
(13, 'Unknown'),
(14, 'Unknown'),
(15, 'User Management and System Administration');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE IF NOT EXISTS `uploads` (
  `image` varchar(255) NOT NULL DEFAULT 'default.png',
  `empid` varchar(10) NOT NULL DEFAULT '0',
  UNIQUE KEY `empid` (`empid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`image`, `empid`) VALUES
('Sunset.jpg', '1'),
('SpiderMan.jpg', '15'),
('17709.jpg', '16'),
('17894.bmp', '20'),
('Winter.jpg', '22');

-- --------------------------------------------------------

--
-- Table structure for table `workcentres`
--

CREATE TABLE IF NOT EXISTS `workcentres` (
  `code` char(5) NOT NULL DEFAULT '',
  `location` char(5) NOT NULL DEFAULT '',
  `description` char(20) NOT NULL DEFAULT '',
  `capacity` double NOT NULL DEFAULT '1',
  `overheadperhour` decimal(10,0) NOT NULL DEFAULT '0',
  `overheadrecoveryact` int(11) NOT NULL DEFAULT '0',
  `setuphrs` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workcentres`
--

INSERT INTO `workcentres` (`code`, `location`, `description`, `capacity`, `overheadperhour`, `overheadrecoveryact`, `setuphrs`) VALUES
('ACT', '', 'Accounting', 1, 50, 560000, 0),
('ASS', 'TOR', 'Assembly', 1, 50, 560000, 0),
('EDP', '', 'EDP', 1, 50, 560000, 0),
('FIN', '', 'Finishing', 1, 50, 560000, 0),
('MAR', '', 'Marketing', 1, 50, 560000, 0),
('QA', '', 'Quality Control', 1, 50, 560000, 0),
('SAL', '', 'Sales', 1, 50, 560000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `www_users`
--

CREATE TABLE IF NOT EXISTS `www_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` text NOT NULL,
  `realname` varchar(35) NOT NULL DEFAULT '',
  `userid` varchar(255) NOT NULL,
  `customerid` varchar(10) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(55) DEFAULT NULL,
  `defaultlocation` varchar(5) NOT NULL DEFAULT '',
  `fullaccess` int(11) NOT NULL DEFAULT '1',
  `lastvisitdate` datetime DEFAULT NULL,
  `branchcode` varchar(10) NOT NULL DEFAULT '',
  `pagesize` varchar(20) NOT NULL DEFAULT 'A4',
  `modulesallowed` varchar(20) NOT NULL DEFAULT '',
  `blocked` tinyint(4) NOT NULL DEFAULT '0',
  `displayrecordsmax` int(11) NOT NULL DEFAULT '0',
  `theme` varchar(30) NOT NULL DEFAULT 'fresh',
  `language` varchar(5) NOT NULL DEFAULT 'en_GB',
  `usergroupid` int(11) NOT NULL DEFAULT '8',
  `active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `CustomerID` (`customerid`),
  KEY `DefaultLocation` (`defaultlocation`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `www_users`
--

INSERT INTO `www_users` (`id`, `password`, `realname`, `userid`, `customerid`, `phone`, `email`, `defaultlocation`, `fullaccess`, `lastvisitdate`, `branchcode`, `pagesize`, `modulesallowed`, `blocked`, `displayrecordsmax`, `theme`, `language`, `usergroupid`, `active`) VALUES
(1, '57b2ad99044d337197c0c39fd3823568ff81e48a', 'cristell reyes', 'cristell', '', '', '', '', 8, '2014-03-10 20:44:19', '', 'A4', '1,1,1,1,1,1,1,1,1,1,', 0, 50, 'professional', 'en_GB', 9, b'1'),
(2, '57b2ad99044d337197c0c39fd3823568ff81e48a', 'Demo User', 'demo', '', '', '', '', 8, '2014-10-11 04:55:58', '', 'A4', '1,1,1,1,1,1,1,1,1,1,', 0, 50, 'professional', 'en_GB', 8, b'1'),
(3, '57b2ad99044d337197c0c39fd3823568ff81e48a', 'Mina Escurel', 'mina', '', '', '', '', 8, '2014-03-03 06:22:11', '', 'A4', '1,1,1,1,1,1,1,1,1,1,', 0, 50, 'professional', 'en_GB', 10, b'1'),
(4, '57b2ad99044d337197c0c39fd3823568ff81e48a', 'sherry', 'sherry', '', '', '', '', 8, '2014-04-21 03:44:01', '', 'A4', '1,1,1,1,1,1,1,1,1,1,', 0, 50, 'professional', 'en_GB', 8, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `www_users_images`
--

CREATE TABLE IF NOT EXISTS `www_users_images` (
  `image` varchar(255) NOT NULL DEFAULT 'default.png',
  `userid` varchar(10) NOT NULL DEFAULT '0',
  UNIQUE KEY `empid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `securitygroups`
--
ALTER TABLE `securitygroups`
  ADD CONSTRAINT `securitygroups_secroleid_fk` FOREIGN KEY (`secroleid`) REFERENCES `securityroles` (`secroleid`),
  ADD CONSTRAINT `securitygroups_tokenid_fk` FOREIGN KEY (`tokenid`) REFERENCES `securitytokens` (`tokenid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
