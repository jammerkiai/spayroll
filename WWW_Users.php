<?php
/*Modified By: ME*/
$PageSecurity = 15;

include('includes/session.inc');

$title = _('User Maintenance');

include('includes/header.inc');
include('includes/footer.inc');
include('includes/SQL_CommonFunctions.inc');
include('includes/prlFunctions.php');

if (isset($_GET['userid'])){
	$userid = $_GET['userid'];
} elseif (isset($_POST['userid'])){
	
	$userid = $_POST['userid'];
} else {
	unset($userid);
}


if (isset($_POST['submit'])) {

	$InputError = 0;

	if (strpos($_POST['realname'],'&')>0 OR strpos($_POST['realname'],"'")>0) {
		$InputError = 1;
		prnMsg( _('The User cannot contain the character') . " '&' " . _('or the character') ." '",'error');
	}
	if (trim($_POST['realname']) == '') {
		$InputError = 1;
		prnMsg( _('The User may not be empty'), 'error');
	}
	
	
	if ($InputError !=1) {
	
			if (!isset($_POST['New'])) {
			
			
			$sql = "UPDATE www_users SET realname='" . DB_escape_string($_POST['realname']) . "' ,
						password = '" . CryptPass($_POST['password']) ."'
						WHERE userid = '$userid'";

			$ErrMsg = _('The user could not be updated because');
			$DbgMsg = _('The SQL that was used to update the user but failed was');
			$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);
			prnMsg(_('The user master record for') . ' ' . $userid . ' ' . _('has been updated'),'success');

		} else { 

			$sql = "INSERT INTO www_users ( 
							realname,
							password,
							usergroupid,
							userid							
							)
					 VALUES ( 
					 	'" .DB_escape_string($_POST['realname']) . "',
						'" . CryptPass($_POST['password']) ."',
						'" .DB_escape_string($_POST['secroleid']) . "',
						'" .DB_escape_string($_POST['userid']) . "'
						)";

			$ErrMsg = _('The user') . ' ' . $_POST['realname'] . ' ' . _('could not be added because');
			$DbgMsg = _('The SQL that was used to insert the user but failed was');
			$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);

			prnMsg(_('A new user for') . ' ' . $_POST['realname'] . ' ' . _('has been added to the database'),'success');

			unset ($userid);
			unset($_POST['realname']);
			

		}
		
	} else {

		prnMsg(_('Validation failed') . _('no updates or deletes took place'),'warn');

	}

} elseif (isset($_POST['delete']) AND $_POST['delete'] != '') {



	$CancelDelete = 0;


	if ($CancelDelete == 0) {
		$sql="DELETE FROM www_users WHERE userid='$userid'";
		$result = DB_query($sql, $db);
		prnMsg(_('User record for') . ' ' . $userid . ' ' . _('has been deleted'),'success');
		unset($userid);
		unset($_SESSION['userid']);
	} 
}


if (!isset($userid)) {

	echo '<div id="content"><br/><div align="left" class="subheader"><a href="index.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp;User Account</div>';
	echo "<FORM METHOD='post' ACTION='" . $_SERVER['PHP_SELF'] . "?" . SID . "'>";

	echo "<INPUT TYPE='hidden' NAME='New' VALUE='Yes'>";

	echo '<CENTER><br /><TABLE class="jinnertable">';
	echo '<TR><TD class="tableheader">' . _('User ID') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='userid' SIZE=41 MAXLENGTH=40></TD></TR>";
	echo '<TR><TD class="tableheader">' . _('Name') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='realname' SIZE=41 MAXLENGTH=40></TD></TR>";
	echo '<TR><TD class="tableheader">' . _('Password') . ":</TD><TD><INPUT TYPE='password' class='intext' NAME='password' SIZE=41 MAXLENGTH=8></TD></TR>";
	echo "<tr><td class='tableheader'>User Group:</td><td><SELECT class='intext' NAME='secroleid'>";
				 DB_data_seek($result, 0);
				$sqlUG = 'SELECT secroleid, secrolename FROM securityroles WHERE Active = 1';
				$resultUG = DB_query($sqlUG, $db);
				while ($myrowUG = DB_fetch_array($resultUG)) {
					if ($_POST['secroleid'] == $myrowUG['secroleid']){
						echo '<OPTION SELECTED VALUE=' . $myrowUG['secroleid'] . '>' . $myrowUG['secrolename'];
					} else {
						echo '<OPTION VALUE=' . $myrowUG['secroleid'] . '>' . $myrowUG['secrolename'];
					}
				}
						
		echo "</td></tr>";
	
	echo "</TABLE><p><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Insert New User') . "'><br />";
	echo '</FORM>';
	
		$sql = "SELECT userid,
			realname			
			FROM www_users
			ORDER BY userid";

	$ErrMsg = _('Could not get user because');
	$result = DB_query($sql,$db,$ErrMsg);
	

	echo '<CENTER><table border=0 width="70%" class="jinnertable">';
	echo "<tr>
		<td class='tableheader'>" . _('User ID') . "</td>
		<td class='tableheader'>" . _('User') . "</td>
	
		<td class='tableheader' colspan='2'>Action</td>
	</tr>";

		
	$k=0; 
	while ($myrow = DB_fetch_row($result)) {

		if ($k==1){
			echo "<TR>";
			$k=0;
		} else {
			echo "<TR>";
			$k++;
		}
		echo '<TD>' . $myrow[0] . '</TD>';
		echo '<TD>' . $myrow[1] . '</TD>';
		
		echo '<TD><A HREF="' . $_SERVER['PHP_SELF'] . '?' . SID . '&userid=' . $myrow[0] . '">' . _('Edit') . '</A></TD>';
		echo '<TD><A HREF="' . $_SERVER['PHP_SELF'] . '?' . SID . '&userid=' . $myrow[0] . '&delete=1">' . _('Delete') .'</A></TD>';
		echo '</TR>';

	} 
	echo '</table></CENTER><p>';


} else {
	echo '<div id="content"><br/><div align="left" class="subheader"><a href="WWW_Users.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp;User Account</div>';
	echo "<FORM METHOD='post' ACTION='" . $_SERVER['PHP_SELF'] . "?" . SID . "'>";
	echo '<CENTER><br /><TABLE class="jinnertable">';

	
	if (!isset($_POST['New'])) {
		$sql = "SELECT userid, 
				realname, password
			FROM www_users 
			WHERE userid = '$userid'";
				  
		$result = DB_query($sql, $db);
		$myrow = DB_fetch_array($result);
		
		$_POST['realname']  = $myrow['realname'];
		$_POST['password']  = $myrow['password'];
		echo "<INPUT TYPE=HIDDEN NAME='userid' VALUE='$userid'>";

	} else {
	
		echo "<INPUT TYPE=HIDDEN NAME='New' VALUE='Yes'>";
		echo '<TR><TD class="tableheader">' . _('ID') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='userid' VALUE='$userid' SIZE=5 MAXLENGTH=4></TD></TR>";
	}
	echo "<TR><TD class='tableheader'>" . _('User') . ':' . "</TD><TD><input type='Text' class='intext' name='realname' SIZE=41 MAXLENGTH=40 value='" . $_POST['realname'] . "'></TD></TR>";
	echo "<TR><TD class='tableheader'>" . _('Password') . ':' . "</TD><TD><input type='password' class='intext' name='password' SIZE=41 MAXLENGTH=8 value='" . $_POST['password'] . "'></TD></TR>";
	
	
	
	
	if (isset($_POST['New'])) {
		echo "</TABLE><P><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Add These New User Record') . "'></FORM>";
	} else {
		echo "</TABLE><P><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Update User Record') . "'>";
		echo '<P><FONT COLOR=red><B>' . _('WARNING') . ': ' . _('There is no second warning if you hit the delete button below') . '. ' . _('However checks will be made to ensure before the deletion is processed') . '<BR></FONT></B>';
		echo "<INPUT TYPE='Submit' class='jinnerbot' NAME='delete' VALUE='" . _('Delete this record') . "' onclick=\"return confirm('" . _('Are you sure you wish to delete this user record?') . "');\"></FORM>";
	}

} 


?>