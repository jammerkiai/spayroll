<?php
/* $Revision: 1.0 $ */

$PageSecurity = 15;

include('includes/session.inc');

$title = _('Holiday Section');

include('includes/header.inc');
include('includes/footer.inc');
if (isset($_GET['HolidayID'])){
	$HolidayID = $_GET['HolidayID'];
} elseif (isset($_POST['HolidayID'])){
	
	$HolidayID = $_POST['HolidayID'];
} else {
	unset($HolidayID);
}


if (isset($_POST['submit'])) {

	//initialise no input errors assumed initially before we test

	$InputError = 0;

	/* actions to take once the user has clicked the submit button
	ie the page has called itself with some user input */

	//first off validate inputs sensible

	if (strpos($_POST['HolidayDesc'],'&')>0 OR strpos($_POST['HolidayDesc'],"'")>0) {
		$InputError = 1;
		prnMsg( _('The holiday description cannot contain the character') . " '&' " . _('or the character') ." '",'error');
	}
	if (trim($_POST['HolidayDesc']) == '') {
		$InputError = 1;
		prnMsg( _('The holiday description may not be empty'), 'error');
	}
 	
/* 	if (strlen($HolidayID) == 0) {
		$InputError = 1;
		prnMsg(_('The holiday short description cannot be empty'),'error');
	}  */

	if ($InputError !=1) {
	
			if (!isset($_POST['New'])) {

			$sql = "UPDATE prlholidaytable SET 
							holidaydate='" . DB_escape_string($_POST['HolidayDate']) . "', 
							holidayshortdesc='" . DB_escape_string($_POST['HolidaySDesc']) . "',
							holidaydesc='" . DB_escape_string($_POST['HolidayDesc']) . "', 
							holidayrate='" . DB_escape_string($_POST['HolidayRate']) . "' 
						WHERE id = '$HolidayID'";

			$ErrMsg = _('The holiday could not be updated because');
			$DbgMsg = _('The SQL that was used to update the holiday but failed was');
			$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);
			prnMsg(_('The holiday master record for') . ' ' . $HolidayID . ' ' . _('has been updated'),'success');

		} else { //its a new overtime

			$sql = "INSERT INTO prlholidaytable (holidaydate,
							holidayshortdesc, 
							holidaydesc, 
							holidayrate)
					 VALUES ('" .DB_escape_string($_POST['HolidayDate']) . "',
						'" .DB_escape_string($_POST['HolidaySDesc']) . "', 
					 	'" .DB_escape_string($_POST['HolidayDesc']) . "', 
						'" . DB_escape_string($_POST['HolidayRate']) . "')";

			$ErrMsg = _('The holiday') . ' ' . $_POST['HolidayDesc'] . ' ' . _('could not be added because');
			$DbgMsg = _('The SQL that was used to insert the holiday but failed was');
			$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);

			prnMsg(_('A new holiday for') . ' ' . $_POST['HolidayDesc'] . ' ' . _('has been added to the database'),'success');

			unset($_POST['HolidayDate']);
			unset($_POST['HolidaySDesc']);
			unset($_POST['HolidayDesc']);
			unset($_POST['HolidayRate']);

		}
		
	} else {

		prnMsg(_('Validation failed') . _('no updates or deletes took place'),'warn');

	}

} elseif (isset($_POST['delete']) AND $_POST['delete'] != '') {

//the link to delete a selected record was clicked instead of the submit button

	$CancelDelete = 0;

// PREVENT DELETES IF DEPENDENT RECORDS IN 'SuppTrans' , PurchOrders, SupplierContacts
	if ($CancelDelete == 0) {
		$sql="DELETE FROM prlholidaytable WHERE id='$HolidayID'";
		$result = DB_query($sql, $db);
		prnMsg(_('Holiday record for') . ' ' . $HolidayID . ' ' . _('has been deleted'),'success');
		unset($HolidayID);
		unset($_SESSION['HolidayID']);
	} //end if Delete paypayperiod
}


if (!isset($HolidayID)) {

/*If the page was called without $SupplierID passed to page then assume a new supplier is to be entered show a form with a Supplier Code field other wise the form showing the fields with the existing entries against the supplier will show for editing with only a hidden SupplierID field*/
	echo '<div id="content"><br/><div align="left" class="subheader"><a href="index.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp;Holiday Table</div>';
	echo "<FORM METHOD='post' ACTION='" . $_SERVER['PHP_SELF'] . "?" . SID . "'>";

	echo "<INPUT TYPE='hidden' NAME='New' VALUE='Yes'>";

	echo '<CENTER><br /><TABLE class="jinnertable">';
	echo '<TR><TD class="tableheader">' . _('Holiday Date') . ":</TD><TD><INPUT TYPE='text' class='intext datepicker' NAME='HolidayDate' SIZE=15 MAXLENGTH=15></TD></TR>";
	echo '<TR><TD class="tableheader">' . _('Holiday Type') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='HolidaySDesc' SIZE=5 MAXLENGTH=4></TD></TR>";
	echo '<TR><TD class="tableheader">' . _('Holiday Description') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='HolidayDesc' SIZE=41 MAXLENGTH=40></TD></TR>";
	echo '<TR><TD class="tableheader">' . _('Holiday Rate') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='HolidayRate' SIZE=7 MAXLENGTH=6></TD></TR>";
//	echo '</SELECT></TD></TR>';
	echo "</SELECT></TD></TR></TABLE><p><CENTER><INPUT class='jinnerbot' TYPE='Submit' NAME='submit' VALUE='" . _('Insert New Holiday') . "'><br/>";
	echo '</FORM>';
	
		$sql = "SELECT id,
			holidaydate,
			holidayshortdesc,
			holidaydesc,
			holidayrate
			FROM prlholidaytable
			ORDER BY holidaydate ASC";

	$ErrMsg = _('Could not get overtime because');
	$result = DB_query($sql,$db,$ErrMsg);
	
	echo '<CENTER><table class="jinnertable" width="70%" border="0">';
	echo "<tr>
		<td class='tableheader'>" . _('Holiday Date') . "</td>
		<td class='tableheader'>" . _('Holiday Type') . "</td>
		<td class='tableheader'>" . _('Holiday Description') . "</td>
		<td class='tableheader'>" . _('Holiday Rate') . "</td>
		<td class='tableheader' colspan='2'>Action</td>
	</tr>";

		
	$k=0; //row colour counter
	while ($myrow = DB_fetch_row($result)) {

		if ($k==1){
			echo "<TR>";
			$k=0;
		} else {
			echo "<TR>";
			$k++;
		}
		echo '<TD>' . $myrow[1] . '</TD>';
		echo '<TD>' . $myrow[2] . '</TD>';
		echo '<TD>' . $myrow[3] . '</TD>';
		echo '<TD>' . $myrow[4] . '</TD>';
		echo '<TD><A HREF="' . $_SERVER['PHP_SELF'] . '?' . SID . '&HolidayID=' . $myrow[0] . '">' . _('Edit') . '</A></TD>';
		echo '<TD><A HREF="' . $_SERVER['PHP_SELF'] . '?' . SID . '&HolidayID=' . $myrow[0] . '&delete=1">' . _('Delete') .'</A></TD>';
		echo '</TR>';

	} //END WHILE LIST LOOP
	echo '</table></CENTER><p>';


} else {
//OverTimeID exists - either passed when calling the form or from the form itself
	echo '<div id="content"><br/><div align="left" class="subheader"><a href="prlHoliday_table.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp;Edit Holiday Table</div>';
	echo "<FORM METHOD='post' ACTION='" . $_SERVER['PHP_SELF'] . "?" . SID . "'>";
	echo '<CENTER><br/><TABLE class="jinnertable">';

	//if (!isset($_POST['New'])) {
	if (!isset($_POST['New'])) {
		$sql = "SELECT id,
				holidaydate,
				holidayshortdesc, 
				holidaydesc, 
				holidayrate
			FROM prlholidaytable 
			WHERE id = '$HolidayID'";
				  
		$result = DB_query($sql, $db);
		$myrow = DB_fetch_array($result);
		
		$_POST['HolidayDate']  = $myrow['holidaydate'];
		$_POST['HolidaySDesc']  = $myrow['holidayshortdesc'];
		$_POST['HolidayDesc']  = $myrow['holidaydesc'];
		$_POST['HolidayRate']  = $myrow['holidayrate'];
		echo "<INPUT TYPE=HIDDEN NAME='HolidayID' VALUE='$HolidayID'>";

	} else {
	// its a new overtime being added
		echo "<INPUT TYPE=HIDDEN NAME='New' VALUE='Yes'>";
		echo '<TR><TD class="tableheader">' . _('Holiday Type') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='HolidayID' VALUE='$HolidayID' SIZE=5 MAXLENGTH=4></TD></TR>";
	}
	echo "<TR><TD class='tableheader'>" . _('Holiday Date') . ':' . "</TD><TD><input type='Text' class='intext datepicker' name='HolidayDate' SIZE=15 MAXLENGTH=15 value='" . $_POST['HolidayDate'] . "'></TD></TR>";
	echo "<TR><TD class='tableheader'>" . _('Holiday Type') . ':' . "</TD><TD><input type='Text' class='intext' name='HolidaySDesc' SIZE=41 MAXLENGTH=40 value='" . $_POST['HolidaySDesc'] . "'></TD></TR>";
	echo "<TR><TD class='tableheader'>" . _('Holiday Description') . ':' . "</TD><TD><input type='Text' class='intext' name='HolidayDesc' SIZE=41 MAXLENGTH=40 value='" . $_POST['HolidayDesc'] . "'></TD></TR>";
	echo "<TR><TD class='tableheader'>" . _('Holiday Rate') . ':' . "</TD><TD><input type='Text' class='intext' name='HolidayRate' SIZE=4 MAXLENGTH=6 value='" . $_POST['HolidayRate'] . "'></TD></TR>";
	echo '</SELECT></TD></TR>';

	if (isset($_POST['New'])) {
		echo "</TABLE><P><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Add These New holiday Details') . "'></FORM>";
	} else {
		echo "</TABLE><P><CENTER><INPUT TYPE='Submit' class='jinnerbot'NAME='submit' VALUE='" . _('Update holiday') . "'>";
		echo '<P><FONT COLOR=red><B>' . _('WARNING') . ': ' . _('There is no second warning if you hit the delete button below') . '. ' . _('However checks will be made to ensure before the deletion is processed') . '<BR></FONT></B>';
		echo "<br/><INPUT TYPE='Submit' class='jinnerbot' NAME='delete' VALUE='" . _('Delete holiday') . "' onclick=\"return confirm('" . _('Are you sure you wish to delete this holiday?') . "');\"></FORM>";
	}

} // end of main ifs


?>