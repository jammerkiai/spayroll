<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
	<script type="text/javascript" src="jquery-ui-timepicker-addon.js"></script>
	<link rel="stylesheet" href="/resources/demos/style.css">
	
	
	<script>
	$(function() {
		$( "#datepicker" ).datepicker();
		$( ".jtime" ).timepicker({
			timeFormat: "hh:mm tt"
		});
	</script>
	
	
<style>
	.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 45%; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

.ui-timepicker-rtl{ direction: rtl; }
.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
.ui-timepicker-rtl dl dt{ float: right; clear: right; }
.ui-timepicker-rtl dl dd { margin: 0 45% 10px 10px; }
.ui-widget{font-size:12px !important;}
	
</style>
	
	

</head>
<?php
include('includes/session.inc');

$title = _('Time Update');

include('includes/header.inc');
?>
<body>

<center> <br /> </center>
	
				<form name="x" action="" method="GET">
				
					<select class='intext' name="payroll">
					<option selected value="">Select Payroll</option>
					<?php
						DB_data_seek($result6, 0);
						$sql6 = 'SELECT payrollid,payrolldesc FROM  prlpayrollperiod ORDER BY payrollid ASC';
						$result6 = DB_query($sql6, $db);
						
						while ($myrow6 = DB_fetch_array($result6)) 
						{
							?>  	  
							<option value="<?php echo $myrow6['payrollid']; ?>"><?php echo $myrow6['payrollid'] . ' - ' . $myrow6['payrolldesc']; ?></option>
						<?php  }

					?>
					</select>
				
					<select class='intext' name="emp_id" onchange="this.form.submit();">
					<option selected value="">Select Employee</option>
					<?php
						DB_data_seek($result, 0);
						$sql = 'SELECT employeeid,lastname,firstname FROM  prlemployeemaster ORDER BY lastname ASC';
						$result = DB_query($sql, $db);
						
						while ($myrow = DB_fetch_array($result)) 
						{
							?>  	  
							<option value="<?php echo $myrow['employeeid']; ?>"><?php echo $myrow['lastname'] . ', ' . $myrow['firstname']; ?></option>
						<?php  }

					?>
					</select>
				</form>

<table class="jinnertable" width="100%" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td class='tableheader' width="24%"></th>Attendance ID:
    <td width="25%">&nbsp;</th>
    <td class='tableheader' width="24%">Company:<td width="27%">&nbsp;</th>
  </tr>
  <tr>
    <td class='tableheader'>Employee ID:</td>
    <td>
	<?php
	  if(isset($_GET['emp_id'])){
	  echo $_GET['emp_id'];
	  } ?>
	</td>
    <td class='tableheader'>Department:</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class='tableheader'>Employee Name:</td>
    <td>
	<?php
	  if(isset($_GET['emp_id'])){
		$res = $_GET['emp_id'];
		
		DB_data_seek($result3, 0);
		$sql3 = 'SELECT CONCAT(lastname, ", ", firstname) as employeename FROM  prlemployeemaster WHERE employeeid = "'. $_GET['emp_id'] .'"';
		$result3= DB_query($sql3, $db);
		
		$employee_name = DB_fetch_array($result3);
	?>
		<?php echo $employee_name['employeename']; ?>
	<?php } ?>	
	</td>
    <td class='tableheader'>Cost Center:</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
		<?php		
		DB_data_seek($result7, 0);
		$sql7 = 'SELECT CONCAT(startdate, " to ",enddate) as period,
							startdate,
							enddate
				FROM 
				prlpayrollperiod 
                WHERE payrollid = "' . $_GET['payroll'] .'"';
		$result7= DB_query($sql7, $db);
		$myrow7 = DB_fetch_array($result7);

		$startdate = $myrow7['startdate'];
		$enddate = $myrow7['enddate'];
		
		//echo 'startdate'. $startdate .'<br>';
		//echo 'enddate'. $enddate .'<br>';
		
		
		$date1 = new DateTime($startdate);
		$date2 = new DateTime($enddate);
		$interval = $date1->diff($date2);
		// echo $interval->d." days ";
		$day_range = $interval->d;
		
		
		//for displaying date loop
		$start2 = $startdate; //start date
		$end2 = $enddate; //end date

		$dates = array();
		$start2 = $current = strtotime($start2);
		$end2 = strtotime($end2);

		while ($current <= $end2) {
			$dates[] = date('Y-m-d', $current);
			$current = strtotime('+1 days', $current);

		}
	?>	
    <td class='tableheader'>Period:</td>
    <td><?php  echo $myrow7['period']; ?></td>
    <td class='tableheader'>Sub Cost Center:</td>
    <td>&nbsp;</td>
  </tr>
</table>
<center> <br /> </center>
<table class="jinnertable" width="100%" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td class='tableheader' height="32" colspan="4" height="29"><div align="center">ATTENDANCE SUMMARY</div></td>
  </tr>
  <tr>
    <td class='tableheader' width="24%" height="29">Total Tardiness:</td>
    <td width="25%">	
	<?php 
	DB_data_seek($result_total_late, 0);
		$sql_total_late = 'SELECT 
				SUM(LATE) as total_late
				FROM 
				prltimeentry
                WHERE employeeid = "' . $_GET['emp_id'] .'" AND day BETWEEN "' . $startdate . '" AND "' . $enddate . '"';
		$result_total_late = DB_query($sql_total_late, $db);			
		$myrow_total_late = DB_fetch_array($result_total_late);
		
		$total_tardiness = $myrow_total_late['total_late'];		
		
			$hours = intval($total_tardiness/60);
			$minutes = $total_tardiness - ($hours * 60);
		echo $hours . ' Hour(s)' . ' and ' . $minutes . ' Minute(s)';
		
		
	?>
	</td>
    <td class='tableheader' width="24%">Tardiness Frequency:</td>
    <td width="27%">&nbsp;</td>
  </tr>
  <tr>
    <td class='tableheader' height="28">Approved OT (hrs):</td>
    <td>&nbsp;</td>
    <td class='tableheader' >AWOL Frequency:</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class='tableheader' height="28">Total Undertime a(hrs.):</td>
    <td>&nbsp;</td>
    <td colspan="2" rowspan="2">&nbsp;</td>
  </tr>
</table>
<center> <br /> </center>
<table class="jinnertable" width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
  	
  <tr>
    <td class='tableheader' width="12%" scope="col"><div align="center">DATE</div></td>
    <td class='tableheader' width="20%" scope="col"><div align="center">WORK SCHEDULE</div></td>
    <td class='tableheader' width="12%" scope="col"><div align="center">TIME IN</div></td>
    <td class='tableheader' width="12%" scope="col"><div align="center">LUNCH TIME IN</div></td>
    <td class='tableheader' width="12%" scope="col"><div align="center">LUNCH TIME OUT</div></td>
    <td class='tableheader' width="12%" scope="col"><div align="center">TIME OUT</div></td>
	<td class='tableheader' width="10%" scope="col"><div align="center">LATE</div></td>
    <td class='tableheader' width="10%" scope="col"><div align="center">OVERTIME</div></td>
  </tr>
  
  
  <?php

		$action = isset($_POST['action']) ? $_POST['action'] : "";
		
		if($action == "update"){
		
				$sql = "UPDATE prltimeentry SET		
											DAY = '" . $_POST['date'] . "',
											TIMEIN = '" . $_POST['timein'] . "',
											LUNCHIN = '" . $_POST['lunchin'] . "',
											LUNCHOUT = '" . $_POST['lunchout'] . "',
											TIMEOUT = '" . $_POST['timeout'] . "',
											LATE = '" . $_POST['late'] . "',
											OT = '" . $_POST['ot'] . "'
											
					WHERE employeeid = '" . $_GET['emp_id'] ."' AND day BETWEEN '" . $startdate . "' AND '" . $enddate . "'
										";
		}

?>
  
  
  
  
<form action="" method="post">  

  
	<?php
		if(isset($_GET['emp_id'])){
		DB_data_seek($result2, 0);
		$sql2 = 'SELECT 
				CONCAT(timeIn, " to " ,timeOut) as worksched,
				timeIn
				FROM 
				prlemployeemaster 
                WHERE employeeid = "' . $_GET['emp_id'] .'"';
		$result2 = DB_query($sql2, $db);
		$myrow2 = DB_fetch_array($result2);
		
		$sched = $myrow2['worksched'];
		
		DB_data_seek($result_edit, 0);
		$sql_time_edit = 'SELECT 
				*
				FROM 
				prltimeentry
                WHERE employeeid = "' . $_GET['emp_id'] .'" AND day BETWEEN "' . $startdate . '" AND "' . $enddate . '"';
		$result_edit = DB_query($sql_time_edit, $db);
			
		while($myrow_edit = DB_fetch_array($result_edit))
		{
		echo "<tr>
				<td>
				<label>". $myrow_edit['DAY'] ."</label>
				</td>
				<td><label>" . $sched . "</label></td>
				<td><input type='text' name='timein". $loop ."' class='jtime' id='timein". $loop ."' value='". $myrow_edit['TIMEIN'] ."'></td>
				<td><input type='text' name='lunchin". $loop ."' class='jtime' id='lunchin". $loop ."' value='". $myrow_edit['LUNCHIN'] ."'></td>
				<td><input type='text' name='lunchout". $loop ."' class='jtime' id='lunchout". $loop ."' value='". $myrow_edit['LUNCHOUT'] ."'></td>
				<td><input type='text' name='timeout". $loop ."' class='jtime' id='timeout". $loop ."' value='". $myrow_edit['TIMEOUT'] ."'></td>
				<td><label>"; ?>
	
				
	<?php		
				$timein = $myrow_edit['TIMEIN'];
				$timein_sched = $myrow2['timeIn'];
				$to_time = strtotime($timein);
				$from_time = strtotime($timein_sched);
				$late = round(abs($to_time - $from_time) / 60,2);	
				
				$hours = intval($late/60);
				$minutes = $late - ($hours * 60);
				
				if($timein == null){
					echo 'NO TIME IN';
				}
				else if($late >= 60){
					echo $hours . ' Hour(s)' . '<br>' . $minutes . ' Minute(s)';
				}else {
					echo $minutes . ' Minute(s)';
				}
	?>
	<?php		echo"</label>
				</td>
				<td><input type='text' name='ot". $loop ."' id='ot". $loop ."' value='". $myrow_edit['OT'] ."'></td>
			  </tr>";
		}
		}
	?>
  <tr>
	<input type="hidden" name="employeeid" value="<?php echo $_GET['emp_id']; ?>">
	<input type="hidden" name="action" value="update">
    <td><input class="jinnerbot" type="submit" value="Submit"/> 
	<div id="jakeholder"></div>
	<div id="jakeholder2"></div>
	</td>
  </tr>
</form>
</table>

</body>
</html>


