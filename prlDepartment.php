<?php

$PageSecurity = 5;

include('includes/session.inc');

$title = _('Department Maintenance');

include('includes/header.inc');
include('includes/SQL_CommonFunctions.inc');
include('includes/prlFunctions.php');


if (isset($_GET['DepartmentID'])){
	$DepartmentID = strtoupper($_GET['DepartmentID']);
} elseif (isset($_POST['DepartmentID'])){
	$DepartmentID = strtoupper($_POST['DepartmentID']);
} else {
	unset($DepartmentID);
}

if (isset($_POST['submit'])) {

	
	$InputError = 0;

	
       
       if ($_POST[DepartmentName]=="")
       {
           echo "<ul><li>Department Name must not be empty.</li></ul>";
           $InputError=1;	
       }
	   
      
 
	if ($InputError != 1){

	
		if (!isset($_POST['New'])) {
				$sql = "UPDATE prldepartment SET
					departmentName='" . DB_escape_string($_POST['DepartmentName']) . "'
                WHERE departmentid = '$DepartmentID'";
			$ErrMsg = _('The department could not be updated because');
			$DbgMsg = _('The SQL that was used to update the department but failed was');
			$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);
			prnMsg(_('The department master record id') . ' ' . $DepartmentID . ' ' . _('has been updated'),'success');

		} else { 
       			$sql = "INSERT INTO prldepartment (		
							departmentName)
						VALUES ( '" . DB_escape_string($_POST['DepartmentName']) ."'
						)";

			$ErrMsg = _('The department') . ' ' . $_POST['DepartmentName'] . ' ' . _('could not be added because');
			$DbgMsg = _('The SQL that was used to insert the department but failed was');
			$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);
			

			prnMsg(_('The new department is') . ' ' . $_POST['DepartmentName'] . ' ' . _('has been added to the database'),'success');

			unset ($DepartmentID);
			unset($_POST['DepartmentName']);
			unset($_POST['Active']);
		}
		
	} else {

		prnMsg(_('Validation failed') . _('no updates or deletes took place'),'warn');

	}

} elseif (isset($_POST['delete']) AND $_POST['delete'] != '') {

	$CancelDelete = 0;
	
	if ($CancelDelete == 0) {
		$sql="DELETE FROM prldepartment WHERE departmentid='$DepartmentID'";
		$result = DB_query($sql, $db);
		prnMsg(_('Department record id') . ' ' . $DepartmentID . ' ' . _('has been deleted'),'success');
		unset($DepartmentID);
		unset($_SESSION['DepartmentID']);
	} 
} 


if (!isset($DepartmentID)) {

	echo '<div id="content"><br/><div align="left" class="subheader"><a href="prlSelectDepartment.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp; Department</div>';
	echo "<FORM METHOD='post' ACTION='" . $_SERVER['PHP_SELF'] . "?" . SID . "'>";
	echo "<INPUT TYPE='hidden' NAME='New' VALUE='Yes'>";
	echo '<CENTER ><TABLE class="jinnertable">';
	
	echo '<TR><TD class="tableheader" width=200 height=20><div align="right"><b>' . _('Department Name') . ":</TD><TD><input type='Text' class='intext' name='DepartmentName' SIZE=42 MAXLENGTH=40></TD></TR>";
	echo "</SELECT></TD></TR></TABLE><p><CENTER><INPUT class='jinnerbot' TYPE='Submit' NAME='submit' VALUE='" . _('Insert New Department') . "'>";
	echo '</FORM>';

} else {
echo '<div id="content"><br/><div align="left" class="subheader"><a href="prlSelectDepartment.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp; Department</div>';
	echo "<FORM METHOD='post' action='" . $_SERVER['PHP_SELF'] . '?' . SID ."'>";
	echo '<CENTER><br/><TABLE class="jinnertable" >';
		if (!isset($_POST['New'])) {
		$sql = "SELECT  departmentid,
					departmentName
			FROM prldepartment
			WHERE departmentid = '$DepartmentID'";
			$result = DB_query($sql, $db);
			$myrow = DB_fetch_array($result);
		$_POST['DepartmentName'] = $myrow['departmentName'];
			
		
		echo "<INPUT TYPE=HIDDEN NAME='DepartmentID' VALUE='$DepartmentID'>";
		} else {
		
		echo "<INPUT TYPE=HIDDEN NAME='New' VALUE='Yes'>";
		}
		
	echo '<TR><TD class="tableheader" width=200 height=20><div align="right"><b>' . _('Department Name') . ":</TD><TD><input type='Text' class='intext' name='DepartmentName' value='" . $_POST['DepartmentName'] . "' SIZE=42 MAXLENGTH=40></TD></TR>";	
		
	if (isset($_POST['New'])) {
		echo "</TABLE><P><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Add These New Department Details') . "'></FORM>";
	} else {
		echo "</TABLE><P><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Update Department') . "'>";
		echo '<P><FONT COLOR=red><B>' . _('WARNING') . ': ' . _('There is no second warning if you hit the delete button below') . '. ' . _('.') . '<BR></FONT></B>';
		echo "<br/><INPUT class='jinnerbot' TYPE='Submit' NAME='delete' VALUE='" . _('Delete Department') . "' onclick=\"return confirm('" . _('Are you sure you wish to delete this department?') . "');\"></FORM></div>";
	}

} // end of main ifs


?>