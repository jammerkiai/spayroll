<?php
$PageSecurity = 5;
include('includes/session.inc');
$title = _('View Payslip');
include('includes/header.inc');
include('includes/footer.inc');
include('includes/SQL_CommonFunctions.inc');
include('includes/prlFunctions.php');
?>
<script type="text/javascript">     
 function PrintDiv() {    
           var divToPrint = document.getElementById('divToPrint');
           var popupWin = window.open('', '_blank', 'width=300,height=300');
           popupWin.document.open();
           popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
           popupWin.document.close();
           }
</script>


<?php
// for employee id
  DB_data_seek($result_name, 0);
		$sql_name = 'SELECT 
				CONCAT(lastname, ", ", firstname) as NAME,
				position,
				employeeid,
				TimeIn,
				TimeOut,
				CONCAT(TimeIn, " to ", TimeOut) as TIME
				FROM 
				prlemployeemaster
                WHERE departmentid = "' . $_GET['dept_id'] .'"';
		$result_name = DB_query($sql_name, $db);
			
		$myrow_name = DB_fetch_array($result_name);
	
		  $employeeid = $myrow_name['employeeid'];
		  

?>

<?php	
// for start to end date	
		DB_data_seek($result_date, 0);
		$sql_date = 'SELECT CONCAT(startdate, " to ",enddate) as period,
							startdate,
							enddate
				FROM 
				prlpayrollperiod 
                WHERE payrollid = "' . $_GET['payroll'] .'"';
		$result_date= DB_query($sql_date, $db);
		$myrow_date = DB_fetch_array($result_date);

		$startdate = $myrow_date['startdate'];
		$enddate = $myrow_date['enddate'];
		
		//echo 'startdate'. $startdate .'<br>';
		//echo 'enddate'. $enddate .'<br>';
		
		
		$date1 = new DateTime($startdate);
		$date2 = new DateTime($enddate);
		$interval = $date1->diff($date2);
		// echo $interval->d." days ";
		$day_range = $interval->d;
		
		
		//for displaying date loop
		$start2 = $startdate; //start date
		$end2 = $enddate; //end date

		$dates = array();
		$start2 = $current = strtotime($start2);
		$end2 = strtotime($end2);

		while ($current <= $end2) {
			$dates[] = date('Y-m-d', $current);
			$current = strtotime('+1 days', $current);

		}

$loop = 1;
foreach ($dates as $date) {		
			  $loop++;
			}	
			$date_columns_count = count($dates);
?>



<html xmlns="http://www.w3.org/1999/xhtml">
<head></head>
<body>
<div id="content">
<div id="innercon">
<br/><div align="left" class="subheader"><a href="<?php echo $rootpath;?>/index.php?"><img src="images/back.png" width="30" height="30" /></a>&nbsp;&nbsp; View Payslip</div><br/>
 <div>
	   <a class="jinnerbot2" href="prlPayTrans.php">View Payroll Data</a><br />
       <input class="jinnerbot" type="button" value="PRINT PAYSLIP" onclick="PrintDiv();" />
 </div>
 <br />
 <form name="x" action="" method="GET">
					<b>Select Payroll:</b>
					<select class="intext" name="payroll">
					<?php
									DB_data_seek($result_payroll, 0);
									$sql_payroll = 'SELECT payrollid,payrolldesc FROM  prlpayrollperiod
									WHERE payclosed="0" AND payrollid = "' . $_GET['payroll'] .'"';
									$result_payroll = DB_query($sql_payroll, $db);
									$myrow_payroll = DB_fetch_array($result_payroll);
								?>								
								<option selected value=""><?php echo $myrow_payroll['payrollid'] . ' ' . $myrow_payroll['payrolldesc']; ?></option>
					<?php
						DB_data_seek($result6, 0);
						$sql6 = 'SELECT payrollid,payrolldesc FROM  prlpayrollperiod WHERE payclosed="0" ORDER BY payrollid ASC';
						$result6 = DB_query($sql6, $db);
						
						while ($myrow6 = DB_fetch_array($result6)) 
						{
							?>  	  
							<option value="<?php echo $myrow6['payrollid']; ?>"><?php echo $myrow6['payrollid'] . ' - ' . $myrow6['payrolldesc']; ?></option>
						<?php  }

					?>
					</select>
					
					<b>Select Company:</b>
					<select class="intext" name="companyagencyid">
					<?php
									DB_data_seek($result_CAName, 0);
									$sql_CAName = 'SELECT companyagency FROM  companyagency
									WHERE CompanyAgencyID = "' . $_GET['companyagencyid'] .'"';
									$result_CAName = DB_query($sql_CAName, $db);
									$myrow_CAName = DB_fetch_array($result_CAName);
								?>
								<option selected value=""><?php echo $myrow_CAName['companyagency'];  ?></option>
					<?php
						DB_data_seek($result, 0);
						$sqlCA = 'SELECT CompanyAgencyID,companyagency FROM  companyagency ORDER BY companyagency ASC';
						$resultCA = DB_query($sqlCA, $db);
						
						while ($myrowCA = DB_fetch_array($resultCA)) 
						{
							?>  	  
							<option value="<?php echo $myrowCA['CompanyAgencyID']; ?>"><?php echo $myrowCA['companyagency']; ?></option>
						<?php  }

					?>
					</select>
					
					<b>Select Department:</b>
					<select class="intext" name="dept_id" onchange="this.form.submit();">
					<?php
									DB_data_seek($result_deptName, 0);
									$sql_deptName = 'SELECT departmentName FROM  prldepartment
									WHERE departmentid = "' . $_GET['dept_id'] .'"';
									$result_deptName = DB_query($sql_deptName, $db);
									$myrow_deptName = DB_fetch_array($result_deptName);
								?>
								<option selected value=""><?php echo $myrow_deptName['departmentName'];  ?></option>
					<?php
						DB_data_seek($result, 0);
						$sql = 'SELECT departmentID,departmentName FROM  prldepartment ORDER BY departmentName ASC';
						$result = DB_query($sql, $db);
						
						while ($myrow = DB_fetch_array($result)) 
						{
							?>  	  
							<option value="<?php echo $myrow['departmentID']; ?>"><?php echo $myrow['departmentName']; ?></option>
						<?php  }

					?>
					</select>
					
					
	</form>
 <div id="divToPrint" >
  <LINK HREF="/pccdev/css/professional/default.css" REL="stylesheet" TYPE="text/css">        
  <style type="text/css">
.psliptable > tbody > tr > td {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #EBEBEB;
    color: #949494;
    padding:3px !important;
}
  </style>   
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="35" width="100%" align="left" valign="top">
	
<?php 
$res = $_GET['payroll'];
$res2 = $_GET['dept_id'];
$res3 = $_GET['companyagencyid'];

	$sql = "SELECT DISTINCT a.employeeid,UPPER(a.lastname)as lastname,UPPER(a.firstname)as firstname,UPPER(a.middlename)as middlename,a.payperiodid,a.departmentid,a.periodrate,a.active,
			b.payrollid,c.startdate,c.enddate,
			(SELECT COUNT(b.DAY) FROM  prltimeentry b WHERE b.employeeid = a.employeeid AND b.TIMEIN  != '' AND b.TIMEOUT  != ''
			AND b.day BETWEEN '" . $startdate . "' AND '" . $enddate . "') AS no_days,
			(SELECT SUM(b.LATE)
					FROM prltimeentry b
					WHERE b.employeeid = a.employeeid) as Late_min,
			(SELECT COUNT(b.TAGS) 
					FROM  prltimeentry b WHERE b.employeeid = a.employeeid AND b.TAGS = 'SH') as SH,
			dep.departmentName, c.deductsss, c.deducthdmf, c.deductphilhealth, c.deducttax,c.startdate,c.enddate
			FROM prlemployeemaster a
			LEFT JOIN prltimeentry b
			ON a.employeeid = b.employeeid
			LEFT JOIN prldepartment dep
			ON a.departmentid = dep.departmentid
			LEFT JOIN prlpayrollperiod c
			ON b.payrollid = c.payrollid
			WHERE a.active = '1'
			AND a.departmentid = '" . $res2 . "' AND b.payrollid = '" . $res . "' AND a.companyagencyid = '" . $res3 . "'
			AND b.day BETWEEN '" . $startdate . "' AND '" . $enddate . "'
			ORDER BY lastname ASC, firstname ASC
			";
	
	$result = mysql_query($sql);
	$rows[] = array();
	
	$num_rows = mysql_num_rows($result);
	$x = 1;
if($num_rows <> 0){
	while($x <= $num_rows){
	 
		while ( $row = mysql_fetch_assoc($result) ) {
				$rows[] = $row;
				
				$id = $row['employeeid'];
				$name = $row['lastname'] . ', ' . $row['firstname'] . ' ' . $row['middlename'];
				$perioddate = $row['startdate'] . ' to ' . $row['enddate'];
				$dept = $row['departmentName'];
				$dSSS = $row['deductsss'];
				$dHDMF = $row['deducthdmf'];
				$dPH = $row['deductphilhealth'];
				$dTax = $row['deducttax'];
				$no_days = $row['no_days'];
				$isdaily = $row['payperiodid'];
				/* if($row['basic_pay'] == NULL){
					$bp = $row['mbasic_pay'];
				}else{
					$bp = $row['basic_pay'];
				} */
				
				//basic pay
				// DB_data_seek($result_basic_pay, 0);
					// $sql_basic_pay = 'SELECT 
							// periodrate
							// FROM 
							// prlemployeemaster
							// WHERE employeeid = "' . $id .'"';
					// $result_basic_pay = DB_query($sql_basic_pay, $db);			
					// $myrow_basic_pay = DB_fetch_array($result_basic_pay);
					// $bp = $myrow_basic_pay['periodrate'];
					$bp = $row['periodrate'];
				//basic pay end
				
				
				// for OT
				DB_data_seek($result_total_ot, 0);
					$sql_total_ot = 'SELECT 
							SUM(otamount) as total_ot
							FROM 
							prlottrans
							WHERE employeeid = "' . $id .'" AND otdate BETWEEN "' . $startdate . '" AND "' . $enddate . '"';
					$result_total_ot = DB_query($sql_total_ot, $db);			
					$myrow_total_ot = DB_fetch_array($result_total_ot);
				$ot = $myrow_total_ot['total_ot'];
				
				
				// for total late
				$sql_hol_tags = "SELECT holidayshortdesc from prlholidaytable";
				$result_hol_tags = DB_query($sql_hol_tags, $db);
				$array = array();
				while($reg_hol_tags = DB_fetch_array($result_hol_tags)){
				$hol_type_tags = $reg_hol_tags['holidayshortdesc'];
				
				$sql_hol_late = "SELECT * from prltimeentry WHERE payrollid = '".$_GET['payroll']."' AND employeeid = '".$id."' AND TAGS = '".$hol_type_tags."'";
				$result_hol_late = DB_query($sql_hol_late, $db);
				$reg_hol_late = DB_fetch_array($result_hol_late);
				$actual_late1 += $reg_hol_late['ACTUAL_LATE'];
				$array[] = "'".$reg_hol_tags['holidayshortdesc']."'";
				}		
				
				$actual_late_res = join(",", $array);
				DB_data_seek($result_total_late, 0);
				$sql_total_late = "SELECT 
						SUM(LATE + BREAKLATE) as total_late
						FROM 
						prltimeentry
						WHERE employeeid = '" . $id ."' AND payrollid = '".$_GET['payroll']."' AND TAGS not in ($actual_late_res)";
				
				$result_total_late = DB_query($sql_total_late, $db);			
				$myrow_total_late = DB_fetch_array($result_total_late);
				
				$total_tardiness = $myrow_total_late['total_late'];

					$total_late2 = $actual_late1 + $total_tardiness;
					
						/* $hours = intval($total_tardiness/60);
						$minutes = $total_tardiness - ($hours * 60); */
						$late_in_decimal = ($total_late2 / 60);
						
						$late_rounded = round($late_in_decimal, 2);
				
				DB_data_seek($result_late_amt, 0);
					$sql_late_amt = 'SELECT 
							hourlyrate
							FROM 
							prlemployeemaster
							WHERE employeeid = "' . $id .'"';
					$result_late_amt = DB_query($sql_late_amt, $db);			
					$myrow_late_amt = DB_fetch_array($result_late_amt);				
					$hourly_rate = $myrow_late_amt['hourlyrate'];
					
					$per_minute = $hourly_rate / 60;
					$late_no_decimal = $total_late2 * $per_minute;
					$late = number_format($late_no_decimal,2);
					
					//echo $hours . ' Hour(s)' . ' and ' . $minutes . ' Minute(s)';
					/* if($total_tardiness >= 60){
					$late = $hours . ' Hour(s)' . '<br>' . $minutes . ' Minute(s)';
					}else if($total_tardiness == 0){
						$late = 'No late';
					}else {
						$late = $minutes . ' Minute(s)';
					} */

				// for total late end
				
				// for HOLIDAYS
				
				   $sql_hol = "SELECT a.holidaydesc, a.holidayrate,
										b.TIMEIN, b.TIMEOUT
								FROM prlholidaytable  as a,
								prltimeentry as b
								WHERE a.holidaydate	 = b.DAY
								AND b.employeeid = '".$id."' AND b.payrollid = '".$_GET['payroll']."'";
					$result_hol = mysql_query($sql_hol) or die ("Error in query: $sql_hol " . mysql_error());
					while($row_hol = mysql_fetch_array($result_hol)){
					$hol_desc = $row_hol['holidaydesc'];
					$hol_rate = $row_hol['holidayrate'];
					$hol_timein = $row_hol['TIMEIN'];
					$hol_timeout = $row_hol['TIMEOUT'];
					
					
					if(($row_hol['TIMEIN'] || $row_hol['TIMEOUT']) == NULL){
						$HOL_amount = 0;
					}
					elseif(($row_hol['TIMEIN'] && $row_hol['TIMEOUT']) !== NULL){
					$to_timeout = strtotime($hol_timeout);
					$from_timeout = strtotime($hol_timein);
					
					$HOL_time = round(abs($from_timeout - $to_timeout) / 60,2);
					$HOL_time_minus_break = $HOL_time - 60;
					$hours = intval($HOL_time_minus_break/60);
					$minutes = $HOL_time_minus_break - ($hours * 60);
						$HOL_amount2 = $hours * $hourly_rate;
						$HOL_amount = $HOL_amount2 * $hol_rate;
					}
					$hol += $HOL_amount;
				   } 
				// for holidays end
				
				// for undertime start
				DB_data_seek($result_total_undertime, 0);
					$sql_total_undertime = 'SELECT 
							SUM(UNDERTIME) as total_undertime
							FROM 
							prltimeentry
							WHERE employeeid = "' . $id .'" AND payrollid = "'.$_GET['payroll'].'" AND day BETWEEN "' . $startdate . '" AND "' . $enddate . '"';
					$result_total_undertime = DB_query($sql_total_undertime, $db);			
					$myrow_total_undertime = DB_fetch_array($result_total_undertime);
					
					$total_undertime = $myrow_total_undertime['total_undertime'];	
						$undertime_in_decimal = ($total_undertime / 60);
						$undertime_rounded = round($undertime_in_decimal, 2);
						
						$undertime_mins = ($total_undertime % 60);
						$ecola_permin = 0.03 * $undertime_mins;
						
						$str = $undertime_rounded;
						$res = explode(".", $str);
						
						$num_of_hour = $res[0];
						$ecola_per_hour = 1.87 * $num_of_hour;
						$ecola_deduct_ut = $ecola_per_hour + $ecola_permin;
						
				DB_data_seek($result_undertime_amt, 0);
					$sql_undertime_amt = 'SELECT 
							hourlyrate
							FROM 
							prlemployeemaster
							WHERE employeeid = "' . $id .'"';
					$result_undertime_amt = DB_query($sql_undertime_amt, $db);			
					$myrow_undertime_amt = DB_fetch_array($result_undertime_amt);				
					$hourly_rate = $myrow_undertime_amt['hourlyrate'];
					
					$undertime_no_decimal = ($undertime_rounded * $hourly_rate);
					
					$undertime = round($undertime_no_decimal, 2);


				// for undertime end
				
				//absent start
				// for total hours based on time schedule
				DB_data_seek($result_tot_hrs, 0);
				$sql_tot_hrs = 'SELECT TimeIn,TimeOut FROM  prlemployeemaster WHERE employeeid = "' . $id .'"';
				$result_tot_hrs = DB_query($sql_tot_hrs, $db);
				$number_tot_hrs = DB_fetch_array($result_tot_hrs);
				$timein_sched = $number_tot_hrs['TimeIn'];
				$timeout_sched = $number_tot_hrs['TimeOut'];
				
					$timein = $timein_sched;
					$timeout = $timeout_sched;
					$to_time = strtotime($timeout);
					$from_time = strtotime($timein);
					$total_hours = round(abs($to_time - $from_time) / 60,2). " minute(s)";	
					$total_hours= ($total_hours - 60);
					
					$hours = intval($total_hours/60);
					$minutes = $total_hours - ($hours * 60);
					
					$total_time_sched = $hours . '.' . $minutes;
					//echo $total_time_sched;
				
				// for number of absent
				DB_data_seek($result_absent, 0);
				$sql_absent = 'SELECT COUNT(TAGS) AS no_tags FROM  prltimeentry WHERE employeeid = "' . $id .'" AND payrollid = "'.$_GET['payroll'].'" AND TIMEIN  = "" AND TIMEOUT  = "" AND day BETWEEN "' . $startdate . '" AND "' . $enddate . '" AND TAGS = "absent"';
				$result_absent = DB_query($sql_absent, $db);
				$number_absent = DB_fetch_array($result_absent);
				$absent_total_num = $number_absent['no_tags']; 
				
				$absent_deduct = $total_time_sched * $absent_total_num;
				// for number of absent end
				//absent end
				
				// for getting the hourly rate to compute absent amount
				DB_data_seek($result_hourly_amt, 0);
				$sql_hourly_amt = 'SELECT hourlyrate FROM prlemployeemaster WHERE employeeid = "' . $id .'"';
				$result_hourly_amt = DB_query($sql_hourly_amt, $db);
				$number_hourly_amt = DB_fetch_array($result_hourly_amt);
				$hourly_amt = $number_hourly_amt['hourlyrate']; 
				
				$absent1 = $hourly_amt * $absent_deduct;
				$absent = number_format($absent1,2);
				// for number of absent end
				
				// for the Ecola
				/* DB_data_seek($result_Ecola, 0);
				$sql_Ecola = 'SELECT SUM(othincamount) AS Ecola FROM  prlothincfile WHERE employeeid = "'. $id .'" AND othdate BETWEEN "'.$row['startdate'].'" AND "'.$row['enddate'].'" AND othincid = "1"';
				$result_Ecola = DB_query($sql_Ecola, $db);
				$number_Ecola = DB_fetch_array($result_Ecola);
				if($undertime > 0){
					$Ecola = 15 / $undertime_rounded * $no_days;
					
				}else{
					$Ecola = 15 * $no_days;
				} *///$Ecola = $row['Ecola'];
				/* $sql_hol_type = "SELECT holidayshortdesc from prlholidaytable";
				$result_hol_type = mysql_query($sql_hol_type, $db);
				
				while($reg__hol_type = mysql_fetch_array($result_hol_type)){
				$hol_type_tags = $reg__hol_type['holidayshortdesc'];
				
				$sql_ecola = "SELECT * from prltimeentry WHERE payrollid = '".$_GET['payroll']."' AND employeeid = '".$id."' AND TAGS = '".$hol_type_tags."'";
				$result_ecola = mysql_query($sql_ecola, $db);
				
				$reg_days1 = mysql_num_rows($result_ecola);
				$reg_days2 += $reg_days1;
				$no_days2 = $no_days + $reg_days2;
				$Ecola = 15 * $no_days2;
				
				} */
				
				$sql_ecola = "SELECT * from prltimeentry WHERE payrollid = '".$_GET['payroll']."' AND employeeid = '".$id."' AND TAGS = 'Absent'";
				$result_ecola = mysql_query($sql_ecola, $db);
				$reg_days1 = mysql_num_rows($result_ecola);
				$reg_days2 = 13 - $reg_days1;
				$Ecola1 = 15 * $reg_days2;
				$Ecola = $Ecola1 - $ecola_deduct_ut;
				//$Ecola = 
				
				// for the Ecola end
				
				// for the number of special holiday
				DB_data_seek($result_SH, 0);
				$sql_SH = 'SELECT SUM(othincamount) AS no_SH FROM  prlothincfile WHERE employeeid = "'. $id .'" AND othdate BETWEEN "'.$row['startdate'].'" AND "'.$row['enddate'].'" AND othincid = "2"';
				$result_SH = DB_query($sql_SH, $db);
				$number_SH = DB_fetch_array($result_SH);
				$SpecialHoliday = $number_SH['no_SH'];
				//$SpecialHoliday = $row['SH'];
				// for the number of special holiday end
				
				// for the number of legal holiday
				DB_data_seek($result_LH, 0);
				$sql_LH = 'SELECT SUM(othincamount) AS no_LH FROM  prlothincfile WHERE employeeid = "'. $id .'" AND othdate BETWEEN "'.$row['startdate'].'" AND "'.$row['enddate'].'" AND othincid = "3"';
				$result_LH = DB_query($sql_LH, $db);
				$number_LH = DB_fetch_array($result_LH);
				$LegalHoliday = $number_LH['no_LH'];
				//$LegalHoliday = $row['LH'];
				// for the number of legal holiday end
				
				//$Salary_Loan = $row['Salary_Loan'];
				//$Cal_Loan = $row['Cal_Loan'];
				//$Pagibig_Loan = $row['Pagibig_Loan'];
				//$PagibigC_Loan = $row['PagibigC_Loan'];
				
			/* 	 if($isdaily ==4){
					$bp1 = $bp * $no_days;
				}else{
					$bp1 = $bp;
				} */
				
				$bp1 = $bp;
				
				$sql_sss_div = 'SELECT payperiodid FROM prlemployeemaster WHERE employeeid = "' . $id .'"';
				$result_sss_div = DB_query($sql_sss_div, $db);			
				$myrow_sss_div = DB_fetch_array($result_sss_div);
				$sss_div1 = $myrow_sss_div['payperiodid'];
							
							if($sss_div1 == 4){
								$divided = 4;
							}elseif($sss_div1 == 2){
								$divided = 1;
							}elseif($sss_div1 == 1){
								$divided = 2;
							}
				$bp_divided =  $bp1 * $divided;
				//sss
				if($dSSS== 0){
				DB_data_seek($result_sss, 0);
					$sql_sss = 'SELECT 
							employeess
							FROM 
							prlsstable
							WHERE "' . $bp_divided . '" BETWEEN rangefrom AND rangeto';
					$result_sss = DB_query($sql_sss, $db);			
					$myrow_sss = DB_fetch_array($result_sss);
						if($myrow_sss['employeess'] == NULL){
							$sss = "0.00";
						}else{
							$sss_tot = $myrow_sss['employeess'] / $divided;
							$sss = $sss_tot;
						}
					}else{
					$sss = "0.00";}
				//sss end
				
				//$hdmf = $row['hdmf'];
				
				//pagibig
				if($dHDMF == 0){
				DB_data_seek($result_hdmf, 0);
					$sql_hdmf = 'SELECT 
							employeeshare
							FROM 
							prlhdmftable
							WHERE "' . $bp_divided . '" BETWEEN rangefrom AND rangeto';
					$result_hdmf = DB_query($sql_hdmf, $db);			
					$myrow_hdmf = DB_fetch_array($result_hdmf);
					$hdmf_tot = $myrow_hdmf['employeeshare'] / $divided;
					$hdmf = $hdmf_tot;
					}else { $hdmf = "0.00"; }
				//pagibig end
				
				//philhealth
				if($dPH == 0){
				DB_data_seek($result_ph, 0);
					$sql_ph = 'SELECT 
							employeeph
							FROM 
							prlphilhealth
							WHERE "' . $bp_divided . '" BETWEEN rangefrom AND rangeto';
					$result_ph = DB_query($sql_ph, $db);			
					$myrow_ph = DB_fetch_array($result_ph);
					$philhealth_tot = $myrow_ph['employeeph'] / $divided;
					$philhealth = $philhealth_tot;
					}else{
					$philhealth = "0.00";}
				//philhealth end
				//$philhealth = $row['philhealth'];
				
				//tax
				/* if($netpay <= 0){
					echo 'Negative';
				}else{
					echo 'Positive or greater than 0';
				} */
				
				if($dTax== 0){
				DB_data_seek($result_tax, 0);
					$sql_tax = 'SELECT 
							total
							FROM 
							prltaxtablerate
							WHERE "' . $bp_divided . '" BETWEEN rangefrom AND rangeto';
					$result_tax = DB_query($sql_tax, $db);			
					$myrow_tax = DB_fetch_array($result_tax);
					$tax_tot = $myrow_tax['total'] / $divided;
					$tax = $tax_tot;
					}else{
					$tax = "0.00";}
				//tax-end
				//+ $SpecialHoliday + $LegalHoliday 
				$gp = $bp1 - $late - $absent1 - $undertime;
 				
				DB_data_seek($result_tot, 0);
									$sql_othinc_tot = "SELECT SUM(othincamount) as tot_othinc
													FROM prlothincfile
													WHERE othdate BETWEEN '".$startdate."' AND '".$enddate."' AND employeeid = '".$id."'";
									$result_othinc_tot = DB_query($sql_othinc_tot, $db);	
									$myrow_othinc_tot = DB_fetch_array($result_othinc_tot);
									$oth_inc = $myrow_othinc_tot['tot_othinc'];
				/* echo '<br>bas_'.$bp1.'<br>'.'late_'.$late.'<br>'.'absent_'.$absent.'<br>'.'UT_'.$undertime.'<br>';
				echo 'OTHInc_'.$oth_inc.'<br>'.'hol_'.$hol.'<br>'.'OT_'.$ot.'<br>'; */
				
				
				$gp2 = $ot + $Ecola + $hol + $oth_inc;
				//echo 'GP1_'.$gp.'<br>'.'GP2_'.$gp_2.'<br>';
				$grosspay = $gp + $gp2;
				//echo 'GP_'.$grosspay.'<br>'.'ecola_'.$Ecola;
				$n = $grosspay - $sss - $hdmf - $philhealth - $tax;
				$n1 = $n;
				
					echo '<div class="jpayslip"><table width="80%" border="1" class="psliptable">
						  <tbody>
						  <tr>
							<td class="pstableheader" height="33" scope="col" colspan="4"><div align="center"><strong>PCC-MLA</strong></div></td>
						  </tr>
						  <tr>
							<td class="pstableheader" height="30" colspan="4"><div align="center"><strong>PAYSLIP</strong></div></td>
						  </tr>
						  <tr>
							<td class="pstableheader" width="30%"><strong>Name:</strong></td>
							<td colspan="3">';?>  <?php echo $name;?>
					<?php echo '</td></tr>
						   <tr>
							<td class="pstableheader" ><strong>For Period covered:</strong></td>
							<td colspan="3">';?><?php echo $perioddate;?>
					<?php echo '</td></tr>
						  <tr> <td class="pstableheader" >Dept.:</td>
							<td>'?><?php echo $dept;?>
					<?php echo '</td>
							<td class="pstableheader"  width="30%">Pay Date:</td>
							<td width="19%">'?><?php echo date('M. d, Y');?>
					<?php echo '</td></tr>
							<tr>
							<td class="pstableheader" ><strong>BASIC PAY:</strong></td>
							<td>';?>
							<?php if($isdaily ==4){?>
							<?php echo '('.$no_days.' days) <br>'.number_format($bp1,2)?>
							<?php }else{?>
							<?php echo number_format($bp1,2);?>
							<?php }?>
					<?php echo '</td> <td class="pstableheader" colspan="2" align="center"><div align="center"><strong>DEDUCTIONS</strong></div></td> </tr>
						  <tr>'; 
						  ?>
							<td class="pstableheader">ECOLA:</td>
							<td><?php echo number_format($Ecola,2);?>
					<?php echo '</td>';?>
							<td class="pstableheader">TAX:</td>
							<td>
					<?php echo number_format($tax,2).'</td></tr>
							<tr>
							<td class="pstableheader" >LATE:</td>
							<td>';?><?php echo number_format($late,2);?>
							<td class="pstableheader">VALE</td>
							<td><?php echo '0.00';?>
					<?php echo '</td></tr> 
						  <tr>
							<td class="pstableheader" >ABSENT</td>
							<td>';?><?php echo $absent;?>
							<td class="pstableheader"  width="30%">SSS PREM.   P:</td>
							<td width="19%"><?php echo number_format($sss,2);?>
					<?php echo '</td></tr>
							<tr>
							<td class="pstableheader" >UNDERTIME:</td>
							<td>';?><?php echo number_format($undertime,2);?>
					<td class="pstableheader">PAG-IBIG PREM.:</td>
							<td><?php echo number_format($hdmf,2);?>					
					<?php echo '</td></tr> 					
						  <tr>
							<td class="pstableheader" colspan="2" align="center">OTHER INCOME</td>
							<td class="pstableheader" >PHILHEALTH:</td>
							<td>';?><?php echo number_format($philhealth,2);?></td></tr>							
							<?php //for other income
							DB_data_seek($result_oth, 0);
							$sql_othrincome = "SELECT ottable.othincdesc, otfile.othincamount
											FROM prlothinctable as ottable, prlothincfile as otfile
											WHERE ottable.othincid = otfile.othincid AND otfile.othdate BETWEEN '".$startdate."' AND '".$enddate."' AND otfile.employeeid = '".$id."'
											";
							$result_oth = DB_query($sql_othrincome, $db);			
							while($myrow_oth = DB_fetch_array($result_oth)){
							?><tr>
								<td class="pstableheader"><?php echo $myrow_oth['othincdesc']; ?></td>
								<td><?php echo number_format($myrow_oth['othincamount'],2); ?></td>								
									<?php
									DB_data_seek($result_tot, 0);
									$sql_othinc_tot = "SELECT SUM(othincamount) as tot_othinc
													FROM prlothincfile
													WHERE othdate BETWEEN '".$startdate."' AND '".$enddate."' AND employeeid = '".$id."'";
									$result_othinc_tot = DB_query($sql_othinc_tot, $db);	
									$myrow_othinc_tot = DB_fetch_array($result_othinc_tot);
									$oth_inc = $myrow_othinc_tot['tot_othinc'];
									?>
								</tr>
							<?php } ?>	
							<tr>
								<td class="pstableheader" colspan='2' align='center'>OVERTIME</td>
							</tr>
							<?php // for overtime
							DB_data_seek($result_ot1, 0);
							$sql_overtime = "SELECT a.overtimedesc, b.otamount
											FROM prlovertimetable as a, prlottrans as b
											WHERE a.overtimeid = b.overtimeid AND b.otdate BETWEEN '".$startdate."' AND '".$enddate."' AND b.employeeid = '".$id."'
											";
							$result_ot1 = DB_query($sql_overtime, $db);			
							while($myrow_ot1 = DB_fetch_array($result_ot1)){ ?>
							<tr>
								<td class="pstableheader"><?php echo $myrow_ot1['overtimedesc']; ?></td>
								<td><?php echo number_format($myrow_ot1['otamount'],2); ?></td>
							</tr>
							<?php } ?>	
							<tr>
								<td class="pstableheader" colspan='2' align='center'>HOLIDAYS</td>
							</tr>
							<?php
						   $sql_hol = "SELECT a.holidaydesc, a.holidayrate,
												b.TIMEIN, b.TIMEOUT
										FROM prlholidaytable  as a,
										prltimeentry as b
										WHERE a.holidaydate	 = b.DAY
										AND b.employeeid = '".$id."' AND b.payrollid = '".$_GET['payroll']."'";
							$result_hol = mysql_query($sql_hol) or die ("Error in query: $sql_hol " . mysql_error());
							while($row_hol = mysql_fetch_array($result_hol)){
							$hol_desc = $row_hol['holidaydesc'];
							$hol_rate = $row_hol['holidayrate'];
							$hol_timein = $row_hol['TIMEIN'];
							$hol_timeout = $row_hol['TIMEOUT'];
								
							if(($row_hol['TIMEIN'] && $row_hol['TIMEOUT']) == NULL){
								$HOL_amount = 0; 
							}
							elseif(($row_hol['TIMEIN'] && $row_hol['TIMEOUT']) !== NULL){
							$to_timeout = strtotime($hol_timeout);
							$from_timeout = strtotime($hol_timein);
							
							$HOL_time = round(abs($from_timeout - $to_timeout) / 60,2);
							$HOL_time_minus_break = $HOL_time - 60;
							$hours = intval($HOL_time_minus_break/60);
							$minutes = $HOL_time_minus_break - ($hours * 60);
								//$hours = $hours * 2;
								$HOL_amount2 = $hours * $hourly_rate;
								$HOL_amount = $HOL_amount2 * $hol_rate;
								
							
								echo "<tr>
										<td class='pstableheader'>".$hol_desc."</td>
										<td>". number_format($HOL_amount,2) ."</td>
									  </tr>"; 
								 
							}
							} ?>
							
							
							
					<?php 
							echo '</td>';
						/* $sqlLoan = "SELECT loantableid, loantabledesc
							FROM prlloantable 
							ORDER BY loantabledesc ASC
							";
							$result_Loan = DB_query($sqlLoan,$db);
							$loanAmt = 0;
						while($row_Loan = DB_fetch_array($result_Loan)){
							$loantableid =  $row_Loan['loantableid'] ; */
							
							$sqlLoan2 = "SELECT a.amortization, b.loantabledesc
							FROM prlloanfile a,
							prlloantable b,
							prlloandeduct c
							WHERE a.loantableid = b.loantableid AND a.employeeid = '".$id."'
							AND c.date BETWEEN '".$startdate."' AND '".$enddate."'
							AND c.skip_deduct = '0'
							AND c.balance >= '0'
							AND c.loan_file_id = a.loanfileid
							";
							$result_Loan2 = DB_query($sqlLoan2,$db);
						while($row_Loan2 = DB_fetch_array($result_Loan2)){
							echo '<td colspan="2"><td class="pstableheader">'.$row_Loan2['loantabledesc'].'</td>';
							echo  '<td>'.number_format($row_Loan2['amortization'],2).'</td>';
							$loanAmt += $row_Loan2['amortization'];
							//echo number_format($loanAmt,2).'<br>';
							echo  '</tr>';
						}//end while
							echo '</td></tr>
							<tr>
							<td class="pstableheader"><strong>GROSS PAY:</strong></td>
							<td>'?><?php 
							echo '<strong>P </strong>'.number_format($grosspay,2);
							
							echo '</td>
							<td class="pstableheader"><strong>NET PAY:</strong></td>
							<td>'?><?php $netpay = $n1 - $loanAmt;
							echo '<strong>P </strong>'.number_format($netpay,2);
					?>						

					<?php echo '</td></tr> 
							<tr>
							<td class="pstableheader" height="46"><strong>Received by:</strong></td>
							<td colspan="3">';?><?php echo $name;?>
					<?php echo '</td>
						  </tr>
						</tbody>
					 </table></div>';
				
				 
			$x++;
		}
	}
}else{
	echo "<font color='red'><b>NO RECORDS FOUND!</b></font>";
}	
?>	
  
  </table>
  </div>
</div>          
</div>
	  
</body>
</html>