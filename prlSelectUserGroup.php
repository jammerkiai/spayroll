<?php
/*Modified By: ME*/
$PageSecurity = 15;

include('includes/session.inc');

$title = _('User Group Maintenance');

include('includes/header.inc');

if (isset($_GET['secroleid'])){
	$secroleid = $_GET['secroleid'];
} elseif (isset($_POST['secroleid'])){
	
	$secroleid = $_POST['secroleid'];
} else {
	unset($secroleid);
}


if (isset($_POST['submit'])) {

	$InputError = 0;

	if (strpos($_POST['secrolename'],'&')>0 OR strpos($_POST['secrolename'],"'")>0) {
		$InputError = 1;
		prnMsg( _('The User Group cannot contain the character') . " '&' " . _('or the character') ." '",'error');
	}
	if (trim($_POST['secrolename']) == '') {
		$InputError = 1;
		prnMsg( _('The User Group may not be empty'), 'error');
	}
	
	
	if ($InputError !=1) {
	
			if (!isset($_POST['New'])) {

			$sql = "UPDATE securityroles SET secrolename='" . DB_escape_string($_POST['secrolename']) . "' 
							 
						WHERE secroleid = '$secroleid'";

			$ErrMsg = _('The user group could not be updated because');
			$DbgMsg = _('The SQL that was used to update the user group but failed was');
			$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);
			prnMsg(_('The user group master record for') . ' ' . $secroleid . ' ' . _('has been updated'),'success');

		} else { 

			$sql = "INSERT INTO securityroles ( 
							secrolename 
							)
					 VALUES ( 
					 	'" .DB_escape_string($_POST['secrolename']) . "')";

			$ErrMsg = _('The user group') . ' ' . $_POST['secrolename'] . ' ' . _('could not be added because');
			$DbgMsg = _('The SQL that was used to insert the user group but failed was');
			$result = DB_query($sql, $db, $ErrMsg, $DbgMsg);

			prnMsg(_('A new user group for') . ' ' . $_POST['secrolename'] . ' ' . _('has been added to the database'),'success');

			unset ($secroleid);
			unset($_POST['secrolename']);
			

		}
		
	} else {

		prnMsg(_('Validation failed') . _('no updates or deletes took place'),'warn');

	}

} elseif (isset($_POST['delete']) AND $_POST['delete'] != '') {



	$CancelDelete = 0;


	if ($CancelDelete == 0) {
		$sql="DELETE FROM securityroles WHERE secroleid='$secroleid'";
		$result = DB_query($sql, $db);
		prnMsg(_('User Group record for') . ' ' . $secroleid . ' ' . _('has been deleted'),'success');
		unset($secroleid);
		unset($_SESSION['secroleid']);
	} 
}


if (!isset($secroleid)) {


	echo "<FORM METHOD='post' ACTION='" . $_SERVER['PHP_SELF'] . "?" . SID . "'>";

	echo "<INPUT TYPE='hidden' NAME='New' VALUE='Yes'>";

	echo '<CENTER><TABLE class="jinnertable">';
	echo '<TR><TD class="tableheader">' . _('User Group') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='secrolename' SIZE=41 MAXLENGTH=40></TD></TR>";
	


	echo "</TABLE><p><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Insert New User Group') . "'><br />";
	echo '</FORM>';
	
		$sql = "SELECT secroleid,
			secrolename			
			FROM securityroles
			ORDER BY secroleid";

	$ErrMsg = _('Could not get user group because');
	$result = DB_query($sql,$db,$ErrMsg);
	
	echo '<CENTER><table border=0 width="70%" class="jinnertable">';
	echo "<tr>
		<td class='tableheader'>" . _('ID') . "</td>
		<td class='tableheader'>" . _('User Group') . "</td>
	
		<td class='tableheader' colspan='2'>Action</td>
	</tr>";

		
	$k=0; 
	while ($myrow = DB_fetch_row($result)) {

		if ($k==1){
			echo "<TR>";
			$k=0;
		} else {
			echo "<TR>";
			$k++;
		}
		echo '<TD>' . $myrow[0] . '</TD>';
		echo '<TD>' . $myrow[1] . '</TD>';
		
		echo '<TD><A HREF="' . $_SERVER['PHP_SELF'] . '?' . SID . '&secroleid=' . $myrow[0] . '">' . _('Edit') . '</A></TD>';
		echo '<TD><A HREF="' . $_SERVER['PHP_SELF'] . '?' . SID . '&secroleid=' . $myrow[0] . '&delete=1">' . _('Delete') .'</A></TD>';
		echo '</TR>';

	} 
	echo '</table></CENTER><p>';


} else {

	echo "<FORM METHOD='post' ACTION='" . $_SERVER['PHP_SELF'] . "?" . SID . "'>";
	echo '<CENTER><br /><TABLE class="jinnertable">';

	
	if (!isset($_POST['New'])) {
		$sql = "SELECT secroleid, 
				secrolename
			FROM securityroles 
			WHERE secroleid = '$secroleid'";
				  
		$result = DB_query($sql, $db);
		$myrow = DB_fetch_array($result);
		
		$_POST['secrolename']  = $myrow['secrolename'];
	
		echo "<INPUT TYPE=HIDDEN NAME='secroleid' VALUE='$secroleid'>";

	} else {
	
		echo "<INPUT TYPE=HIDDEN NAME='New' VALUE='Yes'>";
		echo '<TR><TD class="tableheader">' . _('ID') . ":</TD><TD><INPUT TYPE='text' class='intext' NAME='secroleid' VALUE='$secroleid' SIZE=5 MAXLENGTH=4></TD></TR>";
	}
	echo "<TR><TD class='tableheader'>" . _('User Group') . ':' . "</TD><TD><input type='Text' class='intext' name='secrolename' SIZE=41 MAXLENGTH=40 value='" . $_POST['secrolename'] . "'></TD></TR>";
	
	if (isset($_POST['New'])) {
		echo "</TABLE><P><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Add These New User Group Record') . "'></FORM>";
	} else {
		echo "</TABLE><P><CENTER><INPUT TYPE='Submit' class='jinnerbot' NAME='submit' VALUE='" . _('Update User Group Record') . "'>";
		echo '<P><FONT COLOR=red><B>' . _('WARNING') . ': ' . _('There is no second warning if you hit the delete button below') . '. ' . _('However checks will be made to ensure before the deletion is processed') . '<BR></FONT></B>';
		echo "<INPUT TYPE='Submit' class='jinnerbot' NAME='delete' VALUE='" . _('Delete this record') . "' onclick=\"return confirm('" . _('Are you sure you wish to delete this user group record?') . "');\"></FORM>";
	}

} 

include('includes/footer.inc');
?>