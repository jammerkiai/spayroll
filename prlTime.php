
<?php
include('includes/session.inc');

$title = _('Time Entry');

include('includes/header.inc');
include('includes/footer.inc');
?>

<div id="content">
<br/>
<div align="left" class="subheader">
	<a href="<?php echo $rootpath;?>/index.php?">
		<img src="images/back.png" width="30" height="30" />
	</a>&nbsp;&nbsp;Time Entry
</div>
<center> <br /> </center>
				<form name="x" action="" method="GET">
					<b>Select Payroll:</b>
					<select name="payroll" class="intext">
					<?php
									DB_data_seek($result_payroll, 0);
									$sql_payroll = 'SELECT payrollid,payrolldesc FROM  prlpayrollperiod
									WHERE payrollid = "' . $_GET['payroll'] .'"';
									$result_payroll = DB_query($sql_payroll, $db);
									$myrow_payroll = DB_fetch_array($result_payroll);
								?>								
								<option selected value=""><?php echo $myrow_payroll['payrollid'] . ' ' . $myrow_payroll['payrolldesc']; ?></option>
					<?php
						DB_data_seek($result6, 0);
						$sql6 = 'SELECT payrollid,payrolldesc FROM  prlpayrollperiod ORDER BY payrollid ASC';
						$result6 = DB_query($sql6, $db);
						
						while ($myrow6 = DB_fetch_array($result6)) 
						{
							?>  	  
							<option value="<?php echo $myrow6['payrollid']; ?>"><?php echo $myrow6['payrollid'] . ' - ' . $myrow6['payrolldesc']; ?></option>
						<?php  }

					?>
					</select>
					
					<b>Select Employee:</b>
					<select name="emp_id" class="intext" onchange="this.form.submit();">
					<?php
									DB_data_seek($result_empName, 0);
									$sql_empName = 'SELECT CONCAT(lastname, ", ", firstname) as name FROM  prlemployeemaster
									WHERE employeeid = "' . $_GET['emp_id'] .'"';
									$result_empName = DB_query($sql_empName, $db);
									$myrow_empName = DB_fetch_array($result_empName);
								?>
								<option selected value=""><?php echo $myrow_empName['name'];  ?></option>
					<?php
						DB_data_seek($result, 0);
						$sql = 'SELECT employeeid,lastname,firstname FROM  prlemployeemaster ORDER BY lastname ASC';
						$result = DB_query($sql, $db);
						
						while ($myrow = DB_fetch_array($result)) 
						{
							?>  	  
							<option value="<?php echo $myrow['employeeid']; ?>"><?php echo $myrow['lastname'] . ', ' . $myrow['firstname']; ?></option>
						<?php  }

					?>
					</select>
				</form>
				
				<a href='prlImportCsvSched.php' class='jinnerbot2'>Import CSV File</a>
				<a href='prlTimeViewSheet.php' class='jinnerbot2'>View Time Sheet</a>
				
				
						<?php		
		DB_data_seek($result7, 0);
		$sql7 = 'SELECT CONCAT(startdate, " to ",enddate) as period,
							startdate,
							enddate
				FROM 
				prlpayrollperiod 
                WHERE payrollid = "' . $_GET['payroll'] .'"';
		$result7= DB_query($sql7, $db);
		$myrow7 = DB_fetch_array($result7);

		$startdate = $myrow7['startdate'];
		$enddate = $myrow7['enddate'];
		
		//echo 'startdate'. $startdate .'<br>';
		//echo 'enddate'. $enddate .'<br>';
		
		
		$date1 = new DateTime($startdate);
		$date2 = new DateTime($enddate);
		$interval = $date1->diff($date2);
		// echo $interval->d." days ";
		$day_range = $interval->d;
		
		
		//for displaying date loop
		$start2 = $startdate; //start date
		$end2 = $enddate; //end date

		$dates = array();
		$start2 = $current = strtotime($start2);
		$end2 = strtotime($end2);

		while ($current <= $end2) {
			$dates[] = date('Y-m-d', $current);
			$current = strtotime('+1 days', $current);

		}
	?>
	
	
	<?php
	DB_data_seek($result5, 0);
	$sql5 = "SELECT DAY FROM prltimeentry WHERE employeeid = '". $_GET['emp_id'] ."' AND DAY BETWEEN '" . $startdate . "' AND '" . $enddate . "'
				AND payrollid = '".$_GET['payroll']."'";
	$result5 = DB_query($sql5, $db);
	
		$val = mysql_num_rows($result5);
		if($val >= 1){
			//echo "theres a record found";
			include('prlTimeUpdateForm.php');
		}
		else{
			//echo "no record found";
			include('prlTimeEntryForm.php');
		}
	
	?>
				
				
</div>	
		
</body>
</html>